## Mazkara API

The Mazkara api application is based off the Laravel 5 framework. While it was intended to be based off lumen - migrating from laravel4.2 to lumen was proving to be too much of a pain. Hence to quicken things up, the entire api was swiftly moved to Laravel 5.

This version of the api has a reduced overhead than the original as a number of assets that were html based and crucial to the website have been removed.

Maybe soon we'll move to Lumen but for now the performance boost from this upgrade is well worth it.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
