<?php

// Composer: "fzaninotto/faker": "v1.3.0"

class AssetsFixerTableSeeder extends Seeder {

	public function run()
	{

    $cities = Zone::cities()->lists('id', 'id');
    $categories = Category::all();

    foreach($categories as $category){
      foreach($cities as $i=>$v){
        $cities[$i] = [ 
                        'state'=>$category->state, 
                        'business_count'=>$category->business_count
                      ];
      }
      $category->zones()->sync($cities);
    }

    $services = Service::all();

    foreach($services as $service){
      foreach($cities as $i=>$v){
        $cities[$i] = [
                        'state'=>$service->state,
                        'business_count'=>$service->business_count
                      ];
      }

      $service->zones()->sync($cities);
    }
    
    $highlights = Highlight::all();

    foreach($highlights as $h){
      foreach($cities as $i=>$v){
        $cities[$i] = [
                        'state'=>$h->state,
                        'business_count'=>$h->business_count
                        ];
      }

      $h->zones()->sync($cities);
    }/**/

    $adminUsers = User::whereHas('roles', function($q){
                    $q->where('name', 'moderator');
                  })->get();
    $cities = Zone::cities()->lists('id', 'id');

    foreach($adminUsers as $user){
      $user->zones()->sync($cities);

    }
	}

}