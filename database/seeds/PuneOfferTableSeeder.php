<?php

class PuneOfferTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('highlights')->truncate();

    $offers = Offer::where('state', '=', 'pending')->get();
    foreach($offers as $offer){
      $offer->state = 'active';
      $offer->save();

      $business = $offer->business;

      $business->updateMetaOffers();
    }


		// Uncomment the below to run the seeder
		// DB::table('highlights')->insert($highlights);
	}

}
