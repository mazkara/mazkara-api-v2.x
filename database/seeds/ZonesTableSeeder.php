<?php
use Illuminate\Database\Seeder;
use App\Models\Zone;

class ZonesTableSeeder extends Seeder {

	public function run(){
		// Uncomment the below to wipe the table clean before populating
		// DB::table('zones')->truncate();

    $sharjah_zones = ['Buhaira Corniche'=>['Al Taawun','Al Khan','Al Majaz'],
                      'Suburbs'=>['Wasit Suburb','Mughaidir Suburb','Al Riqa Suburb','Halwan Suburb'],
                      'Al Heera Suburb'=>['Sharqan','Al Qadisia','Al Rifaah','Al Mirgab'],
                      'Al Sharq'=>['Bu Tina','Al Nasseriya','Maysaloon','Al Qulayaa'],
                      'Al Qasimia'=>['Abu Shagara','Al Mahatah','Al Nud','Al Soor'],
                      'Nahda and Around'=>['Al Nahda','Industrial Area'],
                      'Rolla'=>['Al Mujarrah','Al Nabaah','Al Shuwaihean','Al Mareija','Al Ghuwair'],
                      'Outer Sharjah'=>['University City','Saif Zone','Muwailih Commercial']];

    $shj = Zone::firstOrCreate(['name'=>'Sharjah']);

    foreach($sharjah_zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent, 'parent_id'=>$shj->id]);
      $p->parent_id = $shj->id;//
      //$shj->append($p);
      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child, 'parent_id'=>$p->id]);
        $c->parent_id = $p->id;//append($c);
        $c->city_id = $shj->id;
        $c->country_code = 'AE';
        $c->save();

      }
      $p->city_id = $shj->id;
      $p->country_code = 'AE';
      $p->save();
    }
    $shj->country_code = 'AE';
    $shj->save();

    $abdh_zones = ['Abu Dhabi City'=>['Al Mushrif','Eastern Mangrove',
                                      'Al Madina Al Riyadiya','Khalifa Park Area',
                                      'Muroor/Muroor Road','Al Maqta','Khalifa City',
                                      'Masdar City','Yas Island','Al Raha','Al Shahama',
                                      'Al Dhafrah','Al Nahyan','Al Karamah','Al Wahda',
                                      'Marina Village','Al Khubeirah','Al Khalidiya',
                                      'Al Bateen','Baniyas','Al Mafraq','Around Abu Dhabi',
                                      'Tourist Club Area','Al Markaziya','Najda','Madinat Zayed',
                                      'Istiqlal','Al Mina','Al Maryah Island','Al Reem Island',
                                      'Mussafah','Saadiyat Island','Mohammed Bin Zayed City',
                                      'Al Ras Al Akhdar','Embassies District']];

    $adb = Zone::firstOrCreate(['name'=>'Abu Dhabi']);

    foreach($abdh_zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent, 'parent_id'=>$adb->id]);
      $p->parent_id = $adb->id;//
      //$adb->append($p);
      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child, 'parent_id'=>$p->id]);
        $c->parent_id = $p->id;//$p->append($c);
        $c->city_id = $adb->id;
        $c->country_code = 'AE';
        $c->save();

      }
      $p->city_id = $adb->id;
      $p->country_code = 'AE';
      $p->save();
    }
    $adb->country_code = 'AE';
    $adb->save();


    $displayable_cities = [1, 88];
    \DB::statement('UPDATE zones set state = "inactive"');

    \DB::statement('UPDATE zones set state = "active" where city_id in ('.join(',', $displayable_cities).')');
    \DB::statement('UPDATE zones set state = "active" where id in ('.join(',', $displayable_cities).')');


    return;

    $zn = Zone::firstOrCreate(['name'=>'Dubai']);

    DB::table('businesses')
            ->where('zone_id', 0)
            ->update(array('city_id' => $zn->id));

            return;
            //exit;
$dubai_zones = ['Barsha' => ['Al Quoz', 'Arabian Ranches', 'Barsha 1', 'Barsha 2', 'Barsha 3', 'Barsha South', 'Dubai Motor City', 'Mall of the Emirates (MOE)'],
'Bur Dubai' => ['Al Karama', 'Burjuman', 'Khalid Bin Waleed', 'Mankhool', 'Meena Bazaar', 'Oud Metha', 'Satwa'],
'Deira' => ['Al Garhoud', 'Al Muteena', 'Al Ras', 'Al Rigga', 'Baniyas', 'Deira City Centre (DCC)', 'Festival City', 'Naif', 'Rashidiya'],
'Downtown' => ['Business Bay', 'DIFC', 'Dubai Mall Area', 'Emaar Boulevard', 'World Trade Centre Area (WTC)'],
'Jebel Ali' => ['Dubai Investment Park (DIP)', 'Green Community Village', 'JAFZA', 'Jebel Ali Industrial Area'],
'Jumeirah' => ['Al Safa', 'Jumeirah 1', 'Jumeirah 2', 'Jumeirah 3', 'Jumeirah Beach Road', 'Madinat Jumeirah', 'Umm Suqeim'],
'New Dubai' => ['Discovery Gardens', 'Dubai Marina', 'Emirates Hills', 'Ibn Batuta', 'Jumeirah Beach Residence (JBR)', 'Jumeirah Islands', 'Jumeirah Lakes Towers (JLT)', 'Palm Jumeirah', 'The Gardens', 'The Greens'],
'Outer Dubai' => ['Al Nahda', 'Al Qusais', 'Dubai Academic CIty', 'Dubai Land', 'Dubai Silicon Oasis', 'International City', 'Meydan', 'Mirdiff', 'Mirdiff City Centre', 'Nad Al Sheba', 'Ras Al Khor'],
'TECOM' => ['Dubai Internet City', 'Dubai Media City', 'Knowledge Village']];

    $dxb = Zone::firstOrCreate(['name'=>'Dubai']);

    foreach($dubai_zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent, 'parent_id'=>$dxb->id]);
      $dxb->append($p);
      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child, 'parent_id'=>$p->id]);
        $p->append($c);
        $c->country_code = 'AE';
        $c->save();

      }
      $p->country_code = 'AE';
      $p->save();
    }
    $dxb->country_code = 'AE';
    $dxb->save();

//    $in_zones = ['Pune',
//                 'Mumbai', 
//                 'Delhi'];
//
//    foreach($in_zones as $name){
//      $ind = Zone::firstOrCreate(['name'=>$name]);
//      $ind->state = 'inactive';
//      $ind->country_code = 'IN';
//      $ind->save();
//    }

    $zones = Zone::all();
    foreach($zones as $zone){
      if(!$zone->isCity()){
        $zone->city_id = $zone->ancestors()->first()->id;
        $zone->save();
      }
    }

		// Uncomment the below to run the seeder
		// DB::table('zones')->insert($zones);
	}

}
