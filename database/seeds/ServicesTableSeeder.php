<?php

class ServicesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('services')->truncate();

    $services = [ 'Body' => ['Acupuncture','Body Exfoliation Treatments','Body Wrap','Cellulite Treatments','Laser Lipo','Liposuction','Moroccan Bath','Spot Reduction Therapy','Spray Tanning & Sunless Tanning','Steam & Sauna Therapy','Weight Loss Treatments'],
                    'Face' => ['Acne Treatment','Eyebrows & Eyelash Treatments','Eyelash Extensions','Facials','Make Up','Mens’ Facials','Microdermabrasion'],
                    'Hair' => ['Blowdry','Hair Colouring & Highlighting','Hair Consulting','Hair Cuts and Hair Dressings','Hair Loss Treatments','Hair Straightening','Hair Transplant Treatments','Keratin Treatments','Wedding Hair'],
                    'Hair Removal' => ['Brazilian Wax','Male Waxing','Threading','Waxing'],
                    'Massage' => ['Aromatherapy Massage', 'Deep Tissue Massage', 'Foot Massage', 'Head & Shoulder Massage', 'Stone Massage Therapy', 'Swedish Massage', 'Thai Massage', 'Therapeutic Massage'],
                    'Nails' => ['Gel Nails','Manicure','Nail Extensions','Pedicure']];
    foreach($services as $parent=>$children){
      $p = Service::firstOrCreate(['name'=>$parent]);
      foreach($children as $child){
        $c = Service::firstOrCreate(['name'=>$child]);
        $p->append($c);
        $c->save();
      }
      $p->save();
    }

		// Uncomment the below to run the seeder
		// DB::table('services')->insert($services);
	}

}
