<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('businesses', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->string('phone');
			$table->string('email');
			$table->string('website');
			$table->integer('geolocated');
			$table->integer('zone_id');
			$table->string('geolocation_city');
			$table->string('geolocation_state');
			$table->string('geolocation_country');
			$table->string('geolocation_address');
			$table->float('geolocation_longitude');
			$table->float('geolocation_latitude');
			$table->integer('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('businesses');
	}

}
