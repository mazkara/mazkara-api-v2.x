<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotReviewServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('review_service', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('review_id')->unsigned()->index();
			$table->integer('service_id')->unsigned()->index();
			$table->foreign('review_id')->references('id')->on('reviews')->onDelete('cascade');
			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('review_service');
	}

}
