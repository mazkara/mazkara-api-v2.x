<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotHighlightZoneTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('highlight_zone', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('highlight_id')->unsigned()->index();
			$table->integer('zone_id')->unsigned()->index();
			$table->string('state');
			$table->foreign('highlight_id')->references('id')->on('highlights')->onDelete('cascade');
			$table->foreign('zone_id')->references('id')->on('zones')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('highlight_zone');
	}

}
