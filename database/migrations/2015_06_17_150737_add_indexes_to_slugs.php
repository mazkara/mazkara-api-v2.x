<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToSlugs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('zones', function($table)
		{
			$table->index('slug');
		});
		Schema::table('categories', function($table)
		{
			$table->index('slug');
		});
		Schema::table('businesses', function($table)
		{
			$table->index('slug');
		});
		Schema::table('services', function($table)
		{
			$table->index('slug');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
