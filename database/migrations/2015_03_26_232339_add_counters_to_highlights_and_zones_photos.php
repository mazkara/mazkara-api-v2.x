<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountersToHighlightsAndZonesPhotos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('highlights', function($table)
		{
    	$table->integer('business_count')->default(0);
		});
		Schema::table('zones', function($table)
		{
    	$table->integer('business_count')->default(0);
		});
		Schema::table('businesses', function($table)
		{
    	$table->integer('rate_card_count')->default(0);
    	$table->integer('image_count')->default(0);
    	$table->integer('reviews_count')->default(0);
    	$table->integer('favorites_count')->default(0);
    	$table->integer('checkins_count')->default(0);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
