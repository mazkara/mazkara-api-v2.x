<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCounterCacheToPivots extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('highlight_zone', function($table)
		{
    	$table->integer('business_count')->default(0);
		});

		Schema::table('category_zone', function($table)
		{
    	$table->integer('business_count')->default(0);
		});

		Schema::table('service_zone', function($table)
		{
    	$table->integer('business_count')->default(0);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
