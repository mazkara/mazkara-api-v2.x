<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreCacheFieldsToBusiness extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('businesses', function($table)
		{
    	$table->integer('services_count')->default(0);
    	$table->integer('categories_count')->default(0);
    	$table->integer('highlights_count')->default(0);
    	$table->integer('timings_count')->default(0);
    	$table->string('zone_cache')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
