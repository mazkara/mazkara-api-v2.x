<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotBusinessServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business_service', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('business_id')->unsigned()->index();
			$table->integer('service_id')->unsigned()->index();
			$table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_service');
	}

}
