<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreditNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_notes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('invoice_id')->unsigned()->index();
			$table->float('amount');
			$table->text('desc');
			$table->string('type');
			$table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_notes');
	}

}
