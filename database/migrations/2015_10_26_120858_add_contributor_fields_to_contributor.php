<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContributorFieldsToContributor extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
			$table->string('designation');
			$table->string('designated_at_text');
			$table->string('designated_at_url');
			$table->string('contact_email_address');
			$table->string('location');
			$table->string('location_id');
			$table->integer('current_locale')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
