$(function(){


//$('.dropdown').on('show.bs.dropdown', function(e){
//    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
//  });
//
//  // ADD SLIDEUP ANIMATION TO DROPDOWN //
//  $('.dropdown').on('hide.bs.dropdown', function(e){
//    e.preventDefault();
//    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(100, function(){
//      $('.dropdown').removeClass('open');
//        $('.dropdown').find('.dropdown-toggle').attr('aria-expanded','false');
//    });
    //
//  });

  $('.submit-parent-form').click(function(){
    $(this).parents().first().submit();
  });

  $('form.confirmable').submit(function(){
    return confirm('Are you sure?');  
  });

  //if(jQuery.isFunction('magnificPopup'))
  {
    $('.ajax-popup-link').magnificPopup({
      type: 'ajax',
      showCloseBtn:true,
      closeBtnInside:false,
      fixedContentPos: 'auto',
      closeOnBgClick:true,
    });    
  }

  $('[data-toggle="tooltip"]').tooltip();

  //if(jQuery.isFunction('fakeLoader'))
  {
    $(document).on('click', '.fakeloader-link', function(){
      $("#fakeloader").fakeLoader({
        timeToHide:5000,
        zIndex: '89',
        bgColor:"#fff",
        spinner:"spinner6"
      });
    });

    $('#sidebar-navbar-filter a').click(function(){
      $("#fakeloader").fakeLoader({
        timeToHide:5000,
        zIndex: '89',
        bgColor:"#fff",
        spinner:"spinner6"
      });
    });
  }



  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });

  var MZK_PRELOAD = $('a.image').map(function() {
    return $( this ).attr('href');
  }).get();

  $.imgpreload(MZK_PRELOAD);

  /*
    Handler for pushpop History states

  */

  // we get a normal Location object

  /*
   * Note, this is the only difference when using this library,
   * because the object window.location cannot be overriden,
   * so library the returns generated "location" object within
   * an object window.history, so get it out of "history.location".
   * For browsers supporting "history.pushState" get generated
   * object "location" with the usual "window.location".
   */
//  var location = window.history.location || window.location;
//
//  // looking for all the links and hang on the event, all references in this document
//  $(document).on('click', 'a.link-single-venue', function() {
//    // keep the link in the browser history
//    history.pushState(null, null, this.href);
//
//    // here can cause data loading, etc.
//    // make ajax call to the link
//    // link returns json data
//    // select what the view will be here
//    // 
//
//    return false;
//  });
//
//  // hang on popstate event triggered by pressing back/forward in browser
//  $(window).on('popstate', function(e) {
//    // here can cause data loading, etc.
//    // just post
//  });

  /*
  $('.navbar-nav.yamm').on('show.bs.dropdown', function () {
    $('#fullscreen-overlay').show();
  });

  $('.navbar-nav.yamm').on('hide.bs.dropdown', function () {
    $('#fullscreen-overlay').hide();
  });
  */

$('.yamm .dropdown').hover(function() {
    if($('.yamm-fw.dropdown.parent.open').length>0){
      $('.yamm-fw.dropdown.parent.open').removeClass('open');
      $(this).addClass('open');
    }
  }, function() {
    $(this).find('.dropdown-menu').removeClass('open');
});
$.smartbanner({
  title: 'Mazkara - Beauty and Wellness', // What the title of the app should be in the banner (defaults to <title>)
  author: null, // What the author of the app should be in the banner (defaults to <meta name="author"> or hostname)
  price: 'FREE', // Price of the app
  appStoreLanguage: 'us', // Language code for App Store
  inGooglePlay: 'Available in Google Play', // Text of price for Android
  button: 'INSTALL', // Text for the install button
  speedIn: 300, // Show animation speed of the banner
  speedOut: 400, // Close animation speed of the banner
  daysHidden: 1, // Duration to hide the banner after being closed (0 = always show banner)
  daysReminder: 1, // Duration to hide the banner after "VIEW" is clicked *separate from when the close button is clicked* (0 = always show banner)
  hideOnInstall: true, // Hide the banner after "VIEW" is clicked.
})

});
