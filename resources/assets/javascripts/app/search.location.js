$(function(){

  // assign values to the hidden form when you click on a data entry in the search drop down
  // then submit the form
  $('#search-bar-button').click(function(){
    $('#search-bar-form').find('#search-selector-name').val($("#search-selector").val());
    $('#search-bar-form').submit();
  });

//  $('#search-bar-form').submit(function(){
//    sdt = $('#search-selector').data('required');
//    if(sdt){
//      src = $('#location-selector-service').val();
//      if(src==''){
//        $('#search-selector').popover('show');
//        return false;
//      }else{
        //
//        return true;
//      }
//    }else{
//      return true;
//    }
//  })

  var cities = _MAZKARA_CITIES_; 
  var __MZK_SEARCH_LBL__ = $('#search-selector').val();
  var __MZK_SEARCH_VAL__ = $('#search-selector-name').val();

  $('#search-selector').autocomplete({
    search  : function(){ $('.search-box-preloader').show(); },
    open    : function(){ $('.search-box-preloader').hide();},    
    source:  function(request, response) {

      if(request.term == '*'){
        mzk_response = _MAZKARA_CATEGORIES_;

        response(mzk_response);
      }else{
        $.ajax({
          url: '/search',
          minLength: 2,
          data: {
            zone: $('#location-selector-name').val(),
            term: request.term
          },
          success:function(data){
            response(data);
            $('.search-box-preloader').hide();
          }
        });
      }
    },

    select: function(event, ui){
      console.log(ui.item);
      if(ui.item.type=='OUTLET'){
        $('#search-selector').val(ui.item.name);
        window.location.href = ui.item.url;
        return false;
      }else if(ui.item.type=='CATEGORY'){
        $('#location-selector-category').val(ui.item.id);
        $('#search-selector').val(ui.item.name);
        //$('#search-selector').attr('placeholder', ui.item.name);
        $("#search-selector-name").val( ui.item.name ); 
        return false;         
      }else{
        $('#location-selector-service').val(ui.item.id);
        $('#search-selector').val(ui.item.name);
        //$('#search-selector').attr('placeholder', ui.item.name);
        $("#search-selector-name").val( ui.item.name ); 
        return false;         
      }
      //$('#location-selector').focus();

    },
    focus: function( event, ui ) {
      //$( "#search-selector" ).val( ui.item.name );
      if(ui.item.id){
        return false;
      }else{
        return false;
      }
    }

  }).click(function(){
      $('#search-selector').val('');
      $('#location-selector-category').val('');
      $(this).autocomplete('search', "*");

  }).focus(function(){
    if (this.value == ""){
      $("#search-selector-name").val( '' ); 
      $('#location-selector-category').val('');
    }else{
      __MZK_SEARCH_LBL__ = $('#search-selector').val();
      __MZK_SEARCH_VAL__ = $('#search-selector-name').val();
    }
  }).keypress(function(e){

    if(e.which==13){
      if($('#search-selector').val()!=$('#search-selector-name').val()){
        // he is making a search for random - so unset the services selected
        $('#search-selector-name').val($('#search-selector').val());
      }

      $('#search-bar-form').submit();
      return false;
    }else{

    }
  }).keyup(function (e) {
    if ($(this).val() == '') {
      $("#search-selector-name").val( '' ); 
    } 
  }).blur(function (e){
    if($(this).val()==''){
      //$('#search-selector').val(__MZK_SEARCH_LBL__);
      //$('#search-selector-name').val(__MZK_SEARCH_VAL__);
      //$(this).autocomplete('search', "");
    }


    $(this).autocomplete('search', "");
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    if(item.type == 'SERVICE'){
      label = item.name;

      var re = new RegExp( "(" + this.term + ")", "gi" ),
      cls = 'highlight-ui',
      template = "<span class='" + cls + "'>$1</span>",
      label = label.replace( re, template ),

      html = '<a href="javascript:void(0)" class=" dpb">';
      html = html + '<div class=" ">' + label;
      html = html + '<div class="pull-right "><small class="gray">SERVICE</small></div><div class="clearfix" style="display:none"></div>' + '</div>';
      html = html +"</a>";
      return $( "<li>" )
          .append( html )
          .appendTo( ul );
    }else{
      if(item.type == 'OUTLET'){

        label = item.name;

        var re = new RegExp( "(" + this.term + ")", "gi" );
        cls = 'highlight-ui';
        template = "<span class='" + cls + "'>$1</span>";
        label = label.replace( re, template );

        label_zone = item.zone;

        htm = '<a href="' + item.url + '" class=" fakeloader-link dpb">';
        htm = htm + '<div class=" ">';
        htm = htm + label + ' <small class="gray">' + label_zone + '</small>';
        htm = htm + '<div class="pull-right "><small class="gray">VENUE</small></div><div class="clearfix" style="display:none"></div>';
        htm = htm + '</div>';
        htm = htm + '</a>';

        return $( "<li>" )
          .append( htm)
          .appendTo( ul );

      }else if(item.type == 'CATEGORY'){

        label = item.name;

        var re = new RegExp( "(" + this.term + ")", "gi" );
        cls = 'highlight-ui';
        template = "<span class='" + cls + "'>$1</span>";
        label = label.replace( re, template );
        if(item.icon){
          label = '<i class="'+item.icon+'"></i> ' + label;
        }
        htm = '<a href="javascript:void(0)' + '" class=" dpb">';
        htm = htm + '<div>' + label;
        if(item.icon){

        }else{
          htm = htm + '<div class="pull-right "><small class="gray">CATEGORY</small></div>';
        }
        htm = htm + '<div class="clearfix" style="display:none"></div>';
        htm = htm + '</div></a>';

//        label_zone = item.zone;
//
//        htm = '<a href="' + item.url + '" class=" fakeloader-link dpb">';
//        htm = htm + '<div class=" ">';
//        htm = htm + '<div class="pull-right "><small class="gray">CATEGORY</small></div><div class="clearfix" style="display:none"></div>';
//        htm = htm + '</div>';
//        htm = htm + '</a>';
//
        return $( "<li>" )
          .append( htm)
          .appendTo( ul );
      }else if(item.type == 'CHAIN'){

        label = item.name;

        var re = new RegExp( "(" + this.term + ")", "gi" );
        cls = 'highlight-ui';
        template = "<span class='" + cls + "'>$1</span>";
        label = label.replace( re, template );
        htm = '<a href="' + item.url + '" class="fakeloader-link dpb">';
        htm = htm + '<div>' + label;
        htm = htm + '<div class="pull-right "><small class="gray">CHAIN</small></div><div class="clearfix" style="display:none"></div>';
        htm = htm + '</div></a>';

//        label_zone = item.zone;
//
//        htm = '<a href="' + item.url + '" class=" fakeloader-link dpb">';
//        htm = htm + '<div class=" ">';
//        htm = htm + '<div class="pull-right "><small class="gray">CATEGORY</small></div><div class="clearfix" style="display:none"></div>';
//        htm = htm + '</div>';
//        htm = htm + '</a>';
//
        return $( "<li>" )
          .append( htm)
          .appendTo( ul );

      }else if(item.type == 'SERVICE'){
        htm = '<a href="javascript:void(0)" class="clearfix dpb">';
        htm = htm + '<div class="pull-left ">' + item.name + '</div>';
        htm = htm + '<div class="pull-right "><small class="gray">SERVICE</small></div>';
        htm = htm + '</a>';

        return $( "<li>" )
          .append(htm)
          .appendTo( ul );        
      }      
    }
  };
  // location box handler
  var __MZK_LOCALE_LBL__ = $('#location-selector').val();
  var __MZK_LOCALE_VAL__ = $('#location-selector-name').val();

  $('#location-selector').autocomplete({
    search  : function(){$(this).addClass('ui-autocomplete-loading');},
    open: function(event, ui) {
      $('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
      $(this).removeClass('ui-autocomplete-loading');        
    },
    source:  function(request, response) {
      if(request.term == '*'){
        mzk_response = _MAZKARA_CITIES_;
        mzk_response[0] = ({ type:'heading', label:'POPULAR LOCATIONS' });

        response(mzk_response);
      }else{
      $.ajax({
        url: '/search-locations',
        minLength: 1,
        data: {
          term: request.term
        },
        success:function(data){
          response(data);
        }
      });
      }
    },

    /* source: '/search-locations', function(request, response) {
      var results = $.ui.autocomplete.filter(cities, request.term);
      results = results.slice(0, 5);
      //results.unshift({type:'heading', label:'POPULAR LOCATIONS'});
      response(results);
    },*/

    minLength: 1,
    select: function(event, ui){
      $("#location-selector").val( ui.item.label );
      $("#location-selector-name").val( ui.item.value );

      __MZK_LOCALE_LBL__ = $('#location-selector').val();
      __MZK_LOCALE_VAL__ = $('#location-selector-name').val();

      if(ui.item.type == 'city'){

        if(ui.item.url == 'javascript:void(0)'){
          $('#search-bar-form').submit();
        }
      }

      //$('#search-selector').focus();
      return false;         
    },
    focus: function( event, ui ) {
      $( "#location-selector" ).val( ui.item.label );
      return false;
    }
  }).keypress(function(e){
    if(e.which==13){
      if($('#search-selector').val()!=$('#search-selector-name').val()){
        // he is making a search for random - so unset the services selected
        $('#search-selector-name').val($('#search-selector').val());
      }
      
      $('#search-bar-form').submit();
      return false;

    }
  }).blur(function(){
    if($(this).val()==''){
      $('#location-selector').val(__MZK_LOCALE_LBL__);
      $('#location-selector-name').val(__MZK_LOCALE_VAL__);
    }
  }).focus(function(){
    if($(this).val()==''){
      $(this).val('');
    }else{
      __MZK_LOCALE_LBL__ = $('#location-selector').val();
      __MZK_LOCALE_VAL__ = $('#location-selector-name').val();
      $('#location-selector').val('');
      $('#location-selector-name').val('');

      $(this).autocomplete('search', "*");
    }
  }).click(function(){
    if($(this).val()==''){
      $(this).val('');
    }
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    if(item.type == 'heading'){
      //html = '<a href="javascript:void(0)" class="dpb"><div ><b>'+item.label+'</b></div></a>';

      html = '<a href="javascript:void(0);" class=" dpb">';
      html = html + '<div class="text-left "><b>' + item.label + '</b></div>';
      html = html + '</a>';

    }else{
      if(item.type == 'city'){
        html = '<a href="'+(item.url!=false ? item.url : 'javascript:void(0)')+'" class=" dpb">';
        html = html + '<div class="text-left ">' + item.label + '</div>';
        html = html + '</a>';

      }else{

        html = '<a href="javascript:void(0)" class=" dpb">';
        html = html + '<div class="text-left ">' + item.label + '</div>';
        html = html + '</a>';

      }
    } 
    return $( "<li>" )
          .append( html )
          .appendTo( ul );

  };

  $('#lnk-to-change-location').click(function(){
    $('#location-selector').focus();
    $('#location-selector').autocomplete('search', "*");
  });

});