// This is a manifest file that'll be compiled into dashboard.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear in whatever order it 
// gets included (e.g. say you have require_tree . then the code will appear after all the directories 
// but before any files alphabetically greater than 'dashboard.js' 
//
// The available directives right now are require, require_directory, and require_tree
//
//= require jquery
//= require jquery-ui/jquery-ui.min
//= require jquery.fblogin/dist/jquery.fblogin
//= require bootstrap/dist/js/bootstrap.min
//= require bootstrap/js/collapse
//= require bootstrap-hover-dropdown/bootstrap-hover-dropdown
//= require magnific-popup/dist/jquery.magnific-popup
//= require bootbox.js/bootbox
//= require fakeloader/fakeLoader.min
//= require jscroll/jquery.jscroll
//= require string/lib/string.min
//= require bootstrap-star-rating/js/star-rating
//= require bootstrap3-dialog/dist/js/bootstrap-dialog
//= require jquery.imgpreload/jquery.imgpreload.min
//= require bootstrap-select/dist/js/bootstrap-select
//= require jquery-text-counter/textcounter.min
//= require summernote/dist/summernote
//= require jquery.smartbanner/jquery.smartbanner
//= require html5-history-api/history
//= require main
