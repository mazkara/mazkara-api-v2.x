@extends('layouts.scaffold')

@section('main')

<h1>Show Combo_item</h1>

<p>{{ link_to_route('combo_items.index', 'Return to All combo_items', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Business_id</th>
				<th>Name</th>
				<th>Description</th>
				<th>Cost</th>
				<th>Duration</th>
				<th>Cost_type</th>
				<th>Duration_type</th>
				<th>State</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $combo_item->business_id }}}</td>
					<td>{{{ $combo_item->name }}}</td>
					<td>{{{ $combo_item->description }}}</td>
					<td>{{{ $combo_item->cost }}}</td>
					<td>{{{ $combo_item->duration }}}</td>
					<td>{{{ $combo_item->cost_type }}}</td>
					<td>{{{ $combo_item->duration_type }}}</td>
					<td>{{{ $combo_item->state }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('combo_items.destroy', $combo_item->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('combo_items.edit', 'Edit', array($combo_item->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
