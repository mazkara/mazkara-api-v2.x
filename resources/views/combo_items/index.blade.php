@extends('layouts.scaffold')

@section('main')

<h1>All Combo_items</h1>

<p>{{ link_to_route('combo_items.create', 'Add New Combo_item', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($combo_items->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Business_id</th>
				<th>Name</th>
				<th>Description</th>
				<th>Cost</th>
				<th>Duration</th>
				<th>Cost_type</th>
				<th>Duration_type</th>
				<th>State</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($combo_items as $combo_item)
				<tr>
					<td>{{{ $combo_item->business_id }}}</td>
					<td>{{{ $combo_item->name }}}</td>
					<td>{{{ $combo_item->description }}}</td>
					<td>{{{ $combo_item->cost }}}</td>
					<td>{{{ $combo_item->duration }}}</td>
					<td>{{{ $combo_item->cost_type }}}</td>
					<td>{{{ $combo_item->duration_type }}}</td>
					<td>{{{ $combo_item->state }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('combo_items.destroy', $combo_item->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('combo_items.edit', 'Edit', array($combo_item->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no combo_items
@endif

@stop
