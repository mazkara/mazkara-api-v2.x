@extends('layouts.scaffold')

@section('main')

<h1>Show Check_in</h1>

<p>{{ link_to_route('check_ins.index', 'Return to All check_ins', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>User_id</th>
				<th>Business_id</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $check_in->user_id }}}</td>
					<td>{{{ $check_in->business_id }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('check_ins.destroy', $check_in->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('check_ins.edit', 'Edit', array($check_in->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
