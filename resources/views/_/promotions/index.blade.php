@extends('layouts.scaffold')

@section('main')

<h1>All Promotions</h1>

<p>{{ link_to_route('promotions.create', 'Add New Promotion', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($promotions->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Description</th>
				<th>Starts</th>
				<th>Ends</th>
				<th>Type</th>
				<th>Business_id</th>
				<th>Status</th>
				<th>Amount</th>
				<th>Discounted_as</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($promotions as $promotion)
				<tr>
					<td>{{{ $promotion->title }}}</td>
					<td>{{{ $promotion->description }}}</td>
					<td>{{{ $promotion->starts }}}</td>
					<td>{{{ $promotion->ends }}}</td>
					<td>{{{ $promotion->type }}}</td>
					<td>{{{ $promotion->business_id }}}</td>
					<td>{{{ $promotion->status }}}</td>
					<td>{{{ $promotion->amount }}}</td>
					<td>{{{ $promotion->discounted_as }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('promotions.destroy', $promotion->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('promotions.edit', 'Edit', array($promotion->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no promotions
@endif

@stop
