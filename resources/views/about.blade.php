@extends('layouts.page')
@section('content')
<div id="tf-home" class="text-center" style="position:absolute; height:430px;top:45px;width:100%;z-index:0;background:url({{mzk_assets('assets/add-business.jpg')}}) no-repeat fixed center center / cover rgba(200, 82, 126, 0)"></div>
<div class="text-center" style="height:395px;">
  <div class="content" >
    <div class="container " style="margin-top:125px;">
      <div class="fs150 mt50 text-right pull-right text-shadow" style="width:350px;">
        <span class="force-white">Now you can discover thousands of  </span>
        <span class="yellow">Salon's and Spa's</span><span class="force-white"> in your neighborhood.</span>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12 bg-white">
      <h1 class="text-center fw300 notransform">About Mazkara</h1>
      <p class="lead text-center pt20 pb20">
        Troubled, cause your Salon is over-booked? Fret no more, Mazkara is here to save the day! 
      </p>
      <div class"row">
        <div class="col-md-4 text-center ">
          <img src="{{mzk_assets('assets/about/01.png')}}" title="Discover Salons and Spa's around you"  style="width:250px; " />
        </div>
        <div class="col-md-4 text-center">
          <img src="{{mzk_assets('assets/about/02.png')}}" title="Filter and find services that you need" style="width:250px; " />
        </div>
        <div class="col-md-4 text-center">
          <img src="{{mzk_assets('assets/about/03.png')}}" title="Like, Rate and Review your experiences" style="width:250px; " />
        </div>
      </div>
      <div class"row fs125">
        <div class="col-md-8 col-md-offset-2 text-center">
          <p class=" pt20 pb20">&nbsp;</p>
          <h3 class="notransform">What we do is who we are! </h3>
          <p class="text-left">
            We are a Dubai based glam-tech startup. We roam the streets of Dubai, relentlessly gathering Salon and
            Spa information and put it together using slick technology & a viewer friendly layout. 
          </p>
          <h3 class="notransform">Why? </h3>
          <p class="text-left">
            So you don’t have to rely on your regular salon’s schedule or friendly suggestions like ‘Hey you have to 
            try this new place’. Now, you can discover thousands of Salon & Spa’s in your neighborhood.
          </p>
          <h3 class="notransform">Now about us,</h3>
          <p class="text-left">
            We are not really sure how we came up with the name ‘Mazkara’. Lots of coffee, endless hours of
            coding and exhausting content drives later we realized – What’s in a name? 
          </p>
          <p class="text-left">
            So we decided to trust our inner passion, closed our eyes, counted to three and spoke the first name 
            that came to our minds. And we kid you not! We all said ‘Mazkara’ at the same time. 
          </p>
          <p class="text-left">
            Our vision is to technolutionize the Beauty & Wellness industry. And Our mission is to get free massages
            while pulling off all-night coding sessions. :) 
          </p>
          <p class="text-left">
            Through intense research ;P, we realized that one thing people love more than shopping or eating is
            ourselves and keeping ourselves beautiful & healthy.
          </p>
        </div>
      </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <p class="pt20"></p>
        <hr />
        <p class="pt20"></p>
        <h1 class="text-center pt20 p15 pb10  notransform">Meet the <span class="yellow">Mazkarians</span></h1>
          <p class="pt20"></p>
          <div class="row">
            <div class="col-md-4 text-center">
              <img src="{{mzk_assets('assets/team/roy.jpg')}}" class="bw img-circle" />
              <p class="fs125 mt5 mb0">Prasanjeet Roy</p>
              <p class="mb15">Co-Founder & CEO</p>
            </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/ali.jpg')}}" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Mohammad Ali Akmal</p>
                    <p class="mb15">Co - Founder & CTO</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/apoorv.JPG')}}" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Apoorv Chopra</p>
                    <p class="mb15">Head - Launch & Expansion</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/manish.jpg')}}" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Manish Parihar</p>
                    <p class="mb15">iOS</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/munish.jpg')}}" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Munish Thakur</p>
                    <p class="mb15">Android</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/joel.jpg')}}" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Joel D'sa</p>
                    <p class="mb15">Content</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/sumedha.jpg')}}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Sumedha Dhoot</p>
                    <p class="mb15">Content</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/zainab.JPG')}}" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Zainab Varawalla</p>
                    <p class="mb15">Content</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/aziza.JPG')}}" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Aziza Makkar</p>
                    <p class="mb15">Sales</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/gauri.JPG') }}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Gauri Chadha</p>
                    <p class="mb15">Content</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/harsh.png') }}" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Harsh Dhandhania</p>
                    <p class="mb15">Launch & Expansion</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/harshita.PNG') }}" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Harshita Shah</p>
                    <p class="mb15">Human Resources</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/raveena.jpg') }}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Raveena Poon</p>
                    <p class="mb15">Sales</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/ravnoor.jpg') }}" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Ravnoor Kaur</p>
                    <p class="mb15">Content</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/jajal.jpg') }}" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Siddharth Jajal</p>
                    <p class="mb15">Content</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2 text-center">
                    <img src="{{ mzk_assets('assets/team/siva.jpg') }}" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Sivaram Adabala</p>
                    <p class="mb15">Digital Marketing</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/siddharth.jpg') }}" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Siddharth Hiremath</p>
                    <p class="mb15">Content</p>
                </div>
            </div>

        </div>
    </div>

            </div>
    </div>

</div>
@stop
