@extends('layouts.parallax')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-7 col-md-offset-1">
      <h2 class="pt10 text-center item-name mt20">{{$post->title}}</h2>
      <hr class="mb5 pt5" />
      <div class="text-right mb15">
        <div class="pull-left">
          by {{ $post->authors_full_name }}

          @if(Auth::check())
            <a href="javascript:void(0)" class="btn btn-xs {{ Auth::check() ? ($post->liked(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }} btn-default" rel="{{$post->id}}"  data-route="posts">
              <i class="fa fa-heart "></i> 
            </a>
          @else
            <a href="/users/login" class="btn btn-xs ajax-popup-link btn-default " rel="{{$post->id}}">
              <i class="fa fa-heart"></i> 
            </a>
          @endif

        </div>
        Published on {{ mzk_f_date($post->updated_at, 'jS M')}}
      </div>
      @if($post->isVideo())
        <iframe width="100%" height="450" src="{{ $post->videoUrl() }}" frameborder="0" allowfullscreen>
        </iframe>
        <div class="well fs125">
          {{ $post->caption }}
        </div>
      @else
        @if($post->hasCover())
          <img src="{{ $post->cover->image->url('xlarge') }}" width="100%" class="ba"  />
        @endif
        <div class="well fs125">
          {{ $post->caption }}
        </div>
        <div class="pt10 ">
          {{ $post->body}}
        </div>

      @endif
      <hr />
      <div class="mt10 well mb15 clearfix fw300 ">
        <div class="pull-left mb10 mr10">
          {{ViewHelper::userAvatar($post->author, ViewHelper::$avatar80)}}
        </div>
        <div class="p10">
          <p>
            <b>{{$post->author->name}}</b><br/>
            {{$post->authors_designation }}
          </p>
          <p>{{ $post->author->about }}</p>
        </div>
      </div>
      <div class="mt10 pt10 clearfix ">
        @foreach($post->comments as $comment)
          <div class="media pb10 clearfix pt10 bb">
            @if(Auth::check())
              @if($comment->isDeletableBy(Auth::user()))
                {{ Form::open(array('style' => 'display: inline-block;', 
                                    'class'=>'pull-right confirmable',  'method' => 'DELETE', 
                                    'route' => array('comments.destroy', $comment->id))) }}
                  <a href="javascript:void(0)" class="submit-parent-form"><i class="fa fa-times"></i></a>
                {{ Form::close() }}
              @endif
            @endif
            <div class="media-left">
              <a href="/users/{{$comment->user_id}}/profile">
                {{ ViewHelper::userAvatar($comment->user, ViewHelper::$avatar35) }}
              </a>
            </div>
            <div class="media-body dark-gray">
              <b>{{$comment->getDisplayableUsersName()}}</b> <span class="medium-gray">{{$comment->body}}</span>
              <p>
                <small class="gray">
                  {{{ Date::parse($comment->updated_at)->ago() }}}
                </small>
              </p>
            </div>
          </div>
        @endforeach
      </div>
      @if(Auth::check())
        <?php $user = Auth::user();?>
        <div class="media pb10 pt10 clearfix">
          <div class="media-left">
            <a href="/users/{{$user->id}}/profile" >
              {{ViewHelper::userAvatar($user, ViewHelper::$avatar35)}}
            </a>
          </div>
          <div class="media-body" style="width: 100%;">
            {{ Form::open(array('route' => 'comments.store')) }}
              {{ Form::hidden('commentable_type', 'Post')}}
              {{ Form::hidden('commentable_id', $post->id)}}
              {{ Form::hidden('type', 'comment')}}
              {{ Form::text('body', '', ['class'=>'form-control count-limiter', 'placeholder'=>'Write a comment'])}}
            {{ Form::close()}}
          </div>
        </div>
      @else
        <div class="well mt10">
          <a href="/users/create" id="ajax-login-link" class=" ajax-popup-link" title="Sign in to comment"  data-toggle="tooltip" data-placement="left" >
            Sign in
          </a>
           to leave a comment
        </div>
      @endif
      <p class="p10 ">&nbsp;</p>
    </div>
    <div class="col-md-3  pt20">
      @include('elements.coming-soon')
      <div class="fw500 pb10 mb10">POPULAR ON MAZKARA</div>

      @foreach($suggested_posts as $post)
        @include('site.posts.partials.post-niblet')
      @endforeach

    @if($service)

      <div class="fw500 pb10 mb10">POPULAR VENUES FOR {{ strtoupper($service->name)}}</div>
      <hr/>

      @foreach($businesses_with_prices as $business)
        @include('site.businesses.partials.business-service-niblet')

      @endforeach

    @endif


      @include('site.businesses.partials.ad-list')

    </div>
  </div>
</div>
@stop
@section('js')
<script type="text/javascript">
$(function(){
  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',

      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });


});
</script>

@stop