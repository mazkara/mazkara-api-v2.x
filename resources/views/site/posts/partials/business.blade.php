    <div  class=" clearfix">
      <a href="{{ MazkaraHelper::slugSingle($business) }}">
      {{ 
          ViewHelper::businessBgThumbnail($business, [ 'width'=>'100%', 
                                                  'height'=>'100%', 
                                                  'class'=>'  fw300',
                                                  'meta'=>'medium',
                                                  'no-fake'=>' ', 
                            'style'=>'max-width:250px; max-height:173px; display:block;vertical-align:middle;font-size:40px;'])
        }}
      </a>

      <div class=" p10 bg-lite-gray mb15" style="max-height:65px;">
      <div class="text-left">
        <h5 class="search-item-name media-heading mb0 notransform bolder ">
          <a title="{{{ $business->name }}}" href="{{ MazkaraHelper::slugSingle($business) }}" class=" fs80">
            {{{ $business->getTrimmedName(17) }}} 
          </a>
        </h5>
        <span title="{{ $business->zone_cache }}" class="search-item-address fs90">
          {{ mzk_str_trim($business->zone_cache, 17) }}
        </span>
        <div class="pull-right">
          {{ ViewHelper::starRateBasicKnob($business->average_rating)}} 
        </div>

      </div>

        
      </div>
    </div>
