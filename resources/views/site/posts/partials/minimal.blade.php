    <div  class=" clearfix">
      @if($post->hasCover())
      <div class="">
        <a title="{{{ $post->title }}}" href="{{ $post->url() }}" class="  " >
          <img src="{{ $post->cover->image->url('medium') }}" width="100%" class="ba"  />
        </a>
      </div>
      @endif

      <div class=" bg-lite-gray pr10 pt5 pb5 pl10">
          <h3 class="fw500 ovf-hidden text-center mb0 mt0" style="height:32px;">
            <a title="{{{ $post->title }}} " href="{{ $post->url() }}" class=" dpib  fs75">
              <b>{{{ ($post->title) }}}</b>
            </a>
          </h3>
          <p class="fs90 text-center pb0">{{mzk_str_trim($post->caption, 60)}}</p>
        <div class="" >

          <div class="  pb5 pt0">
            <div class="text-center ">
              <div class="fs90">
                <div class="pull-left">
                  <span class="fs125">
                    <i class=" flaticon flaticon-eye110  gray"></i>
                  </span>{{ $post->views }} Views
                </div>
                <div class="pull-right">
                  <span class="fs125">
                    <i class="fa fa-thumbs-up lite-blue"></i>
                  </span>{{ count($post->likes) }} Likes
                </div>
            </div>

              <div class="clearfix"></div>
            </div>
          </div>

        </div>

        
      </div>
    </div>
    <div class="show-only-mobile p10"> </div>
