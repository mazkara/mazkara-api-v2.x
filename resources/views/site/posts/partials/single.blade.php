  <article class=" clearfix  show-only-mobile" style="padding:0px;">
    <div  class=" clearfix">
      <div class="mt10 fw300  pl10 pr10 pb10 mb10">
          <div class=" pull-right">
            {{ join(',',$post->services()->lists('name','name')->all()) }}
          </div>

        <div class="pull-left pr15">
          <a href="{{route('users.profile.show', $post->author->id)}}">
            {{ ViewHelper::userAvatar($post->author)}}
          </a>
        </div>

        <b>
          <a href="{{route('users.profile.show', $post->author->id)}}">
            {{ $post->authors_full_name }}
          </a>
        </b><br/>
        {{ $post->authors_designation }}

        <div class="clear"></div> 
      </div>

      @if($post->hasCover())
        <div class="">
          <a title="{{{ $post->title }}}" href="{{ $post->url() }}" class="  " >
            <img src="{{ $post->cover->image->url('medium') }}" width="100%" class="ba"  />
          </a>
        </div>
      @endif

      <div class=" ">
          <h3 class=" fw500 ovf-hidden mt10 p10  mb0" style="margin-top:0px;">
            <a title="{{{ $post->title }}}" href="{{ $post->url() }}" class=" dpib ">
              {{{ ($post->title) }}}
            </a>
          </h3>
        <div class="" >

          <div class="p10 pt0 bb">
            <div class="text-left ">
              <div class="pull-left">
                <span class="fs175">
                <i class="fa fa-thumbs-up gray"></i>
              </span>&nbsp;&nbsp;
              <span class="fs175">
                <i class=" fa fa-comment-o  gray"></i>
              </span>
            </div>

              <div class="text-right gray pull-right fs90">
                {{ $post->num_comments() }} Comment(s)<br/>
                {{ $post->num_likes() }} Like(s)
              </div>
              <div class="clearfix"></div>
            </div>
          </div>

        </div>

        
      </div>
    </div>

  </article>
  <article class="mb25 clearfix  hide-only-mobile" style="padding:0px;">
    <div  class=" clearfix">

      @if($post->hasCover())
      <div class="pull-left pr10 relative">
        <a title="{{{ $post->title }}}" href="{{ $post->url() }}" class="  " >
          <img src="{{ $post->cover->image->url('medium') }}" width="270" class="ba"  />
        </a>
      </div>
      @endif

      <div class="">
        <div class="pos-relative p10 pt0" >
          <div class="mb10 pl10">
            <div class="text-left bb ml0  m10 mb0 pt0 pr0 pb5">

              <div class="pull-right">
                <span class="">
                  <i class=" flaticon flaticon-eye110 op80 medium-gray"></i>
                  <span class="medium-gray fs90"><b>{{ $post->views }} Views</b></span>
                </span>
                
                &nbsp;
                <span class="">
                  <i class="fa fa-thumbs-up op80 {{ $post->num_likes()>0 ? 'lite-blue':'medium-gray' }} "></i>
                </span>
                <span class="fs90 medium-gray"><b> {{ $post->num_likes() }} Likes</b></span>
              </div>
              
              <div class="fs90 medium-gray">
                {{ strtoupper(join(', ', $post->services()->lists('name','name')->all())) }}

              </div>
            </div>
          </div>
          <h3 class=" fw500 ovf-hidden mt10 mb5" style="margin-top:0px;">
            <a title="{{{ $post->title }}}" href="{{ $post->url() }}" class="hover-underline dpib ">
              {{{ ($post->title) }}}
            </a>
          </h3>
          <div class="mt5 pt5  mb2">
            {{{ ($post->caption) }}}
          </div>
          <div class="mt10 fs90">
            <a title="{{{ $post->title }}}" href="{{ $post->url() }}" >Read More <i class="fa fs125 fa-caret-right"></i></a>
          </div>

          <div class="mt10   ">
            <div class="pull-left pr15">
              <a href="{{route('users.profile.show', $post->author->id)}}">
                {{ ViewHelper::userAvatar($post->author)}}
              </a>
            </div>

              <a href="{{route('users.profile.show', $post->author->id)}}">
                {{ $post->authors_full_name }}
              </a>
              <span class="medium-gray">&nbsp;&nbsp;<i>posted {{{ Date::parse($post->published_on)->ago() }}}</i></span>
            <br/>
            <span class="medium-gray">{{ $post->authors_designation }}</span>

              <div class="clear"></div> 

          </div>
          <div class=""></div>

        </div>

        
      </div>
    </div>

  </article>
