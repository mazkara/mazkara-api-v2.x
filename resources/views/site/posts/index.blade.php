@extends('layouts.master')
@section('content')
<?php mzk_timer_start('inside index- view');?>
<div class="row">
  <div class="col-md-12">
    <div class="pt10 mt10 mb10 pb10 ">
      <h1 class="text-center">STO<span class="yellow">RIES</span></h1>
      @if(isset($filters['second-heading']))
        <p class="text-center">{{$filters['second-heading']}}</p>
      @endif
      <hr/>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-9  li-pl0 ">

    <div class="list-group mt20">
      <ol class="  list-unstyled listing">
        @foreach($posts as $post)
          @if(is_object($post))
            <li class=" single-post-element  pr0">

              @include('site.posts.partials.single', ['post'=>$post])
            </li>
          @endif
        @endforeach
      </ol>
    </div>
    <div class="clearfix">
      <div class="pull-left">
        {{ $posts->currentPage() }} of
        {{ $posts->lastPage() }} pages
      </div>
      <div class="pull-right">
        {{ $posts->appends($params)->render() }}        
      </div>
    </div>

  </div>
  <div class="col-md-3  pt20">

    <div class="pb10 mb10">
      {{ Form::open(['method'=>'GET', 'route'=>'posts.index.all'])}}
        <div class="input-group">
          <input type="text" class="form-control" name="search" placeholder="search for stories">
          <div class = "input-group-btn">      
          <button type="submit" class="btn-square btn btn-inverse yellow bg-black"><i class="fa fa-search"></i></button></div>
        </div>
      {{ Form::close()}}
    </div>



    <div class="pb10 mb10">
      <div class="fw500 pb10 mb10">BROWSE VIA SERVICES</div>
      @foreach($service_tags as $ii=>$servce)
        <div class="pb5 service-link-object {{ $ii>5 ? 'hidden' :''}}">
          <a class="dark-gray hover-underline" href="{{ route('posts.service', [$servce->slug]) }}">{{$servce->name}}</a>
        </div>
      @endforeach
      <a id="lnk-show-all-services" href="javascript:void(0)" class="mt5 mb5">
        <b>View All Services</b>
      </a>
    </div>
    <div class="fw500 pb10 mb10">TRENDING STORIES</div>
    @foreach($suggested_posts as $post)
      @include('site.posts.partials.post-niblet')
    @endforeach
    @if($service)
      <small class=" pb10 mb10">POPULAR VENUES FOR {{$service->name}}</small>
      <hr/>

      @foreach($businesses_with_prices as $business)
        @include('site.businesses.partials.business-service-niblet')



      @endforeach

    @endif

  </div>
</div>
@stop
@section('js')
<script type="text/javascript">
$(function(){
  $('#lnk-show-all-services').click(function(){
    $('.service-link-object').removeClass('hidden');
    $(this).hide();
  });
});
</script>
@stop
<?php mzk_timer_stop('inside index- view');?>
