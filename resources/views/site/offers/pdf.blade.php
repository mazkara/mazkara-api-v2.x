@extends('layouts.blank')
@section('content')

<?php $css = 'style="padding-bottom:0px; margin-bottom:0px;margin-top:0px;"';?>
<?php $csstop = 'style="padding-bottom:0px; margin-bottom:0px;margin-top:30px;"';?>
<?php $fnt = 'font-family: Tahoma, Arial;';?>
<table width="100%">
  <tr>
    <td align="center" width="100%" style="vertical-align:top;padding-left:0px;margin-bottom:20px;margin-left:0px;">
      <img src="{{mzk_assets('assets/mazkaralogo.png')}}" align="center" width="175px"   />
    </td>
  </tr>
</table>
<?php $business = $voucher->offer->business;?>

<table >
  <tr>
    <td align="left" style="width:450px;padding-right:10px;margin-bottom:20px;margin-left:0px;">
      @if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage())
        <img style="width:450px;" src="{{($business->getCoverUrl())}}"/>
      @endif
    </td>
    <td style="width:200px;text-align:center;border:2px solid #00000;padding:10px;">
      <div style=" ">
        <div style="font-size:150%;{{$fnt}};">{{ $voucher->offer->title }}</div>
        <div style="color:pink;border:2px solid pink; text-align:center; font-size:125%; display:inline-block; margin:15px; padding:15px;font-weight:500;">
          {{ mzk_offer_price($voucher->offer) }}
        </div>        
      </div>
    </td>
  </tr>
</table>
<center>
<div style="width:660px; display:inline-block;text-align:left;">
  <div style="">{{ $business->name }}</div>
  <p>{{ $business->getAddressAsString() }}</p>
  @if(is_array($business->displayablePhone()))
    @if(count($business->displayablePhone())>0)
      @foreach($business->displayablePhone() as $phone)
        <p>
          Phone: {{ ViewHelper::phone($phone) }}
        </p>
      @endforeach
    @endif
  @endif
  <p>{{ $voucher->offer->description }}</p>



<hr/>

<p>Your voucher code</p>
<img style="width:400px;" src="{{ $voucher->getBarCodePNG() }}" alt="barcode" />

<p>
  {{ $voucher->code }}
</p>
<p>
  <b>Claimed on:</b> {{ mzk_f_date($voucher->created_at) }}
  <b>Valid Until:</b> {{ mzk_f_date($voucher->offer->valid_until) }}
</p>
{{ mzk_status_tag($voucher->state, ' label text-center ', 'span', ' label') }}
<hr/>
  @if(count($voucher->offer->toc)>0)
    <h5>
      Terms & Conditions
    </h5>
    <div >
      <ul >
        @foreach($voucher->offer->toc as $toc)
          <li>{{$toc}}</li>
        @endforeach
      </ul>
    </div>
  @endif
</div>

</center>
@stop
