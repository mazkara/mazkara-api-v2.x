<div class="  col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 ">
  <div class="row">
    <div class="bg-white p0 col-md-10 col-md-offset-1">
@if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage())
  <div class="text-center" style="background-image:url({{mzk_cloudfront_image($business->getCoverUrl('medium'))}}); background-position:center; background-repeat:no-repeat; height:180px;"></div>
@else
  <div class="text-center bg-burgundy" style="height:180px;"></div>

@endif
  <div class="bg-black force-white text-center p10 mt0" style="opacity:0.7; top: 0; display: block; position: absolute; vertical-align: top; width: 100%; z-index: 2;">
    <b>{{ strtoupper(join(', ', array_filter([$business->name, $business->zone_cache]))) }}</b>
  </div>

  <div class="  mt10">
    <div class="text-center fw500 p5 ">
      {{$offer->title}}
    </div>
    @if(strlen(trim($offer->description))>0)
      <div class="p5 pl10 pr10  fs90 bg-lite-gray">
        About the Offer
      </div>
      <div class="p5 pl10 pr10 fs90 fw300 mb10">
        {{ $offer->description }}
      </div>
    @endif
    @if(count($offer->toc)>0)
      <div class="p5 pl10 pr10  fs90 bg-lite-gray">
        Terms & Conditions
      </div>
      <div class="p5 pl10 pr10 fw300 pb0">
        <ul class="ml10 fs90">
          @foreach($offer->toc as $toc)
            <li>{{$toc}}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="pt5 pb5 text-center">
      {{ mzk_offer_price($offer, ' ', 'dpib  br0 pink p5 pl10 pr10 border-pink bg-white' ) }}
      <p class="mt10">
        ONLY <span class="text-danger">{{$offer->available_vouchers}}</span> VOUCHERS LEFT!
      </p>
    </div>
    <div class="bg-gray  ">
      {{ Form::open(['url'=>route('offers.post.claim.form', [$offer->id]), 'class'=>'claim-form', 'onsubmit'=>'return false;'] ) }}
        <div class="pl15 pr15">
          <div class="dpb text-center fs90">ENTER YOUR MOBILE NUMBER TO CLAIM THIS {{ strtoupper(mzk_label('special')) }}</div>
          {{ Form::text('phone', $user->phone, ['class'=>'form-control mb10', 'id'=>'claim-phone-number', 'required'=>true] )}}
        </div>
        <a href="javascript:void(0)" class="btn no-radius submit-btn btn-pink p10 dpb">CLAIM THIS {{ strtoupper(mzk_label('special')) }}</a>
      {{ Form::close() }}
    </div>

</div>
  </div>
  </div>
</div>

<script type="text/javascript">
$(function(){
  $('.claim-form .submit-btn').click(function(){
    frm = $(this).parents('.claim-form').first();
    data = {};
    data['phone'] = $('#claim-phone-number').val();
    
    if(data['phone'].value == ''){
      alert('Mobile number is required.');return false;
    }


    $.ajax({
      type: "POST",
      url: $(frm).attr('action'),
      data: data,
      success: function(result) {
        $('.mfp-content').html(result['html']);
      }
    });

  });
  
});
</script>