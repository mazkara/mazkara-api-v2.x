<?php $total_photos = isset($total_photos)?$total_photos:5;?>
@if(1==2)
  <div class="{{ $business->total_packages_count > 0 ? '' : 'pt10' }} pb10 bbb">
    <h4 class="item-headers "><a href="{{ MazkaraHelper::slugSinglePackages($business) }}">PACKAGES</a></h4>
    <div class="package-card-holder">
      @foreach(mzk_secure_iterable($business->packages()->get()) as $ii=>$one_photo)
        @if($one_photo->hasImage())
          <a href="{{ $one_photo->photo->image->url('large') }}" class="mr5  {{ ($ii >= 4) ? 'hide' : '' }} image  mb15" style="display:inline-block" ><img src="{{ $one_photo->photo->image->url('small') }}" class=" " /></a>
        @endif
      @endforeach
      @if(count($business->packages)>4)
        <a class="fs150 mb0 no-border-radius fw300 btn btn-default  w175 h175 bg-lite-gray " style="display:inline-block; vertical-align:middle; text-align:center" href="{{MazkaraHelper::slugSinglePackages($business)}}">
          <div class="va-container va-container-v va-container-h ">
            <div class="va-middle text-center">
          {{count($business->packages)-4}}<br/>
          more
        </div>
          </div>
        </a>
      @endif
    </div>
  </div>
@endif
<?php $total_photos = isset($total_photos)?$total_photos:5;?>
@if(count($rateCards) >0)
  <div class=" {{$business->total_packages_count > 0?'pt10':''}} pt10 pb10 ">
    <h4 class="item-headers ">MENU</h4>
    <div class="rate-card-holder">
      @foreach(mzk_secure_iterable($rateCards) as $ii=>$one_photo)
        @if((count($rateCards)>($total_photos-1)) && ($ii >=(count($rateCards)-1)))
        @else

          @if($one_photo->hasImage())
            <a href="{{ mzk_cloudfront_image($one_photo->image->url('base')) }}" class="  {{($ii+1)%(ceil($total_photos/2))==0?'':'mr5'}} {{ ($ii>=($total_photos-1))?'hide':''}}   mb15 image" style="display:inline-block" ><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" data-sequence="{{($ii+1)}}" alt="{{ $business->name }} + Rate Card {{ $ii+1 }}" class="w144 h144 rate-card-image" /></a>
          @endif
        @endif
      @endforeach
      @if(count($rateCards)>($total_photos-1))
        <a style="background-image:url({{ mzk_cloudfront_image($one_photo->image->url('small')) }})" class="fs150 sq-card no-border-radius mb0 fw300 btn btn-default p0 bg-lite-gray w144 h144 image" style="display:inline-block; vertical-align:middle; text-align:center" href="{{ mzk_cloudfront_image($one_photo->image->url('base')) }}">
          <div class="va-container va-container-v va-container-h overlap-transparent">
            <div class="va-middle text-center force-white" style="z-index:999;">
          +{{count($business->rateCards)-($total_photos-1)}}
          
        </div></div>
        </a>
      @endif
    </div>
  </div>
@endif


