<?php $business_url =  MazkaraHelper::slugSingle($business);?>

<li class=" single-business-element mb10 ">
  <article class=" clearfix hide-only-mobile " style="padding:0px;">
    <div  class=" ba clearfix">
      <div class="pull-left   pr10 relative">
        <span style="position:absolute;top:7px;left:10px;">
          @if(Auth::check())
            <a href="javascript:void(0)" style="font-size:20px;" title="Favorite {{ $business->name }}" class=" {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
              <i class="fa fa-heart hide-when-unfavorite"></i> 
              <i class="fa fa-heart-o hide-when-favorite" style="color:#fff;"></i> 
            </a>
          @else
            <a href="/users/login" style="font-size:20px;" title="Favorite {{ $business->name }}" class="page-scroll ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
              <i class="fa fa-heart-o " style="color:#fff;"></i> 
            </a>
          @endif
        </span>
        <a title="{{{ $business->name }}}" href="{{ $business_url }}" class=" link-single-venue " >
          {{ 
              ViewHelper::businessThumbnail($business, [ 'width'=>'250', 
                                                      'height'=>'173', 
                                                      'class'=>'  fw300',
                                                      'meta'=>'medium',
                                                      //'no-meta'=>'small', 
                                'style'=>'width:250px; height:173px; padding-top:46px;font-size:40px;'])
            }}
        </a>
      </div>
      <div class="">
        <div class="pos-relative p10" >
          <div class="pull-right search-item-rating dpib ">
            {{ ViewHelper::starRateBasicKnob($business->average_rating, 's', $business->accumulatedReviewsCount()) }}
          </div>
          <h3 class="search-item-name fw700 ovf-hidden mb0" style="margin-top:0px;">
            <a title="{{{ $business->name }}}" href="{{ $business_url }}" class="link-single-venue dpib hover-underline result-title">
              {{{ mzk_str_trim($business->name) }}}
            </a>
          </h3>
          <div class="mt2 mb2">
          </div>
          <div class="mt2   single-listing-address">
            <span class="color-black"><i class="fa fa-map-marker  "></i>
            {{ $business->zone_cache }}</span>
            <span title="{{ $business->getAddressAsString() }}" class="search-item-address dark-gray">› {{ $business->getAddressAsString() }}</span>
          </div>

          <?php
          $highlights =$business->getMeta('highlights');
          $costOptions = Business::getCostOptions();
          ?>
          <div class="mt2 mb2  ">


            <div class="pull-left dark-gray">
              <ul class="list-unstyled p0 m0 list-inline">
              @foreach((mzk_secure_iterable($highlights)) as $ii=>$highlight)
                @if(MazkaraHelper::isActiveHighlight($ii))
                <li class="p0 mb5">
                  <div class="text-center medium-gray">
                    <!--<a rel="nofollow" title="{{$highlight}} in {{$business->zone_cache}}" href="{{ MazkaraHelper::slugCity(null, ['highlights'=>[$ii]]) }}" class="dpib search-item-text pr5">-->
                      <div class="dpib" >
                        <i style="font-size:175%;" class="highcon medium-gray highcon-{{$ii}}"></i>
                      </div>
                      <div class="fs75 medium-gray">{{ ($highlight)}}</div>
                    <!--</a>-->
                  </div>
                </li>
                @endif
              @endforeach
              @if($business->cost_estimate > 0)
                <li class="p0 mb5">
                  <div class="text-center medium-gray">
                    <div class="dpib" >
                      <i style="font-size:175%;" class=" medium-gray {{mzk_icon_cost_estimate($business->cost_estimate)}}"></i>
                    </div>
                    <div class="fs75 medium-gray">{{ ($costOptions[$business->cost_estimate])}}</div>
                  </div>
                </li>
              @endif
            </ul>

            @if($business->image_count > 0)
              <a href="{{MazkaraHelper::slugSinglePhotos($business)}}" class="btn-xs hover-color-gray hover-border-gray medium-gray no-border-radius fs90 mb5 btn text-center " style="padding:3px;border:1px solid #CCC;color:#CCC;width:90px;">
                {{ $business->image_count }} PHOTO(S)
              </a><br/>
            @endif

            @if($business->rate_card_count > 0)
              <a href="{{MazkaraHelper::slugSingleRateCards($business)}}" class="btn-xs medium-gray hover-color-gray hover-border-gray fs90 btn text-center no-border-radius  "  style="padding:3px;border:1px solid #CCC;width:90px;color:#CCC;">
                VIEW MENU
              </a>
            @endif


            </div>

            <div class="pull-right mt10 text-right">
              <div class="">
                <?php $service_flag = false;?>
                @if(isset($prices[$business->id]))
                  @foreach($prices[$business->id] as $one_price)
                      <?php $service_flag = true;
                      $cost_price = mzk_get_starting_price($one_price['starting_price'], MazkaraHelper::getCitySlugFromID($business->city_id));
                      ?>
                      @if(trim($cost_price)!='')
                      <span class="dpb p5 bg-lite-gray mb10">
                        {{ $one_price['service_name'] }}
                        - {{ $cost_price }}
                      </span>
                      @endif
                  @endforeach
                @endif

                <?php $phones = $business->getMeta('current_numbers');//$business->displayablePhone();?>
                @if($service_flag)
                  <a data-placement="top" data-business="{{$business->id}}" 
                    class="call-button mt25 btn no-border-radius btn-success  popover-desk" 
                    data-toggle="popover" data-phone="{{ count($phones)>0 ? join('|', $phones) : 'No phone number available.' }}" data-container="body" type="button" data-html="true" href="javascript:void(0)" ref="popover-content-for-desk-{{$business->id}}" id="call-to-book-desk-{{$business->id}}">CALL NOW</a>
                @else
                  <a data-placement="top" data-business="{{$business->id}}" 
                    class="call-button mt60 btn no-border-radius btn-success  popover-desk" 
                    data-toggle="popover" data-phone="{{ count($phones)>0 ? join('|', $phones) : 'No phone number available.' }}" data-container="body" type="button" data-html="true" href="javascript:void(0)" ref="popover-content-for-desk-{{$business->id}}" id="call-to-book-desk-{{$business->id}}">CALL NOW</a>
                @endif
              </div>


            </div>
            <a rel="nofollow" href="#" style="visibility:hidden;" class="search-item-text pr5">&nbsp;</a>
              <div class="clear"></div> 

          </div>
          <div class=""></div>

        </div>

        <div class="search-item-links  dpib pt5 ">
          <?php       
            $offers = $business->getMeta('offers');
  
            $offers_block = '';
          ?>
          @if( (count($offers)>0))
            <a class="btn hidden blue-btn-arrow btn-lite-blue btn-xs mr0 mb5 toggler" rel="offers-for-{{$business->id}}" href="javascript:void(0)">Offers</a>
            <?php ob_start();?>
              <div class=" clearfix " id="offers-for-{{$business->id}}">
                <table class="mb0 table">
                  @foreach(mzk_secure_iterable($offers) as $ii=>$offer)
                    @if( strtotime($offer->valid_until) >= strtotime(date('Y-m-d')))
                    <?php //$offer = mzk_array_to_object($offer);?>
                    <tr class="hover-gray">
                      <td class=" bg-lite-gray color-black" style="border-top:3px solid #fff;">
                      <div style="overflow:hidden;text-overflow:ellipsis;height:auto;display:inline-block;">  {{ mzk_offer_title($offer) }}</div>
                      </td>
                      <td style="border-top:3px solid #fff;" width="10%" class="bg-lite-gray color-black">
                          <span style="text-decoration:line-through;" class="medium-gray">{{ mzk_price($offer->original_price, '&nbsp;') }}
                      </td>
                      <td style="border-top:3px solid #fff;" width="10%" class="text-right fw700 bg-lite-gray pink pr0"><b>{{ mzk_offer_price($offer, '&nbsp;') }}</b></td>
                      @if(Offer::areClaimable($offer))
                        <td style="border-top:3px solid #fff;" width="10%" class="text-right bg-lite-gray pr0">
                          <a class="btn btn-pink btn-xs br0 no-border-radius p5 ajax-popup-link" href="{{ route('offers.get.claim.form', [$offer->id]) }}">CLAIM</a>
                        </td>
                      @endif
                    </tr>
                    @endif
                  @endforeach
                </table>
              </div>
            <?php $offers_block = ob_get_contents(); ob_end_clean();?>
          @endif
          <div class="clear"></div>
        </div>
        
      </div>
    </div>
{{ $offers_block }}        

  </article>
  <article class=" pb5 clearfix pb5 show-only-mobile">
    <div class="">
          <div  class="hover-gray ba clearfix">

      <div class="pull-left  mr10 relative">
                <span style="position:absolute;top:7px;left:10px;">
          @if(Auth::check())
            <a href="javascript:void(0)" style="font-size:20px;" title="Favorite {{ $business->name }}" class=" {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
              <i class="fa fa-heart hide-when-unfavorite"></i> 
              <i class="fa fa-heart-o hide-when-favorite" style="color:#fff;"></i> 
            </a>
          @else
            <a href="/users/login" style="font-size:20px;" title="Favorite {{ $business->name }}" class="page-scroll ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
              <i class="fa fa-heart-o " style="color:#fff;"></i> 
            </a>
          @endif
        </span>

        <a title="{{{ $business->name }}}" href="{{ $business_url }}" class="link-single-venue show-only-mobile" >
          {{ ViewHelper::businessThumbnail($business, [ 'width'=>'145', 
                                                      'height'=>'145', 
                                                      'class'=>'   fw300',
                                                      'meta'=>'small',
                                'style'=>'width:145px; height:145px; padding-top:36px;font-size:40px;'])}}
        </a>
      </div>
      <div class="">
        <div class="pos-relative">
          <div class="pull-right  mb10 mr10">
            <div class="search-item-stars show-only-mobile ">
              <span class="">
                {{ ViewHelper::starRateBasic($business->average_rating) }}
                <div class="clear"></div> 
              </span>
            </div>
          </div>
                    <h3 class="search-item-name fw500 ovf-hidden mb0">


            <a title="{{{ $business->name }}}" href="{{ $business_url }}" class="link-single-venue ovf-hidden hover-underline result-title">
              {{{ mzk_str_trim($business->name) }}}
            </a>

            <div class="search-item-rating dpib ">
              <div class="clear"></div>
            </div>
          </h3>
          <div class="mt2 mb2 single-listing-address">
            <b>
              <i class="fa fa-map-marker  "></i> {{ $business->zone_cache }}
            </b>
            <span title="{{ $business->getAddressAsString() }}" class="search-item-address">› {{ $business->getAddressAsString() }}</span>
          </div>
          <?php
          $highlights =$business->getMeta('highlights');
          $highlights = is_array($highlights)?$highlights:[];
          ?>
          <div class="pull-left dark-gray">
            @foreach((mzk_secure_iterable($highlights)) as $ii=>$highlight)
              <?php $genders = [4,5,6];?>
              @if(in_array($ii, $genders))
                @if(MazkaraHelper::isActiveHighlight($ii))
                  <div><a rel="nofollow" title="{{$highlight}} in {{$business->zone_cache}}" href="{{ MazkaraHelper::slugCity(null, ['highlights'=>[$ii]]) }}" class="search-item-text pr5">
                    {{$highlight}}
                  </a></div>
                @endif
              @endif
            @endforeach
          </div>

          <div class="mt15 mr10">
            <a data-placement="top" data-business="{{$business->id}}"  
              class="call-button btn no-border-radius pull-right btn-success mt2 popover-mobile" 
              data-toggle="popover" data-phone="{{ count($phones)>0 ? join('|', $phones):'No phone number available.' }}" data-container="body" type="button" data-html="true" href="javascript:void(0)" ref="popover-content-for-desk-{{$business->id}}" id="call-to-book-desk-{{$business->id}}">CALL NOW</a>
          </div>
          <div class=""></div>
        </div>
        <?php       
          $offers = $business->getMeta('offers');
          $offers_block = '';
        ?>
        @if( (count($offers)>0))
          <a class="btn hidden blue-btn-arrow btn-lite-blue btn-xs mr0 mb5 toggler" rel="offers-for-{{$business->id}}" href="javascript:void(0)">Offers</a>
          <?php ob_start();?>
            <div class=" clearfix pt0 " id="offers-for-{{$business->id}}">
              <table class="mb0 table">
                @foreach(mzk_secure_iterable($offers) as $ii=>$offer)
                  @if( strtotime($offer->valid_until) >= strtotime(date('Y-m-d')))
                    <?php //$offer = mzk_array_to_object($offer);?>
                      <tr>
                        <td class=" bg-lite-gray color-black" style="border-top:3px solid #fff;">
                          <div style="overflow:hidden;text-overflow:ellipsis;height:auto;display:inline-block;">  {{ mzk_offer_title($offer) }}</div>
                        </td>
                        <td style="border-top:3px solid #fff;" class="text-right bg-lite-gray fw700 pink pr0"><b>{{ mzk_offer_price($offer) }}</b></td>
                        @if(Offer::areClaimable($offer))
                          <td style="border-top:3px solid #fff;" width="10%" class="text-right bg-lite-gray pr0">
                            <a class="btn btn-pink no-border-radius p5 btn-xs br0 ajax-popup-link" href="{{ route('offers.get.claim.form', [$offer->id]) }}">CLAIM</a>
                          </td>
                        @endif
                      </tr>
                  @endif
                @endforeach
              </table>
            </div>
          <?php $offers_block = ob_get_contents(); ob_end_clean();?>
        @endif
      </div>

      </div>
        <div class=" dpib pt0"  style="width:100%">
          {{ $offers_block }}
          <div class="clear"></div>
        </div>

    </div>
  </article>
</li>
