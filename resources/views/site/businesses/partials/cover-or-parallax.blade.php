<?php 
if(!isset($phones)):
  $phones = $business->displayablePhone();
endif;
?>
@if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage())
  <div class="cover-parallax  container p0 text-center p0" style="background:url({{mzk_cloudfront_image($business->getCoverUrl('xlarge'))}}) no-repeat center center rgba(200, 82, 126, 0)"></div>
  <div class="overlap-parallax-mobile text-center container p0 overlap-transparent show-only-mobile" style=""></div>
  <div class="overlap-parallax text-center  container p0 overlap-transparent hide-only-mobile" style=""></div>
  <div class="container" style="z-index:100;">
    <div class="overlap-parallax-content" style="">
    <div class="row">
      <div class="col-md-12 pt20 force-white">
        {{ $breadcrumbs->render() }}
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 center-on-mobile" >
        <div class=" force-white " >
          <div class="mt220 pt10 hide-only-mobile">
          </div>
          <div class="mt0 show-only-mobile">&nbsp;</div>
          @include('site.businesses.partials.title-and-rating-for-parallax', ['business'=>$business, 'phones'=>$phones])
          <div class="show-only-mobile fs175">{{ mzk_favorite_small_badge($business); }}</div>
        </div>
        <div class="center-on-mobile btns-ghosted mb10">
          @include('site.businesses.partials.action-bar', ['business'=>$business, 'phones'=>$phones])
          <p class="show-only-mobile mt20 mb20 ">&nbsp;</p>
        </div>
      </div>
      <div class="col-md-2">
        <div class="hide-only-mobile">
          <div class="pull-right ">

        <div class="p10 pt25 text-center ">
          {{ ViewHelper::starRateBusinessMedal($business, true)}}
        </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@else
  <div class="cover-parallax text-center bg-burgundy" style=""></div>
  <div class="container">
  <div class="overlap-parallax-content-no-photo" style="">
    <div class="row">
      <div class="col-md-12 pt20 force-white">
        {{ $breadcrumbs->render() }}
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 center-on-mobile" >
        <div class=" force-white " >
          <div class="mt220 pt10 hide-only-mobile"></div>
          <div class="mt25 show-only-mobile">&nbsp;</div>
          @include('site.businesses.partials.title-and-rating-for-parallax', ['business'=>$business, 'phones'=>$phones])
        </div>
        <div class="btns-ghosted mb10">
          <p class="show-only-mobile mb0   ">&nbsp;</p>
          @include('site.businesses.partials.action-bar', ['business'=>$business, 'phones'=>$phones])
          <p class="show-only-mobile mt20 mb20 ">&nbsp;</p>
        </div>
      </div>
      <div class="col-md-3">
        <div class="hide-only-mobile">
          <div class="pull-right ">

        <div class="p10 pt25 text-center ">
          {{ ViewHelper::starRateBusinessMedal($business, true)}}
        </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endif
