<div class="row">
  <div class="col-md-12">
    <div class="header">
      @if($business->isHomeService())
      <div class="hide-only-mobile mt0"></div>
        <div >
          <span class="label  label-warning"  style="font-size:14px;">
            <span class="fw500" ><i class="fa fa-home"></i> HOME SERVICES</span>
          </span>
        </div>
      @else
        <div class="hide-only-mobile mt5"></div>

      @endif
      <h1 class="item-name mt10 mb0 fs220 fw300">
        <a href="{{MazkaraHelper::slugSingle($business)}}" title="{{ $business->name }}">
          <span class="fw500 ">{{ $business->name }}</span>
          @if(in_array($business->active, ['temporarily.closed','opening.soon']))
            <div style="font-size: 10px;" class="  label label-warning"><b>{{strtoupper(str_replace('.', ' ', $business->active ))}}</b></div>
          @endif
          <div style="font-size:18px;padding-bottom:7px;"  class="pt3">{{$business->zone_cache}}</div>
          <div class="show-only-mobile p10 text-center">
            {{ ViewHelper::starRateBasic($business->rating_average, 'l') }}
          </div>
        </a>
      </h1>
    </div>
  </div>
</div>