<?php 
if(!isset($reviews)):
  if(isset($myreview) && !is_null($myreview)){
    $reviews = $business->completedReviews()->orderBy(DB::RAW('user_id="'.Auth::user()->id.'"'), 'DESC')->get();
  }else{
    $reviews = $business->completedReviews;
  }
endif;
?>

@if(!Auth::check() && (count($reviews)==0))
<?php   return ;?>
@endif

<?php $services = (mzk_get_active_services()) ;?>

<h4 class="item-headers pt5">
  REVIEWS
  @if(count($reviews)>0)
  <span class="gray">({{count($reviews)}})</span>
  @endif
</h4>
<a id="reviews"></a>
@if(Auth::check())
@else
<div class="p10 no-border-radius bg-lite-gray dpib mb15 mt10 ">
  <a href="/users/create" id="ajax-login-link" class="ajax-popup-link pink">SIGN IN</a> TO REVIEW THIS VENUE
</div>
@endif
<?php

?>
@if(count($reviews)==0)
<div class="no-reviews-placeholder">
  <div class="well review-placeholder-removeable">
    <a href="javascript:void(0)" class="btn btn-default review-btn">Review</a>
  </div>
  <p class="lead text-center lite-gray" style="font-size:600%;">
    <i class="fa fa-pencil "></i> 
  </p>
  <p class="lead text-center">
    Be the first to review {{$business->name}}
  </p>
</div>
@endif
@foreach($reviews as $review)
  @include('site.reviews.partials.single', ['review'=>$review])
@endforeach

@section('js-review')

<script type="text/javascript">
$(function(){

  $('#service_ids').selectpicker({maxOptions:3});
  @if(isset($review))
    $('#service_ids').selectpicker('val', [{{ join(',', $review->services()->lists('service_id','service_id')->all()) }}]);
  @endif

  $('#submit-review-button').on('click', function () {
    rev_rating = $('#form-manage-review').find('input[name=rating]').first();
    rev_body = $('#form-manage-review').find('textarea[name=body]').first();
    missing = [];

    if($(rev_rating).val() == 0){
      missing.push('You cannot add a Review without a rating! ');
    }

    if($(rev_body).val().length < 50){
      missing.push('Less than 50 characters? C\'mon babes you can do better than that!');
    }

    if(missing.length>0){
      alert(missing.join("\n"));
      return false;
    }else{
      var $btn = $(this).button('loading')
    }

  })

  $('.review-btn').click(function(){
    

    $.ajax({
      type: "GET",
      url: '{{ route('businesses.review.form', [$business->id] ) }}',
      data: [],
      success: function(result) {
        $.magnificPopup.open({
          items: {
            src: result['html']
          },
          type: 'inline'
        });
        return false;
      }
    });


  });

//  $('.review-btn').click(function(){
//    $('.review-placeholder-removeable').remove();
//    $('.no-reviews-placeholder').hide();
//    $('.review-panel').show();
//    $('.review-panel').effect("highlight");
//  });


$('#text-review-body').textcounter({
    min: 50,
    max: 2000000,
    countDownText: "Characters Left: ",
    minimumErrorText: "Review is too short!",
    counterErrorClass: "text-danger",
    countContainerClass:"p10",
    counterText: "Review Length: "
});



});

</script>
@stop