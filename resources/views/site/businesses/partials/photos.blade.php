<?php $total_photos = isset($total_photos)?$total_photos:5;?>
@if(count($photos) >0)
  <div class="  pt10 pb10   ">
    <h4 class="item-headers">PHOTOS</h4>
    <div class="photos-holder">
      @foreach($photos as $ii=>$one_photo)
        @if((count($photos)>($total_photos-1)) && ($ii >=(count($photos)-1)))
        @else
          <a href="{{ mzk_cloudfront_image($one_photo->image->url('large')) }}" class="image {{($ii+1)%(ceil($total_photos/2))==0?'':'mr5'}} {{ ($ii>=($total_photos-1))?'hide':''}}  mb12" style="display:inline-block"><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" data-sequence="{{($ii+1)}}" alt="{{ $business->name }} + Photo {{ $ii+1 }}" class="w144 h144 " /></a>
        @endif
      @endforeach
      @if(count($photos)>($total_photos))
        <a style="background-image:url( {{ mzk_cloudfront_image($one_photo->image->url('small')) }})" class="fs150 sq-card no-border-radius mb0 fw300 image btn btn-default p0 bg-lite-gray  w144 h144 " style="display:inline-block; vertical-align:middle; text-align:center" href="{{ mzk_cloudfront_image($one_photo->image->url('large')) }}">
          <div class="va-container va-container-v va-container-h overlap-transparent">
            <div class="va-middle text-center force-white" style="z-index:999;">
              +{{count($photos)-($total_photos-1)}}
            </div>
          </div>
        </a>
      @endif
    </div>
  </div>
@endif
