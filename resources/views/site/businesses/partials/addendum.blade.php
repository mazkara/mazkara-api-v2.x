<hr/>

<div class="bb b_g-white  gray pl0 pt0 p15 " >
  <h4 class="item-headers fs125 gray pt5 pt0 mt0">
    Salons and Spas in {{ $business->zone_cache }}
  </h4>
  <?php $categories = Category::byLocaleActive()->get();?>
  <span class="gray">
  @foreach($categories as $ii=>$category)
      <a class="gray hover-underline fs90" href="{{ MazkaraHelper::slugCityZone($business->zone, ['category'=>[$category->id]])}}">{{MazkaraHelper::getPluralName($category->name)}} in {{$business->zone_cache}}
    @if ($ii+1 < count($categories))
      {{ ', ' }}
    @endif
    </a>
  @endforeach
  </span>
</div>
<div class="b_g-white pl0 pt15 p15" >
  <h4 class="item-headers fs125 gray pt5 pt0 mt0">
    Salons and Spas around {{ $business->zone_cache }}
  </h4>
  <?php $categories = Category::byLocaleActive()->take(3)->get();?>

  <?php $zones = Zone::where('parent_id', '=', $business->zone->parent_id)->take(8)->get();?>
  @foreach($categories as $ii=>$category)
<span class="gray">
    @foreach($zones as $iz=>$_zone)
      <a class="gray hover-underline fs90" href="{{ MazkaraHelper::slugCityZone($_zone, ['category'=>[$category->id]])}}">{{MazkaraHelper::getPluralName($category->name)}} in {{$_zone->name}}
      @if ($iz+1 < count($zones))
        {{ ', ' }}
      @endif
      </a>            
    @endforeach
  </span>
  @endforeach
</div>
