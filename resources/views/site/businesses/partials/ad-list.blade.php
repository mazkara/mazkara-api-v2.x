@if(count($ads)>0)
<div >
  <small class=" pb10 ">SPONSORED</small>
  @foreach($ads as $ad)

      @if(isset($ad->photo_url))

<?php ob_start();?>
@if(strlen($ad->corner_text)>0)
<span style="position:absolute; top:5px; right:5px;">
<span class="color-{{$ad->corner_text_color}}">{{ ($ad->corner_text) }}</span>
</span>
@endif
<div class=" ad-content-holder" >
<div class="yellow ad-title" >
<div class="mt2 pt2 dpib" style="width:100%">
@if($ad->hasHeadline())
<div class="force-white pull-right   fs125"><b>{{$ad->headline}}</b></div>
@endif
{{$ad->caption}}

</div>
</div>
</div>

</div>
<div class="p5 ad-caption">
{{ strtoupper($ad->title)}} 
<?php $banners = ob_get_contents();ob_end_clean();?>


      <a href="{{ $ad->url }}" class="no-underline single-native-ctrable" data-ad="{{ $ad->id }}">

        <?php 
          $image = $ad->getPhotoUrl();
          if($image!=false){
            $image = mzk_cloudfront_image($image);
            $css = "background-size:100%;background-position:center top;position:relative;background-repeat:no-repeat;background-image:url(".$image.");";
          }else{
            $css = "background-color:black;position:relative;";
          }
        ?>


        <div class="box-lite-shadow hide-only-mobile mb10">
        <div class=" native-ad-v02 " style="height:142px;{{$css}}">
          {{ $banners }}
        </div>
      </div>
      <div class="box-lite-shadow show-only-mobile mb10">
        <div class=" native-ad-v02-mobile " style="height:202px;{{$css}}">
          {{ $banners }}
        </div>
      </div>

      </a>

    @endif
  @endforeach
</div>
@endif
