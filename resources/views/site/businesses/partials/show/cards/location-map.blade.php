<div class="  bbb bg-lite-gray text-center" >

  @if($business->isLocationSet())
    <div class="item-map-holder "><div id="item-map" class="ba item-map mr10 bbb" style="height:250px"></div></div>
  @endif
  <p class="p10">
    <b >{{ $business->getZoneName() }}</b> ›
    <span >{{ $business->getAddressAsString() }}</span>
  </p>

    @if(count($business->chain) > 0)
      @if(count($business->chain->businesses)>0)
        <p class="p10 pt0" style="margin-top:-10px;">
          <a href="{{ MazkaraHelper::slugCityChain(null, $business->chain)}}" class="text-success">
            {{ count($business->chain->businesses) - 1 }} more venues
          </a>
        </p>
      @endif
    @endif

</div>
