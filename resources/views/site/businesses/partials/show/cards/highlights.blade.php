@if(!isset($highlights))
  <?php $highlights = $business->highlights;?>
@endif
@if(count($highlights)>0)
  <div class="pt5 pb5  ">
      <ul class="list-unstyled list-inline">
      @foreach($highlights as $highlight)
        @if(MazkaraHelper::isActiveHighlight($highlight->id))
          <li class="mr15 pr15">
            <div class="text-center">
              <div style="font-size:300%;">
                <i class="highcon highcon-{{$highlight->id}}"></i></div>
              {{ ($highlight->name)}}
            </div>
          </li>
        @endif
      @endforeach
      @if($business->cost_estimate > 0)
          <?php $costOptions = Business::getCostOptions();?>

        <li class="mr15 pr15">
          <div class="text-center">
            <div style="font-size:300%;">
              <i class="{{mzk_icon_cost_estimate($business->cost_estimate)}}"></i></div>
              {{ ($costOptions[$business->cost_estimate])}}
          </div>
        </li>
      @endif


    </ul>
  </div>

@endif
