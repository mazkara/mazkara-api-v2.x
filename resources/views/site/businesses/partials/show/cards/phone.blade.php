<?php 
if(!isset($phones)):
  $phones = $business->displayablePhone();
endif;
?>

@if(is_array($phones))
  <div class="p5 pb5 mt10   text-center " >
    <p class="show-only-mobile mt20 mb20 ">&nbsp;</p>
    <span class="hoverable" >
      <div class="hide-on-hover pt20 pb20 btn btn-success desktop btn-lg no-border-radius call-button" data-business="{{$business->id}} "> CALL NOW</div>
      <div class="show-on-hover">
        @if(count($phones)>0)
          @foreach($phones as $phone)
            <p class="fs125 ">
              <span class="fa-stack fa">
                <i class="fa fa-circle fa-stack-2x fw300 fs150 green"></i>
                <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
              </span>        
              <span class="fw900">{{ ViewHelper::phone($phone) }}</span>
            </p>
          @endforeach
        @else
          <p class="fs125 ">
              <i class="fa fa-warning fa-inverse yellow"></i>
            <span class="fw900">No phone number available</span>
          </p>
        @endif

      <p class="text-success">Prior bookings are recommended</p>
        
      </div>
    </span>
  </div>
@endif
