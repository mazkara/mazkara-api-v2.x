@if($business->photos()->count() >0)
  <div class="  pt10 pb10 bbb ">
    <h4 class="item-headers">PHOTOS</h4>
    <div class="photos-holder">
      @foreach($business->photos()->get() as $ii=>$one_photo)
        <a href="{{ mzk_cloudfront_image($one_photo->image->url('large')) }}" class="image {{($ii+1)%2==0?'':'mr5'}} {{ ($ii>=7)?'hide':''}}  mb12" style="display:inline-block"><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" alt="{{ $business->name }} + Photo {{ $ii+1 }}" class="w150 h150 " /></a>
      @endforeach
      @if(count($business->photos)>3)
        <a style="background-image:url({{ $business->photos()->orderby('photos.id', 'desc')->first()->image ? mzk_cloudfront_image($business->photos()->orderby('photos.id', 'desc')->first()->image->url('small')):'' }})" class="fs150 sq-card no-border-radius mb0 fw300 image btn btn-default p0 bg-lite-gray  w150 h150 " style="display:inline-block; vertical-align:middle; text-align:center" href="{{ $one_photo->image->url('large') }}">
          <div class="va-container va-container-v va-container-h overlap-transparent">
            <div class="va-middle text-center force-white" style="z-index:999;">
          
          {{count($business->photos)-7}}<br/>
          more
        </div></div>
        </a>
      @endif
    </div>
  </div>
@endif
