<?php $services = $business->services()->orderby('parent_id', 'ASC')->get();?>
@if(count($services)>0)
  <div class="pt10 pb10 service-container">
    <h4 class="item-headers ">SERVICES</h4>
    <?php
      $current_heading = '';
    ?>
    @foreach($services as $ii=>$service)
      @if($service->parent_id != $current_heading)
        <?php $current_heading = $service->parent_id;?>
        <div style="" class="p5 {{ $ii>8 ? 'hidden':'' }} service-object bg-lite-gray">
          <b>{{ strtoupper(MazkaraHelper::getServicesAttribute($service->parent_id, 'name')) }}</b>
        </div>
      @endif
      <div style=" " class="bb {{ $ii>8 ? 'hidden':'' }} service-object p5">
        {{ strtoupper($service->name) }}
        <div class="pull-right">
          {{ $service->getStartingPrice(MazkaraHelper::getCitySlugFromID($business->city_id)) }}
        </div>
      </div>
    @endforeach
    <a id="lnk-to-display-all-services" href="javascript:void(0)" class="dpb mt10 ba pb10 pt10 text-center">VIEW ALL SERVICES</a>
  </div>
@endif

