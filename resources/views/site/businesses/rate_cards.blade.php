@extends('layouts.parallax')
@section('content')

<div>
@include('site.businesses.partials.cover-or-parallax', ['business'=>$business])


<div class="container mt5">
<div class="row"  >  
  <div class="col-md-9 bg-white pt20">
    <!-- End of Header -->
    <div class="row">
      <div class="col-md-12">
        <!--
        @include('site.businesses.partials.show.phone', ['business'=>$business])
        @include('site.businesses.partials.show.location-map', ['business'=>$business])
      -->
        @if(count($rateCards) >0)
          <div class="p10 pl0">
            <h4 class="item-headers">Rate Cards of {{$business->name}}</h4>
            <div class="rate-card-holder">
              @foreach($rateCards as $ii=>$one_photo)
                @if($one_photo->hasImage())
                  <a href="{{ mzk_cloudfront_image($one_photo->image->url('base')) }}" class="mr10 image mb15" style="display:inline-block"><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" data-sequence="{{($ii+1)}}" class=" " /></a>
                @endif
              @endforeach
            </div>  
          </div>
        @endif





      </div>
    </div>
  </div>
  <div class="col-md-3 pt20">
    @include('site.businesses.partials.ad-list')
    <small class=" pb10 mb5">FEATURED</small>
    @foreach($suggested_spas as $native_ad)
      @include('site.businesses.partials.business-niblet', ['business'=>$native_ad])
    @endforeach

  </div>
</div>
</div>
</div>
<?php ob_start();?>
@include('elements.report')
<?php $reportable = ob_get_contents(); ob_end_clean();?>
@section('js')
@include('site.businesses.partials.show.js', compact('reportable', 'business'))

@stop
@endsection