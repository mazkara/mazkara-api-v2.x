@extends('layouts.parallax')
@section('preheader')
  
@stop
@section('content')

<?php 

?>
<div>
@include('site.businesses.partials.cover-or-parallax', ['business'=>$business, 'phones'=>$phones])
<div class="bg-lite-gray hide-only-mobile">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        @include('site.businesses.partials.show.cards.highlights', ['business'=>$business, 'highlights'=>$highlights])
      </div>
      <div class="col-md-4">
        @include('site.businesses.partials.show.cards.phone', ['business'=>$business, 'phones'=>$phones])
      </div>
    </div>
  </div>
</div>

<div class="container">
<?php ob_start();?>
<div class="bg-lite-gray text-center" style="margin:-20px -20px 20px -20px;;" >
  @include('site.businesses.partials.show.cards.highlights', ['business'=>$business, 'highlights'=>$highlights])
</div>

  @include('site.businesses.partials.offers-blocks', ['business'=>$business, 'offers'=>$offers])        
  @include('site.businesses.partials.rate-cards-packages', ['business'=>$business, 'rateCards'=>$rateCards, 'total_photos'=>4])        
  @include('site.businesses.partials.photos', ['business'=>$business, 'photos'=>$photos, 'total_photos'=>4])        
<?php $mobile_page_images = ob_get_contents();ob_end_clean(); ?>
<?php ob_start();?>
  @include('site.businesses.partials.rate-cards-packages', ['business'=>$business, 'rateCards'=>$rateCards])        
  @include('site.businesses.partials.photos', ['business'=>$business, 'photos'=>$photos])
<?php $page_images = ob_get_contents();ob_end_clean(); ?>
<div class="row" >  
  <div class="col-md-8">
    <!-- End of Header -->
    <div class="row">
      <div class="col-md-12 pt20 bg-white ">
        <div class="show-only-mobile">
          {{$mobile_page_images}}
        </div>
        <div class="show-only-mobile">
        @if(!$business->isHomeService())
          @include('site.businesses.partials.show.location-map', ['business'=>$business])
          @if($business->isLocationSet())
            <div class="item-map-holder ">
              <a target-"_blank" href="https://www.google.com/maps/dir/Current+Location/{{$business->geolocation_latitude}},{{$business->geolocation_longitude}}" class="show-only-mobile">
                <img src="https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=350x275&maptype=roadmap
  &markers=color:red|{{$business->geolocation_latitude}},{{$business->geolocation_longitude}}" class="ba mr10 bbb" style="width:100%" />
              </a>
            </div>
          @endif
        @endif

        </div>
        @if($business->hasDescription())
        <div class="row ">
          
          <div class="col-md-12">

          <div class="pb15 ">
          <h4 class="item-headers ">ABOUT</h4>
            @if(strlen($business->description)>400)
              <span>{{ nl2br(substr($business->description, 0, 400)) }}</span><span class="hide-when-view-more-desc">... 
                <a class="lnk-view-more-desc pink" href="javascript:void(0)">
                  view more
                </a>
              </span><span style="display:none;" class="show-when-view-more-desc">{{ nl2br(substr($business->description, 400, strlen($business->description))) }}</span>
            @else
              {{ nl2br($business->description) }}
            @endif
          </div></div>
        </div>
        <div class="row ">
          <div class="col-md-12">
            <div class=" "></div>
          </div>
        </div>

        @endif

        <div class="hide-only-mobile ">
          @include('site.businesses.partials.offers', ['business'=>$business, 'offers'=>$offers])        
        </div>

        <div class="row ">

          <div class="col-md-6">

          </div>
          <div class="col-md-6">

          </div>
        </div>
        <div class="row ">
          <div class="col-md-12">
            <div class=" "></div>
          </div>
        </div>

        <div class="hide-only-mobile">
          {{$page_images}}
        </div>

        <div class=" pt10 pb10">
          @include('site.businesses.partials.show.services', ['business'=>$business] )
        </div>
        <div class=" pt10 pb10">
          @include('site.businesses.partials.reviews', ['business'=>$business, 'reviews'=>$reviews] )
        </div>
        <div class="pt10 pb10  ">
          <p>
            <a  href="{{MazkaraHelper::getClaimUrl($business)}}" class="text-danger">Claim your Business</a>
          </p>
          <p>
            <a href="javascript:void(0)" class="text-danger report-link" id="report-link">Report a Problem?</a>
          </p>
        </div>
        <div class=" pt10 pb10">
          @include('site.businesses.partials.addendum', ['business'=>$business] )
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        
      </div>
    </div>
  </div>
<div class="col-md-4 pt20">

    <div class="row">
      <div class="col-md-10 col-md-offset-1">
  <div class="hide-only-mobile">
    <div class="ba" style="background-color:#fff;">

        @if(!$business->isHomeService())
          @include('site.businesses.partials.show.cards.location-map', ['business'=>$business])
        @endif
        @include('site.businesses.partials.show.cards.timings', ['business'=>$business])

    </div>
  </div>

        <div class="mt15">
          @include('site.businesses.partials.client-dashboard-link', ['business'=>$business])        
        </div>
        @include('site.businesses.partials.ad-list')

        <div class="fw500 pb10 mb10">FEATURED</div>

        @foreach($suggested_spas as $_business)
        <?php $_business->is_favourited = in_array($_business->id, $favorited_suggested_spas) ? true : false;?>
        
          @include('site.businesses.partials.business-niblet', ['business'=>$_business])
        @endforeach

      </div>
    </div>

  </div>
</div>
</div>
</div>
<script src="http://maps.google.com/maps/api/js?language=en&libraries=places&sensor=true"></script>
<?php ob_start();?>
@include('elements.report')
<?php $reportable = ob_get_contents(); ob_end_clean();?>
@section('js')
@include('site.businesses.partials.show.js', compact('reportable', 'business'))
<script type="text/javascript">
  $(function(){
    $('.lnk-view-more-desc').click(function(){
      $('.lnk-view-more-desc').hide();
      $('.hide-when-view-more-desc').hide();
      $('.show-when-view-more-desc').show();
    })
  })
</script>
@stop

@stop
