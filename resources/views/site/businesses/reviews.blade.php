@extends('layouts.parallax')
@section('content')
<div>
@include('site.businesses.partials.cover-or-parallax', ['business'=>$business])


<div class="container mt5">
<div class="row" >  
  <div class="col-md-9 bg-white pt20">

    <!-- End of Header -->
    <div class="row">
      <div class="col-md-12">
        <!--
        @include('site.businesses.partials.show.phone', ['business'=>$business])
        @include('site.businesses.partials.show.location-map', ['business'=>$business])
      -->
          @include('site.businesses.partials.reviews')
          <div class="bbb p15"></div>

      </div>
    </div>
  </div>
  <div class="col-md-3 pt20">
    @include('site.businesses.partials.ad-list')
    <small class=" pb10 mb5">FEATURED</small>
    @foreach($suggested_spas as $native_ad)
      @include('site.businesses.partials.business-niblet', ['business'=>$native_ad])
    @endforeach

  </div>
</div>
</div>
</div>
<?php ob_start();?>
@include('elements.report')
<?php $reportable = ob_get_contents(); ob_end_clean();?>

@section('js')
@include('site.businesses.partials.show.js', compact('reportable', 'business'))
@stop