@extends('layouts.parallax')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-9 ">
      <center>
        <p class="p10 mb10">
          {{ViewHelper::userAvatar($user, ViewHelper::$avatar325Square)}}
        </p>
      </center>

      @if(!$user->isUserMeSignedIn() && $user->isUserBoring())
        <div class="p30 bg-lite-gray text-center">
          <div class="fs220"><span class="fs175"><i class="flaticon flaticon-plant59 "></i></span></div>
          <div class="">{{$user->full_name}}'s profile is dry as a desert!</div>
        </div>
      @endif





      @if($user->isContributorable())
        <?php $posts_count = $user->posts()->onlyPosts()->isViewable()->count();?>
        <div class="pt15 ">
          <p class="mt15 mb15">
            @if($posts_count>0)
              <h4 class="item-headers ">
                {{ strtoupper($user->full_name) }}'S STORIES({{$posts_count}})
                @if(Auth::check())
                  @if(Auth::user()->id == $user->id)
                    @if(Auth::user()->can('manage_posts') || Auth::user()->hasRole('admin'))
                      @if($posts_count>0)
                        <a href="{{route('posts.create')}}" class="mt10 fs75 fw300 pull-right">
                          <i class="fa fa-edit"></i>
                          Add a Story
                        </a>
                      @else
                      @endif
                    @endif
                  @endif
                @endif

              </h4>
            @endif


          </p>
          <div class="clearfix"></div>

            @if(Auth::check())
              @if(Auth::user()->id == $user->id)
                @if(Auth::user()->can('manage_posts') || Auth::user()->hasRole('admin'))
                  @if($posts_count==0)
                    <a style="clear:both" class="p30 mb15 dpb hover-bg-yellow text-center bg-lite-gray" href="{{ route('posts.create') }}" >
                      <div class="fs220"><i class="fa fa-edit "></i></div>
                      <div class="">Add Your First Story</div>
                    </a>
                  @endif
                @endif
              @endif
            @endif

          <div class="row">
            @foreach($user->posts()->onlyPosts()->isViewable()->take(4)->get() as $post)
              <div class="col-md-3">
                @include('site.posts.partials.minimal')
              </div>
            @endforeach
          </div>
        </div>
        @if($user->posts()->onlyPosts()->isViewable()->get()->count() > 4)
          <p class="pb10 text-right">
            <small><a href="{{ route('posts.index.all', ['author'=>$user->id])}}">See all {{$user->posts()->onlyPosts()->isViewable()->count()}} stories</a></small>
          </p>
        @endif
      @endif


      <div class="pt15 ">
        <p class="mt15 mb15">
          <?php $selfies_count = $user->posts()->onlySelfies()->isViewable()->count();?>
          @if($selfies_count > 0)
            <h4 class="item-headers  ">
              {{ strtoupper($user->full_name) }}'S PHOTO'S({{ $selfies_count }})
              @if(Auth::check())
                @if(Auth::user()->id == $user->id)
                  @if($selfies_count>0)
                    <a href="{{route('selfies.create')}}" class="mt10 fs75 fw300 pull-right">
                      <i class="fa fa-camera"></i>
                      Add a Selfie
                    </a>
                  @endif
                @endif
              @endif

            </h4>
          @endif

        </p>
        <div class="clearfix"></div>

          @if(Auth::check())
            @if(Auth::user()->id == $user->id)
              @if($selfies_count==0)
                <a style="clear:both" class="p30 mb15 dpb hover-bg-yellow text-center bg-lite-gray" href="{{route('selfies.create')}}" >
                  <div class="fs220"><i class="fa fa-camera "></i></div>
                  <div class="">Add Your First Photo</div>
                </a>
              @endif
            @endif
          @endif

        <div class="row">
          @foreach($user->posts()->onlySelfies()->isViewable()->take(4)->get() as $post)
            <div class="col-md-3">
              @include('site.posts.partials.selfie')
            </div>
          @endforeach
        </div>
      </div>
      @if($user->posts()->onlySelfies()->isViewable()->get()->count() > 4)

      <p class="pb10 text-right">
        <small>
          <a href="{{ route('users.photos.show', $user->id)}}">
            See all {{$user->posts()->onlySelfies()->isViewable()->count()}} pictures
          </a>
        </small>
      </p>
      @endif

      @if($user->isContributorable())
        <div class="pt15 ">
          <p class="mt15 mb15">
            <?php $videos_count = $user->posts()->onlyVideos()->isViewable()->count();?>
            @if($videos_count>0)
              <h4 class="item-headers">
                {{ strtoupper($user->full_name) }}'S VIDEOS({{$videos_count}})
    
                @if(Auth::check())
                  @if(Auth::user()->id == $user->id)
                    @if(Auth::user()->can('manage_posts') || Auth::user()->hasRole('admin'))
                      @if($videos_count>0)
                        <a href="{{ route('videos.create') }}" class="mt10 fs75 fw300 pull-right">
                          <i class="fa fa-video-camera"></i>
                          Add a Video
                        </a>
                      @endif
                    @endif
                  @endif
                @endif


              </h4>
            @endif

          </p>
          <div class="clearfix"></div>
          @if(Auth::check())
            @if(Auth::user()->id == $user->id)
              @if(Auth::user()->can('manage_posts') || Auth::user()->hasRole('admin'))
                @if($videos_count==0)
                  <a style="clear:both" class="p30 mb15 dpb hover-bg-yellow text-center bg-lite-gray" href="{{ route('videos.create') }}" >
                    <div class="fs220"><i class="fa fa-video-camera "></i></div>
                    <div class="">Add Your First Video</div>
                  </a>
                @endif
              @endif
            @endif
          @endif

          <div class="row">
            @foreach(Video::ofAuthors([$user->id])->isViewable()->take(4)->get() as $post)
              <div class="col-md-3">

                @include('site.posts.partials.video')
              </div>
            @endforeach
          </div>
        </div>
        @if($user->posts()->onlyVideos()->isViewable()->get()->count() > 4)
          <p class="pb10 text-right">
            <small>
              <a href="{{ route('users.videos.show', $user->id)}}">
                See all {{$user->posts()->onlyVideos()->isViewable()->count()}} videos
              </a>
            </small>
          </p>
        @endif
      @endif


      @if(Auth::check() && (Auth::user()->id == $user->id))
        @if($user->followsBusinesses()->count()>0)
        <div class="pt15 ">
          <p class="mt15 mb15">
            <h4 class="item-headers ">
              {{ strtoupper($user->full_name) }}'S SHORTLIST({{$user->followsBusinesses()->get()->count()}})
            </h4>
          </p>
          <div class="row">
              @foreach($user->followsBusinesses()->get() as $ii=>$favourite)
              <?php $business = $favourite->favorable;?>
              <div class="col-md-3 {{ $ii > 7 ? 'hidden followed-businesses' : '' }}">
                @include('site.posts.partials.business')
              </div>
            @endforeach
          </div>
          @if($ii > 7)
            <p class="pb10 text-right">
              <small>
                <a href="javascript:void(0)" class="link-display-followed-businesses">
                  See all {{($ii+1)}} Shortlisted
                </a>
              </small>
            </p>
          @endif
        </div>
        @endif
      @endif
      @if($user->completedReviews()->count() >0)
      <div class="pt15 ">
        <p class="mt15 mb15">
          <h4 class="item-headers ">
            {{ strtoupper($user->full_name) }}'S REVIEWS({{$user->completedReviews()->count()}})
          </h4>
        </p>
        <div class="row">
          @foreach($user->completedReviews as $ii=>$review)
            <?php $business = $review->business;?>
            @if($business)
              <div class="col-md-12">
                @include('site.users.partials.review-no-favorite', ['review'=>$review, 'business'=>$business])
              </div>
            @endif

          @endforeach
        </div>
      </div>
      @endif
    </div>

  <div class="col-md-3 pt20">
    <div class="p10 bg-lite-gray">
      <h2 class="item-name">
        {{ $user->name }}
      </h2>
      @if($user->isContributorable() == true)
        <p class="">
          {{ $user->authors_designation }}
        </p>
      @endif
      @if($user->hasLocation())
        <p>
          Location: {{ $user->users_location }}
        </p>
      @endif

      @if(Auth::check())
        @if($user->id != Auth::user()->id)
          <a href="javascript:void(0)" class="btn no-border-radius btn-default mt5 user-follow {{ $user->isFollowed(Auth::user()->id) ? 'favourited' : 'favourite' }} btn-default small-text" title="{{ ($user->isFollowed(Auth::user()->id)? 'Following' : 'Follow ') }} {{$user->full_name}}"  
            data-toggle="tooltip" data-placement="left"  data-route="users" rel="{{$user->id}}">
            <span class="show-when-followed">FOLLOWING</span>
            <span class="hide-when-followed">FOLLOW</span>
          </a>
        @endif
      @else
        <a href="/users/create" id="ajax-login-link" class=" ajax-popup-link btn no-border-radius btn-default mt5 user-follow btn-default small-text" title="Sign in to follow {{$user->full_name}}"  data-toggle="tooltip" data-placement="left"  data-route="users">
          <i class="fa fa-user-plus"></i> 
            FOLLOW
        </a>
      @endif


      @if(Auth::check())
        @if($user->id != Auth::user()->id)
        @else
          <a href="/users/edit" class="btn btn-small btn-default mt5 no-border-radius small-text">
            <i class="fa fa-cog"></i> SETTINGS
          </a>
        @endif
        @if(Auth::user()->hasRole('admin'))
          <a href="/content/accounts/{{$user->id}}" class="btn mt5 no-border-radius btn-small btn-default small-text">
            <i class="fa fa-shield"></i> EDIT ADMIN
          </a>
        @endif
      @endif

      <div class="text-danger pt10 pb10 ">{{ $user->roleLabel() }}</div>

      <div class="row ">
        <div class="col-md-6 fs90 fw300 ">
          <p>
            <i class="fa fa-user fs125 medium-gray"></i> {{ $user->followers()->count() }} Follower(s)
          </p>
          <p>
            <i class="fa fa-camera fs125 medium-gray"></i> {{ $user->posts()->onlySelfies()->count() }} Photo(s)
          </p>
          <p>
            <i class="fa fa-pencil-square-o fs125 medium-gray"></i> {{ $user->completedReviews()->count() }} Review(s)
          </p>
        </div>
        <div class="col-md-6 fs90 fw300">
          <p>
            <i class="fa fa-file fs125 medium-gray"></i> {{ $user->posts()->count() }} Storie(s)
          </p>
          <p>
            <i class="fa fa-comment-o fs125 medium-gray"></i> {{ $user->comments()->count() }} Comment(s)
          </p>
        </div>
      </div>

      @if($user->hasAbout())
        <p>
          <b>ABOUT ME</b>
        </p>
        <p>
          {{ $user->about }}
        </p>
      @endif
    </div>

    @if($user->hasContactDetails())
      <div class=" pt10 pb10 "><b>CONTACT</b></div>
      <div class="row ">
        <div class="col-md-12 ">
          @if(!empty($user->phone))
            <p><i class="fa fa-phone"></i> {{ $user->phone }}</p>
          @endif

          @if(!empty($user->contact_email_address))
            <p><i class="fa fa-envelope"></i> <a href="mailto:{{ $user->contact_email_address }}">{{ $user->contact_email_address }}</a></p>
          @endif

          @if(!empty($user->twitter))
            <p><i class="fa fa-twitter"></i> <a href="http://www.twitter.com/{{ $user->twitter }}"> {{ '@'.$user->twitter }}</a></p>
          @endif

          @if(!empty($user->instagram))
            <p><i class="fa fa-instagram"></i> <a href="http://www.instagram.com/{{ $user->instagram }}">/{{ $user->instagram }}</a></p>
          @endif
        </div>
      </div>
    @endif



  </div>
  </div>
</div>
@stop
@section('js')

<script type="text/javascript">
$(function(){
  $('.show-hidden').click(function(){
    p = $(this).parents('.panel-body').first();
    console.log(p);
    $(p).find('.hidden').removeClass('hidden');
    $(this).hide();
  })

  $('.link-display-followed-businesses').click(function(){
    $('.followed-businesses').removeClass('hidden');
    $(this).hide();
  });

  $('.jscroll').jscroll({
      padding: 20,
      loadingHtml: '<p><b>Loading more...</b></p>',

      nextSelector: '.pagination a[rel=next]',
      contentSelector: '.single-activity-element'
  });
  
  $(".popover-mobile").popover({
        container: 'body',
        html: true,
        content: function () {
            p = $(this).data('phone').split('|');
            html = '';
            for(i in p){
              html = html + '<div><a class="btn btn-default dpb text-center" href="tel:'+p[i]+'">'+p[i]+'</a></div>';
            }
            return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });


  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',

      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });



})
</script>
@stop