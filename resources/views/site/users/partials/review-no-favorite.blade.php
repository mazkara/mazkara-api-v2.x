<div class="media pb5 ">
  <div class="pull-right">
  <p class="gray">{{{ Date::parse($review->updated_at)->ago() }}}
  </p>


  </div>
  <div class="media-left">
    <a href="{{MazkaraHelper::slugSingle($business)}}">
          {{ViewHelper::businessThumbnail($business, ['width'=>'40', 'height'=>'40', 'meta'=>'small', 'class'=>'img-rounded', 
                                'style'=>'width:40px; height:40px; padding-top:3px;'])}}
    </a>
  </div>
  <div class="media-body">
    <b class="media-heading   pt5">
      <a title="{{{ $business->name }}}"  href="{{MazkaraHelper::slugSingle($business)}}" class="ovf-hidden  result-title">
        {{{ $business->name }}} 
      </a>
    </b><br/>
    <small title="{{ $business->geolocation_address }}" class="search-item-address">
      {{ $business->zone_cache }}
    </small><br/>

  </div>
</div>
<div class="media pb10 pt0 mb10 mt0 bb" >
  <div>
        <b>
<span class="dpib"><span class="dpib">RATED:</span> {{ ViewHelper::starRateSmallBasic($review, 'xs')}}</span>       </b> 
      @if(count($review->services)>0)
        <b>for 
        {{ join(', ',$review->services()->lists('name', 'name')->all())}}</b><br/>
      @endif

    {{ nl2br($review->body) }}
  </div>
</div>