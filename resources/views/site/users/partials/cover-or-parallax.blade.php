@if(count($user->avatar)>0)
<?php  $url = $user->avatar->image->url('xlarge');?>
  <div class="text-center blur" style="position:absolute; height:350px;top:45px;width:100%;z-index:0;background:url({{$url}}) no-repeat fixed center center / cover rgba(200, 82, 126, 0)"></div>
  <div class="text-center overlap-transparent" style="position:absolute; height:350px;top:45px;width:100%;z-index:0;"></div>
  <div class="container" style="z-index:100;margin-bottom: 20px;">
    <div class="" style="height:273px;;">
    <div class="row">
      <div class="col-md-12 center-on-mobile text-center" >
        <div class=" force-white mt50" >


          <div class="row mt25">
            <div class="col-md-12">
              <div class="header ">
          {{ViewHelper::userAvatar($user, ViewHelper::$avatar80)}}

                <h1 class="item-name fs500 fw300">
                    {{ $user->name }}
                </h1>
              </div>
            </div>
          </div>



        </div>
      </div>
    </div>
  </div>
</div>
@else
  <div class="text-center" style="position:absolute; height:350px;top:45px;width:100%;z-index:0;background-color:{{ViewHelper::genColorCodeFromText($user->name.$user->id)}}"></div>
  <div class="container">
  <div class="" style="height:290px;">
    <div class="row">
      <div class="col-md-12 center-on-mobile text-center" >
        <div class=" force-white mt50" >


          <div class="row mt25">
            <div class="col-md-12">
              <div class="header ">
                <h1 class="item-name fs500 fw300" >
                    {{ $user->name }}
                </h1>
              </div>
            </div>
          </div>



        </div>
      </div>
    </div>
  </div>
</div>

@endif
