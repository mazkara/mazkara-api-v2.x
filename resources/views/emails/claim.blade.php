<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Claim for Business ID {{ $data['business_id']}} from Mazkara</h2>
    <p>Sent from Mazkara</p>
    <div>
      Name: {{ $data['name']}}<br/>
      Email: {{ $data['email']}}<br/>
      Phone: {{ $data['phone']}}<br/>
      BusinessID: {{ $data['business_id']}}<br/>
      Business Name: <a href="{{MazkaraHelper::slugSingle($data['business'])}}">{{ $data['business']->name}}</a><br/>
    </div>

    <hr/>

  </body>
</html>
