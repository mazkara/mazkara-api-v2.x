<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <p>Hello,</p>
    <div>
      {{ $job_details }}
      <p>
        @if(count($downloaded_calls)>0)
          These were downloaded :)<br/>
          <table>
          @foreach($downloaded_calls as $ii=>$vv)
            <tr>
              <td>{{ $ii }}</td>
              <td>{{ $vv }}</td>
            </tr>
          @endforeach
          </table>
        @endif
      </p>
    </div>
    <hr/>
    <p style="color:red;">
      @if(count($failed_downloads)>0)
        These could not be downloaded ;(<br/>
        <table>
        @foreach($failed_downloads as $ii=>$vv)
          <tr>
            <td>{{ $ii }}</td>
            <td>{{ $vv }}</td>
          </tr>
        @endforeach
        </table>
      @endif
    </p>
    <p>Have a nice day</p>
  </body>
</html>
