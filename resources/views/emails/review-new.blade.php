<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>
      {{$data['user']['name']}} has posted a review about 
      <a href="{{MazkaraHelper::slugSingle($data['business'])}}">
        {{ $data['business']->name}}
      </a>
    </h2>
    <div>
      @foreach($data['review'] as $ii=>$vv)
        @if(!is_array($vv))
          <b>{{ ucwords(str_replace('_', ' ', $ii)) }}</b>:
          {{ $vv }}<br/>
        @endif
      @endforeach

    </div>

    <hr/>

  </body>
</html>
