<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <p>Hello,</p>
    <p>You've just recieved a call from mazkara.</p>
    <div>
      {{ $data['business_name']}}<br/>
      Call From: {{ $data['caller_number']}}<br/>
      Date: {{ $data['dated']}} {{ $data['timed']}}<br/>
      Duration: {{ $data['caller_duration']}} seconds<br/>
    </div>
    Please login to your account at www.mazkara.com/partner/{{ $data['business_id']}} to view logged calls.
    <p>Have a nice day</p>
  </body>
</html>
