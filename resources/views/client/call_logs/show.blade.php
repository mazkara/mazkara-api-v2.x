@extends('layouts.client')
@section('content')

<h1>Show Call log</h1>



	<table class="table table-striped">
		<thead>
			<tr>
				<th>Date</th>
				<th>Time</th>
				<th>Caller </th>
			</tr>
		</thead>

		<tbody>
				<tr>
					<td>{{{ $call_log->dated }}}</td>
					<td>{{{ $call_log->timed }}}</td>
					<td>{{{ $call_log->caller_number }}}</td>
				</tr>
			</tbody>
		</table>
	<div>
	  @if(strlen($call_log->media_url))
	     <audio controls>
	    <source src="{{ $call_log->media_url }}" type="audio/ogg">
	    <source src="{{ $call_log->media_url }}" type="audio/mpeg">
	  Your browser does not support the audio element.
	    </audio>
	    @endif
	</div>

  @if($call_log->isPotentialLead())
    <i class="fa fa-money" title="This call is a potential lead and is billable."></i>
  @endif
  {{ mzk_call_logs_state_helper($call_log->state) }}
  @if($call_log->isDisputable())
    <a href="javascript:void(0)" class="btn lnk-to-dispute btn-default btn-lg" data-call="{{ $call_log->id }}">DISPUTE</a>
  @endif
  @if($call_log->canBeResolvedByUser())
    @if($call_log->isResolvable())
      <a href="javascript:void(0)" class="btn lnk-to-resolve btn-default btn-lg" data-call="{{ $call_log->id }}">RESOLVE</a>
    @endif
  @endif

<div>
@foreach($call_log->comments as $comment)
<div class="well bg-white bb">
  <div class="p5">
    {{ $comment->body }}
  </div>
  <div class="pull-right">
    {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.comments.destroy', $comment->id))) }}
      {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs')) }}
  {{ Form::close() }}
  </div>
  <div class="p5 pt0">
    @if($comment->type == Comment::CALL_LOG_DISPUTE)
        <span class="label label-danger">DISPUTE</span>
    @elseif($comment->type == Comment::CALL_LOG_FOLLOWUP)
        <span class="label label-info">FOLLOW UP</span>
    @endif
    - <small>
    <b>{{ $comment->user->full_name }}</b> posted {{{ Date::parse($comment->updated_at)->ago() }}}</small>
  </div>
</div>
@endforeach


</div>

@stop
