@extends('layouts.client')
@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="portlet portlet-default">
      <div class="portlet-header">
        <h1 class=" portlet-title">Analytics</h1>
      </div>
    </div>
  </div>
    <!-- /.col-lg-12 -->
</div>

<!-- /.row -->
<div class="row">
  <div class="col-md-12">
  <div class="row">
    <div class="col-md-12">
      <div class="portlet-header">
        <h3>Page views for {{$business->name}}</h3>
      </div>
    </div>
  </div>
    <div class="row">
      <div class="col-md-8">
        <div id="reports-line-chart" class="chart-holder-300"></div>
      </div>
      <div class="col-md-4">
    <div class="portlet portlet-boxed">
      <div class="portlet-header">
        <h4>Filter Graph</h4>
      </div>
      <div class="portlet-body">
        <form id="views-graph-date-range-picker">
          <div class="input-daterange  " id="datepicker">
              <input type="text" placeholder="Select Start Date" value="{{date('Y-m-d', strtotime($start_date))}}" class="input-sm form-control" id="start-date" name="start" />
              <br/>
              <input type="text" placeholder="Select End Date" value="{{date('Y-m-d', strtotime($end_date))}}" class="input-sm form-control" id="end-date" name="end" />
          </div>            <br/>
          <p>
            <a href="javascript:void(0)" id="apply-to-graph" class="btn-block btn btn btn-primary">Apply</a>
          </p>
        </form>
      </div>
    </div>

      </div>
    </div>
    <hr/>
    <div class="row">
      <div class="col-md-4 text-center"><div class="row-stat"><h2  id="count-page-views" >{{$count_page_views}}</h2> <small class="text-muted">Total Page Views</small></div></div>
      <div class="col-md-4 text-center"><div class="row-stat"><h2>{{$count_favorites}}</h2> <small class="text-muted">Total Favorites</small></div></div>
    </div>
</div>
</div>
<hr />
<div class="row">
  <div class="col-md-12">
  <div class="row">
    <div class="col-md-12">
      <div class="portlet-header">
        <h3>Call to Book Impressions {{$business->name}}</h3>
      </div>
    </div>
  </div>
    <div class="row">
      <div class="col-md-3 text-center">
        <div class="row-stat"><h2  id="count-call-views" >{{$count_call_views}}</h2> <small class="text-muted">Total Call to Book Views</small>
        </div>

      </div>
      <div class="col-md-9">
    <div class="portlet portlet-boxed">
      <div class="portlet-header">
        <b>Call to Book Clicks Between</b>
      </div>
      <div class="portlet-body">
        <form id="views-graph-date-range-picker">
          <div class="input-daterange  " id="datepicker">
            <div class="row">
              <div class="col-md-5">
              <input type="text" placeholder="Select Start Date" value="{{date('Y-m-d', strtotime($start_date))}}" class="input-sm form-control" id="call-start-date" name="call-start" />
            </div>
            <div class="col-md-5">
              <input type="text" placeholder="Select End Date" value="{{date('Y-m-d', strtotime($end_date))}}" class="input-sm form-control" id="call-end-date" name="call-end" />
            </div>
            <div class="col-md-2">
            <a href="javascript:void(0)" id="apply-to-calls" class="btn-block btn btn-sm btn btn-primary">Apply</a>
</div>
            </div>
          </div>           
        </form>
      </div>
    </div>

      </div>
    </div>
</div>
</div>

<script type="text/javascript">

$(function () {

  function callbackViewsAPI(){
    var d1, data, chartOptions;



    $.ajax({
      url: '/partner/{{ mzk_client_bid() }}/api/views',
      minLength: 0,
      data: {
        start: $('#start-date').val(),
        end: $('#end-date').val(),
        business_id: {{ $business->id }}
      },
    }).success(function(r){
      data = [{ 
        label: "Visits", data: r.data
      }];

      chartOptions = {
        xaxis: {
          min: r.start, max: r.end, mode: "time", tickSize: [1, "day"]
        },
        series: {
          lines: {
            show: true, fill: false, lineWidth: 3
          },
          points: {
            show: true, radius: 3, fill: true, fillColor: "#ffffff", lineWidth: 2
          }
        },
        grid: { 
          hoverable: true, clickable: false, borderWidth: 0 
        },
        legend: {
          show: true
        },
        tooltip: true,
        tooltipOpts: {
          content: '%s: %y'
        },
        colors: mvpready_core.layoutColors
      };

      var holder = $('#reports-line-chart');

      if (holder.length) {
        $.plot(holder, data, chartOptions );
      }

      $('#count-page-views').html(r['count_page_views']);

    });
  }

  $('#views-graph-date-range-picker .input-daterange').datepicker({
    format: "yyyy-mm-dd"
  });    

  $('#apply-to-graph').click(function(){

    callbackViewsAPI();
  })

  function callbackCallsApi(){
    $.ajax({
      url: '/partner/{{ mzk_client_bid() }}/api/calls',
      minLength: 0,
      data: {
        start: $('#call-start-date').val(),
        end: $('#call-end-date').val(),
        business_id: {{ $business->id }}
      },
    }).success(function(r){
      $('#count-call-views').html(r['count_call_views']);

    });

  }

  $('#apply-to-calls').click(function(){
  callbackCallsApi();
  })

  callbackViewsAPI();
  callbackCallsApi();





  function callbackCallLogsAPI(){
    var d1, data, chartOptions;

    $.ajax({
      url: '/partner/{{ mzk_client_bid() }}/api/call_logs',
      minLength: 0,
      data: {
        start: $('#calls-start-date').val(),
        end: $('#calls-end-date').val(),
        business_id: {{ $business->id }}
      },
    }).success(function(r){
      data = [{ 
        label: "Calls Made", data: r.data
      }];

      chartOptions = {
        xaxis: {
          min: r.start, max: r.end, mode: "time", tickSize: [1, "day"]
        },
        series: {
          lines: {
            show: true, fill: false, lineWidth: 3
          },
          points: {
            show: true, radius: 3, fill: true, fillColor: "#ffffff", lineWidth: 2
          }
        },
        grid: { 
          hoverable: true, clickable: false, borderWidth: 0 
        },
        legend: {
          show: true
        },
        tooltip: true,
        tooltipOpts: {
          content: '%s: %y'
        },
        colors: mvpready_core.layoutColors
      };    

      var holder = $('#calls-line-chart');

      if (holder.length) {
        $.plot(holder, data, chartOptions );
      }

    });
  }

  $('#views-calls-date-range-picker .input-daterange').datepicker({
        format: "yyyy-mm-dd"
  });    

  $('#apply-to-calls-graph').click(function(){

      callbackCallLogsAPI();
    })

      //callbackCallLogsAPI();




});

</script>
@stop

