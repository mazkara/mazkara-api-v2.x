<div class="bg-white  col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 p0">                    
  <div class="bg-green text-center p10 mt0">
    <h4 class="mb0 white">SUCCESS</h4>
  </div>
  <div class="p5 mb10 mt10">
    <div class="text-center pt10 pb10">
      YOUR VOUCHER HAS BEEN REDEEMED SUCCESSFULLY
    </div>
    <div class="text-center pt10 pb10">
      <a href="javascript:void(0)" class="btn btn-lite-gray no-border-radius p10 text-center btn-validate-another-voucher">VALIDATE ANOTHER VOUCHER</a>
    </div>

  </div>
</div>
<script type="text/javascript">
$(function(){
  $('.btn-validate-another-voucher').click(function(){
    $.ajax({
      type: "GET",
      url: '{{ route('client.voucher.validate.form', [mzk_client_bid()] ) }}',
      data: data,
      success: function(result) {
        $.magnificPopup.instance.items[0] = { src: result, type: 'inline' };
        $.magnificPopup.instance.updateItemHTML();
        return false;
      }
    });

    return false;
  });
})
</script>

