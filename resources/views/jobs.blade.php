@extends('layouts.page')
@section('content')

<div id="tf-home" class="text-center" style="position:absolute; height:350px;top:45px;width:100%;z-index:0;background:url({{mzk_assets('assets/careers-2.jpg')}}) no-repeat fixed center center / cover rgba(200, 82, 126, 0)">

</div>
    <div class="text-center" style="height:345px;">
      <div class="content " >
<div class="container " style="margin-top:40px;">
        <div class="fs150 mt25 text-right pull-right text-shadow" style="width:400px;">
        <span class="force-white">Mazkara is a glam-tech startup aiming to be the </span>
        <span class="yellow">world's leading online</span><span class="force-white"> engagement platform for 
        the personal grooming industry.</span>
        </div>
      </div></div></div>

<div class="container">
    <div class="row">
        <div class="col-md-12 bg-white">
            <h1 class="text-center fw300 notransform">We're Hiring</h1>
            <p class="lead text-center">
              <b>Are you Mazkara fabulous!</b>
            </p>
            <p class="">We're looking for top talent to join our expanding squad of Mazkarians, aiming to bring a revolution in the beauty and wellness industry. Through slick technology and a fabulous idea coupled with amazing people, Mazkara is out to change the world of fab!</p>
            <p class="">We're currently on the lookout for super top talent in Sales, PR, Marketing, Mobile and Application development - heck if you have a knack for anything that's beauty related we might have a place for you yet. If you have what it takes - send us your resume with a short tweet on why should we hire you?</p>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12 bg-white">

    <div class="row">
        <div class="col-md-3 ">
   <?php
    $pune = ['Senior iOS Developer ','Android Developer ','Software Engineer ','Front End Developer ','Senior Product Manager ',
    'Digital Marketing Manager ','HR & Recruiter ','Finance Controller ','Sales Manager ','Content Associate '];
    $dxb = ['Community Manager',
    'Sales Manager'];

    ?>

        <div role="tabpanel ">
            <h3>City </h3>
          <ul id="jobs-tab-list" class="mt10  nav nav-tabs nav-stacked" role="tablist">
    @foreach($cities as $ii=>$vv)
            <li role="presentation">
              <a href="#jobs-for-{{$ii}}" aria-controls="jobs-for-{{$vv}}" role="tab" data-toggle="tab">
                {{$vv}}
                <span class="pull-right"><i class="fa fa-chevron-circle-right"></i></span>
              </a>
            </li>
    @endforeach


        </ul>
    </div>




        </div>
        <div class="col-md-8 bl">
        <div role="tabpanel">
          <div class="tab-content">


    @foreach($cities as $ii=>$vv)
        <div role="tabpanel" class="tab-pane {{$ii != 'dubai' ? '':'active'}}  pb5"  id="jobs-for-{{$ii}}">
            <h3>Positions in {{$vv}} </h3>
        <table class="table">
        @foreach($jobs as $job)
            @if($job->location == $ii)
            <tr>
                <td >
                    <a href="/jobs/{{$job->id}}">
                        <span >{{$job->title}}</span> 
                    </a>
                    <a data-toggle="popover" title="Apply as {{$vv}}" data-content="Email your resume to hello@mazkara.com" class="pull-right btn btn-xs btn-lite-blue" href="mailto:hello@mazkara.com&Subject={{$vv}}+in+Pune">Apply Now</a></p>
                </td>
            </tr>
            @endif
        @endforeach
    </table>
</div>
    @endforeach
</div></div>

            <p class="text-left fs125">
                Email us on:
                <a data-toggle="popover" title="Apply as {{$job->title}}" data-content="Email your resume to hello@mazkara.com" class="  " href="mailto:jobs@mazkara.com&Subject={{$job->title}}+in+{{$cities[$job->location]}}">jobs@mazkara.com</a>
            </p>

        </div>
    </div>

</div></div></div>
<script type="text/javascript">
$(function(){

    $(function () {
      $('[data-toggle="popover"]').popover({trigger:'hover', placement:'left'});
    })    
})
</script>

@stop
