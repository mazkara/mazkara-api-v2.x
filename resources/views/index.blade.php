@extends('layouts.home')
@section('content')
<div class="tf-section bg-lite-gray bbb pt0 ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-left">
          <div style="font-weight:500;" class=" fs175 pt20  mb15">POPULAR SERVICES</div>
       </div>
    </div>
    <div class="row">
      <?php 
      $cards = [
        [
          'name'=>'Blow Dry',
          'image'=>(mzk_assets('assets/home/blow-dry.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[22]])
        ],
        [
          'name'=>'Body Scrub',
          'image'=>(mzk_assets('assets/home/body-scrub.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[59]])
        ],
        
        [
          'name'=>'Yoga',
          'image'=>(mzk_assets('assets/home/yoga.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[87]])
        ],
        
        [
          'name'=>'Manicure',
          'image'=>(mzk_assets('assets/home/manicure.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[47]])
        ],

        [
          'name'=>'Waxing',
          'image'=>(mzk_assets('assets/home/waxing.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[35]])
        ],

        [
          'name'=>'Shaving',
          'image'=>(mzk_assets('assets/home/shaving.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[62]])
        ],

        [
          'name'=>'Deep Tissue Massage',
          'image'=>(mzk_assets('assets/home/deep-tissue.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[38]])
        ],

        [
          'name'=>'Microdermabrasion',
          'image'=>(mzk_assets('assets/home/microderm.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[20]])
        ]
      ];
      ?>
      @foreach($cards as $vv)
      <div class="col-md-3">
        @include('elements.card', ['card'=>$vv])
      </div>
      @endforeach
    </div>
  </div>
</div>

<div class="tf-section bg-white pt15 pb0 mb0">
  <div class="container">

    <div class="row">
      <div class="col-md-12 text-left">
        <div style="font-weight:500;" class="mt0 fs175 pt0  mb15">LATEST STORIES</div>
      </div>
    </div>

    <div class="row">
      @foreach($posts as $post)
        <div class="col-md-20">
          @include('site.posts.partials.minimal')
        </div>
      @endforeach
    </div>
    <div class="row">
      <div class="col-md-12">
        <p class="text-right mt10 pt10"><a class="hover-underline" href="{{route('posts.index.all')}}">See all Stories</a></p>
      </div>
    </div>
    
  </div>
</div>


<div class="tf-section pt0 bg-white mt0   pb0">
  <div class="container">

    <div class="row">
      <div class="col-md-12 text-left">
        <div style="font-weight:500;" class="mt0 fs175 pt0  mb15">FEATURED VENUES</div>
      </div>
    </div>

    <div class="row">
      @foreach($featured_venues as $business)
        <div class="col-md-20">
          @include('site.posts.partials.business')
        </div>
      @endforeach
    </div>
    
  </div>
</div>

<div class="tf-section h300 pt15  show-only-mobile" style="background-position: left; background-image:url({{mzk_assets('assets/home/pinkified.jpg')}});">
  <div class="container">

    <div class="row">
      <div class="col-md-6 col-md-offset-6 text-right force-white">
        <div style="font-weight:500;" class="mt0 text-center fs125 pt20 mt15 pb10 mb25">TIRED OF PAYING FULL PRICE FOR
          <p>EVERYDAY BEAUTY SERVICES?</p>
          <a href="{{ MazkaraHelper::slugCity(null, ['specials'=>true]) }}" class="btn pt10 pb10 pl15 pl15 mb15 mt10 force-white btn-default fs125 no-border-radius" style="border-color:#fff;background-color:transparent;">
            CLICK NOW
          </a>
          <p>Check out our exclusive Mazkara {{ ucwords(mzk_label('specials')) }}</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="tf-section h300 pt15  hide-only-mobile" style="background-size:100%; background-position: left; background-image:url({{mzk_assets('assets/home/pinkified.jpg')}})">
  <div class="container">

    <div class="row">
      <div class="col-md-6 col-md-offset-6 text-right force-white">
        <div style="font-weight:500;" class="mt0 text-center fs125 pt20 mt15 pb10 mb25">TIRED OF PAYING FULL PRICE FOR
          <p>EVERYDAY BEAUTY SERVICES?</p>
          <a href="{{ MazkaraHelper::slugCity(null, ['specials'=>true]) }}" class="btn pt10 pb10 pl15 pl15 mb15 mt10 force-white btn-default fs125 no-border-radius" style="border-color:#fff;background-color:transparent;">
            CLICK NOW
          </a>
          <p>Check out our exclusive Mazkara {{ ucwords(mzk_label('specials')) }}</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
