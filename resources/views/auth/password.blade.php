@extends('layouts.master')
@section('content')
<div class="container">    
  <div id="" class="mainbox col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">                    
    <h1 class="text-center notransform">Forgot Password?</h1>
    <hr/>
    @include('elements.messages')

    <p class="text-center lead"> Don't fret it happens to the best of us, enter your email and we'll email you instructions on how to reset your password.</p>
<form method="POST" class="form" action="{{ URL::to('/users/forgot_password') }}" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

    <div class="form-group">
            <input class="form-control" placeholder="EMAIL ADDRESSS" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
    </div>
    <div class="form-group">
        <input class="dpb form-control btn btn-primary btn-default" type="submit" value="Retrieve Password">
    </div>
<br/><br/><br/><br/><br/>
</form>
</div></div>
@endsection