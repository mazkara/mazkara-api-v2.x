<?php
$all_zones = ViewHelper::zonesComboAsSelectableArray();
//dump(ViewHelper::zonesComboAsArray());
?>
<nav id="footer" class="container mt0">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-6">
        <div class="text-left  fnav">
          <nav id="footer  " style="padding-top:0px">
            <a href="/" class="dpb hover-color-yellow p5 pt0 page-scroll">HOME</a>
            <a href="/about" class="dpb hover-color-yellow p5 pt0 page-scroll">ABOUT US</a>
            <a href="http://blog.mazkara.com" class="dpb hidden hover-color-yellow p5 pt0 page-scroll ">BLOG</a>
            <a href="/jobs" class="page-scroll hover-color-yellow p5 pt0 dpb">WE'RE HIRING!</a>
            <a href="/add-your-business" class="dpb hover-color-yellow p5 pt0 page-scroll ">ADD YOUR BUSINESS</a>
            <a href="/contact" class="page-scroll hover-color-yellow p5 pt0 dpb">CONTACT US</a>

          </nav>
        </div>
      </div>
      <div class="col-md-6 ">


        <div class="btn-group p5 pt0 ">
          <a class="btn btn-default no-border-radius   dropdown-toggle city" data-toggle="dropdown" href="#" style="background-color:transparent;color:white;">
            {{ strtoupper(MazkaraHelper::getLocale()) }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span>
          </a>
          <ul class="dropdown-menu dropdown-user no-border-radius" style="background-color:#000;">
            @foreach(['dubai'=>'Dubai, U.A.E', 'pune'=>'Pune, India'] as $c=>$city)
            <li><a class="hover-color-yellow" href="{{ route('home.change.locale', [MazkaraHelper::getLocale(), $c]) }}">
              {{ strtoupper($city) }}
            </a></li>
            @endforeach

          </ul>
        </div>


        <a href="/tos" class="page-scroll dpb hover-color-yellow p5 pt0">TERMS OF SERVICE</a>
        <a href="/privacy-policy" class="page-scroll hover-color-yellow p5 pt0 dpb">PRIVACY POLICY</a>
        <a href="/cookie-policy" class="page-scroll hover-color-yellow p5 pt0 dpb">COOKIE POLICY</a>
        <a href="/faq" class="page-scroll hover-color-yellow p5 pt0 dpb">FAQs</a>

      </div>
    </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-12 text-right">
            <p>
              <a target="_blank" href="https://itunes.apple.com/us/app/id1086643190">
                <img height="50" src="{{mzk_assets('assets/home/apple-store.png')}}" />
              </a>
              &nbsp;&nbsp;
              <a target="_blank" href="https://play.google.com/store/apps/details?id=com.mazkara.user">
                <img height="50" src="{{mzk_assets('assets/home/google-store.png')}}" />
              </a>
            </p>
            <div id="tf-menu" class="mb10 pb10 pr0" style="padding-right:0px;" >
              <a class="navbar-brand" style="background-position:right;" href="/"></a>
            </div>
            THE ULTIMATE BEAUTY AND WELLNESS GUIDE

          </div>
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="text-center">
          <div class="bb pb10 mb10 mt10 pt10"></div>
          <ul class="footer-social mt10 pt10">
            <li><a target="_blank" href="https://www.facebook.com/pages/Mazkara/726537997492274?sk=timeline"><i class="fa fa-facebook"></i></a></li>
            <li><a target="_blank" href="https://twitter.com/mazkara"><i class="fa fa-twitter"></i></a></li>
            <li><a target="_blank" href="https://instagram.com/mazkaracom"><i class="fa fa-instagram"></i></a></li>
            <li><a target="_blank" href="https://www.linkedin.com/company/mazkara-fz-llc?"><i class="fa fa-linkedin"></i></a></li>
            <li><a target="_blank" rel="publisher" href="https://plus.google.com/u/1/b/114618111391601401260"><i class="fa fa-google-plus"></i></a></li>
          </ul>

          <div class="p10 m10">
            <small>&copy; MAZKARA FZ. LLC. </small>
          </div>



        </div>
      </div>
    </div>
  </div>
</nav>

@if(isset($javascript_include))
  {{ mzk_js_tag($javascript_include, true) }}
@else
  {{ mzk_js_tag('application', true) }}
@endif

<script type="text/javascript" src="https://s3.amazonaws.com/mazkaracdn/js/form-validator/jquery.form-validator.min.js"></script>
@section('js')
@show
@section('js-review')
@show

@include('elements.fb')
{{ mzk_dump_console()}}
<script type="text/javascript">
  var _MAZKARA_CITIES_ = {{ json_encode($all_zones) }};
  var _MAZKARA_CATEGORIES_ = {{ json_encode(MazkaraHelper::getCategoriesList())}};





</script>
{{ mzk_js_tag('application.footer', true) }}
