
<nav id="tf-menu" class="navbar container mb0   navbar-default no-radius on"  style="background-color:#1E1E1E;">
    <div id="top-main-logo-container" >

  <div class="container">
    <div class="ml0 pl0 navbar-brand"><a class="navbar-brand" href="/"></a></div>

  <!-- /.dropdown -->
  <!--
  <ul class="nav dpib navbar-top-links pull-left navbar-left ">
    <li class="dropdown" style="margin-left:-20px;padding-top: 11px; font-size:11px;">
      <a class="btn btn-default no-border-radius btn-xs  dropdown-toggle city" data-toggle="dropdown" href="#" style="color:white;padding-right:5px; padding-bottom:2px;padding-left:5px;padding-top:0px; margin-top:5px;">
        {{ ucwords(MazkaraHelper::getLocale()) }} <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-user no-border-radius">
        @foreach(['dubai'=>'Dubai', 'pune'=>'Pune'] as $c=>$city)
        <li><a href="{{ route('home.change.locale', [MazkaraHelper::getLocale(), $c]) }}">
          {{ $city }}
        </a></li>
        @endforeach
      </ul>
    </li>
  </ul>
-->

    <div class="navbar-header">
@if (Auth::check())
  <a class=" mr5 navbar-toggle hide-only-mobile collapsed" style="border:0px;vertical-align:middle; padding:5px 0px" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      {{ViewHelper::userAvatar(Auth::user(), ViewHelper::$avatar25)}}
  </a>
@else
      <button type="button" class="pt6 pb7 mr5 hide-only-mobile border-radius-5  navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
  <span class="fa fa-user "></span>
      </button>

@endif
      <button type="button" class="navbar-toggle hide-only-mobile border-radius-5  collapsed" data-toggle="collapse" data-target="#bs-main-navbar-collapse-1">
        <span class="sr-only">Toggle main menu</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      @if((1==1) || (MazkaraHelper::getLocale()=='dubai'))
      <a href="{{ MazkaraHelper::slugCity(null, ['specials'=>true]) }}" style="margin-top:13px;" class="btn no-border-radius btn-xs pull-right mr10 dpib show-only-mobile btn-default btn-pink ">
        {{ strtoupper(mzk_label('specials')) }}
      </a>
      @endif
    </div>



    <!-- Collect the nav links, forms, and other content for toggling -->
        <?php mzk_timer_start('navigation');?>

    

    <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
      <ul class="nav mt5 navbar-nav navbar-right">
      @if((1==1) || (MazkaraHelper::getLocale()=='dubai'))
        <li>
          <a href="{{ MazkaraHelper::slugCity(null, ['specials'=>true]) }}" class="btn btn-default no-border-radius btn-pink ">{{ strtoupper(mzk_label('specials')) }}</a>
        </li>
        @endif
        @if (Auth::check())
          @if (Auth::user()->hasRole('client'))
            <li>
              <a href="{{{ URL::to('/partner') }}}" class="page-scroll ">Dashboard!</a>
            </li>
          @endif
        @endif
        @include('elements.menu')
      </ul>
    </div><!-- /.navbar-collapse -->
    <?php mzk_timer_stop('navigation');?>
    </div>
  </div>
    <div id="top-main-menu-container" >
    <div class="container">
    <div class="collapse navbar-collapse pl0" id="bs-main-navbar-collapse-1">
    <ul class="nav navbar-nav yamm  navbar-secondary">
          @include('elements.main-menu')
      </ul>
    </div></div>

  </div><!-- /.container-fluid -->
</nav>
<div id="fullscreen-overlay"></div>