<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  @include('elements.menus.admin-navbar-top')


  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>
        @if(Auth::user()->can("manage_virtual_numbers") || Auth::user()->can('manage_merchants'))
        <li>
          <a href="#"><i class="fa fa-star fa-fw"></i> CRM Panel<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
          @if(Auth::user()->can('manage_merchants'))
            <li>
              <a href="{{ route('admin.merchants.index') }}">Merchants</a>
            </li>
          @endif
          @if(Auth::user()->can("manage_virtual_numbers"))
            <li>
              <a href="{{ route('admin.virtual_numbers.index') }}">Virtual Numbers</a>
            </li>
            <!-- <li>
              <a href="{{ route('admin.virtual_number_allocations.index') }}">Businesses > Numbers</a>
            </li> -->
          @endif

          </ul>
          <!-- /.nav-second-level -->
        </li>
        @endif
        @if(Auth::user()->can("manage_ads") || Auth::user()->can('manage_campaigns'))
        <li>
          <a href="#"><i class="fa fa-star fa-fw"></i> Native Ad's Panel<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            @if(Auth::user()->can('manage_campaigns'))
            <li>
              <a href="{{ route('admin.campaigns.index') }}">Campaigns</a>
            </li>
            @endif
            @if(Auth::user()->can('manage_ads'))
            <li>
              <a href="{{ route('admin.business_zones.index') }}">Business Zones</a>
            </li>
            <li>
              <a href="{{ route('admin.ad_zones.index') }}">Ad Zones</a>
            </li>
            <li>
              <a href="{{ route('admin.ads.index') }}">Ads</a>
            </li>
            @endif
          </ul>
          <!-- /.nav-second-level -->
        </li>
        <li>
          <a href="{{ route('admin.featured.index') }}">Featured Listings</a>
        </li>

        @endif
            <li>
              <a href="{{ route('admin.newsletters.index') }}">Newsletter Emails</a>
            </li>

      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>