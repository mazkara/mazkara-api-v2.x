  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="">

<link rel="apple-touch-icon" sizes="57x57" href="https://s3.amazonaws.com/mazkaracdn/icons/apple-icon-57x57.png?v=2">
<link rel="apple-touch-icon" sizes="60x60" href="https://s3.amazonaws.com/mazkaracdn/icons/apple-icon-60x60.png?v=2">
<link rel="apple-touch-icon" sizes="72x72" href="https://s3.amazonaws.com/mazkaracdn/icons/apple-icon-72x72.png?v=2">
<link rel="apple-touch-icon" sizes="76x76" href="https://s3.amazonaws.com/mazkaracdn/icons/apple-icon-76x76.png?v=2">
<link rel="apple-touch-icon" sizes="114x114" href="https://s3.amazonaws.com/mazkaracdn/icons/apple-icon-114x114.png?v=2">
<link rel="apple-touch-icon" sizes="120x120" href="https://s3.amazonaws.com/mazkaracdn/icons/apple-icon-120x120.png?v=2">
<link rel="apple-touch-icon" sizes="144x144" href="https://s3.amazonaws.com/mazkaracdn/icons/apple-icon-144x144.png?v=2">
<link rel="apple-touch-icon" sizes="152x152" href="https://s3.amazonaws.com/mazkaracdn/icons/apple-icon-152x152.png?v=2">
<link rel="apple-touch-icon" sizes="180x180" href="https://s3.amazonaws.com/mazkaracdn/icons/apple-icon-180x180.png?v=2">
<link rel="icon" type="image/png" sizes="192x192"  href="https://s3.amazonaws.com/mazkaracdn/icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://s3.amazonaws.com/mazkaracdn/icons/favicon-32x32.png?v=2">
<link rel="icon" type="image/png" sizes="96x96" href="https://s3.amazonaws.com/mazkaracdn/icons/favicon-96x96.png?v=2">
<link rel="icon" type="image/png" sizes="16x16" href="https://s3.amazonaws.com/mazkaracdn/icons/favicon-16x16.png?v=2">
<link rel="manifest" href="https://s3.amazonaws.com/mazkaracdn/icons/manifest.json">
<link rel="canonical" href="{{ isset($canonical_url)?$canonical_url:Request::url() }}" />

<meta name="google-play-app" content="app-id=com.mazkara.user">

<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="https://s3.amazonaws.com/mazkaracdn/icons/ms-icon-144x144.png?v=2">
<meta name="theme-color" content="#ffffff">
<meta name="format-detection" content="telephone=no">
@if(isset($jsonLD))
  <script type="application/ld+json">
    {{ json_encode($jsonLD) }}
  </script>
@endif

@if( isset($meta))

{{ $meta->display(array('title' => Lang::get('seo.title'), 
              'description' => Lang::get('seo.description'),
              'og'=>[
                'title' => Lang::get('seo.title'),
                'image'=> mzk_assets('icons/ms-icon-310x310.png'),
                'description' => Lang::get('seo.description'),
                'url' => Request::url()
              ]
      ), true) }}
@else
<title>Mazkara</title>
@endif

  @section('preheader')
  @show
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css">
  <?php echo mzk_stylesheet_link_tag('application', true); ?>
  <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" />
  @section('header')
  @show
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

<?php if (App::environment('production')): ?>
  <!-- Start Alexa Certify Javascript -->
  <script type="text/javascript">
  _atrk_opts = { atrk_acct:"57TCm1akKd60fn", domain:"mazkara.com",dynamic: true};
  (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
  </script>
  <noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=57TCm1akKd60fn" style="display:none" height="1" width="1" alt="" /></noscript>
  <!-- End Alexa Certify Javascript -->  



  
  <?php if(MazkaraHelper::getLocale()=='pune'):?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-61958488-2', 'auto');
      ga('send', 'pageview');
    </script>
  <?php elseif(MazkaraHelper::getLocale()=='dubai'):?>
    <script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-61958488-1', 'auto');
      ga('send', 'pageview');
    </script>
  <?php endif;?>
<!-- Facebook Conversion Code for Key Page Views - Mazkara -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6031480514575', {'value':'0.00','currency':'INR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6031480514575&amp;cd[value]=0.00&amp;cd[currency]=INR&amp;noscript=1" /></noscript>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '923868234315051');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=923868234315051&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<script type="text/javascript">
   var clevertap = {event:[], profile:[], account:[], enum:function(e){return '$E_' + e}};
   clevertap.account.push({"id": "844-6KZ-R44Z"});
   (function () {
       var wzrk = document.createElement('script');
       wzrk.type = 'text/javascript';
       wzrk.async = true;
       wzrk.src = ('https:' == document.location.protocol ? 'https://d2r1yp2w7bby2u.cloudfront.net' : 'http://static.clevertap.com') + '/js/a.js';
       var s = document.getElementsByTagName('script')[0];
       s.parentNode.insertBefore(wzrk, s);
   })();
</script>


<?php endif;?>

<?php if(1==2):?>
<!-- Start of mazkara Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","mazkara.zendesk.com");/*]]>*/</script>
<!-- End of mazkara Zendesk Widget script -->
<?php endif;?>