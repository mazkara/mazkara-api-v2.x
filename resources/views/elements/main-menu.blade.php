<?php $services = MazkaraHelper::getServicesHierarchyList();?>

@foreach($services as $ii=>$service)
  <li class="dropdown yamm-fw parent" style="">
    <a class="dropdown-toggle main-menu-btn text-center" data-animations="fadeInDown" data-toggle="dropdown"  href="#">
    {{ strtoupper($service['name']) }}
    </a>
    @if(count($service['children']) >0)
    <ul class="dropdown-menu bg-lite-gray" role="menu" style="min-height: 308px">
      <li class="container">
        <div class="yamm-content">
          <div class=""> 
            <div class="row"> 
              <div class="col-sm-2 hide-only-mobile">
                <img style="position: absolute;top:-25px;height:306px;left:-30px;" alt="{{ mzk_slug_to_words($service['slug']) }}" src="{{mzk_assets('assets/menu-'.$service['slug'].'.jpg')}}"  />
              </div>
              <div class="col-sm-10">
                <div class="row">
                  @foreach(array_chunk($service['children'], 8) as $childs)
                    <div class="col-md-4">
                      <ul class="list-unstyled">
                    @foreach($childs as $ix=>$child)
                      <li>
                        <a class=" hover-underline  m5 p5  btn no-border-radius fs80" href="{{ MazkaraHelper::slugCity(null, ['service'=>[$child['id']]]) }}">
                          {{ $child['name'] }}
                        </a>
                      </li>
                    @endforeach
                    </ul></div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </li>

      </ul>
    @endif
  </li>
@endforeach
  <li class="yamm-fw dropdown parent">
    <a class=" fuscia text-center" href="/stories">
    STORIES
    </a>
  </li>


