<?php 
  $user = $activity->user;
  $payment = $activity->itemable;
?>
  <div class="feed-item">
    <div class="date">{{ $activity->created_at }}</div>
    <div class="text">{{$user->name}} marked payment {{$payment->id}} as {{$activity->verb}} </div>
  </div>
