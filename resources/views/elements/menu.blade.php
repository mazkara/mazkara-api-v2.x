
@if (Auth::check())
  
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="padding:10px 12px">
      {{ViewHelper::userAvatar(Auth::user(), ViewHelper::$avatar25)}}
    </a>
    <ul class="dropdown-menu" role="menu">
      <li>
        <div class="p5 pl10">
          <div class="pl10">
            <small>
              Hello {{ Auth::user()->full_name}}
            </small>
          </div>
        </div>
      </li>
      <li class="divider">
        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('finance') || Auth::user()->hasRole('sales-admin') || Auth::user()->hasRole('moderator') )
          <li><a href="{{{ URL::to('/admin') }}}">Admin Panel</a></li>
          <li class="divider"></li>
        @endif
      <li>
        <a href="{{{ URL::to('users/'.Auth::user()->id.'/profile') }}}">
          <i class="fa fa-user"></i> Profile
        </a>
      </li>
<!--  <li>
        <a href="{{{ URL::to('users/vouchers') }}}">
          <i class="fa fa-money"></i> My Vouchers
        </a>
      </li>
    -->
      <li>
        <a href="{{{ URL::to('users/'.Auth::user()->id.'/profile#reviews') }}}">
          <i class="fa fa-check-square-o"></i> Reviews
        </a>
      </li>
      <li>
        <a href="{{{ URL::to('users/edit') }}}">
          <i class="fa fa-pencil"></i> Edit Profile
        </a>
      </li>
      <li class="divider"></li>
      <li>
        <a href="{{{ URL::to('users/logout') }}}">
          <i class="fa fa-lock"></i> Logout</a>
      </li>
    </ul>
  </li>
@else
  <li><a href="/users/facebook/login" class="page-scroll facebook-login-link">Login with Facebook</a></li>
  <li><span style="display:none;"><img src="https://s3.amazonaws.com/mazkaracdn/css/images/authenticate-background-lite.jpg"/></span><a href="/users/create" id="ajax-login-link" style=" padding-left:0px;padding-right:0px;"  class="page-scroll ajax-popup-link"><span class="show-only-mobile" style="padding-left:15px">Login</span><span class="hide-only-mobile">Login</span></a></li>
@endif
