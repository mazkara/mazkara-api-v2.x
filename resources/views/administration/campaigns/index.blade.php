@extends('layouts.admin-crm')
@section('content')

<div class="row">
  <div class="col-md-12">
  <div class="pull-right">
    <p></p>
    {{ link_to_route('admin.campaigns.create', 'Add New Campaign', null, array('class' => 'btn btn-sm btn-success')) }}
  </div>
  <h1>All Campaigns</h1>
</p>



@if ($campaigns->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Title</th>
				<th>Caption</th>
				<th>Starts</th>
				<th>Ends</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($campaigns as $campaign)
				<tr>
          <td>{{{ $campaign->id }}}</td>
					<td>{{{ $campaign->title }}}</td>
					<td>{{{ $campaign->caption }}}</td>
					<td>{{{ $campaign->starts }}}</td>
					<td>{{{ $campaign->ends }}}</td>
          <td>
              {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm(\'Are you sure\')', 'method' => 'DELETE', 'route' => array('admin.campaigns.destroy', $campaign->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
              {{ link_to_route('admin.campaigns.show', 'View', array($campaign->id), array('class' => 'btn btn-xs btn-success')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $campaigns->render() }}
  <div class="pull-right">
    {{ count($campaigns) }} / {{ $campaigns->total() }} entries
  </div></div>
</div>


@else
	There are no campaigns
@endif
</div></div>

@stop
