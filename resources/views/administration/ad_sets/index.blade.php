@extends('layouts.admin-crm')
@section('content')

<h1>All Ad_sets</h1>

<p>{{ link_to_route('admin.ad_sets.create', 'Add New Ad_set', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($ad_sets->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Campaign_id</th>
				<th>Ad_zone_id</th>
				<th>Slot</th>
				<th>Ad_id</th>
				<th>Status</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($ad_sets as $ad_set)
				<tr>
					<td>{{{ $ad_set->campaign_id }}}</td>
					<td>{{{ $ad_set->ad_zone_id }}}</td>
					<td>{{{ $ad_set->slot }}}</td>
					<td>{{{ $ad_set->ad_id }}}</td>
					<td>{{{ $ad_set->status }}}</td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.ad_sets.destroy', $ad_set->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
            {{ link_to_route('admin.ad_sets.edit', 'Edit', array($ad_set->id), array('class' => 'btn btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no ad_sets
@endif
@stop