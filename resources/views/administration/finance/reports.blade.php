@extends('layouts.admin-finance')
@section('content')
<div class="row">
  <div class="col-md-12">
    <br/><br/>
    <?php

    ?>
    <div class="well">
      {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
        {{ Form::select('month', mzk_month_array(), $current_month, array('class'=>'form-control ' )) }}
        {{ Form::select('year', mzk_year_array(), $current_year, array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
        {{ Form::select('city', [ '0'=>'All', '1'=>'Dubai', '88'=>'Pune'], $current_city, array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
        {{ Form::select('user_id', ['0'=>'All']+User::select()->byPocs()->get()->lists('selectable_full_name', 'id')->all(), Input::old('user_id'), array('class'=>'form-control', 'style'=>'width:250px;')) }}
        <button type="submit" class="btn btn-default">GENERATE REPORT</button>
      {{ Form::close() }}
    </div>
  </div>
  <h2>Statement of Income for <?php $m = mzk_month_array(); echo $m[$current_month];?> {{$current_year}}</h2>
  <hr/>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ '#' }}</th>
        <th>INVOICE DESC</th>
        <th>MERCHANT</th>
        <th>CITY</th>
        <th>SALES POC</th>
        <th>START DATE</th>
        <th>END DATE</th>
        <th>MONTHLY (AED)</th>
        <th>MONTHLY (INR)</th>
        <th>MONTHLY (USD)</th>
      </tr>
    </thead>
    <tbody>
<?php 
  $income_aed = 0;
  $income_inr = 0;
  $income_usd = 0;
  $credit_aed = 0;
  $credit_inr = 0;
  $credit_usd = 0;
?>
    @foreach($invoices as $invoice)
      <tr>
        <td>{{ $invoice->id }}</td>
        <td>{{ link_to_route('admin.invoices.show', $invoice->title, [$invoice->id]) }}</td>

        <td><a href="{{route('admin.merchants.show', [$invoice->merchant_id])}}">{{ ($invoice->merchant->name)}}</a></td>
        <td>{{ $invoice->merchant->city_name() }}</td>
        <td>{{ $invoice->poc_name() }}</td>
        <td>{{ mzk_f_date($invoice->start_date) }}</td>
        <td>{{ mzk_f_date($invoice->end_date) }}</td>
        <td>AED {{{ number_format($invoice->currentMonthContributionAED($current_month, $current_year), 2) }}}</td>
        <td>INR {{{ number_format($invoice->currentMonthContributionINR($current_month, $current_year), 2) }}}</td>
        <td>USD {{{ number_format($invoice->currentMonthContributionUSD($current_month, $current_year), 2) }}}</td>
      </tr>
      <?php 
      $income_aed += $invoice->currentMonthContributionAED($current_month, $current_year);
      $income_inr += $invoice->currentMonthContributionINR($current_month, $current_year);
      $income_usd += $invoice->currentMonthContributionUSD($current_month, $current_year);


      ?>        
    @endforeach
    @foreach($credit_notes as $credit_note)
    <?php 

      $credit_current_aed = $credit_note->currentMonthContributionAED($current_month, $current_year);
    
      $credit_current_inr = $credit_note->currentMonthContributionINR($current_month, $current_year);
      $credit_current_usd = $credit_note->currentMonthContributionUSD($current_month, $current_year);
    ?>
      <tr class="danger">

        <td>{{ $credit_note->id }}</td>
        <td>
          <a href="{{ route('admin.credit_notes.pdf', array($credit_note->id)) }}" class=" ">
            {{ $credit_note->title }}
          </a>
        </td>
        <td><a href="{{route('admin.merchants.show', [$invoice->merchant_id])}}">{{ ($credit_note->invoice->merchant->name)}}</a></td>
        <td>{{ $credit_note->invoice->merchant->city_name() }}</td>
        <td></td>
        <td></td>
        <td></td>
        <td>AED {{{ number_format($credit_current_aed, 2) }}}</td>
        <td>INR {{{ number_format($credit_current_inr, 2) }}}</td>
        <td>USD {{{ number_format($credit_current_usd, 2) }}}</td>
      </tr>
      <?php 
      $credit_aed += $credit_current_aed;
      $credit_inr += $credit_current_inr;
      $credit_usd += $credit_current_usd;

      ?>
    @endforeach

      <tr>
        <td colspan="7" >
          <b>INCOME TOTAL</b>
        </td>
        <td>
          <b>AED {{ number_format($income_aed, 2) }}</b>
        </td>
        <td>
          <b>INR {{ number_format($income_inr, 2) }}</b>
        </td>
        <td>
          <b>USD {{ number_format($income_usd, 2) }}</b>
        </td>
      </tr>
      <tr class="danger">
        <td  colspan="7" >
          <b>CREDIT TOTAL</b>
        </td>
        <td>
          <b>AED {{ number_format($credit_aed, 2) }}</b>
        </td>
        <td>
          <b>INR {{ number_format($credit_inr, 2) }}</b>
        </td>
        <td>
          <b>USD {{ number_format($credit_usd, 2) }}</b>
        </td>
      </tr>
<?php 

$expenses_usd = 0;
$expenses_inr = 0;
$expenses_aed = 0;


foreach($current_month_expenses as $expense){
  $expenses_usd  += $expense->amount_usd;
  $expenses_inr  += $expense->amount_inr;
  $expenses_aed  += $expense->amount_aed;
}


?>
      <tr class="warning">
        <td colspan="7" >
          <b>EXPENSES</b>
        </td>
        <td>
          <b>AED {{ number_format($expenses_aed, 2) }}</b>
        </td>
        <td>
          <b>INR {{ number_format($expenses_inr, 2) }}</b>
        </td>
        <td>
          <b>USD {{ number_format($expenses_usd, 2) }}</b>
        </td>
      </tr>

      <tr>
        <td colspan="7" >
          <b>FINAL INCOME TOTAL</b>
        </td>
        <td>
          <b>AED {{ number_format($income_aed - ($expenses_aed + $credit_aed), 2) }}</b>
        </td>
        <td>
          <b>INR {{ number_format($income_inr - ($expenses_inr + $credit_inr), 2) }}</b>
        </td>
        <td>
          <b>USD {{ number_format($income_usd - ($expenses_usd + $credit_usd), 2) }}</b>
        </td>
      </tr>

    </tbody>
  </table>
  
</div>
@stop
