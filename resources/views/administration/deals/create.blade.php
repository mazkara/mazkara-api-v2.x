@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="col-md-10 ">
        <h1>Create {{$type}}</h1>

<?php $type = strtolower($type);?> 
        @if(($type) == 'discount')
            @include('administration.deals.forms.discount')
        @elseif(($type) == 'promo')
            @include('administration.deals.forms.promo')
        @elseif(($type) == 'package')
            @include('administration.deals.forms.package')
        @endif
    </div>
</div>
<script type="text/javascript">
$(function(){
    $('textarea').wysihtml5();
});
</script>

@stop
