@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
    <h1>Newsletters HTML</h1>
    <hr/>
    <p>
      <a class="btn btn-primary" href="{{ route('admin.newsletters.weekly.posts') }}" target="_blank">GENERATE WEEKLY ARTICLES NEWSLETTER</a>
    </p>
  </div>
</div>
@stop
