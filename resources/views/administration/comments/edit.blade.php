@extends('layouts.admin-content')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Comment</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($comment, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('comments.update', $comment->id))) }}

        <div class="form-group">
            {{ Form::label('body', 'Body:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('body', Input::old('body'), array('class'=>'form-control', 'placeholder'=>'Body')) }}
            </div>
        </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('comments.show', 'Cancel', $comment->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}

@stop
