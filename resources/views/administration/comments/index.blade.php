@extends('layouts.admin-content')
@section('content')

<h1>All Comments</h1>

<p>{{ link_to_route('admin.comments.create', 'Add New Comment', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($comments->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Body</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($comments as $comment)
				<tr>
					<td>{{{ $comment->body }}}</td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.comments.destroy', $comment->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
            {{ link_to_route('admin.comments.edit', 'Edit', array($comment->id), array('class' => 'btn btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no comments
@endif

@stop
