@extends('layouts.admin-content')
@section('content')

<h1>All Accounts</h1>

<p>{{ link_to_route('admin.accounts.create', 'Add New Account', null, array('class' => 'btn btn-lg btn-success')) }}</p>
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}

    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2',  'placeholder'=>'Search')) }}
    <button type="submit" class="btn btn-default">Filter</button>

  {{ Form::close() }}
  <a href="{{ route('admin.accounts.export')}}" class="btn btn-info pull-right">
    Export To Excel
  </a>
</div>
@if ($accounts->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th col-span="2">Name</th>
				<th>Username</th>
				<th>Email</th>
        <th>Account(s)?</th>
				<th>Confirmed</th>
        <th>Joined</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($accounts as $account)
				<tr>
          <td>      {{ViewHelper::userAvatar($account)}}</td>
          <td>{{{ $account->full_name }}}</td>
					<td>{{{ $account->username }}}</td>
					<td>{{{ $account->email }}}</td>
          <td>
            @if(trim($account->username)!='')
              <span class="label label-default" title="Signed up with email and password"><i class="fa fa-lock"></i></span>
            @endif
            @foreach($account->accounts as $acc)
              
              @if($acc->provider=='facebook')
              <span class="label label-primary"  title="Signed up with facebook"><i class="fa fa-facebook"></i></span>
              @endif
              @if($acc->provider=='google')
              <span class="label label-danger"  title="Signed up with google"><i class="fa fa-google"></i></span>
              @endif
            @endforeach
          </td>
					<td>{{ $account->confirmed > 0 ? '<span class="label label-success">CONFIRMED</span>':'<span class="label label-danger">UNCONFIRMED</span>' }}</td>
          <td>
            {{{ Date::parse($account->created_at)->ago() }}}
          </td>
          <td>
              <span style="display:none;">
                {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.accounts.destroy', $account->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
              </span>
              {{ link_to_route('admin.accounts.edit', 'Edit', array($account->id), array('class' => 'btn btn-xs btn-info')) }}
              {{ link_to_route('users.profile.show', 'View', array($account->id), array('class' => 'btn btn-xs btn-success')) }}

          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $accounts->render() }}
  <div class="pull-right">
    {{ count($accounts) }} / {{ $accounts->total() }} entries
  </div></div>
</div>


@else
	There are no accounts
@endif
@stop