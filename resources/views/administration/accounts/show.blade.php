@extends('layouts.admin-content')
@section('content')

<h1>Show Account</h1>

<p>{{ link_to_route('admin.accounts.index', 'Return to All accounts', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Username</th>
				<th>Email</th>
				<th>Confirmed</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $account->username }}}</td>
					<td>{{{ $account->email }}}</td>
					<td>{{{ $account->confirmed }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.accounts.destroy', $account->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('admin.accounts.edit', 'Edit', array($account->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>
@stop