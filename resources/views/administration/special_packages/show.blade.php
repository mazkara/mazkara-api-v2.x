
<h1>Show Special_package</h1>

<p>{{ link_to_route('special_packages.index', 'Return to All special_packages', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
				<th>Photo_id</th>
				<th>Business_id</th>
				<th>State</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $special_package->title }}}</td>
					<td>{{{ $special_package->photo_id }}}</td>
					<td>{{{ $special_package->business_id }}}</td>
					<td>{{{ $special_package->state }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('special_packages.destroy', $special_package->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('special_packages.edit', 'Edit', array($special_package->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>
