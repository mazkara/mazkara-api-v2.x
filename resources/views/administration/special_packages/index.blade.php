<h1>All Special_packages</h1>

<p>{{ link_to_route('special_packages.create', 'Add New Special_package', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($special_packages->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Photo_id</th>
				<th>Business_id</th>
				<th>State</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($special_packages as $special_package)
				<tr>
					<td>{{{ $special_package->title }}}</td>
					<td>{{{ $special_package->photo_id }}}</td>
					<td>{{{ $special_package->business_id }}}</td>
					<td>{{{ $special_package->state }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('special_packages.destroy', $special_package->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('special_packages.edit', 'Edit', array($special_package->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no special_packages
@endif
