@extends('layouts.admin-crm')
@section('content')
<h1>Show Ad_zone</h1>

<p>{{ link_to_route('admin.ad_zones.index', 'Return to All ad_zones', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
				<th>Caption</th>
				<th>Zone_id</th>
				<th>Category_id</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $ad_zone->title }}}</td>
			<td>{{{ $ad_zone->caption }}}</td>
			<td>{{{ $ad_zone->zone_id }}}</td>
			<td>{{{ $ad_zone->category_id }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.ad_zones.destroy', $ad_zone->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.ad_zones.edit', 'Edit', array($ad_zone->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

@stop
