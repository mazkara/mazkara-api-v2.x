@extends('layouts.admin-content')
@section('content')

<h1>Show Service</h1>

<p>{{ link_to_route('admin.services.index', 'Return to All services', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
				<th>Parent_id</th>
				<th>Description</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $service->name }}}</td>
			<td>{{{ $service->parent_id }}}</td>
			<td>{{{ $service->description }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.services.destroy', $service->id))) }}
          {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.services.edit', 'Edit', array($service->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

@stop
