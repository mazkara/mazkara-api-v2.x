@extends('layouts.admin-crm')
@section('content')
<h1>All Ads</h1>

<p>{{ link_to_route('admin.ads.create', 'Add New Ad', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($ads->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th></th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($ads as $ad)
				<tr>
					<td class="col-md-4">
					<a href="{{ $ad->url }}"><div class="native-ad">
					    <div class="post-img-content">
					        @if($ad->photo)
					        <img src="{{ $ad->photo->image->url('large')}}" class="img-responsive" />
					        @endif
					        <span class="post-title"><span>{{$ad->title}}</span></span>
					    </div>
					    <div class="content">
					        <div class="author">
					            {{$ad->caption}}
					        </div>
					    </div>
					</div></a>
					</td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.ads.destroy', $ad->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
            {{ link_to_route('admin.ads.edit', 'Edit', array($ad->id), array('class' => 'btn btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no ads
@endif
@stop