@extends('layouts.admin-finance')
@section('content')

<div class="row">
	<div class="col-md-12">

<h1>All Invoices
<p class="pull-right">
  {{ link_to_route('admin.invoices.create', 'Add New Invoice', null, array('class' => 'btn btn-sm btn-success')) }}
  {{ link_to_route('admin.invoices.ppl.create', 'Add New PPL Invoice', null, array('class' => 'btn btn-sm btn-info')) }}
</p>
</h1>

@if ($invoices->count())
<div class="row">
	<div class="col-md-12">

<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:250px;', 'placeholder'=>'Search')) }}
    {{ Form::select('state', ([''=>'State?'] + Invoice::getStates()), Input::get('state'), array('class'=>'form-control ', 'placeholder'=>'Category')) }}
    {{ Form::select('merchant_id', ([''=>'Merchant?'] + Merchant::lists('name', 'id')->all()), Input::get('merchant_id'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>

	<table  class="table table-striped">
		<thead>
			<tr>
				<th>Invoice No.</th>
				<th>Merchant</th>
        <th>Period</th>
        <th>Month Contribution</th>
        <th>Monthly Average</th>
				<th>Contract Amount</th>
        <th>Amount Due</th>
				<th>State</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($invoices as $invoice)
				<tr>
					<td>{{ link_to_route('admin.invoices.show', $invoice->title, [$invoice->id]) }}</td>
					<td><span title="{{{ $invoice->merchant->name }}}">{{ mzk_str_trim($invoice->merchant->name, 10)}}</span></td>
          <td>{{{ mzk_f_date($invoice->start_date) }}} - {{{ mzk_f_date($invoice->end_date) }}}</td>
          <td>{{{ $invoice->currency }}} {{{ $invoice->currentMonthContribution() }}}</td>
          <td>{{{ $invoice->currency }}} {{{ $invoice->monthlyAverage() }}}</td>
					<td>{{{ $invoice->currency }}} {{{ $invoice->amount }}}</td>
          <td>{{{ $invoice->currency }}} {{{ $invoice->amount_due }}}</td>
          <td>
            <span class="label label-{{mzk_invoice_state_label_css($invoice->state)}}">{{ $invoice->state }}</span> 
          </td>
          <td>
            @if(Auth::user()->hasRole('admin'))
              {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>"return confirm('Are you sure?')", 'method' => 'DELETE', 
              										'route' => array('admin.invoices.destroy', $invoice->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
            @endif
            {{ link_to_route('admin.invoices.edit', 'Edit', array($invoice->id), 
            									array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
	</div>
</div>

  <div class="row">
    <div class="col-md-12">
  {{ $invoices->appends($params)->render() }}
  <div class="pull-right">
    {{ count($invoices) }} / {{ $invoices->total() }} entries
  </div></div>
</div>


@else
	There are no invoices
@endif

	</div>
</div>

@stop

