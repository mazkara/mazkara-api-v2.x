@extends('layouts.admin-finance')
@section('content')

<div class="row">
  <div class="col-md-12">

<p>
    <div class="alert alert-{{mzk_invoice_state_label_css($invoice->state)}}">{{ucwords($invoice->state)}}</div>
</p>


<h2>
  {{ $invoice->title }}

  <p class="pull-right">
    @if($invoice->isCleared()==false)
      <a id="btn-add-credit-note" href="javascript:void(0)" class="btn btn-sm btn-default">Add Credit Note</a>
      <a id="btn-add-bad-debt" href="javascript:void(0)" class="btn btn-sm btn-default">Add Bad Debt</a>
    @endif
    <a href="{{ route('admin.invoices.pdf', array($invoice->id)) }}" class="btn btn-sm btn-warning">
      <i class="fa fa-file-pdf-o"></i>
      Download PDF
    </a>

    {{ link_to_route('admin.invoices.edit', 'Edit', array($invoice->id), array('class' => 'btn btn-sm btn-info')) }}
    {{ link_to_route('admin.invoices.index', 'Return to All invoices', null, array('class'=>'btn btn-sm btn-primary')) }}
  </p>
</h2>

<div class="row">
  <div class="col-md-6">
    <table class="table">
      <tr>
        <th>MERCHANT</th><td><a href="{{ route('admin.merchants.show', [$invoice->merchant_id]) }}">{{ $invoice->merchant->name }}</a></td>
      </tr>
      <tr>
        <th>SALES POC</th><td>{{ $invoice->poc_name() }}</td>
      </tr>
      <tr>
        <th>DATE</th><td>{{ mzk_f_date($invoice->dated) }}</td>
      </tr>
      <tr>
        <th>START DATE</th><td>{{ mzk_f_date($invoice->start_date) }}</td>
      </tr>
      <tr>
        <th>END DATE</th><td>{{ mzk_f_date($invoice->end_date) }}</td>
      </tr>
      <tr>
        <th>CURRENCY</th><td>{{ $invoice->currency }}</td>
      </tr>
      <tr>
        <th>MAZKARA ENTITY</th><td>{{ mzk_get_entities($invoice->entity_id)['name'] }}</td>
      </tr>

    </table>
  </div>
</div>


  <div class="form-group">
    <div>
      <table class="table">
        <thead>
          <tr>
            <th colspan="2">DESC</th>
          </tr>
        </thead>
        <tbody id="items-here">
            @foreach($invoice->items as $item)
              <tr>
                <td colspan="2">
                  <a href="javascript:void(0)" class="item-inline-editable" id="desc" data-type="textarea" data-pk="{{$item->id}}" data-url="{{ route('admin.invoices.items.update') }}" data-title="Enter Description">{{ $item->desc }}</a></td>
                </td>
              </tr>
            @endforeach
            @foreach($invoice->payments()->get() as $item)
              <tr class="{{ $item->isCleared() ? 'success' : 'warning'}}">
                <td colspan="2">
                  {{ $item->desc }}{{'('.$item->type.($item->isCheque() ? ' - '.$item->state : '' ).')' }}</td>
              </tr>
            @endforeach
            @foreach($invoice->credit_notes()->get() as $item)
              <tr class="danger">
                <td colspan="2">
                  {{ $item->desc.'('.$item->type.')' }}
                  &nbsp;&nbsp;
                  (START DATE: 
                    <a href="javascript:void(0)" class="item-inline-editable" id="start_date" data-type="date" data-pk="{{$item->id}}" data-url="{{ route('admin.credit_notes.updatable') }}" data-title="Edit Start Date">{{ $item->start_date}}</a>)
                  &nbsp;&nbsp;
                  (END DATE: 
                    <a href="javascript:void(0)" class="item-inline-editable" id="end_date" data-type="date" data-pk="{{$item->id}}" data-url="{{ route('admin.credit_notes.updatable') }}" data-title="Edit End Date">{{ $item->end_date}}</a>)

                  &nbsp;&nbsp;

                  <a href="{{ route('admin.credit_notes.pdf', array($item->id)) }}" class="btn btn-xs btn-warning">
                    <i class="fa fa-file-pdf-o"></i>
                    Download PDF
                  </a>

                </td>
              </tr>
            @endforeach

        </tbody>
        <tfoot>
          <tr>
            <td colspan="1" align="right"><h5>Pending</h5></td>
            <td colspan="1" align="right" id="total-invoice"><h5>{{ $invoice->currency }} {{ (isset($invoice) ? $invoice->amount_due.' / '.$invoice->amount : '') }}</h5></td>
          </tr>
          <tr>
            <td colspan="1" align="right"><h4>Total</h4></td>
            <td colspan="1" align="right" id="total-invoice"><h4>{{ $invoice->currency }} {{ (isset($invoice) ? $invoice->amount : '') }}</h4></td>
          </tr>
        </tfoot>
      </table>
      <div class="">
        <table class="table">
          <tr>
            <td><b>Current Month Contribution:</b> {{ $invoice->currency }} {{ $invoice->currentMonthContribution()}}</td>
            <td><b>Monthly Average:</b> {{ $invoice->currency }} {{ $invoice->monthlyAverage()}}</td>
            <td><b>Contract Value:</b> {{ $invoice->currency }} {{ $invoice->contractValue()}}</td>
          </tr>
        </table>
      </div>
    </div>
  </div>


  </div>
</div>
<div id="form-credit-note" class="hidden">
{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.credit_notes.store'))->encodingType('multipart/form-data') }}

  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  {{ BootForm::text('Amount', 'amount')->placeholder('Amount') }}
  {{ BootForm::textarea('Details', 'desc')->placeholder('Details/Description') }}
  {{ Form::hidden('invoice_id', $invoice->id) }}
  {{ Form::hidden('type', '__REPLACE_ME___') }}

  {{ BootForm::token() }}
{{ BootForm::close() }}
</div>
<script type="text/javascript">
$(function(){

  validateCrediteNote = function(){
    vl = $('.bootbox-body #amount').first().val();
    if(vl>{{$invoice->amount_due}}){
      alert('Amount cannot be more than the invoice due amount of {{$invoice->amount_due}}')
      return false;
    }
    if(vl<=0){
      alert('Amount cannot be negative or zero!');
      return false;
    }
    
    $('.bootbox-body form').first().submit();
  };

  $('#btn-add-bad-debt').click(function(){
    html = $('#form-credit-note').html();
    html = html.replace('__REPLACE_ME___', 'bad-debt');
    bootbox.dialog({
      title: "Add a Bad Debt",
      message: html,
        buttons: {
          success: {
            label: "Save",
            className: "btn-success",
            callback: validateCrediteNote
          }
        }
      
    });
  });

  $('.item-inline-editable').editable();

  $('#btn-add-credit-note').click(function(){
    html = $('#form-credit-note').html();
    html = html.replace('__REPLACE_ME___', 'credit-note');
    bootbox.dialog({
      title: "Add a Credit Note",
      message: html,
        buttons: {
          success: {
            label: "Save",
            className: "btn-success",
            callback: validateCrediteNote
          }
        }
    });
  });

});
</script>

@stop
