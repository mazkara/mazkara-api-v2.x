@extends('layouts.blank')
@section('content')
<style>
.page-break {
    page-break-after: always;
}
</style>
<?php $css = 'style="padding-bottom:0px; margin-bottom:0px;margin-top:0px;"';?>
<?php $csstop = 'style="padding-bottom:0px; margin-bottom:0px;margin-top:30px;"';?>
<table width="100%">
  <tr>
    <td width="50%" align="left" style="vertical-align:top;padding-left:0px;margin-left:0px;">
      <img src="{{mzk_assets('assets/mazkaralogo.png')}}" align="left" width="175px" style="text-align:left;"  />
    </td>
    <td align="right" width="50%">
      <?php $entity = mzk_get_entities($invoice->entity_id);?>
      <div >{{ $entity['name'] }}</div>
      @foreach($entity['address'] as $vv)
        <div>{{ $vv }}</div>
      @endforeach
    </td>
  </tr>
</table>

    <table class="table" {{$csstop}}  width="50%">
      <tr>
        <th width="30%">INVOICE #</th>
        <td width="70%">{{ $invoice->title }}</td>
      </tr>
    </table>
    <table class="table" {{$css}}  width="50%">
      <tr>
        <th width="30%">INVOICE DATE</th>
        <td width="70%">{{ mzk_f_date($invoice->created_at) }}</td>
      </tr>
    </table>
    <table class="table" {{$css}}  width="50%">
      <tr>
        <th width="30%">MERCHANT</th>
        <td width="70%">{{ $invoice->merchant->name }}</td>
      </tr>
    </table>
    <table class="table" {{$css}}  width="50%">
      <tr>
        <th width="30%">CITY</th>
        <td width="70%">{{ $invoice->city_name() }}</td>
      </tr>
    </table>
      @if($invoice->isPPL())
      <table class="table table-striped" {{$csstop}} width="100%">
        <tr>
          <td width="5%"><b>SR</b></td>
          <td width="50%"><b>PARTICULARS</b></td>
          <td width="15%"><b>BILLABLE CALLS</b></td>
          <td width="15%"><b>UNIT PRICE</b></td>
          <td width="15%" align="right"><b>TOTAL</b></td>
        </tr>
      </table>
        @foreach($items as $i=>$item)
          <table class="table" {{$css}}  width="100%">
            <tr>
              <td width="5%">{{($i+1)}}.</td>
              <td width="50%"> {{ $item->business->name.', '.$item->business->zone_cache }}</td>
              <td width="15%"> {{ $item->num_calls }}</td>
              <td width="15%">{{$invoice->currency}} {{ $item->price }}</td>
              <td width="15%" align="right">{{$invoice->currency}} {{ (int)$item->calls_total  }}</td>
            </tr>
          </table>
        @endforeach

      @else
      <table class="table table-striped" {{$csstop}} width="100%">
        <tr>
          <td><b>ITEM(s) DESCRIPTION</b></td>
        </tr>
      </table>
        @foreach($invoice->items as $i=>$item)
          <table class="table" {{$css}}  width="100%">
            <tr>
              <td width="5%">{{($i+1)}}.</td>
              <td width="95%"> {{ nl2br($item->desc) }}</td>
            </tr>
          </table>
        @endforeach
      @endif
<!--            @foreach($invoice->payments()->get() as $item)
            <table class="table" {{$css}}  width="100%">
              <tr class="{{ $item->isCleared() ? 'success' : 'warning'}}">
                <td colspan="2">{{ $item->desc.'('.$item->type.($item->isCheque() ? ' - '.$item->state : '' ).')' }}</td>
              </tr>
            </table>
            @endforeach
            @foreach($invoice->credit_notes()->get() as $item)
            <table class="table" {{$css}}  width="100%">
              <tr class="danger">
                <td colspan="2">{{ $item->desc.'('.$item->type.')' }}</td>
              </tr>
            </table>
            @endforeach
-->
        <table class="table" {{$css}}  width="100%">
          <tr>
            <td width="70%"  align="right"><span style="font-size:110%">Total</span></td>
            <td width="30%"  align="right" id="total-invoice"><span style="font-size:110%">{{ $invoice->currency }} {{ (isset($invoice) ? $invoice->amount : '') }}</span></td>
          </tr>
        </table>
        <table class="table" {{$css}}  width="100%">
          <tr>
            <td  align="left" >
              <span >
                {{ ucwords(mzk_convert_number_to_words((int)$invoice->amount))  }} 
                {{ mzk_currency_name_from_sym($invoice->currency) }} 
                Only
              </span>
            </td>
          </tr>
        </table>

<p style="margin-top:150px;"><br/></p>
<div class="alert alert-warning">
  Note: This invoice is computer generated and doesn't require a signature.
  @if(mzk_is_entity_india($invoice->entity_id))
    <p>
      All cheques to be made in favor of "Mazkara Media Pvt. Ltd".
    </p>
  @endif
</div>
@if(mzk_is_entity_india($invoice->entity_id))
<p><b>BANK DETAILS</b></p>
<p>ACCOUNT NAME: MAZKARA MEDIA PVT. LTD.</p>
<p>ACCOUNT NUMBER: 05660 500 2600</p>
<p>BANK NAME: ICICI BANK.</p>
<p>IFSC CODE: ICIC0000566</p>
@endif

@if($invoice->isPPL())
<div class="page-break"></div>
  <h1>APPENDIX: CALL LOGS</h1>
  <table class="table "  {{$css}} width="100%">
    <tr>
      <th></th>
      <th width="25%">Dated</th>
      <th width="20%">Called</th>
      <th width="20%">Caller</th>
      <th width="10%">Duration</th>
      <th width="25%">Cost Per Call</th>
    </tr>
    @foreach ($invoice->items as $invoice_item)
      @foreach ($invoice_item->call_logs()->get() as $call_log)
        <tr>

          <td width="25%"><span style="font-size:10px;">{{{ $call_log->dated }}} - {{{ $call_log->timed }}}</span></td>
          <td width="20%"><span style="font-size:10px;">{{{ $call_log->called_number }}}</span></td>
          <td width="20%"><span style="font-size:10px;">{{{ $call_log->caller_number }}}</span></td>
          <td width="10%"><span style="font-size:10px;">{{{ $call_log->caller_duration }}}</span></td>
          <td width="25%"><span style="font-size:10px;">{{ $invoice->currency }} {{{ $invoice_item->price }}}</span></td>
        </tr>
      @endforeach
    @endforeach
  </table>



@endif
@stop
