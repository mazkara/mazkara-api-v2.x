@extends('layouts.admin-crm')
@section('content')

<h1>Show Call_log</h1>

<p>{{ link_to_route('admin.call_logs.index', 'Return to All call_logs', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Dated</th>
				<th>Timed</th>
				<th>Called_number</th>
				<th>Caller_number</th>
				<th>Caller_duration</th>
				<th>Agent_list</th>
				<th>Call_connected_to</th>
				<th>Call_transfer_status</th>
				<th>Call_transfer_duration</th>
				<th>Call_recording_url</th>
				<th>Call_start_time</th>
				<th>Call_pickup_time</th>
				<th>Caller_circle</th>
				<th>Business_id</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $call_log->dated }}}</td>
			<td>{{{ $call_log->timed }}}</td>
			<td>{{{ $call_log->called_number }}}</td>
			<td>{{{ $call_log->caller_number }}}</td>
			<td>{{{ $call_log->caller_duration }}}</td>
			<td>{{{ $call_log->agent_list }}}</td>
			<td>{{{ $call_log->call_connected_to }}}</td>
			<td>{{{ $call_log->call_transfer_status }}}</td>
			<td>{{{ $call_log->call_transfer_duration }}}</td>
			<td>{{{ $call_log->call_recording_url }}}</td>
			<td>{{{ $call_log->call_start_time }}}</td>
			<td>{{{ $call_log->call_pickup_time }}}</td>
			<td>{{{ $call_log->caller_circle }}}</td>
			<td>{{{ $call_log->business_id }}}</td>
      <td>
          {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.call_logs.destroy', $call_log->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
          {{ Form::close() }}
          {{ link_to_route('admin.call_logs.edit', 'Edit', array($call_log->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>

@stop
