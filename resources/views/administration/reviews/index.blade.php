@extends('layouts.admin-content')
@section('content')


<div class="row">
  <div class="col-md-12">
    <h1>All Reviews</h1>

<p>{{ link_to_route('reviews.create', 'Add New Review', null, array('class' => 'btn btn-sm pull-right btn-success')) }}</p>

<div class="well clearfix">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}

    {{ Form::select('business', [0=>'Select Business'] + Business::where('reviews_count', '>', 0)
                                                    ->whereOr('ratings_count', '>', 0)->orderby('name', 'asc')->byLocale()
                                                    ->lists('name', 'id')->all(), 
                                                    Input::get('business'),  array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Users')) }}
    {{ Form::select('users', [0=>'Select User'] + User::where('reviews_count', '>', 0)
                                                    ->whereOr('ratings_count', '>', 0)
                                                    ->orderby('name', 'asc')
                                                    ->lists('name', 'id')->all(), 
                                                    Input::get('users'),  array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Users')) }}
    {{ Form::select('type', ['all'=>'All', 'ratings'=>'Ratings Only', 'reviews'=>'Reviews Only'],
                                                    Input::get('type'),  array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Users')) }}

    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>


@if ($reviews->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID{{ '#' }}</th>
				<th>Rating</th>
				<th width="60%">Review</th>
        <th>Flags?</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($reviews as $review)
				<tr>
          <td>{{ $review->id }}</td>
					<td>{{{ $review->rating }}}</td>
					<td>

            @if($review->isARating())
            @else
              <div class="well">{{{ $review->body }}}</div>
            @endif
            <small>
            @if($review->user)
              By
              {{ link_to_route('users.profile.show', $review->user->getFullNameAttribute(), array($review->user_id)) }}
            @else
              From {{ $review->ip }}
            @endif
              For 
              @if($review->business!=null)
              <a href="/{{ MazkaraHelper::getLocale().'/'.$review->business->slug}}" class=" ">
                {{{ $review->business->name }}}
              </a>
              @else
                Unknown Business of ID {{{ $review->business_id }}}

              @endif

              on {{ \Carbon\Carbon::parse($review->created_at)->toDayDateTimeString()}}
            </small>
          </td>
          <td>
            @if(!empty($review->flags))
              <span class="label label-warning">
                {{ $review->flags }}
              </span>
            @endif
          </td>
          <td>
            @if(Auth::user()->can("manage_highlights"))
              {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onclick'=>'return confirm(\'Are you sure?\');', 'route' => array('admin.reviews.destroy', $review->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
              {{ link_to_route('admin.reviews.edit', 'Edit', array($review->id), array('class' => 'btn btn-xs  btn-info')) }}
            @endif
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>


  <div class="row">
    <div class="col-md-12">
  {{ $reviews->appends($params)->render() }}
  <div class="pull-right">
    {{ count($reviews) }} / {{ $reviews->total() }} entries
  </div></div>
</div>

@else
	There are no reviews
@endif
</div></div>

@stop
