  <script id="service-item-template" type="text/x-handlebars-template">
  <tr id="single-service-item-{{ id }}" tabindex="-1" class="draggable" data-item="{{ id }}">
    <td >
      <input type="checkbox" name="items[]" value="{{ id }}" />

      {{ services }}
    </td>
    <td >
      <a href="javascript:void(0)"  class="item-inline-editable" id="name" data-type="text" data-pk="{{ id }}" data-url="/content/special_items/updatable" data-title="Enter name">
      {{ name }}
      </a>({{state}})</td>
    <td >
      <a href="javascript:void(0)"  class="item-inline-editable" id="description" data-type="text" data-pk="{{ id }}" data-url="/content/special_items/updatable" data-title="Enter name">
      {{ description }}
      </a></td>
    <td >
      <a href="javascript:void(0)" class="item-inline-editable" id="cost" data-type="text" data-pk="{{ id }}" data-url="/content/special_items/updatable" data-title="Enter Cost">
        {{ cost }}
      </a>
    </td>
    <td >
      <a href="javascript:void(0)" class="item-inline-editable" id="cost_type" data-type="select" data-pk="{{ id }}" data-url="/content/special_items/updatable" data-title="Cost Type">
        {{ cost_type }}
      </a>
    </td>
    <td >
      <a href="javascript:void(0)"  class="item-inline-editable" id="duration" data-type="text" data-pk="{{ id }}" data-url="/content/special_items/updatable" data-title="Enter duration">
      {{ duration }} 
      </a></td>
    <td >
      <a href="javascript:void(0)"  class="item-inline-editable" id="duration_type" data-type="select" data-pk="{{ id }}" data-url="/content/special_items/updatable" data-title="Enter duration type">
      {{ duration_type }} 
      </a></td>
    <td>
      <form method="POST" action="/content/service_items/{{ id }}" class="deletable-form" accept-charset="UTF-8" style="display: inline-block;">
        <input name="_method" value="DELETE" type="hidden">
        <input class="btn btn-danger deletable-form-btn btn-xs" data-item="{{id}}" value="Delete" type="submit">
      </form>
    </td>    
  </tr>
  </script>
