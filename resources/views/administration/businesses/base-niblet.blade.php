<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">{{ $business->id }} - {{ $business->name }}</h3>
  </div>
  <div class="panel-body">
    <dl class="dl-horizontal">
      <dt>Business ID</dt>
      <dd><b style="text-decoration:underline;">{{ $business->id }}</b></dd>
      <dt>Slip ID</dt>
      <dd><b>{{ $business->ref }}</b></dd>
      <dt>Lot ID</dt>
      <dd><b>{{ $business->lot_id }}</b></dd>
      <dt>Slug</dt>
      <dd><b>{{ $business->slug }}</b></dd>
      <dt>Phone</dt>
      <dd>{{ join(',', $business->phone) }}</dd>
      <dt>Email</dt>
      <dd>{{ join(',', $business->email) }}</dd>
      <dt>Zone</dt>
      <dd>{{ $business->zone_id > 0 ? Zone::find($business->zone_id)->stringPath():'' }}</dd>
      <dt>Cost Estimate</dt>
      <dd>{{ Business::getCostOptions()[$business->cost_estimate] }}</dd>

      <dt>Chain?</dt>
      <dd>{{ count($business->chain) > 0 ? '<a href="'.route('admin.groups.show', $business->chain_id).'">'.$business->chain->name.'</a>' : 'None' }}</dd>
      <dt>Website</dt>
      <dd>{{ $business->website }}</dd>
      <dt>Facebook</dt>
      <dd>{{ $business->facebook }}</dd>
      <dt>Twitter</dt>
      <dd>{{ $business->twitter }}</dd>
      <dt>Google Plus</dt>
      <dd>{{ $business->google }}</dd>
      <dt>Categories</dt>
      <dd>
      @foreach($business->categories as $category)
        <span class="label label-info">
          {{ $category->name }}
        </span>&nbsp; 
      @endforeach

      </dd>

    </dl>
  </div>
  <div class="panel-footer">
    <a title="View Front End"  href="/{{ MazkaraHelper::getLocale().'/'.$business->slug}}" class="btn btn-success">
      <i class="fa fa-eye"></i>
    </a>
  </div>  
</div>
