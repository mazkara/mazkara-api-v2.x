@if(isset($brand))
  {{ Form::model($brand, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'PATCH', 'route' => array('admin.brands.update', $brand->id))) }}
@else
  {{ Form::open(array('route' => 'admin.brands.store', 'files'=>true, 'class' => 'form-horizontal')) }}
@endif
  <div class="form-group">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
    </div>
  </div>


  <div class="form-group">
    {{ Form::label('logo', 'Logo', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::file('logo', array( 'accept'=>"image/*", 'capture'=>'camera')) }}
      @if(isset($brand))
        @if($brand->logo)
          <a href="{{ $brand->logo->image->url() }}" class="lightbox"><img src="{{ $brand->logo->image->url('thumbnail') }}" class="img-thumbnail" /></a>
          {{ Form::checkbox("deletablePhotos[]", $brand->logo->id, false ) }}
          Delete?
        @endif
      @endif

    </div>
  </div>




  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
      @if(isset($brand))
        {{ link_to_route('admin.brands.show', 'Cancel', $brand->id, array('class' => 'btn btn-lg btn-default')) }}
      @endif
    </div>
  </div>
{{ Form::close() }}
