<li class="list-group-item">
  {{ $business['name'] }} {{Form::hidden('businesses[]', $business['id'])}}
  <span class="pull-right">
  {{Form::checkbox('deletableBusinesses[]', $business['id'], '', ['class'=>'deletable','style'=>'display:none'] )}}<a href="javascript:void(0)" class="deletable-link"><i class="fa fa-times"></i></a>
  </span> 
</li>