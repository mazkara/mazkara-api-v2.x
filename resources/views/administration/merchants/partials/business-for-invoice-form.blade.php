  <div class="form-group">
    {{ Form::label('outlets[]', 'Outlet(s):', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      @foreach($merchant->businesses as $business)
        <p>
          <?php $val = false;?>
          @if(isset($invoice))
          @endif
          {{ Form::checkbox('outlets[]', $business->id, $val, array('class'=>'form-control', 'style'=>'width:10px;display:inline-block;height:10px;')) }}
          {{ $business->name }}, {{ $business->zone_cache }}
          <a target="_blank" href="/{{ MazkaraHelper::getLocale().'/'.$business->slug}}" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
          <a target="_blank" href="/partner/{{$business->id}}" class="btn btn-xs btn-primary"><i class="fa fa-user"></i></a>
        </p>
      @endforeach
    </div>
  </div>
