@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
    <hr/>
    <div class="pull-right">
      @if(Auth::user()->hasRole('admin'))
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.merchants.destroy', $merchant->id))) }}
          {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs')) }}
        {{ Form::close() }}
      @endif
      {{ link_to_route('admin.merchants.edit', 'Edit', array($merchant->id), array('class' => 'btn btn-info btn-xs')) }}
    </div>
    <dl class="dl-horizontal">
      <dt>Merchant Name</dt>
      <dd>{{ $merchant->name }}</dd>
      <dt>Email</dt>
      <dd>{{ $merchant->email }}</dd>
      <dt>Location</dt>
      <dd>{{ $merchant->city_name() }}</dd>
      <dt>Sales POC</dt>
      <dd>{{ $merchant->poc_name() }}</dd>
    </dl>
<div class="row">
  <div class="col-md-6">
    <small>BUSINESSES ALLOCATED</small>
    @foreach($merchant->businesses as $business)


      <div class="media">
      <div class="">
        <div class="pull-right">
          @if($business->isCurrentlyAllocatedAVirtualNumber() == true)
            <span title="Allocated Number">
              <i class="fa fa-phone"></i> {{$business->current_virtual_number_allocation_number()}}
              <?php $cn = $business->current_virtual_number_allocation();?>
                <input {{ ( $cn->state !='active' ? ' checked="checked" ' : '') }} data-business="{{$business->id}}" type="checkbox" name="my-checkbox" data-number="{{ $cn->id }}" class=" toggle-button for-v-nums"  />
            </span>
          @else
            <span class="btn-group">
            <a title="No Number" class="btn btn-xs btn-danger">
              <i class="fa fa-phone"></i>  No Number 
            </a> 
            <a href="{{route('admin.virtual_numbers.for.business.allocate', $business->id)}}" class="btn btn-default btn-xs">
              Allocate</a>
            </span>
          @endif


        </div>
        <h4 class="media-heading"><a href="{{route('admin.businesses.show', ['id'=>$business->id])}}">{{$business->name}}</a></h4>

          <a title="History of virtual numbers allocated" href="{{ route('admin.businesses.virtual_numbers.show', array($business->id) ) }}">
            <span><i class="glyphicon glyphicon-calendar"></i> History </span></li>
          </a>
          <a href="{{ route('admin.businesses.call_logs', array($business->id) ) }}">
            <span><i class="glyphicon glyphicon-phone"></i> Call Logs </span></li>
          </a>

       </div>
    </div>
    <hr/>


    @endforeach
  </div>
  <div class="col-md-6">
    <small>USERS ALLOCATED</small>
    @foreach($merchant->users as $user)
      <div class="media">
        <div class="">
          <h4 class="media-heading">{{$user->full_name}}</h4> <small>{{$user->email}}</small>
        </div>
      </div>
      <hr/>
    @endforeach

  </div>

</div>
<p>{{ link_to_route('admin.merchants.index', 'Return to All merchants', null, array('class'=>'btn btn-xs btn-primary')) }}</p>
</div>
</div>
<script type="text/javascript">
  $(function(){
    $('.toggle-button')
              .bootstrapToggle({size:'mini', on: 'Enabled', off: 'Disabled'})
                .change(function(){
                  url = '{{ route('admin.businesses.virtual_numbers.toggle', '__replace__')}}'.replace('__replace__', $(this).data('business'));
                  $.ajax({ url:url })
                });
    $('.toggle-button.disable').bootstrapToggle('disable');

  });
</script>
@stop