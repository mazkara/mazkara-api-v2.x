@extends('layouts.scaffold')

@section('main')

<h1>Show Menu_group</h1>

<p>{{ link_to_route('menu_groups.index', 'Return to All menu_groups', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
				<th>Business_id</th>
				<th>Sort</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $menu_group->name }}}</td>
					<td>{{{ $menu_group->business_id }}}</td>
					<td>{{{ $menu_group->sort }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('menu_groups.destroy', $menu_group->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('menu_groups.edit', 'Edit', array($menu_group->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
