@extends('layouts.scaffold')

@section('main')

<h1>All Menu_groups</h1>

<p>{{ link_to_route('menu_groups.create', 'Add New Menu_group', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($menu_groups->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Business_id</th>
				<th>Sort</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($menu_groups as $menu_group)
				<tr>
					<td>{{{ $menu_group->name }}}</td>
					<td>{{{ $menu_group->business_id }}}</td>
					<td>{{{ $menu_group->sort }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('menu_groups.destroy', $menu_group->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('menu_groups.edit', 'Edit', array($menu_group->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no menu_groups
@endif

@stop
