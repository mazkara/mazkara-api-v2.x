<!DOCTYPE html>
<html lang="en" ng-app="mazkara">
  <head>
  @include('elements.head')
  
  </head>
  <body>
    @include('elements.post-body')
    <div id="fakeloader" class="op50"></div>
    @include('elements.nav')
    <div class="container p0">
    <div id="tf-home" class="text-center" style="">

      <div class="banner-head-content content" >
        <div class="row mr0 ">
          <div class="col-md-8 col-md-offset-2">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class=" border-radius-10">
                  <h1 class="hide-only-mobile text-shadow force-white  notransform " style="font-weight:500; margin-top:150px;font-size:24px; text-transform:uppercase; line-height:1.1;">
                    FIND SALONS, SPAS AND FITNESS VENUES IN {{ strtoupper(MazkaraHelper::getLocale()) }}
                  </h1>
                  
                </div>
              </div>
            </div>

          </div>
        </div>

        <div class="banner-content-form" style="">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="banner-head" style="background-color:transparent;">
                <div class="input-group pt5 box-shadow text-left force-center">
                  <div id="search-box-home-holder" class="inner-addon left-addon" style="position:relative; ">
                    <label class="glyphicon glyphicon-search  " style="padding:13px 10px; z-index:99;" rel="tooltip "></label>
                    {{ Form::text('category_', null, array('class'=>'p10 no-border-radius pointer-cursor pr15 pl15 form-control selectable', 
                                          'id'=>'search-selector', 
                                          'style'=>'height:auto; text-align:left; ', 
                                          'placeholder'=>'Salon, service or category')) }}
                  <div style="width:100%;vertical-align:middle;text-align:center;height:120px;background-color:#fff;top:0px;position:absolute;border: 1px solid #ccc; padding-top:70px;display:none;" class="search-box-preloader">
                    <b>Coming right up!</b> <img src="{{ mzk_assets('assets/indicator.gif')}}" />
                  </div>
                  </div><div id="search-location-holder" class="inner-addon left-addon" style=" ">
                    <label class="glyphicon glyphicon-map-marker " style="padding:13px 10px; z-index:99;"  rel="tooltip "></label>
                    {{ Form::text('zone_', MazkaraHelper::getLocaleLabel(), 
                                array('class'=>'form-control no-border-radius pointer-cursor selectable pr15 pl15 p10  ', 
                                      'id'=>'location-selector', 
                                      'style'=>' height:auto;text-align:left; ', 
                                      'placeholder'=>'Please Type a Location')) }}
                  </div><div class="input-group-btn"><button id="search-bar-button" type="submit" style="height:auto;margin-left:-7px;" class="btn no-border-radius btn-yellow p10">SEARCH</button></div>
                </div>
              </div>
              <form id="search-bar-form" class="hide-only-mobile navbar-left navbar-form" action="/search/businesses"  style="display:none;" method="POST">
                {{ Form::hidden('zone[]', MazkaraHelper::getLocaleId(), array('class'=>'form-control selectable', 'id'=>'location-selector-name'))}}
                {{ Form::hidden('search', null, array('class'=>'', 'id'=>'search-selector-name'))}}
                {{ Form::hidden('category[]', '', array('class'=>'form-control selectable', 'id'=>'location-selector-category'))}}
                {{ Form::hidden('service[]', '', array('class'=>'form-control selectable', 'id'=>'location-selector-service'))}}
              </form>
            </div>
          </div>
          <!-- I WILL GET YOU BASTARD!-->
          <div class="text-center home-categories">
            <?php $cats = App\Models\Category::query()->byLocaleActive()->get();?>
            @foreach($cats as $ii=>$category)
              <div class="home-category" style="display:inline-block;{{ count($cats)!=($ii+1)?';':''}}">
                <a class="white pt10 hover-color-yellow" href="{{ MazkaraHelper::slugCity(null, ['category' => [$category->id]]) }}">
                  <span class="fw500 dpib  p5 pr0 {{$category->slug=='spa'?'pb5 pt0':'pb0 pt5'}}" style="display:inline-block;vertical-align:middle;"><i class=" {{ mzk_slug_to_icon_css($category->slug) }}" ></i></span>
                  <span class="dpib">
                    <span class="dpib   pl0">
                      {{ str_replace(' ', '&nbsp;', strtoupper(str_plural($category->name)))}}
                    </span>
                  </span>
                </a>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <!--<div id="tf-home-text-overlay" class="text-center  " style="">
    </div>-->
    <div class="show-only-mobile _bg-lite-gray" style="height:20px"></div>
    <!-- <div class="hide-only-mobile bg-lite-gray " style="height:50px"></div>-->
    <!-- <div class="hide-only-mobile bg-yellow pb20 mb25 text-center pt20 fs125" style="z-index:999">
      <a href="{{MazkaraHelper::slugCity(null, ['offers'=>'true'])}}">Click here to Check out Mazkara Offers for Salons and Spas</a>
    </div> -->
    <div class="container show-only-mobile">
      <?php $services = MazkaraHelper::getServicesHierarchyList();?>
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-warning">
          <div class="panel-heading" >
            <h4 class="panel-title">
              <a class="dpb" href="{{ route('posts.index.all')}}">
                <span class="pull-left"><i class="pr10 glyphicon glyphicon-book"></i></span>
                Stories
              </a>
            </h4>
          </div>
        </div>
        @foreach($services as $parent)
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{{$parent['id']}}">
              <h4 class="panel-title">
                <a class="dpb" data-toggle="collapse" role="button" data-parent="#accordion" href="#collapse{{$parent['id']}}" aria-expanded="true" aria-controls="collapse{{$parent['id']}}">
                  <span class="pull-left"><i class="pr10 glyph-icon iconz-{{$parent['slug']}}"></i></span>
                  {{$parent['name']}} 
                  <i class="indicator glyphicon fa fa-chevron-down  pull-right"></i>
                </a>
              </h4>
            </div>
            <div id="collapse{{$parent['id']}}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{$parent['id']}}">
              <div class="panel-body">
                <ul class="list-group">
                  @foreach($parent['children'] as $child)
                    <li class="list-group-item">
                      <a class="fs80" href="{{ MazkaraHelper::slugCity(null, ['service'=>[$child['id']]]) }}">
                        {{ $child['name'] }}
                      </a>
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
    @yield('content')
    </div>
    @include('elements.footer')
    <script type="text/javascript">
$(function(){
function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('fa-chevron-down fa-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
})
    </script>
  </body>
</html>