<!DOCTYPE html>
<html lang="en" ng-app="mazkara">
  <head>
  @include('elements.head')
  <style type="text/css">
  </style>
  </head>
  <body>
    @include('elements.post-body')
    <div id="fakeloader" class="op50"></div>
    @include('elements.nav-alt')
    
    <div class="container bg-white p0">
    @yield('content')  
    </div>
    
    @include('elements.footer')
  </body>
</html>