var elixir = require('laravel-elixir');

var paths = {
  'bower': './resources/assets/components/',
  'stylesheets': './resources/assets/stylesheets/',
  'javascripts': './resources/assets/javascripts/'
}

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([  paths.stylesheets + 'fonts.css',
                  paths.bower + 'yamm3/yamm/yamm.css',
                  paths.stylesheets + 'responsive.css',
                  paths.stylesheets + 'services.css',
                  paths.stylesheets + 'Glyphter.css',
                  paths.stylesheets + 'flaticon.css',
                  paths.stylesheets + 'highlights.css',
                  paths.stylesheets + 'parent-ids.css',
                  paths.stylesheets + 'pricicon.css',
                  paths.bower + 'magnific-popup/dist/magnific-popup.css',
                  paths.bower + 'bootstrap-select/dist/css/bootstrap-select.css',
                  paths.bower + 'fakeloader/fakeLoader.css',
                  paths.bower + 'bootstrap-star-rating/css/star-rating.css',
                  paths.bower + 'summernote/dist/summernote.css',
                  paths.bower + 'bootstrap3-dialog/dist/css/bootstrap-dialog.css',
                  paths.bower + 'jquery.smartbanner/jquery.smartbanner.css',
                  paths.stylesheets + 'style.css',

                 ],'public/css/application.css');

    mix.styles([  paths.stylesheets + 'fonts.css',
                  paths.bower + 'bootstrap/dist/css/bootstrap.css',
                  paths.bower + 'yamm3/yamm/yamm.css',
                  paths.stylesheets + 'responsive.css',
                  paths.bower + 'jquery-ui/themes/flick/jquery-ui.min.css',
                  paths.bower + 'components-font-awesome/css/font-awesome.css',
                  paths.stylesheets + 'services.css',
                  paths.stylesheets + 'parent-ids.css',
                  paths.stylesheets + 'pricicon.css',
                  paths.bower + 'magnific-popup/dist/magnific-popup.css',
                  paths.bower + 'fakeloader/fakeLoader.css',
                  paths.bower + 'bootstrap-star-rating/css/star-rating.css',
                  paths.stylesheets + 'style.css',

                 ],'public/css/application.home.css');

    mix.styles([  paths.bower + 'metisMenu/dist/metisMenu.css',
                  paths.bower + 'morrisjs/morris.css',
                  paths.stylesheets + 'main.css',
                  paths.bower + 'jquery-bonsai/jquery.bonsai.css',
                  paths.bower + 'jt.timepicker/jquery.timepicker.css',
                  paths.bower + 'jt.timepicker/lib/bootstrap-datepicker.css',
                  paths.bower + 'Jcrop/css/jquery.Jcrop.min.css',
                  paths.bower + 'bootstrap-toggle/css/bootstrap-toggle.css',
                  paths.bower + 'bootstrap-select/dist/css/bootstrap-select.css',
                  paths.bower + 'ekko-lightbox/dist/ekko-lightbox.min.css',
                  paths.bower + 'x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',

                  paths.bower + 'bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css',
                  paths.bower + 'country-select-js/build/css/countrySelect.css',
                  paths.bower + 'summernote/dist/summernote.css',
                  paths.stylesheets + 'admin.css',

                 ],'public/css/dashboard.css');

    mix.styles([  paths.stylesheets + 'fonts-admin.css',
                  paths.bower + 'components-font-awesome/css/font-awesome.css',
                  paths.bower + 'bootstrap/dist/css/bootstrap.css',
                  paths.bower + 'jt.timepicker/jquery.timepicker.css',
                  paths.bower + 'jt.timepicker/lib/bootstrap-datepicker.css',
                  paths.bower + 'bootstrap-select/dist/css/bootstrap-select.css',
                  paths.bower + 'bootstrap3-dialog/dist/css/bootstrap-dialog.css',
                  paths.bower + 'magnific-popup/dist/magnific-popup.css',
                  paths.stylesheets + 'mvpready-admin.css',
                  paths.stylesheets + 'admin.css',

                 ],'public/css/client.css');

    mix.scripts([ paths.javascripts + 'app/app.js',
                  paths.javascripts + 'app/search.location.js',

                 ],'public/js/application.footer.js');

    mix.scripts([
                  paths.bower + 'jquery/dist/jquery.min.js',
                  paths.bower + 'jquery-ui/jquery-ui.min.js',
                  paths.bower + 'jquery.fblogin/dist/jquery.fblogin.js',
                  paths.bower + 'bootstrap/dist/js/bootstrap.min.js',
                  paths.bower + 'bootstrap/js/collapse.js',
                  paths.bower + 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.js',
                  paths.bower + 'magnific-popup/dist/jquery.magnific-popup.js',
                  paths.bower + 'bootbox.js/bootbox.js',
                  paths.bower + 'fakeloader/fakeLoader.min.js',
                  paths.bower + 'jscroll/jquery.jscroll.js',
                  paths.bower + 'string/lib/string.min.js',
                  paths.bower + 'bootstrap-star-rating/js/star-rating.js',
                  paths.bower + 'bootstrap3-dialog/dist/js/bootstrap-dialog.js',
                  paths.bower + 'jquery.imgpreload/jquery.imgpreload.min.js',
                  paths.bower + 'bootstrap-select/dist/js/bootstrap-select.js',
                  paths.bower + 'jquery-text-counter/textcounter.min.js',
                  paths.bower + 'summernote/dist/summernote.js',
                  paths.bower + 'jquery.smartbanner/jquery.smartbanner.js',
                  paths.bower + 'html5-history-api/history.js',
                  paths.javascripts + 'main.js'
                ],'public/js/application.js');

    mix.scripts([
                  paths.bower + 'jquery/dist/jquery.min.js',
                  paths.bower + 'jquery-ui/jquery-ui.min.js',
                  paths.bower + 'jquery.fblogin/dist/jquery.fblogin.js',
                  paths.bower + 'bootstrap/dist/js/bootstrap.min.js',
                  paths.bower + 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.js',
                  paths.bower + 'magnific-popup/dist/jquery.magnific-popup.min.js',
                  paths.bower + 'gmaps/gmaps.js',
                  paths.bower + 'bootbox.js/bootbox.js',
                  paths.bower + 'bootstrap3-dialog/dist/js/bootstrap-dialog.js',
                  paths.bower + 'fakeloader/fakeLoader.min.js',
                  paths.bower + 'bootstrap-star-rating/js/star-rating.js',
                  paths.bower + 'jquery-character-counter/jquery.charactercounter.js',
                  paths.bower + 'jscroll/jquery.jscroll.js',
                  paths.bower + 'underscore/underscore.js',
                  paths.bower + 'string/lib/string.min.js',
                  paths.bower + 'bootstrap-select/dist/js/bootstrap-select.js',
                  paths.bower + 'jquery.imgpreload/jquery.imgpreload.min.js',
                  paths.bower + 'jquery-text-counter/textcounter.min.js',
                  paths.bower + 'jquery.smartbanner/jquery.smartbanner.js',
                  paths.javascripts + 'main.js'
                ],'public/js/application.with.map.ratings.gallery.js');

    mix.scripts([ paths.bower + 'jquery/dist/jquery.min.js',
                  paths.bower + 'jquery-ui/jquery-ui.min.js',
                  paths.bower + 'jQuery-Geolocation/jquery.geolocation.js',
                  paths.bower + 'jQuery-Geolocation/jquery.geolocation.js',
                  paths.bower + 'jquery-locationpicker-plugin/dist/locationpicker.jquery.js',
                  paths.javascripts + 'mapjs.js',
                  paths.bower + 'jquery-bonsai/jquery.bonsai.js',
                  paths.bower + 'jquery-qubit/jquery.qubit.js',
                  paths.bower + 'bootstrap/dist/js/bootstrap.min.js',
                  paths.bower + 'string/lib/string.min.js',
                  paths.bower + 'jt.timepicker/jquery.timepicker.min.js',
                  paths.bower + 'jt.timepicker/lib/bootstrap-datepicker.js',
                  paths.bower + 'Jcrop/js/jquery.Jcrop.min.js',
                  paths.bower + 'ekko-lightbox/dist/ekko-lightbox.min.js',
                  paths.bower + 'bootbox.js/bootbox.js',
                  paths.bower + 'bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js',
                  paths.bower + 'bootstrap-select/dist/js/bootstrap-select.js',
                  paths.bower + 'magnific-popup/dist/jquery.magnific-popup.js',
                  paths.bower + 'bootstrap3-dialog/dist/js/bootstrap-dialog.js',
                  paths.bower + 'flot/excanvas.min.js',
                  paths.bower + 'flot/jquery.flot.js',
                  paths.bower + 'flot/jquery.flot.pie.js',
                  paths.bower + 'flot/jquery.flot.resize.js',
                  paths.bower + 'flot/jquery.flot.time.js',
                  paths.bower + 'flot.tooltip/js/jquery.flot.tooltip.js',
                  paths.javascripts + 'mvpready-core.js',
                  paths.javascripts + 'mvpready-helpers.js',
                  paths.javascripts + 'app/client.js'
                ],'public/js/client.js');

    mix.scripts([ 
                  paths.bower + 'jquery/dist/jquery.min.js',
                  paths.bower + 'jquery-ui/jquery-ui.min.js',
                  paths.bower + 'bootstrap/dist/js/bootstrap.min.js',
                  paths.bower + 'jQuery-Geolocation/jquery.geolocation.js',
                  paths.bower + 'jQuery-Geolocation/jquery.geolocation.js',
                  paths.bower + 'jquery-locationpicker-plugin/dist/locationpicker.jquery.js',
                  paths.bower + 'jquery-bonsai/jquery.bonsai.js',
                  paths.bower + 'jquery-qubit/jquery.qubit.js',
                  paths.bower + 'string/lib/string.min.js',
                  paths.bower + 'jt.timepicker/jquery.timepicker.min.js',
                  paths.bower + 'jt.timepicker/lib/bootstrap-datepicker.js',
                  paths.bower + 'Jcrop/js/jquery.Jcrop.min.js',
                  //paths.bower + 'handlebars/handlebars.js',
                  paths.bower + 'ekko-lightbox/dist/ekko-lightbox.min.js',
                  paths.bower + 'bootbox.js/bootbox.js',
                  paths.bower + 'bootstrap-select/dist/js/bootstrap-select.js',
                  paths.bower + 'bootstrap-toggle/js/bootstrap-toggle.js',
                  paths.bower + 'bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js',
                  paths.bower + 'country-select-js/build/js/countrySelect.js',
                  paths.bower + 'jquery-stupid-table/stupidtable.js',
                  paths.bower + 'summernote/dist/summernote.js',
                  paths.bower + 'x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js',
                  paths.bower + 'raphael/raphael-min.js',
                  paths.bower + 'morrisjs/morris.js',
                  paths.javascripts + 'mapjs.js'

                ],'public/js/dashboard.js');


});
