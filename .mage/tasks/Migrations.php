<?php
namespace Task;

use Mage\Task\AbstractTask;

class Migrations extends AbstractTask
{
  public function getName(){
    return 'Running migrations';
  }

  public function run(){

    $env = $this->getParameter('env', 'production'); 
    $folders = array(
                      'demo'=>'sandbox.mazkara.com',
                      'production'=>'api.mazkara.com',
                      'test'=>'sandbox.mazkara.com'
                    );
    $folder = $folders[$env];
    echo "Migrating remaining migrations on ".$env.' ... ';

    $command = 'cd /var/www/'.$folder.'/current; php artisan migrate;';
    $result = $this->runCommandRemote($command);

    return $result;
  }
}