<?php
namespace Task;

use Mage\Task\AbstractTask;

class Clearcache extends AbstractTask
{
  public function getName(){
    return 'Clearing Views and Cache';
  }

  public function run(){
    $env = $this->getParameter('env', 'production'); 
    $folders = array(
                      'demo'=>'sandbox.mazkara.com',
                      'production'=>'api.mazkara.com',
                      'test'=>'sandbox.mazkara.com'
                    );
    $folder = $folders[$env];
    echo 'Clearing cache on '.$folder.' in '.$env.' environment ... ';
    $command = 'cd /var/www/'.$folder.'/current; sudo php artisan cache:clear;';
    $result = $this->runCommandRemote($command);
    
    //\Session::forget('mazkara.city');

    return $result;
  }
}