<?php
namespace Task;

use Mage\Task\AbstractTask;

class Sethtaccess extends AbstractTask
{
  public function getName(){
    return 'Making 301 redirects to www.mazkara.com';
  }

  public function run(){

    $command = "echo 'RewriteCond %{HTTP_HOST} !^www\.mazkara\.com$ [NC]' >> /var/www/mazkara.com/current/public/.htaccess";
    $result = $this->runCommandRemote($command);

    $command = "echo 'RewriteRule ^(.*)$ http://www.mazkara.com [R=301,L]' >> /var/www/mazkara.com/current/public/.htaccess";
    $result = $this->runCommandRemote($command);
    //\Session::forget('mazkara.city');
    $command = "echo '<IfModule mod_expires.c>' >> /var/www/mazkara.com/current/public/.htaccess";
    $result = $this->runCommandRemote($command);

    $command = "echo 'ExpiresActive On' >> /var/www/mazkara.com/current/public/.htaccess";
    $result = $this->runCommandRemote($command);
    $command = "echo 'ExpiresByType image/jpg \"access 1 day\"' >> /var/www/mazkara.com/current/public/.htaccess";
    $result = $this->runCommandRemote($command);
    $command = "echo 'ExpiresByType image/jpeg \"access 1 day\"' >> /var/www/mazkara.com/current/public/.htaccess";
    $result = $this->runCommandRemote($command);

    $command = "echo 'ExpiresByType text/css \"access 1 day\"' >> /var/www/mazkara.com/current/public/.htaccess";
    $result = $this->runCommandRemote($command);
    $command = "echo '</IfModule>' >> /var/www/mazkara.com/current/public/.htaccess";
    $result = $this->runCommandRemote($command);




## EXPIRES CACHING ##


    return $result;
  }
}