<?php
namespace App\Models;

use Eloquent;

class Invoice_item extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'desc' => 'required',
		'price' => 'required',
		'qty' => 'required',
		'total' => 'required',
		'invoice_id' => 'required',
		'state' => 'required'
	);

  public function call_logs(){
    return $this->hasMany('App\Models\Call_log');
  }
}
