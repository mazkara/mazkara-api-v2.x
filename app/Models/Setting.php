<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

class Setting extends Eloquent
{
  //
  public function zone(){
    return $this->belongsTo('App\Models\Zone');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale ? $locale : mzk_get_localeID();
    return $query->where('zone_id', '=', $locale);
  }

}
