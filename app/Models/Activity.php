<?php
namespace App\Models;

use Eloquent;
use App\Models\User;

class Activity extends Eloquent{
  protected $guarded = array();
  protected $morphClass = 'Activity';

  protected $classes = [
    'Activity'=>'App\Models\Activity',
    'Business'=>'App\Models\Business',
    'Category'=>'App\Models\Category',
    'Highlight'=>'App\Models\Highlight',
    'Photo'=>'App\Models\Photo',
    'Post'=>'App\Models\Post',
    'Selfie'=>'App\Models\Selfie',
    'Video'=>'App\Models\Video',
    'Favorite'=>'App\Models\Favorite',
    'Group'=>'App\Models\Group',
    'Review'=>'App\Models\Review',
    'Service'=>'App\Models\Service',
    'User'=>'App\Models\User',
    'Zone'=>'App\Models\Zone',
  ];

  public function getItemableTypeAttribute($cls) {
      
      $cls = ucwords($cls);
      // to make sure this returns value from the array
      return array_get($this->classes, $cls, $cls);
      // which is always safe, because new 'class'
      // will work just the same as new 'Class'
  }



  public static $rules = array(
    'actor' => 'required',
    'verb' => 'required',
    'itemable_id' => 'required',
    'itemable_type' => 'required'
  );

  public function itemable(){
    return $this->morphTo();
  }

  public function user(){
    return $this->belongsTo('App\Models\User', 'user_id');
  }

  public function scopeByUserIds($query, $user_ids){
    return $query->whereIn('user_id',  $user_ids);
  }
  
  public function getMetaAttribute($value){
    return (array)json_decode($value);
  }

  public function setMetaAttribute($value){
    $this->attributes['Meta'] = json_encode($value);
  }

}
