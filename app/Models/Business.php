<?php
namespace App\Models;


use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use LaravelArdent\Ardent\Ardent;
//use Meta;
use App\Models\Call_log;
use App\Models\Category;
use App\Models\Check_in;
use App\Models\Deal;
use App\Models\Favorite;
use App\Models\Group;
use App\Models\Highlight;
use App\Models\Merchant;
use App\Models\Photo;
use App\Models\Review;
use App\Models\Service;
use App\Models\Slug;
use App\Models\Timing;
use App\Models\User;
use App\Models\Virtual_number_allocation;
use App\Models\Zone;
use App\Models\Service_item;

use Config, Request, Response, File, DB;

class Business extends Ardent implements SluggableInterface{
  //public $timestamps  = false;
  protected $blameable = array('created', 'updated', 'deleted');
  use SluggableTrait;
  protected $morphClass = 'Business';

  public static $niblet_fields = array('id', 'name', 'slug', 'zone_cache', 'meta', 'rating_average' );

  public function countCaches() {

  }

  public function setFeatured(){
    $this->is_featured= 1;
  }

  public function setUnFeatured(){
    $this->is_featured= 0;
  }



  public function getTimezone(){
    return ($this->zone ? $this->zone->time_zone : 'Asia/Dubai');
  }

  public function isUAE(){
    return ($this->city_id == 1 ? true:false);
  }


  public static function getTypes(){
    return ['business'=>'Business - with location', 'home-service'=>'Home Service Based Business'];
  }

  public function isHomeService(){
    return $this->type == 'home-service' ? true : false;
  }

  public function canHaveAddress(){
    return $this->type == 'business'?true:false;
  }

  public static function getStockPhotos(){
    $p = [];
    for($i=1;$i<48;$i++){
      $p[] = $i;
    }
    return $p;
  }

  public function useStockImage(){
    return $this->has_stock_cover_image > 0 ? true : false;
  }
  public function doesNotUseStockImage(){
    return $this->has_stock_cover_image > 0 ? false : true;
  }

  public function scopeHasNoStockImage($query){
    return $query->where('has_stock_cover_image', '=', '0');
  }

  public function scopeIsFeatured($query){
    return $query->where('is_featured', '=', '1');
  }

  public function scopeIsNotFeatured($query){
    return $query->where('is_featured', '=', '0');
  }

  public function isSpa(){
    return in_array(2, $this->categories->lists('id', 'id')->all());
  }

  public function isSalon(){
    return in_array(1, $this->categories->lists('id', 'id')->all());
  }

  public function isMassage(){
    return in_array(4, $this->categories->lists('id', 'id')->all());
  }

  public function isFitness(){
    return in_array(5, $this->categories->lists('id', 'id')->all());
  }

  public function isClinic(){
    $lst = $this->categories->lists('id', 'id')->all();
    return in_array(6, $lst) || in_array(7, $lst);
  }

  public function isForLadies(){
    $lst = $this->highlights()->get()->lists('id', 'id')->all();
    return in_array(4, $lst);
  }

  public function hasService($service_id){
    $lst = $this->services->lists('id', 'id')->all();
    return in_array($service_id, $lst);
  }


  public function isForGents(){
    $lst = $this->highlights->lists('id', 'id')->all();
    return in_array(6, $lst);
  }

  public function getStockImage(){
    $url = 'https://s3.amazonaws.com/mazkaracdn/stock/';
    $url.=$this->has_stock_cover_image.'.jpg';

    return $url;
  }

  public function beforeSave() {
    $this->setCity();
  }

  public function afterCreate() {
    $this->updateCategoriesCount();
    $this->updateZoneCache();
    $this->updateCategories();
    $this->updateZone();
    $this->updateCity();
  }

  public function afterSave() {
    $this->updateCategories();
  }

  public function updateAllMetaCache(){
    $this->updateMetaCache();
  }

  public function updateAllCache(){

    $this->updateMetaCache();
    //$this->updateServicesCount();
    //$this->updateCategoriesCount();
    //$this->updateHighlightsCount();
    //$this->updateZoneCache();
    //$this->updateReviewCount();
    //$this->updateImageCount();
    //$this->updateRateCardCount();
    //$this->updateCheckInCount();
    //$this->updateFavoriteCount();
    //$this->updateAverageRating();
    //$this->updateRatingAndReviewsCount();
    //$this->updateCity();

    $this->updateMetaOffers();
  }

  public function resetMetaForApi(){
    $vs = ['services', 'categories', 'highlights'];
    foreach($vs as $v){
      $m = $this->getMeta($v);
      $result = [];
      if(!$m){
        continue;
      }
      foreach($m as $index=>$val){
        $result[] = ['id'=>$index, 'name'=> $val]; 
      }
      $this->addMeta($v, $result);
    }
  }

  public function updateMetaOffers(){
    $offers = $this->offers()->onlyActive()->get()->toArray();


    $this->addMeta('offers', $offers);
    $this->active_offers_count = count($this->offers()->onlyActive()->get());
    $this->save();
  }

  public function setCity(){
    if($this->zone_id){
      $z = Zone::find($this->zone_id);
      if(isset($z->id)){
        if($z->parent_id){
          $this->city_id = $z->ancestors()->first()->id;
        }
      }
    }
  }

  public function updateCity(){
    $this->setCity();
    $this->save();
  }

  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function markComments(){
    foreach($this->comments()->notViewed() as $comment){
      $comment->markViewed();
    }
  }


  public function getSeoTitle(){
    return $this->name.' in '.$this->zone_cache;
  }

  public function getSeoDesc(){

    $categories = [];
    foreach($this->categories as $ii=>$vv){
      $categories[$ii] = MazkaraHelper::getPluralName($vv->name);
    }

    $str = join(',', array_filter($categories)).' in '.$this->zone_cache;
    $str.=' for services in '.(join(',', $this->getMeta('services')));
    return $str;
  }

  public function addMeta($item, $value){
    $meta = $this->meta;
    $meta[$item] = $value;
    $this->meta = $meta;
  }

  public function getMeta($item, $default = false){
    $meta = $this->meta;
    return isset($meta[$item])? (is_object($meta[$item])?(array)$meta[$item]:$meta[$item]):$default;
  }

  public function updateDealsCount(){
    $this->total_deals_count = count($this->deals);
    $this->save();
  }

  public function updatePackagesCount(){
    $this->total_packages_count = count($this->packages);
    $this->active_packages_count = count($this->packages);
    $this->save();
  }


  public function updateActiveDealsCount(){
    $this->active_deals_count = $this->deals()->hasStatus('active')->count();
    $this->save();
  }

  public function updateServicesCount(){
    $services = $this->services;
    $this->services_count = count($services);
    $this->addMeta('services', $this->services ? $this->services->lists('name', 'id')->all():[]);
    $this->save();
  }

  public function updateCategoriesCount(){
    $categories = $this->categories;
    $this->categories_count = count($categories);
    $this->addMeta('categories', $this->categories ? $this->categories->lists('name', 'id')->all():[]);
    $this->save();
  }

  public function updateHighlightsCount(){
    $highlights = $this->highlights;
    $this->highlights_count = count($highlights);
    $this->addMeta('highlights', $this->highlights ? $this->highlights->lists('name', 'id')->all():[]);
    $this->save();
  }

  public function updateTimingsCount(){
    $this->timings_count = count($this->timings);
    $this->save();
  }

  public function updateZoneCache(){
    $zone = $this->zone;
    $this->zone_cache = $this->zone ? $this->zone->name : '';
    $this->addMeta('zone', $this->zone ? $this->zone->toArray():[]);
    $this->save();
  }

  public function updateZone(){
    $zone = $this->zone;// ? $this->zone : false;
    if($zone){
      $zone->updateBusinessesCount();
    }
  }

  public function updateCategories(){
    foreach($this->categories as $category){

      $category->updateBusinessesCount($this->city_id);
    }
  }

  public function updateHighlights(){
    foreach($this->highlights as $highlight){
      $highlight->updateBusinessesCount($this->city_id);
    }
  }

  public function updateServices(){
    foreach($this->services as $service){
      $service->updateBusinessesCount($this->city_id);
    }
  }

  public function updateRatingAndReviewsCount(){
    $this->updateReviewCount();
    $this->updateRatingsCount();
    $this->updateAverageRating();
    $this->setPopularity();
  }

  public function incrementViewCount(){
    //DB::statement(DB::raw('CALL mzkIncrementCounter("Ad", '.$this->id.', "'.Counter::PAGE_VIEWS.'");'));
    $c = Counter::firstOrCreate(['countable_type'=>'Business', 'countable_id'=>$this->id, 'type'=>Counter::PAGE_VIEWS, 'dated'=>date('Y-m-d')]);
    $c->incrementViewCount();
    $c->save();

  }

  public function incrementCallsCount(){
    //$c = Counter::select(['id', 'views'])->byBusiness($this->id)->byType(Counter::CALL_VIEWS)->today()->get()->first();
    $c = Counter::firstOrCreate(['countable_type'=>'Business', 'countable_id'=>$this->id, 'type'=>Counter::CALL_VIEWS, 'dated'=>date('Y-m-d')]);
    $make_increment = Mzk_meta::addCallEntry( ['name'=>$this->name, 'zone'=>$this->zone_cache, 'time'=>time(), 'ip'=>Request::getClientIp()] );


    if($make_increment != false){
      $c->incrementViewCount();
      $c->save();
    }


    $url = storage_path().'/exports/call-tracks-'.date('Y-m-d').'.csv';
    $fields = [ 'business_id', 'name', 'time', 'ip' ];

    $data = [$this->id, $this->name.','.$this->zone_cache, date('Y-m-d h:m'), Request::getClientIp()];

    if (!File::exists($url)){
      $fp = fopen($url, 'w+');//Excel::create($url, function($excel) {});
      fputcsv($fp, $fields);      
    }else{
      $fp = fopen($url, 'a');//Excel::create($url, function($excel) {});

    }
    fputcsv($fp, $data);      

    fclose($fp);


  }


  public function updateReviewCount(){
    $this->reviews_count = $this->reviews()->isComplete()->count();
    $this->save();
  }

  public function updateRatingsCount(){
    $this->total_ratings_count = $this->reviews()->isIncomplete()->count();
    $this->save();
  }

  public function updateAverageRating(){
    $this->rating_average = (float)$this->reviews()->avg('rating');
    $this->save();
  }

  public function updateImageCount(){
    $photos = $this->photos()->get();

    $this->image_count = count($photos);
    if($photos->count() > 0){
      $photo = $photos->first();
      $this->addMeta('photo', $photo->image->url('thumbnail'));
      $this->addMeta('photo-medium', $photo->image->url('medium'));
    }

    $this->save();
  }

  public function updateRateCardCount(){
    $this->rate_card_count = count($this->rateCards);
    $this->save();
  }

  public function updateCheckInCount(){
    $this->checkins_count = count($this->check_ins);
    $this->save();
  }

  public function updateFavoriteCount(){
    $this->favorites_count = count($this->favourites);
    $this->save();
  }

  public function getTrimmedName($l = 24, $tail = '...'){
    if(strlen($this->name)>($l+3)){
      return trim(substr($this->name, 0, $l)).$tail;
    }

    return $this->name;
  }


  public function canCurrentUserAccessClientDashboard($user){
    if($user->hasRole('client')):
      if($user->canManageBusiness($this->id)):
        return true;
      endif;
    elseif($user->can('access_client_dashboards')):
      return true;
    endif;
    return false;
  }


  public function getSlugnameAttribute(){
    // check first is the parent zone for this business set
    // in the slug exceptions config

    $cities = Config::get('mazkara.city-region-slugs');
    $current_city_slug = $this->city?$this->city->slug:'';
    if(isset($cities[$this->city->slug])){
      return $this->$cities[$this->city->slug]();
    }
    return $this->name.' '.$this->getZoneName();
  }

  public function slugWithCityRegion(){
    return $this->name.' '.$this->getZoneName().' '.$this->getLevelOneZoneName();
  }

  public function getShownameAttribute(){
    return $this->name.' ('.$this->getZoneName().')';
  }

  protected $sluggable = array(
    'build_from' => 'slugname',
    'save_to'    => 'slug',
    'max_length' => 200,
    'unique'     => true,      
  );

  protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		'description' => '',
		//'phone' => 'required',
		'email' => '',
		'website' => '',
		'zone_id' => 'required',
		'geolocation_city' => '',
		'geolocation_state' => '',
		'geolocation_country' => '',
		'geolocation_address' => '',
		'geolocation_latitude' => '',
		'geolocation_longitude' => '',
		'active' => 'required'
	);

  public static $basic_rules = array(
    'name' => 'required',
    'description' => '',
    'phone' => 'required',
    'email' => '',
    'website' => '',
    'active' => 'required'
  );

  public function counts(){
    return $this->morphMany('Counter', 'countable');
  }

  public function brands(){
    return $this->belongsToMany('App\Models\Brand');
  }

  public function canAddMoreOffers(){
    if($this->offers()->byState('active')->count() >= 4){
      return false;
    }

    return true;
  }



  public function offers(){
    return $this->hasMany('App\Models\Offer');
  }

  public function active_offers(){
    return $this->hasMany('App\Models\Offer')->onlyActive();//where('offers.state', '=', 'active');
  }

  public function serviceItems(){
    return $this->hasMany('App\Models\Service_item');
  }

  public function comboItems(){
    return $this->hasMany('App\Models\Combo_item');
  }

  public function accumulatedReviewsCount(){
    return $this->reviews_count + $this->total_ratings_count;
  }

  public function isCurrentlyAllocatedAVirtualNumber(){
    return  $this->allocated_a_current_virtual_number() ? true : false;
  }



  public function currently_allocated_number(){
    $num = '';
    if($this->isCurrentlyAllocatedAVirtualNumber()){
      $num = $this->current_virtual_number_allocation()->body;
    }

    return $num;
  }


  // SCOPES
  public function shares(){
    return $this->morphMany('Share', 'sharable');
  }


  public function follows(){
    return $this->morphMany('Favorite', 'favorable');
  }

  public function comments(){
    return $this->morphMany('Comment', 'commentable');
  }

  public function scopeTakeRandom($query, $size=1){
    return $query->orderBy(DB::raw('RAND()'))->take($size);
  }

  public function scopeNotFollowedBy($query, $user_id){
    return $query->follows()->where('favorable_type', '=', 'User')->where('user_id', '<>', $user_id);
  }

  public function scopeAllocatableToMerchants($query){
    return $query->has('merchants', '=', 0);
  }

  public function hasDescription(){
    if(strlen(trim($this->description))>0){
      return true;
    }

    return false;
  }


  public function scopeIsDisplayable($query, $show_all = false){
    if($show_all == true){
      return $query;
    }
    $states = ['active', 'temporarily.closed', 'opening.soon'];
    return $query->whereIn('active', $states);
  }

  public function scopeActiveState($query, $state){
    return $query->where('active', '=', $state);
  }

  public function scopeOnlyActive($query){
    return $query->where('active', '=', 'active' );
  }

  // Scope for Categories

  public function scopeHasCategories($query){
    return $query->where('categories_count', '>', '0');
    //deprecated
    $businesses_with_categories = \DB::table('business_category')->groupBy('business_id')->lists('business_id');
    return $query->whereIn('businesses.id', $businesses_with_categories);
  }

  public function scopeHasNoCategories($query){
    return $query->where('categories_count', '=', '0');
    //deprecated
    $businesses_with_categories = \DB::table('business_category')->groupBy('business_id')->lists('business_id');
    return $query->whereNotIn('businesses.id', $businesses_with_categories);
  }

  public function scopeIsChainable($query){ // are not a chain of anyone
    return $query->where('chain_id', '=', '0');
  }

  //Scope for deals
  public function scopeHasActivePackages($query){
    return $query->where('total_packages_count', '>', '0');
  }

  public function scopeHasActiveOffers($query){
    return $query->where('active_offers_count', '>', '0');
  }

  public function scopeHasActiveDeals($query){
    return $query->where('active_deals_count', '>', '0');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }


  public function scopeHasDeals($query, $state = false){
    if($state){
      $businesses_with_deals = \DB::table('deals')->where('type', '=', $state)->lists('business_id');
    }else{
      $businesses_with_deals = \DB::table('deals')->lists('business_id');
    }
    return $query->whereIn('businesses.id', $businesses_with_deals);
  }

  public function scopeByDealsStatus($query, $state = false){
    if($state){
      $businesses_with_deals = \DB::table('deals')->where('status', '=', $state)->lists('business_id');
    }else{
      $businesses_with_deals = \DB::table('deals')->lists('business_id');
    }
    return $query->whereIn('businesses.id', $businesses_with_deals);
  }

  // Scope for Services

  public function scopeHasServices($query){
    return $query->where('services_count', '>', '0');
    //deprecated

    $businesses_with_services = \DB::table('business_service')->lists('business_id');
    return $query->whereIn('businesses.id', $businesses_with_services);
  }

  public function scopeHasNoServices($query){
    return $query->where('services_count', '=', '0');
    //deprecated
    $businesses_with_services = \DB::table('business_service')->lists('business_id');
    return $query->whereNotIn('businesses.id', $businesses_with_services);
  }

  // Scope for Photos

  public function scopeHasPhotos($query){
    return $query->where('image_count', '>', '0');
    //deprecated

    $businesses_with_photos = \DB::table('photos')->select('imageable_id')
                                      ->where('imageable_type', '=', 'Business')
                                      ->where('type', '=', 'image')
                                      ->lists('imageable_id');
    return $query->whereIn('businesses.id', $businesses_with_photos);
  }

  public function scopeHasNoPhotos($query){
    return $query->where('image_count', '=', '0');
    //deprecated

    $businesses_with_photos = \DB::table('photos')->select('imageable_id')
                                      ->where('imageable_type', '=', 'Business')
                                      ->where('type', '=', 'image')
                                      ->lists('imageable_id');
    return $query->whereNotIn('businesses.id', $businesses_with_photos);
  }

  // Scope for Rate Cards

  public function scopeHasRateCards($query){
    return $query->where('rate_card_count', '>', '0');
    //deprecated

    $businesses_with_rate_cards = \DB::table('photos')->select('imageable_id')
                                        ->where('imageable_type', '=', 'Business')
                                        ->where('type', '=', 'rate-card')->lists('imageable_id');
    return $query->whereIn('businesses.id', $businesses_with_rate_cards);
  }

  public function scopeHasNoRateCards($query){
    return $query->where('rate_card_count', '=', '0');
    //deprecated

    $businesses_with_rate_cards = \DB::table('photos')->select('imageable_id')
                                        ->where('imageable_type', '=', 'Business')
                                        ->where('type', '=', 'rate-card')->lists('imageable_id');
    return $query->whereNotIn('businesses.id', $businesses_with_rate_cards);
  }

  // Scope for Highlights

  public function scopeHasHighlights($query){
    return $query->where('highlights_count', '>', '0');
    //deprecated

    $businesses_with_highlights = \DB::table('business_highlight')->groupBy('business_id')
                                        ->lists('business_id');
    return $query->whereIn('businesses.id', $businesses_with_highlights);
  }


  public function scopeHasNoHighlights($query){
    return $query->where('highlights_count', '=', '0');
    //deprecated

    $businesses_with_highlights = \DB::table('business_highlight')->groupBy('business_id')
                                        ->lists('business_id');
    return $query->whereNotIn('businesses.id', $businesses_with_highlights);
  }



  public function scopeSearch($query, $search){
    return $query->where('name', 'like', '%'.$search.'%');
                //->orWhere('landmark', 'like', '%'.$search.'%')
                //->orWhere('geolocation_city', 'like', $search.'%')
                //->orWhere('geolocation_address', 'like', $search.'%');
  }

  public function scopeSearchNameServices($query, $search){

    $services = Service::showOnly()->byLocaleActive()->showOnly()->search($search)->lists('id','id')->all();
    $overlaps = [];
    $buffer = [0];

//    $sql = '((businesses.name like "%'.addslashes($search).'%") ';
//    if(count($services)>0){
//      $sql.=' OR (business_service.service_id IN ('.join(',', $services).'))';
//    }
//
//    $sql.=')';
    //
//    return $query->join('business_service', 'businesses.id', '=', 'business_service.business_id')->whereRaw($sql);

    foreach($services as $service){
      $srs = \DB::table('business_service')
                    ->where('service_id', '=', $service)
                    ->lists('business_id');

      $buffer = array_merge($srs, $buffer);
    }


    //$overlaps = array_pop($buffer);

   // //foreach($buffer as $buf){
    //  $overlaps = array_intersect($overlaps, $buf);
    //}

    return $query->whereRaw('((businesses.name like "%'.$search.'%") OR (businesses.id IN ('.join(',', $buffer).')))');
  }



  public function scopeOfZones($query, $zones){
    return $query->whereIn('zone_id', $zones);
  }

  public function scopeOfChains($query, $chains){
    return $query->whereIn('chain_id', $chains);
  }


  public function scopeOfServices($query, $services){
    $services = array_filter($services);

//    $query->join('business_service', function($join) use($services){
//        $join->on('businesses.id', '=', 'business_service.business_id');
//      });
//
//    if(count($services)>0){
//      $query->whereIn('business_service.service_id', $services);
//    }
//
//    return $query;


    $with_services = \DB::table('business_service')
                          ->whereIn('service_id', $services)
                          ->lists('business_id');



    return $query->whereIn('id', $with_services);
  }

  public function scopeByCoordinatesNearby($query, $latitude, $longitude, $distance = 1500){
    $query->whereRaw(mzk_haversine($latitude, $longitude, $distance));
  }



const DISTANCE_UNIT_KILOMETERS = 111.045;
const DISTANCE_UNIT_MILES      = 69.0;

/**
 * @param $query
 * @param $lat
 * @param $lng
 * @param $radius numeric
 * @param $units string|['K', 'M']
 */
public function scopeNearLatLng($query, $lat, $lng, $radius = 10, $units = 'K')
{
    $distanceUnit = $this->distanceUnit($units);

    if (!(is_numeric($lat) && $lat >= -90 && $lat <= 90)) {
        throw new Exception("Latitude must be between -90 and 90 degrees.");
    }

    if (!(is_numeric($lng) && $lng >= -180 && $lng <= 180)) {
        throw new Exception("Longitude must be between -180 and 180 degrees.");
    }

    //$haversine = sprintf('*, (%f * DEGREES(ACOS(COS(RADIANS(%f)) * COS(RADIANS(geolocation_latitude)) * COS(RADIANS(%f - geolocation_longitude)) + SIN(RADIANS(%f)) * SIN(RADIANS(geolocation_latitude))))) AS distance',

    $haversine = sprintf(' (%f * DEGREES(ACOS(COS(RADIANS(%f)) * COS(RADIANS(geolocation_latitude)) * COS(RADIANS(%f - geolocation_longitude)) + SIN(RADIANS(%f)) * SIN(RADIANS(geolocation_latitude))))) AS distance',
        $distanceUnit,
        $lat,
        $lng,
        $lat
    );

    $subselect = clone $query;
    $subselect
        ->selectRaw(DB::raw($haversine));

    // Optimize the query, see details here:
    // http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/

    $latDistance      = $radius / $distanceUnit;
    $latNorthBoundary = $lat - $latDistance;
    $latSouthBoundary = $lat + $latDistance;
    $subselect->whereRaw(sprintf("geolocation_latitude BETWEEN %f AND %f", $latNorthBoundary, $latSouthBoundary));

    $lngDistance     = $radius / ($distanceUnit * cos(deg2rad($lat)));
    $lngEastBoundary = $lng - $lngDistance;
    $lngWestBoundary = $lng + $lngDistance;
    $subselect->whereRaw(sprintf("geolocation_longitude BETWEEN %f AND %f", $lngEastBoundary, $lngWestBoundary));

    return $query
        ->from(DB::raw('(' . $subselect->toSql() . ') as d'))
        ->where('distance', '<=', $radius);
}
private function distanceUnit($units = 'K')
{
    if ($units == 'K') {
        return static::DISTANCE_UNIT_KILOMETERS;
    } elseif ($units == 'M') {
        return static::DISTANCE_UNIT_MILES;
    } else {
        throw new Exception("Unknown distance unit measure '$units'.");
    }
}

  public function scopeWithServices($query, $services, $starting_priced = false){
    $overlaps = [];
    $buffer = [];
    $services = array_filter($services);
//    if(!$starting_priced){
//
//      $query->join('business_service', function($join) use($services){
//        $join->on('businesses.id', '=', 'business_service.business_id');
//      });
//      if(count($services)>0){
//        $query->whereIn('business_service.service_id', $services);
//      }
//    }else{
//      return $query->join('business_service', function($join) use($services){
//        $join->on('businesses.id', '=', 'business_service.business_id');
//      })->where('business_service.starting_price', '>', 0);
//      if(count($services)>0){
//        $query->whereIn('business_service.service_id', $services);
//      }
//
//
//    }
//
//    return $query;


    foreach($services as $service){
      if(!$starting_priced){
        $buffer[] = \DB::table('business_service')
                      ->where('service_id', '=', $service)
                      ->lists('business_id');
      }else{
        $buffer[] = \DB::table('business_service')
                      ->where('service_id', '=', $service)
                      ->where('starting_price', '>', 0)
                      ->lists('business_id');
      }

    }

    $overlaps = array_pop($buffer);

    foreach($buffer as $buf){
      $overlaps = array_intersect($overlaps, $buf);
    }

    return $query->whereIn('id', $overlaps);
  }

  public function scopeByCostEstimate($query, $cost){
    return $query->where('cost_estimate', '=', $cost);
  }


  public function scopeOfHighlights($query, $highlights){
    $with_highlights = \DB::table('business_highlight')
                          ->whereIn('highlight_id', $highlights)
                          ->lists('business_id');
    return $query->whereIn('id', $with_highlights);
  }

  public function scopeWithHighlights($query, $highlights){
    $overlaps = [];
    $buffer = [];

    // remove genders from the highlights here
    $genders = [];
    foreach($highlights as $highlight){
      if(in_array($highlight, [4,5,6])){
        $genders[] = $highlight;
      }else{
        $buffer[] = \DB::table('business_highlight')
                      ->where('highlight_id', '=', $highlight)
                      ->lists('business_id');
      }
    }

   if(count($genders)>0){
     $buffer[] = \DB::table('business_highlight')
                ->whereIn('highlight_id', $genders)
                ->lists('business_id');
   }
  


    $overlaps = array_pop($buffer);

    foreach($buffer as $buf){
      $overlaps = array_intersect($overlaps, $buf);
    }


    return $query->whereIn('businesses.id', $overlaps);
  }


  public function scopeOfCategories($query, $categories){
    $with_categories = \DB::table('business_category')
                            ->whereIn('category_id', $categories)
                            ->lists('business_id');


    // return $query->join('business_category', function($join) use($categories){
    //   $join->on('businesses.id', '=', 'business_category.business_id');
    // })->whereIn('business_category.category_id', $categories);



    return $query->whereIn('id', $with_categories);
  }


  // Scope for location



  public function scopeHasNoLocation($query){
    return $query->where('geolocation_latitude', '=', '0')
              ->where('geolocation_longitude', '=', '0');
  }

  public function scopeHasLocation($query){
    return $query->where('geolocation_latitude', '<>', '0')
              ->where('geolocation_longitude', '<>', '0');
  }


  public function getAddressAsString(){
    return trim($this->landmark);
    //$address = [trim($this->landmark), 
    //            trim($this->getlocation_address)//,
    //            //trim($this->geolocation_city)
    //            ];
    //return join(',', array_filter($address));
  }


  public static function activeStates(){
    return array( 'active' => 'Active',
                  'inactive' => 'Inactive',
                  'duplicate'=>'Duplicate',
                  'temporarily.closed' => 'Temporarily Closed',
                  'permanently.closed' => 'Permanently Closed',
                  'opening.soon' => 'Opening Soon');
  }

  public static function getCostOptions(){
    return [ 0=>'Not set', 1=>'Economical', 2=>'Premium', 3=>'Luxury']; //, 4=>'Fabuxpensive'
  }

  public function thumbnail($size = 'thumbnail'){
    $photo = $this->getMeta('photo');
    if($photo &&(trim($photo)!='') && ($size=='thumbnail')){
      return $photo;
    }
    $photos = $this->photos()->get();
    if($photos->count() > 0){
      $photo = $photos->first();
      return $photo->image->url($size);
    }else{
      $dimensions = ['thumbnail'=>'200/200', 'medium'=>'400/200', 'large'=>'400/200'];
      return 'http://lorempixel.com/'.$dimensions[$size].'/fashion/'.rand(1, 8);
    }
  }

  public function hasMetaThumbnail($size=''){
    $photo = $this->getMeta('photo'.($size==''?'':'-'.$size));
    if($photo &&(trim($photo)!='')){
      return true;
    }else{
      return false;
    }
  }
  public function getInitials(){
    $initial = substr(trim($this->name), 0, 2);
    return $initial;
  }


  public function getMetaThumbnailUrl($size=''){
    if($size=='thumbnail'){
      $photo = $this->getMeta('thumbnail', false);
      if($photo!=false){
        return $photo;
      }
    }

    if($this->hasMetaThumbnail($size)){
      return $this->getMeta('photo'.($size==''?'':'-'.$size));
    }else{
      return false;
    }
  }

  public function hasThumbnail($size = ''){
    //$photos = $this->photos()->get();
    if($this->hasMetaCover()){
      return true;
    }

    if(count($this->photos) > 0){
      return true;

    }else{
      return false;
    }
  }

  public function hasMetaCover(){
    $photo = $this->getMeta('photo', false);
    return $photo!=false ? true:false;
  }

  public function hasCover($size = ''){
    //$cover = $this->cover()->get();

    if(count($this->cover) > 0){
      return true;

      $cover = $cover->first();
      return $cover->image->url($size);
    }else{

      return false;
    }
  }

  public function getCityNameAttribute(){
    return $this->city ? $this->city->name : '';
  }

  public function getQuickDescriptionAttribute(){
    return join(',', [$this->name, 
                      $this->zone_cache,
                      $this->city_name, 
                      join(',', $this->categories()->lists('name','name')->all()),
                      $this->zone_cache,
                      $this->city_name]);
  }



  public function getCoverUrl($size = null, $include_meta = false){
    $photo = false;
    if($include_meta){
      if($size=='medium'){
        $photo = $this->getMeta('photo', false);
      }

      if(($size=='small')||($size=='thumbnail')){
        $photo = $this->getMeta('thumbnail', false);
      }
    }


    if($photo != false){
      mzk_console('Photo sent from the meta '.$size);
      return $photo;
    }else{
      mzk_console('Photo NOT sent from the meta '.$size);
      if($this->useStockImage()==true){
        return $this->getStockImage();
      }elseif($this->hasCover()){

        $cover = $this->cover()->first();
        return $cover->image->url($size);
      }else{

        return $this->getThumbnailUrl($size);
      }
    }


  }


  public function getThumbnailUrl($size = null){
    if($this->hasThumbnail()){
      if(count($this->photos) > 0){
        $photo  = $this->photos->first();
        return $photo->image->url($size);
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  public function getSeoThumbnailUrl(){
    $url = $this->getThumbnailUrl();
    $url = $url == false? ViewHelper::defaultSeoImage():$url;
    return $url;
  }

  public function displayablePhone(){
    //$nums = $this->getMeta('current_numbers', []);
    //if(count($nums)>0){
    //  return $nums;
    //}

    // is a virtual number allocated
    $virtual_number = $this->current_virtual_number_allocation();
    if($virtual_number!= false){

      if($virtual_number->state == 'active'){
        return [$virtual_number->virtual_number_text()];
      }
    }

    return $this->phone;
  }




  public function getPhoneAttribute($value){
    if(is_array(json_decode($value))){
      return json_decode($value);
    }else{
      return [$value];
    }
  }

  public function setPhoneAttribute($value){
    $val = [];
    foreach($value as $v){
      if(trim($v)!='')
        $val[] = $v;
    }
    $this->attributes['phone'] = json_encode($val);
  }


  public function getEmailAttribute($value){
    if(is_array(json_decode($value))){
      return json_decode($value);
    }else{
      return [$value];
    }
  }

  public function setEmailAttribute($value){
    $val = [];
    foreach($value as $v){
      if(trim($v)!='')
        $val[] = $v;
    }
    $this->attributes['email'] = json_encode($val);
  }

  public function getMetaAttribute($value){
    return (array)json_decode($value);
  }

  public function setMetaAttribute($value){
    $this->attributes['meta'] = json_encode($value);
  }

  public function updateMetaCache(){
    /*$this->meta = array('photo'     => (($this->hasThumbnail() || $this->hasCover() || $this->useStockImage()) ? $this->getCoverUrl('medium', false) : false),
            'thumbnail' => (($this->hasThumbnail() || $this->hasCover() || $this->useStockImage()) ? $this->getCoverUrl('small', false)  : false),
            'services'  => ($this->services() ? $this->services->lists('name', 'id'):[]),
            'categories'  => ($this->categories() ? $this->categories->lists('name', 'id'):[]),
            'zone'        => ($this->zone ? $this->zone->toArray():[]),
            'highlights'  => ($this->highlights() ? $this->highlights()->get()->lists('name', 'id'):[]),
            'current_numbers' =>($this->displayablePhone()) );*/

    $this->addMeta('photo',     (($this->hasThumbnail() || $this->hasCover() || $this->useStockImage()) ? $this->getCoverUrl('medium', false) : false));
    $this->addMeta('thumbnail', (($this->hasThumbnail() || $this->hasCover() || $this->useStockImage()) ? $this->getCoverUrl('small', false)  : false));
    $this->addMeta('services',   ($this->services()   ? $this->services->lists('name', 'id')->all()  : []));
    $this->addMeta('categories', ($this->categories() ? $this->categories->lists('name', 'id')->all(): []));
    $this->addMeta('zone',       ($this->zone         ? $this->zone->toArray()  : []));
    $this->addMeta('highlights', ($this->highlights() ? $this->highlights()->get()->lists('name', 'id')->all() : []));
    $this->addMeta('current_numbers', $this->displayablePhone());

    $this->save();
  }

  public function updateCurrentNumberCache(){
    $this->addMeta('current_numbers', $this->displayablePhone());
    $this->save();
  }


  // associations

  public function virtual_number_allocations(){
    return $this->hasMany('App\Models\Virtual_number_allocation');
  }


  public function virtual_number_allocations_current(){
    return $this->hasMany('App\Models\Virtual_number_allocation')->whereIn('virtual_number_allocations.state',['active', 'hold'])->get();;
  }





  public function allocated_a_current_virtual_number(){
    $r = $this->virtual_number_allocations()->whereIn('virtual_number_allocations.state',['active', 'hold'])->get();

    if(count($r)>0){
      return $r->first();
    }

    return false;
  }


  public function current_virtual_number_allocation(){
    $r = $this->virtual_number_allocations()->whereIn('virtual_number_allocations.state',['active', 'hold'])->get();
    if(count($r)>0){
      return $r->first();
    }

    //if($this->virtual_number_allocations()->where('virtual_number_allocations.state', '=', 'active')->count()>0){
    //  return $this->virtual_number_allocations()->where('virtual_number_allocations.state', '=', 'active')->first();
    //}

    return false;
  }

  public function current_held_virtual_number_allocation(){

    if($this->virtual_number_allocations()->where('virtual_number_allocations.state', '=', 'hold')->count()>0){
      return $this->virtual_number_allocations()->where('virtual_number_allocations.state', '=', 'hold')->first();
    }

    return false;
  }

  public function current_virtual_number_allocation_number(){
    $c = $this->current_virtual_number_allocation();
    if($c){
      if($c->virtual_number()->count()>0){
        return $c->virtual_number()->first()->body;
      }
    }

    return false;
  }


  public function setPopularity(){
    $views = Counter::select()->byBusiness($this->id)->byType('page-views')->sum('views');
    $popularity = floor($views/50);

    $popularity += $this->favourites()->count();
    $popularity += $this->completedReviews()->sum('rating');
    
    $s = $this->inCompleteReviews()->sum('rating');
    $s = $s>0 ? $s/2 : 0;
    $popularity += $s;

    // add photos
    $p = $this->image_count;
    $popularity += ($p >=3 ? 2 : 0);

    $p = $this->rate_card_count;
    $is = $this->serviceItems()->count();
    $popularity += (($p > 0) || ($is > 0) ? 2 : 0);

    $this->popularity = $popularity;
    $this->save();
  }




  public function call_logs(){
    return $this->hasMany('App\Models\Call_log');
  }


  public function deals(){
    return $this->hasMany('App\Models\Deal');
  }

  public function reviews(){
    return $this->hasMany('App\Models\Review');
  }

  public function ratings(){
    return $this->reviews;//hasMany('Review');
  }

  public function completedReviews(){
    return $this->reviews()->with('services', 'user')->where('user_id', '>', '0')->where('body', '<>', ' ');//hasMany('Review');
  }

  public function inCompleteReviews(){
    return $this->reviews()->whereIn('body',  ['',' ']);//hasMany('Review');
  }


  public function reviewers(){
    return $this->hasManyThrough('App\Models\User', 'App\Models\Review');
  }

  public function favourites(){
    return $this->morphMany('App\Models\Favorite', 'favorable');
  }

  public function favourited($user_id){
    return $this->favourites()->where('user_id', '=', $user_id)->count() > 0;
  }

  public function checkedin($user_id){
    return $this->check_ins()->where('user_id', '=', $user_id)->count() > 0;
  }

  public function check_ins(){
    return $this->hasMany('App\Models\Check_in');
  }

  public function zone(){
    return $this->belongsTo('App\Models\Zone', 'zone_id');
  }

  public function merchants() {
    return $this->belongsToMany('App\Models\Merchant'); 
  }

  public function categories() {
    return $this->belongsToMany('App\Models\Category'); 
  }

  public function timings() {
    return $this->hasMany('App\Models\Timing')->orderby('open', 'asc'); 
  }

  public function chain(){
    return $this->belongsTo('App\Models\Group', 'chain_id');
  }

  public function services() {
    return $this->belongsToMany('App\Models\Service')->withPivot('starting_price'); 
  }

  public function highlights() {
    return $this->belongsToMany('App\Models\Highlight'); 
  }

  public function cover(){
    return $this->morphMany('App\Models\Photo', 'imageable')->where('type', Photo::COVER);
  }
  public function slugs(){
    return $this->morphMany('App\Models\Slug', 'sluggable')->orderby('id', 'DESC');
  } 

  public function photos(){
    return $this->morphMany('App\Models\Photo', 'imageable')
                  ->whereIn('type', [Photo::IMAGE, Photo::SERVICE, Photo::COVER])->orderby('sort', 'ASC');
  } 

  public function rateCards(){
    return $this->morphMany('App\Models\Photo', 'imageable')->where('type', Photo::RATECARD)->orderby('sort', 'ASC');
  }

  public function packages(){
    return $this->hasMany('App\Models\Special_package')->orderby('sort', 'ASC');//->where('type', Photo::PACKAGE);
  }


  public function getBySlug($slug, $show_all = false){
    //onlyActive()
    
    $business = $this->isDisplayable($show_all)->where('slug', '=', $slug)->first();
    if($business){
      return $business;
    }

    $b = Slug::select()->where('body', '=', $slug)->orderby('id', 'desc')->first();
    if($b){
      return $b->sluggable_id;//$this->isDisplayable()->where('id', '=', $b->sluggable_id)->first();
    }

    return false;
  }


  public function getZoneName(){
    if(trim($this->zone_cache) == ''):
      return $this->zone ? $this->zone->name : '';
    else:
      return $this->zone_cache;
    endif;
  }

  public function getLevelOneZoneName(){
    if($this->zone){
      $zone = $this->zone;
      return $zone->ancestors()->skip(1)->first()->name;
    }else{
      return '';
    }

  }

  public static $fields = array('name', 'description', 'phone', 'email', 'website', 'geolocated', 'zone_id', 'geolocation_city', 'geolocation_state', 'geolocation_country', 'geolocation_address', 'geolocation_latitude', 'geolocation_longitude', 'active' );


  public function getAverageRatingAttribute(){
    return number_format(round((float)$this->rating_average, 2),1);
  }

  public function getTotalReviewsAttribute(){
    return count($this->reviews);
  }

  public function saveCover($image){
    if(!is_null($image)){
      $this->cover()->delete();
      $this->saveAllImages([$image], Photo::COVER);
    }
  } 

  public function logThisSlug(){
    $slug = Slug::create(['body'=>$this->slug, 'sluggable_type'=>'Business', 'sluggable_id'=>$this->id]);
    $slug->save();
  }

  public function removeTimings($ids){
    if(!is_array($ids)){
      $ids = [$ids];
    }

    foreach($ids as $id){
      $t = Timing::findOrFail($id);
      $t->delete($id);
    }
    $this->updateTimingsCount();
  }

  public function saveTimings($timings){
    if(!is_array($timings)){
      return false;
    }
    foreach($timings as $timing):

      if(isset($timing['id']) && ($timing['id'] > 0)){ // if we have an id so consider this is an update request

        $t = Timing::find($timing['id']);

        // if this has been marked for deletion 
        if($t):
          if(isset($timing['deletable']) && ($timing['deletable'] > 0)){
            $t->delete($timing['id']);  // delete it
          }else{              // else
            unset($timing['deletable']);
            $t->update($timing);  // update it
          }
        endif;
      }else{
        unset($timing['deletable']);
        $this->timings()->save(new Timing($timing)); // save a new timing
      }
    endforeach;

    $this->updateTimingsCount();

  }	

  public function scopeOpenNow($query){

    return $query->whereHas('timings', function($q) {
      $now =  \Carbon\Carbon::now('UTC');
      return  $q->where('timings.daysOfWeek', 'like', '%'.date('D', strtotime($now)).'%')
                ->where('timings.open_utc', '<=', (strtotime(date('Y-m-d H:i', strtotime($now)))))
                ->where('timings.close_utc', '>=', (strtotime(date('Y-m-d H:i', strtotime($now)))));
    });
  }

  public function isOpenNow($at_time_zone=false){
    // what day is it today and what time is it now at this zone
    if($this->zone_id == 0){
      return false;
    }


    if($this->zone->time_zone == ''){
      return false;
    }

    // time now at the salons zone
    $now =  \Carbon\Carbon::now($this->zone->time_zone);

    $ts = $this->timings()->where('daysOfWeek', 'like', '%'.date('D', strtotime($now)).'%')->get();
    foreach($ts as $tme){
      if((strtotime($now) >= strtotime(date('Y-m-d ', strtotime($now)).$tme->open)) && (strtotime($now) < strtotime(date('Y-m-d ', strtotime($now)).$tme->close))){
        return true;
      }
    }
    return false;

  }

  public function openUntilNow($at_time_zone=false){
    // what day is it today and what time is it now at this zone
    if($this->zone_id == 0){
      return '';
    }

    if($this->zone->time_zone == ''){
      return '';
    }

    // time now at the salons zone
    $now =  \Carbon\Carbon::now($this->zone->time_zone);

    $ts = $this->timings()->where('daysOfWeek', 'like', '%'.date('D', strtotime($now)).'%')->get();
    foreach($ts as $tme){
      if((strtotime($now) >= strtotime(date('Y-m-d ', strtotime($now)).$tme->open)) && (strtotime($now) < strtotime(date('Y-m-d ', strtotime($now)).$tme->close))){
        return $tme->close;
      }
    }
    return '';

  }

  public function willOpen($at_time_zone=false){
    // what day is it today and what time is it now at this zone
    if($this->zone_id == 0){
      return '';
    }

    if($this->zone->time_zone == ''){
      return '';
    }

    // time now at the salons zone
    $now =  \Carbon\Carbon::now($this->zone->time_zone);

    $ts = $this->timings()->where('daysOfWeek', 'like', '%'.date('D', strtotime($now)).'%')->get();
    foreach($ts as $tme){
      if((strtotime($now) <= strtotime(date('Y-m-d ', strtotime($now)).$tme->open)) ){
        return $tme->open;
      }
    }
    
    return '';

  }



  public function isLocationSet(){
    return (($this->geolocation_longitude == 0) && ( 0 == $this->geolocation_latitude)) ? false : true ;
  }


  public function saveCategories($categories){
    if(count($categories)>0){
      $this->categories()->sync($categories);
    }else{

    }
  }

  public function updateImageForCover($image_id){
    foreach($this->photos()->get() as $photo){
      $photo->type = $image_id == $photo->id ? Photo::COVER : $photo->type;
      $photo->save();
    }
  }

  public function saveImages($images){
  	$this->saveAllImages($images, Photo::IMAGE);
    $this->updateImageCount();
  }

  public function saveRateCards($images){
  	$this->saveAllImages($images, Photo::RATECARD);
    $this->updateRateCardCount();
  } 


  public function removeAllImages($ids){
    if(!is_array($ids)){
      $ids = [$ids];
    }

    foreach($ids as $id){
      $p = Photo::findOrFail($id);
      $p->delete($id);
    }
  }


  protected function saveAllImages($images, $type){
    $result = [];
    if(!is_array($images)){
      return $result;
    }

    foreach($images as $image)
    {
      if(is_null($image)){
        continue;
      }
      $photo = new Photo(['type'=>$type]);
      $photo->image = $image;
      $photo->type = $type;
      $photo->save();
      $this->photos()->save($photo);
      $result[] = $photo;
    }
    return $result;
  }

  public function rate($data, $is_cheat = false){

    if(isset($data['user_id'])){ // if the user is logged in? ensure this is an update
      $review = Review::firstOrCreate([ 'user_id'=>$data['user_id'], 
                                        'business_id'=>$this->id]);
    }else{
      $review = Review::firstOrCreate([ 'user_id'=>'0',
                                        'ip'=>$data['ip'], 
                                        'business_id'=>$this->id]);
      $review->user_id =  0;
      $review->business_id =  $this->id;
      $review->ip =  $data['ip'];
    }

    if(trim($review->body)==''){
      $review->body = ' ';
    }else{
      
    }


    $review->rating = (float)$data['rating']['rating'];
    $review->is_cheat = $is_cheat == false ? 0 : 1;
    $review->save();
    $this->updateReviewCount();
    $this->updateAverageRating();
  }


  public function cheatRateBulk($data, $is_cheat = false){

    if(isset($data['user_id'])){ // if the user is logged in? ensure this is an update
      $review = Review::firstOrCreate([ 'user_id'=>$data['user_id'], 
                                        'business_id'=>$this->id]);
    }else{
      $review = Review::firstOrCreate();
      $review->user_id =  0;
      $review->business_id =  $this->id;
      $review->ip =  $data['ip'];
    }

    if(trim($review->body)==''){
      $review->body = ' ';
    }else{
      
    }


    $review->rating = (float)$data['rating']['rating'];
    $review->is_cheat = $is_cheat == false ? 0 : 1;
    $review->save();
    $this->updateReviewCount();
    $this->updateAverageRating();
  }



  public function isReviewedBy($user_id){
    $review = $this->reviews()->where('user_id', '=', $user_id)->get()->first();
    if($review){
      return $review;
    }else{
      return false;
    }

  }
  public function ratingBy($user_id){
    $review = $this->reviews()->where('user_id', '=', $user_id)->get()->first();

    if(!$review)
      return 0;
    return $review->rating;
  }

  public function ratingByIP($ip){
    $review = $this->reviews()->where('ip', '=', $ip)->get()->first();

    if(!$review)
      return 0;
    return $review->rating;
  }


}
//Business::observe(new Culpa\BlameableObserver);