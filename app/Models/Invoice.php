<?php
namespace App\Models;

use DateTime;

use LaravelArdent\Ardent\Ardent;

use App\Models\Invoice_item;
use App\Models\Merchant;
use App\Models\Call_log;
use App\Models\User;
use App\Models\Zone;
use App\Models\Payment;

class Invoice extends Ardent {
	protected $guarded = array();

	public static $rules = array(
//		'title' => 'required',
		'merchant_id' => 'required',
//		'dated' => 'required',
//		'type' => 'required',
//		'currency' => 'required',
//		'amount' => 'required',
//		'state' => 'required'
	);

	public static $fields = array(
		'merchant_id', 'dated', 'amount', 'entity_id', 'start_date', 'end_date', 'user_id', 'city_id', 'currency'
	);

	public static function getStates(){
		return [ 'pending'=>'pending', 'bad-debt'=>'bad-debt', 'credit note'=>'credit note', 'cleared'=>'cleared'];
	}

	public function beforeCreate(){
		//$this->setCurrency();
		$this->setState();
		$this->setAmountDue();
	}

	public function setCurrency(){
		$this->currency = mzk_currency_symbol();
	}

	public function beforeSave(){
		$this->setState();
	}

  public function setAsPPL(){
    $this->is_ppl = 1;
  }

  public function isPPL(){
    return (isset($this->is_ppl)) && ($this->is_ppl == 1) ? true : false;
  }

	public function credit_notes(){
		return $this->hasMany('App\Models\Credit_note');
	}

	public function setAmountDue(){
		$this->amount_due = $this->amount;
	}

  public function scopeByMonthYear($query, $month = false, $year = false){

    // get offers which have expired in the last 7 days
    $month = $month ? $month : date('m');
    $year = $year ? $year : date('Y');

    $start_of = \Carbon\Carbon::createFromDate($year, $month, 1);
    $end_of = \Carbon\Carbon::createFromDate($year, $month, date("t", strtotime($start_of)));

    return $query->whereRaw('(start_date < "'.$end_of->toDateString().'" AND end_date > "'.$start_of->toDateString().'")');
  }


  public function scopeByMonthYearRange($query, $start_month = false, $start_year = false, $end_month = false, $end_year = false){

    // get offers which have expired in the last 7 days
    $start_month = $start_month ? $start_month : date('m');
    $start_year = $start_year ? $start_year : date('Y');

    $end_month = $end_month ? $end_month : date('m');
    $end_year = $end_year ? $end_year : date('Y');

    $start_of = \Carbon\Carbon::createFromDate($start_year, $start_month, 1);
    $end_of = \Carbon\Carbon::createFromDate($end_year, $end_month, date("t", strtotime($end_year.'-'.$end_month.'-16')));

    return $query->whereRaw('(start_date < "'.$end_of->toDateString().'" AND end_date > "'.$start_of->toDateString().'")');
  }



	public function setState(){
		if($this->amount_due == 0){
			if($this->credit_notes()->get()->count() > 0){
				$this->state = $this->credit_notes()->get()->first()->type == 'bad-debt'?'bad debt':'credit note';
			}else{
				$this->state = 'cleared';
			}
		}else{
				$this->state = 'pending';
		}
	}
  public function reset(){
    $this->amount_due = $this->amount;
    $this->save();
  }

	public function afterCreate(){
		$this->assignInvoiceNumber();
		$this->save();
	}

	public function assignInvoiceNumber(){
		$this->title = $this->generateInvoiceNumber();
	}

	public function items(){
		return $this->hasMany('App\Models\Invoice_item');
	}
	public function merchant(){
		return $this->belongsTo('App\Models\Merchant', 'merchant_id');
	}

  public function call_logs()
  {
    return $this->hasManyThrough('App\Models\Call_log', 'App\Models\Invoice_item');
  }

  public function poc(){
    return $this->belongsTo('App\Models\User', 'user_id');
  }

  public function poc_name(){
    return $this->poc() ? $this->poc()->get()->first()->name:'';
  }
  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function city_name(){
    return $this->city() ? $this->city()->get()->first()->name:'';
  }

  public function scopeByMerchants($query, $merchants){

  	if(!is_array($merchants)){
  		$merchants = [$merchants];
  	}

  	return $query->whereIn('merchant_id', $merchants);
  }

  public function scopeByCity($query, $city){
    $lists = Merchant::select()->byLocale($city)->get()->lists('id', 'id')->all();

    return $query->byMerchants($lists);
  }

  public function scopeByPOCs($query, $pocs){
    return $query->whereIn('user_id', $pocs);
  }

  public function scopeByStates($query, $states){
  	if(!is_array($states)){
  		$states = [$states];
  	}

  	return $query->whereIn('state', $states);
  }
  public function scopeSearch($query, $search){
  	return $query->where('title', 'like', $search.'%');
  }

  public function isCleared(){
  	return ($this->amount_due == 0) ? true : false ;
  }


  public function payments(){
  	return $this->belongsToMany('App\Models\Payment');
  }

  public function contractValue(){
    return $this->amount;
  }



  public function resetAmountApplicable(){
    $total_amount_applied = 0;

    foreach($this->payments()->withPivot('amount')->get() as $payment):
      if($payment->isApplicablePayment()):
        $invoice = self::find($this->id);
        $amount_applied = $payment->pivot->amount;
        $payment->markApplicableAmountForInvoice($invoice, $amount_applied);
        $total_amount_applied += $amount_applied;
        //$this->amount_applicable = $this->amount_applicable - $total_amount_applied;
        //$this->save();
      endif;
    endforeach;
    
    // if invoice amount due is more than available on check
    if($this->amount > $total_amount_applied){
      $this->amount_due = $this->amount - $total_amount_applied;
    }else{ // else subtract the amount due from applicable and mark check as cleared
      $this->amount_due = 0; 
    }
    $this->save();


  }


  public function monthlyAverage(){
    return round($this->amount/(round(mzk_date_intervals($this->start_date, $this->end_date)/30)), 2);
  }

  public function currentMonthContribution($m = false, $y = false){

    $avg = $this->monthlyAverage();
    $m = $m ? $m : date('n');
    $y = $y ? $y : date('Y');
    
    // is this starting in the mid of the current month
    //date('nY')
    if($m.$y == date('nY', strtotime($this->start_date)) && ($m.$y!=date('nY', strtotime($this->end_date))) && (date('d', strtotime($this->start_date))>1)){
      return $avg/2;
    }

    if($m.$y == date('nY', strtotime($this->end_date))  && ( $m.$y!=date('nY', strtotime($this->start_date))) && (  date('d', strtotime($this->end_date))!=(date('t', strtotime($this->end_date)))   )){
      return $avg/2;
    }

    // is this within the current month to begin with?
    // loop from 1st month to the last month
    $start_from = strtotime($this->start_date);
    $end_at = strtotime($this->end_date);


    $start_month = date('Y-m-d', strtotime($y.'-'.$m.'-15'));
    $end_month = date('Y-m-t', strtotime($y.'-'.$m.'-15'));


    // loop through intervals
    $overlap = mzk_dates_overlap($this->start_date, $this->end_date, $start_month, $end_month);
    if($overlap>0){
      return round($avg, 2);
    }



    return 0;

  }

	public function generateInvoiceNumber(){
		$num = ['PF'];
		$num[] = mzk_get_localeID();
		$num[] = date('my');
		$num[] = str_pad($this->id, 10, "0", STR_PAD_LEFT);
		return join('/', $num);
	}

  public function getAmountInrAttribute(){
    if($this->currency == 'INR'){
      return $this->amount;
    }

    if($this->currency == 'AED'){
      return mzk_currency_exchange($this->amount, 'AED', 'INR');
    }

  }

  public function getAmountAedAttribute(){
    if($this->currency == 'AED'){
      return $this->amount;
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($this->amount, 'INR', 'AED');
    }

  }

  public function getAmountUsdAttribute(){
    if($this->currency == 'AED'){
      return mzk_currency_exchange($this->amount, 'AED', 'USD');
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($this->amount, 'INR', 'USD');
    }

  }

  public function currentMonthContributionINR($m = false, $y = false){
    $amount = $this->currentMonthContribution($m, $y);
    if($this->currency == 'INR'){
      return $amount;
    }

    if($this->currency == 'AED'){
      return mzk_currency_exchange($amount, 'AED', 'INR');
    }

  }
  public function currentMonthContributionAED($m = false, $y = false){
    $amount = $this->currentMonthContribution($m, $y);
    if($this->currency == 'AED'){
      return $amount;
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($amount, 'INR', 'AED');
    }

  }


  public function currentMonthContributionUSD($m = false, $y = false){
    $amount = $this->currentMonthContribution($m, $y);

    if($this->currency == 'AED'){
      return mzk_currency_exchange($amount, 'AED', 'USD');
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($amount, 'INR', 'USD');
    }
  }




}
