<?php
namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Discount extends Deal implements SluggableInterface{
  use SluggableTrait;

  public function getSlugnameAttribute(){
    return $this->title.' '.$this->business->name;
  }

  protected $sluggable = array(
    'build_from' => 'slugname',
    'save_to'    => 'slug',
    'max_length' => 200,
    'unique'     => true,      
  );

	//protected $table_type = 'Discount';
	//protected static $table_type_field = 'type';
  protected static $singleTableType = 'Discount';
  protected static $persisted = ['offer_amount','original_amount','starts','ends'];

	//protected static $my_attributes = array('offer_amount','original_amount', 'starts', 'ends');
	protected $guarded = array();

	public static $rules = array(
		'title' => 'required',
		'caption' => 'required',
		'description' => 'required',
		'fine_print' => 'required',
		'offer_amount' => 'required',
		'starts' => 'required',
		'ends' => 'required'
	);
}
