<?php
namespace App\Helpers;

use App\Models\Service;
use App\Models\Category;
use App\Models\Business;
use App\Models\Zone;
use App\Models\Campaign;
use App\Models\Ad;
use App\Models\Photo;
use Cache,Auth, Str, Session, Request;

class ViewHelper{
  public static function money($amount, $symbol = '<sup>AED</sup>'){
    return $symbol .''. $amount;//money_format('%i', $amount);
  }

  public static function time($t){
    return date('h:i a', strtotime($t));
  }

  public static function defaultSeoImage(){
    return mzk_assets('assets/splash.jpg');
  }
  
  public static  function iconCategory($slug){
    $map = ['salon' =>' parenticon parenticon-salon-01-01 __flaticon-hairsalon4',
      'spa' =>' parenticon parenticon-salon-02 __flaticon-flowers38',
      'medi-spa'  =>' flaticon-heart36',
      'massage-center'  =>' parenticon parenticon-salon-03 __flaticon-spa21',
      'personal-fitness'  =>' parenticon parenticon-salon-04  __flaticon-chronometer19',
      'personal-fitness-center'  =>' parenticon parenticon-salon-04 __flaticon-chronometer19',
      'cosmetic-clinic' =>' parenticon parenticon-salon-05 __flaticon-mouth3',
      'skin-clinic' =>'flaticon-fruit24',
      'alternative-therapies' =>'flaticon-mortar4',
      'dental-clinic' =>'flaticon-tooth12'];
    if(is_numeric($slug)){
      $keys = array_keys($map);
      $slug = $keys[$slug-1];
    }

    if(isset($map[$slug])){
      return $map[$slug];
    }else{

    }
  }

  public static function iconHighlight($slug){

    $map = ['parking-available'=>'flaticon-parking12',
      'appointment-only'=>'flaticon-calentar',
      'walk-ins-allowed'=>'flaticon-enter3',
      'ladies-only'=>'flaticon-woman139',
      'unisex'=>'flaticon-family21',
      'mens-only'=>'flaticon-users68',
      'couple-massage'=>'flaticon-massage2',
      'outdoor-view'=>'flaticon-night12',
      'refreshments-served'=>'flaticon-fast-food',
      'deals-available'=>'flaticon-tags9',
      'wi-fi'=>'flaticon-wifi74',
      '4-or-5-star-luxury-hotel'=>'flaticon-jewelry',
      'home-services'=>'flaticon-house204',
      'kids-services'=>'flaticon-kids1',
      'credit-card'=>'flaticon-money179',
      'has-tv'=>'flaticon-television35',
      'medi-spa'=>'flaticon-33'];

    if(is_numeric($slug)){
      $keys = array_keys($map);
      $slug = isset($keys[$slug-1])?$keys[$slug-1]:0;
    }

    if(isset($map[$slug])){
      return $map[$slug];
    }else{
      
    }
  }


  public static function ratingColor($n){
    if($n == 0){
      return '#ccc';
    }elseif($n < 1.5){
      return 'red';
    }elseif($n < 2.5){
      return 'orange';
    }elseif($n < 3){
      return '#FFD800';
    }elseif($n < 4){
      return '#ABED04';
    }elseif($n <= 5){
      return '#39B500';
    }
  }

  public static function rateSmallBusinessNiblet($business, $google = false){
    $html = '<span class="btn btn-xs" ';
    $html.=' ';
    if(($business->average_rating > 0) || (($business->reviews_count + $business->total_ratings_count) > 0)){
      $html.='style="color:white;background-color:'.self::ratingColor($business->rating_average).'"><b>';
      $html.=$business->average_rating.'</b>';
    }else{
      $html.='style="color:white;background-color:#ccc" title="Not reviewed yet">&nbsp; - &nbsp;';
    }
    if($google){
//       $html.='<span class="hide" itemprop="ratingCount">'.$business->accumulatedReviewsCount().'</span>';
    }

    $html.='</span>'; 
    return $html;
  }

  public static function formatUsername($user, $default = 'You' ){
    if(Auth::check()){
      if(Auth::user()->id == $user->id){
        return $default;
      }
    }
    
    return $user->full_name;
  }


  public static function rateBusinessNiblet($business, $google = false){
    $html = '<span class="btn" ';
    if($google){
      // $html .= ' itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating" ';
    }

    $html.=' ';
    if(($business->average_rating > 0) || (($business->reviews_count + $business->total_ratings_count) > 0)){
      $html.='style="color:white;background-color:'.self::ratingColor($business->rating_average).'">';
      $html.='<span ';
      if($google){
        // $html.=' itemprop="ratingValue" ';
      }
      $html.=' >';
      $html.=$business->average_rating;
      $html.='</span>'; 
    }else{
      $html.='style="color:white;background-color:#ccc" title="Not reviewed yet">';
      // $html.='<meta  ';
      if($google){
        // $html.=' itemprop="ratingValue" ';
      }

      // $html.=' content="0.0" />'; 
      $html.='&nbsp; - &nbsp;';

    }
    if($google){

      // $html.='<meta itemprop="ratingCount" content="'.$business->accumulatedReviewsCount().'" />';
      // $html.='<meta itemprop="bestRating" content="5" />';
      // $html.='<meta itemprop="worstRating" content="0.0" />';

    }
    $html.='</span>'; 
    return $html;
  }



// star rating functions

  public static function starRateBusinessMedal($business, $google = false){
    if(($business->average_rating > 0) || (($business->reviews_count + $business->total_ratings_count) > 0)){
      $rated = true;      
      $title = 'Rated '.($business->rating_average).' stars';
    }else{
      $rated = false;      
      $title = 'Not Rated yet';
    }

    $html= '<span title="'.$title.'" ';

    if($google){
      // $html .= ' itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating" ';
    }

    $html.=' ';
    
    {
      $html.=' data-number="5" data-score="'.($business->rating_average).'">';
      if($rated == false){
        $html.='<div  style="margin-top:240px;" class="force-white dpb fw300 fs125 text-center pt5 search-item-rating-num-votes">';
        $html.='(not rated yet)';
  

        $html.='</div>';

      }else{


        $html.='<div class="pt5 pb0  dpib force-white" style="margin-top:240px;" >';
        $html.='<div class="border-radius-3 dpib bg-yellow  " style="font-size:220%;line-height: 1;padding:2px 6px;color:#000;">'.mzk_format_rating($business->average_rating).'</div>';

        //$html.= '<span style="font-size:370%;line-height: 0.5">'.$business->average_rating.'</span>';
        $html.='</div>';
        $html.='<div class="force-white dpb  text-center pt5 fw500 search-item-rating-num-votes" style="font-size:110%">';
        $html.='<b>'.$business->accumulatedReviewsCount();
        $html.=' votes</b></div>';
      }


    }

    if($google){

      // $html.='<meta itemprop="ratingCount"  content="'.$business->accumulatedReviewsCount().'" />';
      // $html.='<meta itemprop="bestRating"   content="5" />';
      // $html.='<meta itemprop="worstRating"  content="0.0" />';

    }
    $html.='</span>'; 
    return $html;
  }


  public static function starRateBusinessMedalInverse($business, $google = false){
    if(($business->average_rating > 0) || (($business->reviews_count + $business->total_ratings_count) > 0)){
      $rated = true;      
      $title = 'Rated '.($business->rating_average).' stars';
    }else{
      $rated = false;      
      $title = 'Not Rated yet';
    }

    $html= '<span title="'.$title.'" ';

    if($google){
      //$html .= ' itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating" ';
    }

    $html.=' ';
    
    {
      $html.=' data-number="5" data-score="'.($business->rating_average).'">';
      if($rated == false){
        $html.='<div class=" dpb fw300 fs125 text-center pt5 search-item-rating-num-votes">';
        $html.='(not rated yet)';


        $html.='</div>';

      }else{

        $html.='<div class="border-radius-3 dpib bg-yellow  " style="font-size:370%;line-height: 0.5;padding:2px 6px;color:#000;"><b>'.mzk_format_rating($business->average_rating).'</b></div>';



        //$html.='<div class="pt5 pb0  dpib  "   ><span style="font-size:370%;line-height: 0.5">'.$business->average_rating.'</span></div>';
        $html.='<div class="  dpb fs80 text-center pt5 search-item-rating-num-votes">';

        $html.=$business->accumulatedReviewsCount();
        $html.=' votes</div>';
      }

    }

    if($google){

    }
    $html.='</span>'; 
    return $html;
  }


  public static function starRateBusinessNiblet($business, $google = false){
    if(($business->average_rating > 0) || (($business->reviews_count + $business->total_ratings_count) > 0)){
      $title = 'Rated '.($business->rating_average).' stars';
    }else{
      $title = 'Not Rated yet';
    }

    $html = '<span title="'.$title.'" ';
    if($google){

    }

    $html.=' ';
    
    {
      $html.=' data-number="5" data-score="'.($business->rating_average).'">';
      $html.='<span ';
      if($google){
      }
      $html.=' >';
      $html.='<input data-title="'.$title.'" title="'.$title.'" data-readonly="true" data-show-clear="false" data-step="1" data-max="5" data-size="xs" data-show-caption="false" data-min="0" value="'.($business->rating_average).'" ';
      $html.=' class="star-rating-single form-control hide" id="rating-for-business-'.$business->id.'">';      
      $html.='<span class="hide">'.$business->average_rating.'</span>';
      $html.='</span>'; 
    }


    if($google){


    }
    $html.='</span>'; 
    return $html;
  }

  public static function starRateSmallBusinessNiblet($business, $google = false){
    if(($business->average_rating > 0) || (($business->reviews_count + $business->total_ratings_count) > 0)){
      $title = 'Rated '.($business->rating_average).' stars';
    }else{
      $title = 'Not Rated yet';
    }

    $html = '<span title="'.$title.'" >';
    //if(($business->average_rating > 0) || (($business->reviews_count + $business->total_ratings_count) > 0))
    {
      //$html.='style="color:white;background-color:'.self::ratingColor($business->rating_average).'"><b>';
      $html.='<input data-title="'.$title.'" title="'.$title.'" data-readonly="true" data-show-clear="false" ';
      $html.=' data-step="1" data-max="5" data-size="xxs" data-show-caption="false" data-min="0" value="'.($business->rating_average).'" ';
      $html.=' class="star-rating-single form-control hide" id="rating-for-business-'.$business->id.'">';      
      //$html.=$business->average_rating.'</b>';
    }

    /*else{
      $html.='style="color:white;background-color:#ccc" title="Not reviewed yet">&nbsp; - &nbsp;';
    }*/
    if($google){
     // $html.='<span class="hide" itemprop="ratingCount">'.$business->accumulatedReviewsCount().'</span>';
    }

    $html.='</span>'; 
    return $html;
  }

  public static function starRateSmall($review){

    if($review->rating > 0)
    {
      $title = 'Rated '.($review->rating).' stars';
    }else{
      $title = 'Not Rated yet';
    }
    $html = '<span title="'.$title.'" class="inline-block" >';

    {
      //$html.='style="color:white;background-color:'.self::ratingColor($business->rating_average).'"><b>';
      $html.='<input data-readonly="true" data-show-clear="false" ';
      $html.=' data-step="1" data-max="5" data-size="xxs" data-show-caption="false" data-min="0" ';
      $html.=' data-score="'.($review->rating).'" value="'.($review->rating).'" data-value="'.($review->rating).'" ';
      $html.=' class="star-rating-single form-control hide" id="rating-of-review-'.$review->id.'-'.strtotime($review->updated_at).'">';      
      //$html.=$business->average_rating.'</b>';
    }

    $html.='</span>'; 
    return $html;
  }

  public static function starRateBasic($review, $size = 's', $show_num_votes = false){
 
    if(is_object($review)){
      $rating = $review->rating;      
    }else{
      $rating = $review;      
    }

    $css = 'padding:2px 6px;font-size:12px;';
    $css2 = 'padding:2px 12px;font-size:12px;';

    if(($size == 's')||($size == 'm')){
      $css = 'padding:2px 6px;font-size:12px;';
      $css2 = 'padding:2px 12px;font-size:12px;';

    }elseif($size == 'l'){
      $css = 'padding:2px 6px;font-size:20px;color:#000;';
      $css2 = 'padding:2px 12px;font-size:16px;';
    }elseif($size == 'xs'){
      $css = 'padding:0px 3px;font-size:11px;color:#000;';
      $css2 = 'padding:0px 6px;font-size:11px;';

    }elseif($size == 'xxs'){
      $css = 'padding:0px 2px;font-size:11px;color:#000;';
      $css2 = 'padding:0px 6px;font-size:11px;';
    }

    if($rating > 0)
    {
      $title = 'Rated '.($rating).' stars';
      $hide_mobile_css = '';
      $append = '';
      if($show_num_votes != false){
        $append = '<div class="text-center" style="margin-top:-5px;"><small class="gray fs80">('.$show_num_votes.' '.($show_num_votes>1?'votes':'vote').')</small></div>';
      }
    }else{
      $title = 'Not Rated yet';
      $hide_mobile_css = ' hide-only-mobile ';

      $append = in_array($size, ['xm','s']) ? '' : '<div style="margin-top:-5px;"><small class="gray fs80">(not rated yet)</small></div>';
    }

    if($rating > 0){
      $html = '<span title="'.$title.'" class="border-radius-2 dpib bg-yellow  " style="'.($css).'" >';
      $html.='<div ><b>'.mzk_format_rating($rating).'</b></div>';
      $html.=$append.'</span>'; 
  
    }else{
      $html = '<span title="'.$title.'" class="border-radius-2 dpib bg-medium-gray  force-white" style="'.($css2).'" >';
      $html.='<b>-</b>';
      $html.='</span>'; 

    }

    {
//      $html.='<div class="star-rating dpib no-text-shadow rating-'.$size.' '.$hide_mobile_css.' rating-disabled">';
//        $html.='<div class="rating-container '.$hide_mobile_css.' rating-gly-star" data-content="">';
//          $html.='<div class="rating-stars" data-content="" style="width: '.(($rating/5)*100).'%;"></div>';
//        $html.='</div>';
//      $html.='</div>';
    }

    return $html;
  }

  public static function starRateBasicKnob($review, $size = 's', $show_num_votes = false){

    if(is_object($review)){
      $rating = $review->rating;      
    }else{
      $rating = $review;      
    }

    if($rating > 0)
    {
      $title = 'Rated '.($rating).' stars';
      $hide_mobile_css = '';
      $append = '';
      if($show_num_votes != false){
        $append = '<div class="text-center" style="margin-top:5px;"><small class="gray fs80">'.$show_num_votes.' '.($show_num_votes>1?'votes':'vote').'</small></div>';
      }
    }else{
      $title = 'Not Rated yet';
      $hide_mobile_css = ' hide-only-mobile ';
      $counts = '<div style="margin-top:5px;"><small class="gray fs80">(not rated yet)</small></div>';
      $append = in_array($size, ['xm','s']) ? '' : $counts;
      return '<div class="border-radius-2 dpib bg-medium-gray  force-white" style="padding:2px 12px;font-size:12px;"><b>-</b></div>';

      return $counts;

    }

    $html = '<span title="'.$title.'" class="text-right inline-block" >';
    if($rating > 0){
      $html.='<div class="border-radius-2 dpib bg-yellow  " style="padding:2px 6px;font-size:12px;"><b>'.mzk_format_rating($rating).'</b></div>';
    }else{
      $html.='<div class="border-radius-2 dpib bg-medium-gray  force-white" style="padding:2px 12px;font-size:12px;"><b>-</b></div>';

    }


    {
      //$html.='<div class="star-rating dpib no-text-shadow rating-'.$size.' '.$hide_mobile_css.' rating-disabled">';
      //$html.='<div class="rating-container '.$hide_mobile_css.' rating-gly-star" data-content="">';
      //$html.='<div class="rating-stars" data-content="" style="width: 100%;"></div>';
      //$html.='</div></div> '.($rating);

    }

    $html.=$append.'</span>'; 
    return $html;
  }



  public static function starRateSmallBasic($review, $params = false){

    return self::starRateBasic($review, $params);
    $params = is_array($params)?$params:[];
    $google = isset($params['google'])?$params['google']:false;
    $num_votes = isset($params['count'])?$params['count']:false;
    $skip_zero = isset($params['skip_zero'])?$params['skip_zero']:false;
    if(is_object($review)){
      $rating = $review->rating;      
    }else{
      $rating = $review;      
    }

    if($rating > 0)
    {
      $title = 'Rated '.($rating).' stars';
      $append = '';
      $hide_mobile_css = '';
    }else{
      if($skip_zero == true){
        return '';
      }
      
      $title = 'Not Rated yet';
      $hide_mobile_css = ' hide-only-mobile ';
      $append = '<small class="gray ">(not rated yet)</small>';
    }
    $html = '<span title="'.$title.'" class="inline-block"  ';
    if($google){
    }

    $html.=' ';
    
    $html.=' data-number="5" data-score="'.($rating).'">';
    $html.='<span ';
    if($google){
    }
    $html.=' >';
    
    if($rating > 0){
      $html.='<div class="border-radius-2 dpib bg-yellow  " style="padding:2px 6px;font-size:12px;"><b>'.mzk_format_rating($rating).'</b></div>';
    }else{
      $html.='<div class="border-radius-2 dpib bg-gray  force-white" style="padding:2px 6px;font-size:12px;"><b>-</b></div>';
    }
    {
      //$html.='<div class="star-rating dpib no-text-shadow rating-xxs rating-disabled">';
      //$html.= '<div class="rating-container '.$hide_mobile_css.' rating-gly-star" data-content="">';
      //$html.=   '<div class="rating-stars" data-content="" style="width: '.(($rating/5)*100).'%;"></div>';
      //$html.= '</div>';
      //$html.='</div>';
    }
    if($google){

    }

    $html.=$append.'</span></span>'; 
    return $html;
  }



  public static function phone($phone){
    $codes = ['00971', '+971', '+ 971'];
    $phone = trim($phone);

    foreach($codes as $prefix){
      if (substr($phone, 0, strlen($prefix)) == $prefix) {
        $phone = '0'.substr($phone, strlen($prefix));
        return $phone;
      }
    }

    return $phone;
  }


  public static function setZonesComboAsArray($force = false){

    $zs = Zone::query()->showActive()->byLocale()->defaultOrder()->get()->linkNodes();

    if (!Cache::has('zones.all.combo.list.'.mzk_get_localeID())||($force==true))
    {
      $all_zones = array();
      $all_zones[mzk_get_localeID()] = 'All of '.Zone::find(mzk_get_localeID())->name;
      Cache::forget('zones.all.combo.list.'.mzk_get_localeID());

      foreach($zs as $zone){
        $zone_name = $zone->name;
        
        if($zone->parent_id > 0){
          $zone_name.=' ('.$zone->parent()->first()->name.')';
        }
        $all_zones[$zone->id] = $zone_name;
      }
      Cache::forever('zones.all.combo.list.'.mzk_get_localeID(), $all_zones);
    }

    return Cache::get('zones.all.combo.list.'.mzk_get_localeID());
  }

  public static function zonesComboAsArray(){
    return self::setZonesComboAsArray();
  }

  public static function zonesComboAsSelectableArray(){
    $z = self::zonesComboAsArray();
    $z = array_chunk($z, 6, true);
    $z = $z[rand(0, (count($z)-1))];
    $result = [];
    foreach($z as $id=>$name){
      $result[] = ['value'=>$id, 'label'=>$name];
    }
    return $result;
  }

  public static function genColorCodeFromText($text,$min_brightness=100,$spec=10)
  {
    // Check inputs
    if(!is_int($min_brightness)) throw new Exception("$min_brightness is not an integer");
    if(!is_int($spec)) throw new Exception("$spec is not an integer");
    if($spec < 2 or $spec > 10) throw new Exception("$spec is out of range");
    if($min_brightness < 0 or $min_brightness > 255) throw new Exception("$min_brightness is out of range");
    
    
    $hash = md5($text);  //Gen hash of text
    $colors = array();
    for($i=0;$i<3;$i++)
      $colors[$i] = max(array(round(((hexdec(substr($hash,$spec*$i,$spec)))/hexdec(str_pad('',$spec,'F')))*255),$min_brightness)); //convert hash into 3 decimal values between 0 and 255
      
    if($min_brightness > 0)  //only check brightness requirements if min_brightness is about 100
      while( array_sum($colors)/3 < $min_brightness )  //loop until brightness is above or equal to min_brightness
        for($i=0;$i<3;$i++)
          $colors[$i] += 10;  //increase each color by 10
          
    $output = '';
    
    for($i=0;$i<3;$i++)
      $output .= str_pad(dechex($colors[$i]),2,0,STR_PAD_LEFT);  //convert each color to hex and append to output
    
    return '#'.$output;
  }


  public static function businessThumbnail($business, $params=null){
    $url = '';
    $str = '';
    
    $params = $params?$params: ['width'=>'40', 'height'=>'40', 'class'=>'img-rounded', 
                                'style'=>'width:40px; height:40px; padding-top:6px;'];
    foreach($params as $ii=>$vv){
      if(($ii=='style')||($ii=='no-meta')||($ii=='class'))
        continue;
      $str.=$ii.'="'.$vv.'" ';
    }

    $class =(isset($params['class'])?$params['class']:'');
    $size = isset($params['meta'])?$params['meta']:'';

    if( $business->hasMetaCover()  /*|| ($business->hasThumbnail() || $business->hasCover() ||  $business->useStockImage())*/):
      $url = $business->getCoverUrl($size, true);
      mzk_console('Meta Image found for '.$business->id);
    elseif($business->useStockImage()):
      $url = $business->getCoverUrl($size);
      mzk_console('Cover Image found for '.$business->id);

    else:
      mzk_console('Image missed for '.$business->id);
      if($business->hasMetaThumbnail($size) && !isset($params['no-meta'])){
        $url = $business->getMetaThumbnailUrl($size).'&p=1';
      }elseif($business->hasThumbnail()){

        $url = $business->getThumbnailUrl(isset($params['no-meta'])?$params['no-meta']:null).'&p=1';
      }//*/
    endif;

    if($url!=''){
      $url = mzk_cloudfront_image($url);
      return '<img src="'.$url.'" '.$str.' alt="'.$business->name.'  photo" class="'.$class.'" />';
    }

    if(isset($params['no-fake'])){
      return '';
    }

    $css =(isset($params['style'])?$params['style']:'');
    $css.='background-color:'.(self::genColorCodeFromText($business->name.$business->id)).';';

    $class.= ' dumatar ';

    $html = '<span class="'.$class.'" '.$str.' style="'.$css.'">';

    $html .=strtoupper($business->getInitials());

    $html .='</span>';

    return $html;
  }


    public static function businessBgThumbnail($business, $params=null){
    $url = '';
    $str = '';
    
    $params = $params?$params: ['width'=>'40', 'height'=>'40', 'class'=>'img-rounded', 
                                'style'=>'width:40px; height:40px; padding-top:6px;'];
    foreach($params as $ii=>$vv){
      if(($ii=='style')||($ii=='no-meta')||($ii=='class'))
        continue;
      $str.=$ii.'="'.$vv.'" ';
    }

    $class =(isset($params['class'])?$params['class']:'');
    $size = isset($params['meta'])?$params['meta']:'';

    if( $business->hasMetaCover()  /*|| ($business->hasThumbnail() || $business->hasCover() ||  $business->useStockImage())*/):
      $url = $business->getCoverUrl($size, true);
      mzk_console('Meta Image found for '.$business->id);
    elseif($business->useStockImage()):
      $url = $business->getCoverUrl($size);
      mzk_console('Cover Image found for '.$business->id);

    else:
      mzk_console('Image missed for '.$business->id);
      if($business->hasMetaThumbnail($size) && !isset($params['no-meta'])){
        $url = $business->getMetaThumbnailUrl($size).'&p=1';
      }elseif($business->hasThumbnail()){

        $url = $business->getThumbnailUrl(isset($params['no-meta'])?$params['no-meta']:null).'&p=1';
      }//*/
    endif;

    if($url!=''){
      $url = mzk_cloudfront_image($url);
      return '<div style="background-image:url('.$url.');background-size:cover;min-height:145px;"></div>';
      // '<img src="'.$url.'" '.$str.' alt="'.$business->name.' photo" class="'.$class.'" />';
    }

    if(isset($params['no-fake'])){
      return '';
    }

    $css =(isset($params['style'])?$params['style']:'');
    $css.='background-color:'.(self::genColorCodeFromText($business->name.$business->id)).';';

    $class.= ' dumatar ';

    $html = '<span class="'.$class.'" '.$str.' style="'.$css.'">';

    $html .=strtoupper($business->getInitials());

    $html .='</span>';

    return $html;
  }


 
  public static $avatar325 = [ 'width'=>'275', 'height'=>'275', 'size'=>'medium', 'class'=>'img-circle', 
                              'style'=>'width: 275px; height: 275px; font-size: 70px; padding-top: 75px; text-align: center;'];
  public static $avatar325Square = [ 'width'=>'275',  'size'=>'original', 'class'=>'img-rounded', 
                              'style'=>'width: 275px; height: 275px; font-size: 70px; padding-top: 75px; text-align: center;'];

  public static $avatar175 = [ 'width'=>'175', 'height'=>'175', 'size'=>'small', 'class'=>'img-circle', 
                              'style'=>'width: 175px; height: 175px; font-size: 40px; padding-top: 55px; text-align: center;'];
  public static $avatar25 = [ 'width'=>'25', 'height'=>'25', 'size'=>'micro', 'class'=>'img-circle', 
                              'style'=>'width: 25px; height: 25px; font-size: 10px; padding-top: 5px; text-align: center;'];
  
  public static $avatar35 = [ 'width'=>'35', 'height'=>'35', 'size'=>'micro',  'class'=>'img-circle', 
                              'style'=>'width: 35px; height: 35px; font-size: 11px; padding-top: 8px; text-align: center;margin-left:3px;'];

  public static $avatar45 = [ 'width'=>'45', 'height'=>'45', 'size'=>'micro',  'class'=>'img-circle', 
                              'style'=>'width: 45px; height: 45px; font-size: 11px; padding-top: 11px; text-align: center; margin-left:3px;'];
  
  public static $business60 = [ 'width'=>'60', 'height'=>'60', 'size'=>'thumbnail', 'class'=>'img-rounded', 
                              'style'=>'width:60px; height:60px; padding-top:10px;font-size:26px;'];
  public static $business40 = [ 'width'=>'40', 'height'=>'40', 'size'=>'micro',  'class'=>'img-rounded', 
                              'style'=>'width:40px; height:40px; padding-top:6px;font-size:18px;'];
  public static $avatar80 = [ 'width'=>'100', 'height'=>'100', 'size'=>'thumbnail', 'class'=>'img-circle', 
                              'style'=>'width:100px; height:100px; padding-top:20px;font-size:36px;'];

  public static function userAvatar($user, $params =  null){
    $url = '';
    $str = '';
    $params = $params?$params: ['width'=>'40', 'height'=>'40', 'size'=>'micro', 'class'=>'img-circle', 
                                'style'=>'width:40px; height:40px; padding-top:6px;'];
    foreach($params as $ii=>$vv){
      if(($ii=='style')||($ii=='class'))
        continue;
      $str.=$ii.'="'.$vv.'" ';
    }

    $class =(isset($params['class'])?$params['class']:'');
    $css =(isset($params['style'])?$params['style']:'');

    $size = isset($params['size'])?$params['size']:'small';

    if(count($user->avatar)>0){
      $url = $user->avatar->image->url($size);
      if($size=='original'){
        return '<img src="'.$url.'" style="max-width:'.$params['width'].'px;" class="'.$class.'" />';
      }
      
      $html = '<div style ="background-image: url('.$url.');';
      $html.= 'background-repeat: no-repeat;';
      if(isset($params['class']) &&(strstr($params['class'], 'img-rounded')))
      {
        $html.='background-position: 50%;';
      }else{
        $html.='background-position: 50%;border-radius: 50%;';
        
      }
      $html.= $css;
      $html.='"></div>';
      return $html;
    }



    $css.='background-color:'.(self::genColorCodeFromText($user->full_name.$user->id)).';';

    $class.= ' dumatar ';

    $html = '<span class="'.$class.'" '.$str.' style="'.$css.'">';

    $html .=strtoupper(mzkGetInitials($user->full_name, $user->email));

    $html .='</span>';

    return $html;
  }
}