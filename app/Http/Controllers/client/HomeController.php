<?php
namespace App\Http\Controllers\client;
use App\Http\Controllers\BaseController;

use Confide, View, Config, Validator, Redirect, Input;
use  App, Auth, DB, Response;

use App\Models\User;
use App\Models\Ad_set;
use App\Models\Call_log;
use App\Models\Check_in;
use App\Models\Ad_slot;
use App\Models\Campaign;
use App\Models\Business ;
use App\Models\Counter;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function dashboard($bid = false)
	{
		$user = User::find(Auth::user()->id);
		$businesses = $user->businesses();

		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = Business::find($current_business_id);

		$data = [];

		$start_date = \Carbon\Carbon::now()->subDays(30);
		$end_date  = \Carbon\Carbon::now();
		$counts = Counter::select()->byBusiness($current_business_id)->betweenDates($start_date, $end_date)->get();

		$buffer = [];
		foreach($counts as $vv){
			$buffer[strtotime($vv->dated)*1000]= $vv->views;
		}
		
		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));

		foreach($date_range as $vv){
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			$data[] = [$dt, ( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		}

		$count_page_views = Counter::select()->byBusiness($current_business_id)->byType('page-views')->betweenDates($start_date, $end_date)->sum('views');
		$count_checkins = $business->check_ins()->count();
		$count_favorites = $business->favourites()->count();


		$counts = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->get();

		$buffer = [];
		foreach($counts as $vv){
			$buffer[strtotime($vv->dated)*1000]= $vv->views;
		}
		
		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));

		foreach($date_range as $vv){
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			$data[] = [$dt, ( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		}

		$count_call_views = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->sum('views');

		return View::make('client.home.dashboard', compact(  'user', 'count_call_views', 'start_date', 'end_date',
    																																			  'business', 'count_page_views',
    																																			  'count_checkins', 'count_favorites',
    																																			  'businesses', 'data'));
	}

	public function setCurrentBusiness($id){
		mzk_client_set_default_business($id);
    return Redirect::back();
	}

	public function clientHomeRedirector(){
		if(Auth::user()->hasRole('admin')){
			$b_id = Business::onlyActive()->orderBy(DB::raw('RAND()'))->take(1)->get()->first()->id;
		}else{
	    $users_businesses = Auth::user()->businesses();
	    $ids = [];
	    foreach($users_businesses as $business){
	      $ids[] = $business->id;
	    }

	    $b_id = $ids[0];
		}

    mzk_client_set_default_business($b_id);
		return Redirect::to('/partner/'.$b_id);
	}


	public function getViewsDataApi($bid = false)
	{

		$input = Input::all();

		$start_date = $input['start'];
		$end_date  = $input['end'];
		$current_business_id = $input['business_id'];
		$counts = Counter::select()->byBusiness($current_business_id)->byType('page-views')->betweenDates($start_date, $end_date)->get();

		$buffer = [];
		foreach($counts as $vv){
			$buffer[strtotime($vv->dated)*1000]= $vv->views;
		}
		
		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));

		foreach($date_range as $vv){
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			$data[] = [$dt, ( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		}

		$count_page_views = Counter::select()->byBusiness($current_business_id)->byType('page-views')->betweenDates($start_date, $end_date)->sum('views');

		$response = ['data'=>$data, 'count_page_views'=>$count_page_views, 'start'=>strtotime($start_date)*1000, 'end'=>strtotime($end_date)*1000];

		return Response::Json($response);
	}


	public function getCallsDataApi($bid =  false)
	{

		$input = Input::all();

		$start_date = $input['start'];
		$end_date  = $input['end'];
		$current_business_id = $input['business_id'];
		$counts = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->get();

		$buffer = [];
		foreach($counts as $vv){
			$buffer[strtotime($vv->dated)*1000]= $vv->views;
		}
		
		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));

		foreach($date_range as $vv){
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			$data[] = [$dt, ( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		}

		$count_call_views = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->sum('views');

		$response = ['data'=>$data, 'count_call_views'=>$count_call_views, 'start'=>strtotime($start_date)*1000, 'end'=>strtotime($end_date)*1000];

		return Response::Json($response);
	}



	public function getCallLogsDataApi(){

		$input = Input::all();

		$start_date = $input['start'];
		$end_date  = $input['end'];
		$current_business_id = $input['business_id'];
		$call_logs = DB::table('call_logs')->select('id', DB::raw('count(*) as total'))
													->where('business_id', '=', $current_business_id)
													->where('dated', '>', $start_date)->where('dated', '<=', $end_date)
													->groupBy('dated')->get();
		$buffer = [];
		foreach($call_logs as $vv){
			$buffer[strtotime($vv->dated)*1000] = $vv->total;
		}

		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));

		foreach($date_range as $vv){
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			$data[] = [$dt, ( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		}

		$response = ['data'=>$data, 'start'=>strtotime($start_date)*1000, 'end'=>strtotime($end_date)*1000];

		return Response::Json($response);


	}






}
