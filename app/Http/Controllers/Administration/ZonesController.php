<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input, Cache;
use App\Models\Zone;

use App\Http\Controllers\Administration\AdminBaseController;

class ZonesController extends AdminBaseController {

	/**
	 * Zone Repository
	 *
	 * @var Zone
	 */
	protected $zone;

	public function __construct(Zone $zone)
	{
		$this->zone = $zone;
    $this->layout = 'layouts.admin-content';

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$zones = Zone::defaultOrder()->get()->linkNodes();
		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.zones.index', compact('zones'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.zones.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Zone::$rules_for_zone);
		$data = Input::only(Zone::$fields);

		if ($validation->passes())
		{
			$images = Input::only('images');
			$zone = $this->zone->create($data);
			$zone->saveImages($images['images']);
			$zone->setCity();
			mzk_reset_all_zone_cache();

			return Redirect::route('admin.zones.index');
		}

		return Redirect::route('admin.zones.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$zone = $this->zone->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.zones.show', compact('zone'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$zone = $this->zone->find($id);

		if (is_null($zone))
		{
			return Redirect::route('admin.zones.index');
		}

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.zones.edit', compact('zone'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method', 'deletablePhotos');
		$validation = Validator::make($input, Zone::$rules_for_zone);
		$data = Input::only(Zone::$fields);

		if ($validation->passes())
		{
			$zone = $this->zone->find($id);
			$zone->update($data);
			$images = Input::only('images');
			$zone->saveImages($images['images']);
			$deletablePhotos = Input::only('deletablePhotos');
			$zone->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);
			mzk_reset_all_zone_cache();

			return Redirect::route('admin.zones.show', $id);
		}

		return Redirect::route('admin.zones.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$zone = $this->zone->find($id);
		$zone->delete();

		return Redirect::route('admin.zones.index');
	}

}
