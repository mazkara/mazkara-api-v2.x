<?php
namespace App\Http\Controllers\Administration;

use App, Excel, DB, Controller, View, Config, Validator, Redirect, Input;
use App\Models\User;
use App\Models\Zone;
#use Chrisbjr\ApiGuard\Models\ApiKey;
use App\Http\Controllers\Administration\AdminBaseController;


class AccountsController extends AdminBaseController {

	/**
	 * Account Repository
	 *
	 * @var Account
	 */
	protected $account;

	public function export(){

    $input = Input::all();

    $nm = 'Mazkara_Data_Dump_Users__';//.($input['page']+1);

Excel::create($nm, function($excel) {

    $excel->sheet('Users', function($sheet) {
    $input = Input::all();

    //$skip = 1000*$input['page'];


			$columns = DB::connection()->getSchemaBuilder()->getColumnListing("users");

			$users = User::select('id', 'username', 'email', 'confirmed', 'created_at', 'updated_at', 
					'name', 'about', 'gender', 'twitter', 'instagram', 'favorites_count', 
					'slug', 'ratings_count', 'reviews_count', 'check_ins_count', 'followers_count', 
					'follows_count', 'phone', 'designation', 'designated_at_text', 'designated_at_url', 
					'contact_email_address', 'location', 'location_id', 'current_locale')->get();



			//byLocale()->take(1000)->skip($skip)->get();
			$users = $users->toArray();
//			foreach($users as $ii=>$user){
//				$user['current_zone'] = '';
//				$user['current_city'] = '';
//				$ap = ApiKey::where('user_id', '=', $user['id'])->get()->first();
//				if($ap){
//					if($ap->current_zone > 0){
//						$zone = Zone::find($ap->current_zone);
//						$user['current_zone'] = $zone->name;
//						$user['current_city'] = $zone->city_id;
//					}
//				}
//				$users[$ii] = $user;
//			}

        $sheet->fromArray($users);

    });

})->export('csv');
	}




	public function __construct(User $account)
	{
		$this->account = $account;
    $this->layout = 'layouts.admin-content';

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$accounts = $this->account->query();

		$input = Input::all();
		if(isset($input['search'])){
			$accounts->search($input['search']);
		}

    $accounts = $accounts->paginate(30);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.accounts.index', compact('accounts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.accounts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function hackValidate($zones, $roles){
		if(in_array('2', $roles) && (count($zones)==0)){
			return false;
		}else{
			return true;
		}
	}

	public function store()
	{
		//$validation = Validator::make($input, User::$rules);
    $data = Input::only(User::$profile_fields);
    $validation = Validator::make($data, User::$profile_rules);
    $roles = Input::only('roles');
    $zones = Input::only('zones');

    if($validation->passes() && ($this->hackValidate($zones['zones'], $roles['roles'])==true) ):

	    $repo = App::make('UserRepository');
	    $user = $repo->signup(Input::all());

	    if ($user->id) {
	    	$user->confirmed = 1;
	    	$user->save();
	      $user->update($data);

	      if(count($roles['roles'])>0)
		      $user->roles()->attach($roles['roles']?$roles['roles']:[]);

	      if(count($zones['zones'])>0)
		      $user->zones()->attach($zones['zones']?$zones['zones']:[]);

				//$this->account->create($input);

				return Redirect::route('admin.accounts.index');
			}else{
		    $error = $user->errors()->all(':message');


			return Redirect::route('admin.accounts.create')
	                ->withInput(Input::except('password'));
	    }
    else:
            //$error = $user->errors()->all(':message');

        return Redirect::back()->withInput(Input::except('password'))
                ->withErrors($validation);
    endif;

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$account = $this->account->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.accounts.show', compact('account'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$account = $this->account->find($id);

		if (is_null($account))
		{
			return Redirect::route('admin.accounts.index');
		}

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.accounts.edit', compact('account'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		//$validation = Validator::make($input, Account::$rules);

    $data = Input::only(User::$profile_fields);
    $validation = Validator::make($data, User::$profile_rules);

    $roles = Input::only('roles');
    $zones = Input::only('zones');

    if($validation->passes() && ($this->hackValidate($zones['zones'], $roles['roles'])==true) ) 
		{
			$account = $this->account->find($id);
			if(strlen($input['password'])==0){
				unset($input['password']);
				unset($input['password_confirmation']);
			}else{
		    $repo = App::make('UserRepository');
        $account->password = $input['password'];
        $account->password_confirmation = $input['password_confirmation'];
				$repo->save($account);
			}

			$account->update($data);
			$account->save();
      //$account->update($data);
      if(count($roles['roles'])>0)
	      $account->roles()->sync($roles['roles']?$roles['roles']:[]);
  
      if(count($zones['zones'])>0)
	      $account->zones()->sync($zones['zones']?$zones['zones']:[]);

			return Redirect::route('admin.accounts.show', $id);
		}

		return Redirect::route('admin.accounts.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->account->find($id)->delete();

		return Redirect::route('admin.accounts.index');
	}

}
