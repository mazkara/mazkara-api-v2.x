<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input;

use App\Models\Review;

use App\Http\Controllers\Administration\AdminBaseController;

class ReviewsController extends AdminBaseController {

	/**
	 * Review Repository
	 *
	 * @var Review
	 */
	protected $review;

	public function __construct(Review $review)
	{
		$this->review = $review;
    $this->layout = 'layouts.admin-content';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$params = Input::all();
		$reviews = $this->review->select('reviews.*');


		if(isset($params['type']) && ($params['type']!='all')){
			if($params['type']=='reviews'){
				$reviews->isReview();
			}elseif($params['type']=='ratings'){
				$reviews->isRating();
			}
		}

		if(isset($params['users'])&& ($params['users']!=0)){
			$reviews->byUser($params['users']);
		}



		if(isset($params['business']) && ($params['business']!=0)){
			$reviews->byBusiness($params['business']);
		}

		$reviews->isNotCheatRate()->byLocale()->orderBy('id', 'DESC');
    $reviews = $reviews->paginate(20);

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.reviews.index', compact('reviews', 'params'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.reviews.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['user_id'] = Auth::user()->id;
		$validation = Validator::make($input, Review::$rules);
		if ($validation->passes())
		{
			$this->review->create($input);

			return Redirect::route('admin.businesses.show', array('id'=>$input['business_id']));
		}

		return Redirect::route('admin.businesses.show', array('id'=>$input['business_id']))
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$review = $this->review->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.reviews.show', compact('review'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$review = $this->review->find($id);

		if (is_null($review))
		{
			return Redirect::route('admin.reviews.index');
		}

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.reviews.edit', compact('review'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		//$validation = Validator::make($input, Review::$rulesForEdit);

		//if ($validation->passes())
		{
			$review = $this->review->find($id);
			$review->update($input);

			return Redirect::route('admin.reviews.show', $id);
		}

		//return Redirect::route('admin.reviews.edit', $id)
			//->withInput()
			//->withErrors($validation)
			//->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$review = $this->review->find($id);
    $business = $review->business;
		$review->delete();

    $business->updateRatingAndReviewsCount();
		
		return Redirect::back();//('admin.reviews.index');
	}

}
