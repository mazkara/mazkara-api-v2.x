<?php
namespace App\Http\Controllers\Administration;

use Confide, BaseController, View, Response, Config, Validator, Redirect, Input;
use App\Models\Business;
use App\Models\Location;
use App\Models\Category;
use App\Models\Merchant;
use App\Models\User;	
use DB, Image;

use App\Http\Controllers\Administration\AdminBaseController;


class MerchantsController extends AdminBaseController {

	/**
	 * Merchant Repository
	 *
	 * @var Merchant
	 */
	protected $merchant;

	public function __construct(Merchant $merchant)
	{
		$this->merchant = $merchant;
    $this->layout = 'layouts.admin-crm';
	}

	public function getUsersForMerchant(){
		$input = Input::all();
		$users = User::byName($input['term'])->get()->toArray();
		foreach($users as $ii=>$user){
			$html = View::make('administration.merchants.partials.user', ['user' => $user]);
			$users[$ii]['html'] = (string) $html;
		}

		return Response::json($users);
	}

	public function getOutletsForMerchant(){
		$input = Input::all();
		$businesses = Business::search($input['term'])->allocatableToMerchants()->get()->toArray();
		foreach($businesses as $ii=>$business){
			$html = View::make('administration.merchants.partials.business', ['business' => $business]);
			$businesses[$ii]['html'] = (string) $html;
		}

		return Response::json($businesses);
	}

	public function getOutletsForMerchantInvoiceForm(){
		$input = Input::all();
		$merchant = Merchant::find($input['merchant_id']);

		$result = array();
		$view = View::make('administration.merchants.partials.business-for-invoice-form', ['merchant' => $merchant]);
		$result['html'] = $view->render();
		
		return Response::json($result);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$merchants = $this->merchant->query()->byLocale();

		$params = [];
		$input =  Input::all();
    if(Input::has('search') && ($input['search']!='')){
      $merchants->bySearch($input['search']);
      $params['search'] = $input['search'];
    }

    if(Input::has('phone') && ($input['phone']!='')){
      $merchants->byVirtualNumber($input['phone']);
      $params['phone'] = $input['phone'];
    }

		$merchants = $merchants->orderby('id', 'desc')->paginate(20);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.merchants.index', compact('params', 'merchants'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.merchants.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::only(Merchant::$fillables);
		$validation = Validator::make($input, Merchant::$rules);

		if ($validation->passes()){
			$merchant = $this->merchant->create($input);
			$merchant->setCity();
			$input = Input::only('businesses', 'users', 'sales_admins');
			$merchant->allocateUsersNPocs($input['users'], $input['sales_admins']);
			$merchant->allocateBusinesses($input['businesses']);

			return Redirect::route('admin.merchants.index');
		}

		return Redirect::route('admin.merchants.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$merchant = $this->merchant->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.merchants.show', compact('merchant'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$merchant = $this->merchant->find($id);

		if (is_null($merchant))
		{
			return Redirect::route('admin.merchants.index');
		}

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.merchants.edit', compact('merchant'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::only(Merchant::$fillables);
		$validation = Validator::make($input, Merchant::$rules);

		if ($validation->passes())
		{
			$merchant = $this->merchant->find($id);
			$merchant->update($input);
			$input = Input::only('businesses', 'users', 'sales_admins');
			$merchant->allocateUsersNPocs($input['users'], $input['sales_admins']);
			$merchant->allocateBusinesses($input['businesses']);

			return Redirect::route('admin.merchants.show', $id);
		}

		return Redirect::route('admin.merchants.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->merchant->find($id)->delete();

		return Redirect::route('admin.merchants.index');
	}

}
