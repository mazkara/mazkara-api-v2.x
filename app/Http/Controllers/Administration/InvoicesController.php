<?php
namespace App\Http\Controllers\Administration;

use Auth, View, Config, Response, App, Validator, Redirect, Mail;
use Input, DB, MazkaraHelper, ViewHelper;

use App\Models\Highlight;

use App\Models\Deal;
use App\Models\Call_log;
use App\Models\Mzk_meta;
use App\Models\Counter;
use App\Models\Business;
use App\Models\Invoice_item;
use App\Models\Invoice;
use App\Models\Category;

use App\Http\Controllers\Administration\AdminBaseController;


class InvoicesController extends AdminBaseController {

	/**
	 * Invoice Repository
	 *
	 * @var Invoice
	 */
	protected $invoice;

	public function __construct(Invoice $invoice)
	{
		$this->invoice = $invoice;
	  $this->layout = 'layouts.admin-finance';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$invoices = $this->invoice->query();//->byLocale();
		$params = [];
		$input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $invoices->search($input['search']);
      $params['search'] = $input['search'];
    }

    if(Input::has('state') && ($input['state']!="")){
      $invoices->byStates($input['state']);
      $params['state'] = $input['state'];
    }

    if(Input::has('merchant_id') && ($input['merchant_id']!="")){
      $invoices->byMerchants($input['merchant_id']);
      $params['merchant_id'] = $input['merchant_id'];
    }

		$invoices = $invoices->orderby('id', 'desc')->paginate(100);
		
		return  View::make('administration.invoices.index', compact('params', 'invoices'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return    View::make('administration.invoices.create');
	}

	public function getInvoice(){
		$input = Input::all();
		$invoice = Invoice::where('title', '=', $input['invoice_no'])->get()->first();
		$result = false;
		if($invoice){
			$result = ['invoice'=>$invoice->toArray()];
		}
		return Response::json($result);//::back();
	}

	public function createPPLForm(){
		return    View::make('administration.invoices.ppl.create');
	}

	public function getCallLogs(){
		$params = Input::all();
		$call_logs = Call_log::query();
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function storePPL(){
		$input = Input::all();
		$validation = Validator::make($input, Invoice::$rules);

		$data = Input::only(Invoice::$fields);
		$outlets = count($input['outlets'])>0 ? $input['outlets'] : [];
		$start_date = $data['start_date'] ? $data['start_date'] : date('Y-m-d');
		$end_date = $data['end_date'] ? $data['end_date'] : date('Y-m-d');
		$unit_price = $input['unit_price'] ? $input['unit_price'] : 5;

		$items = Input::only('items');

		if($validation->passes()){
			$invoice = $this->invoice->create($data);
			$invoice->setAsPPL();
			$invoice->save();

			$call_log_ids = Call_log::query()
															->billable()
															->byBusinesses($outlets)
															->onlyConnected()
															->betweenDates($start_date, $end_date)
															->lists('id', 'id');

			foreach($items['items'] as $itm){
				$item = new Invoice_item($itm);
				$item = $invoice->items()->save($item);
			}

			\DB::table('call_logs')->whereIn('id', $call_log_ids)
							->update(array(	'invoice_item_id' => $item->id, 
															'state'=>Call_log::STATE_BILLED));

			return Redirect::route('admin.invoices.show', [$invoice->id]);
		}

		return Redirect::route('admin.invoices.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}

	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Invoice::$rules);
		$data = Input::only(Invoice::$fields);
		$items = Input::only('items');
		if ($validation->passes())
		{
			$invoice = $this->invoice->create($data);
			foreach($items['items'] as $itm){
				$item = new Invoice_item($itm);
				$invoice->items()->save($item);
			}


		$html =   View::make('administration.invoices.pdf', compact('invoice'))->render();
		$pdf = App::make('dompdf.wrapper');
		$path = storage_path().'/media/invoice-'.md5(time()).'.pdf';
		$pdf->loadHTML($html)->save($path);

		$data = ['invoice'=>$invoice];
		Mail::queue('emails.invoice-ready', $data, function($message) use($invoice, $path)
		{
		    $message->from('finance@mazkara.com', 'Mazkara Finance');

		    $message->to($invoice->poc->email)->subject('You have an invoice #'.$invoice->title.' ['.time().']');

		    $message->attach($path);
		});


			return Redirect::route('admin.invoices.index');
		}

		return Redirect::route('admin.invoices.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$invoice = $this->invoice->findOrFail($id);

		return    View::make('administration.invoices.show', compact('invoice'));
	}

	public function getPdf($id){
		$invoice = $this->invoice->findOrFail($id);
		$businesses = [];
		$items = [];
		if($invoice->isPPL()){
			$business_ids = $invoice->call_logs()->lists('business_id','business_id');
			$businesses = Business::whereIn('id', $business_ids)->get();
			
			foreach($businesses as $business){
				$item = $invoice->items()->first();

				$item->business = $business;
				$item->num_calls = $invoice->call_logs()->where('business_id','=', $business->id)->count();
				$item->calls_total = $item->price*$item->num_calls;
				$items[] = $item;
			}
		}
		$html =   View::make('administration.invoices.pdf', compact('invoice', 'items', 'businesses'))->render();
		$pdf = App::make('dompdf.wrapper');
		$pdf->loadHTML($html);
		return $pdf->download('invoice-'.($invoice->title).'.pdf');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$invoice = $this->invoice->find($id);

		if (is_null($invoice))
		{
			return Redirect::route('admin.invoices.index');
		}

		$merchant = $invoice->merchant;

		return    View::make('administration.invoices.edit', compact('invoice', 'merchant'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Invoice::$rules);

		$data = Input::only(Invoice::$fields);
		$items = Input::only('items');
		$existing_items = Input::only('existing_items');
		if ($validation->passes())
		{

			$invoice = $this->invoice->find($id);
			$invoice->update($data);

			if(is_array($existing_items) && (isset($existing_items['existing_items']))):
				foreach($existing_items['existing_items'] as $id=>$itm){
					$item = Invoice_item::find($id);//($itm);
					$invoice->items()->save($item);
				}
			endif;

			if(is_array($items) && (isset($items['items']))):
				foreach($items['items'] as $itm){
					$item = new Invoice_item($itm);
					$invoice->items()->save($item);
				}
			endif;

			return Redirect::route('admin.invoices.show', $id);
		}

		return Redirect::route('admin.invoices.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}



	public function updateInvoiceItem(){
		$input = Input::all();

		$data = ['id' => $input['pk']];

		$invoice_item = Invoice_item::find($data['id']);

		$invoice_item->$input['name'] = $input['value'];
		$invoice_item->save();
		return Redirect::back();

	}




	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->invoice->find($id)->delete();

		return Redirect::route('admin.invoices.index');
	}

}
