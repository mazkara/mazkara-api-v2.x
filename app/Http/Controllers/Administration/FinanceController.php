<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, DateTime, Redirect, Auth, Input, MazkaraHelper;
//use Vinelab\Http\Client as HttpClient;

use GuzzleHttp\Client as HttpClient;

use App\Models\Invoice;
use App\Models\Credit_note;
use App\Models\Category;
use App\Models\Merchant;
use App\Models\Business;
use App\Models\Finance;
use App\Models\User;
use App\Models\Zone;

use App\Http\Controllers\Administration\AdminBaseController;

class FinanceController extends AdminBaseController {

	/**
	 * Virtual_number_allocation Repository
	 *
	 * @var Virtual_number_allocation
	 */
  protected $finance;


	public function __construct(Finance $finance)
	{
    $this->finance = $finance;
    $this->layout = 'layouts.admin-crm';    
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function reports(){
		//$this->layout = View::make('layouts.admin');

    $input = Input::all();

    $current_month = isset($input['month']) ? $input['month'] : date('n');
    $current_year = isset($input['year']) ? $input['year'] : date('Y');
    $current_city = isset($input['city']) ? $input['city'] : 0;
    $current_user = isset($input['user_id']) ? $input['user_id'] : 0;

    $invoices = Invoice::select()->byMonthYear($current_month, $current_year);
    $current_month_expenses = Finance::select()->byMonthYear($current_month, $current_year)->onlyExpenses()->get();

    if($current_user>0){
      $invoices->byPocs([$current_user]);
    }

    if($current_city>0){
      $invoices->byCity($current_city);
    }


    $invoices = $invoices->get();

    $invoice_ids = $invoices->lists('id', 'id')->all();

    $credit_notes = Credit_note::byInvoices($invoice_ids)->get();
    
    $income = 0;
    $total = 0;
    foreach($invoices as $invoice){
      $c = $invoice->currentMonthContribution($current_month, $current_year);
      $income+=$c;
      $total+=$invoice->amount;
    }
    
    return View::make('administration.finance.reports', compact('invoices', 'current_month_expenses', 'credit_notes', 'current_city', 'total', 'current_month', 'current_year', 'income'));
	}


  public function billingsByPOC(){
    $input = Input::all();

    $start_month = isset($input['start_month']) ? $input['start_month'] : 12;
    $start_year = isset($input['start_year']) ? $input['start_year'] : 2015;

    $end_month = isset($input['end_month']) ? $input['end_month'] : date('n');
    $end_year = isset($input['end_year']) ? $input['end_year'] : date('Y');
    $aed = isset($input['aed'])?$input['aed']:0;
    $usd = isset($input['usd'])?$input['usd']:0;
    $inr = isset($input['inr'])?$input['inr']:0;

    if(($aed+$inr+$usd) == 0){
      $aed = 1;
    }


    $current_city = isset($input['city']) ? $input['city'] : 0;
    $current_user = isset($input['user_id']) ? $input['user_id'] : 0;

    // get all the sales pocs here
    $all_user_ids = Invoice::select()->byMonthYearRange($start_month, $start_year, $end_month, $end_year)->lists('user_id', 'user_id')->all();
    $all_users = User::whereIn('id', $all_user_ids)->get();

    $all_invoice_ids = Invoice::select()->byMonthYearRange($start_month, $start_year, $end_month, $end_year)->lists('id', 'id');

    $result = array();
    $current_month = $start_month;
    $current_year = $start_year;

    $credit_notes = Credit_note::byInvoices($all_invoice_ids)->get();

    while(($current_month.'-'.$current_year != $end_month.'-'.$end_year)):

      $current_month_invoices = Invoice::select()->byMonthYear($current_month, $current_year)->orderby('user_id', 'asc')->get();
      $current_month_expenses = Finance::select()->byMonthYear($current_month, $current_year)->onlyExpenses()->get();

      $period = array();
      $period['month'] = $current_month;
      $period['year'] = $current_year;
      $period['users'] = array();

      $period['total_usd'] = 0;
      $period['total_inr'] = 0;
      $period['total_aed'] = 0;

      $period['credit_total_usd'] = 0;
      $period['credit_total_inr'] = 0;
      $period['credit_total_aed'] = 0;

      $period['expenses_usd'] = 0;
      $period['expenses_inr'] = 0;
      $period['expenses_aed'] = 0;


      $current_user = 0;
      foreach($current_month_invoices as $invoice){
        if($current_user != $invoice->user_id){
          $current_user = $invoice->user_id;
        }

        if(!isset($period['users'][$current_user])){
          $period['users'][$current_user] = array('usd'=>0, 'inr'=>0, 'aed'=>0);
        }

        // add current month contribution to the user
        $period['users'][$current_user]['usd'] += $invoice->currentMonthContributionUSD($current_month, $current_year);
        $period['users'][$current_user]['inr'] += $invoice->currentMonthContributionINR($current_month, $current_year);
        $period['users'][$current_user]['aed'] += $invoice->currentMonthContributionAED($current_month, $current_year);

        $period['total_usd'] += $invoice->currentMonthContributionUSD($current_month, $current_year);
        $period['total_inr'] += $invoice->currentMonthContributionINR($current_month, $current_year);
        $period['total_aed'] += $invoice->currentMonthContributionAED($current_month, $current_year);



      }

        foreach($credit_notes as $credit_note){
          $period['credit_total_usd'] += $credit_note->currentMonthContributionUSD($current_month, $current_year);
          $period['credit_total_inr'] += $credit_note->currentMonthContributionINR($current_month, $current_year);
          $period['credit_total_aed'] += $credit_note->currentMonthContributionAED($current_month, $current_year);
        }

      foreach($current_month_expenses as $expense){
        $period['expenses_usd'] += $expense->amount_usd;
        $period['expenses_inr'] += $expense->amount_inr;
        $period['expenses_aed'] += $expense->amount_aed;
      }


      $result[] = $period;
      $dte = \Carbon\Carbon::createFromDate($current_year, $current_month, 15);
      $dte = $dte->addMonth();
      $current_month = date('n', strtotime($dte));
      $current_year = date('Y', strtotime($dte));

    endwhile;



    
    return View::make('administration.finance.billings-poc', compact('result', 'aed', 'inr', 'usd', 'start_month','start_year','end_month','end_year','all_users'));
  }


  public function billingsByCity(){
    $input = Input::all();

    $start_month = isset($input['start_month']) ? $input['start_month'] : 12;
    $start_year = isset($input['start_year']) ? $input['start_year'] : 2015;

    $end_month = isset($input['end_month']) ? $input['end_month'] : date('n');
    $end_year = isset($input['end_year']) ? $input['end_year'] : date('Y');

    $aed = isset($input['aed'])?$input['aed']:0;
    $usd = isset($input['usd'])?$input['usd']:0;
    $inr = isset($input['inr'])?$input['inr']:0;

    if(($aed+$inr+$usd) == 0){
      $aed = 1;
    }

    $current_city = isset($input['city']) ? $input['city'] : 1;

    // get all the sales pocs here
    $all_user_ids = Invoice::select()->byMonthYearRange($start_month, $start_year, $end_month, $end_year)->lists('user_id', 'user_id')->all();
    $all_cities = Zone::cities()->get();
    $merchants_by_cities = Merchant::select()->lists('city_id', 'id')->all();

    //$all_invoice_ids = Invoice::select()->byMonthYearRange($start_month, $start_year, $end_month, $end_year)->lists('id', 'id');

    $result = array();
    $current_month = $start_month;
    $current_year = $start_year;

    $all_invoice_ids = Invoice::select()->byMonthYearRange($start_month, $start_year, $end_month, $end_year)->lists('id', 'id');

    $credit_notes = Credit_note::byInvoices($all_invoice_ids)->get();

    while(($current_month.'-'.$current_year != $end_month.'-'.$end_year)):

      $current_month_invoices = Invoice::select()->byMonthYear($current_month, $current_year)->orderby('user_id', 'asc')->get();
      $current_month_expenses = Finance::select()->byMonthYear($current_month, $current_year)->onlyExpenses()->get();

      $period = array();
      $period['month'] = $current_month;
      $period['year'] = $current_year;
      $period['cities'] = array();

      $period['total_usd'] = 0;
      $period['total_inr'] = 0;
      $period['total_aed'] = 0;

      $period['credit_total_usd'] = 0;
      $period['credit_total_inr'] = 0;
      $period['credit_total_aed'] = 0;

      $period['expenses_usd'] = 0;
      $period['expenses_inr'] = 0;
      $period['expenses_aed'] = 0;


      $current_city = 1;
      foreach($current_month_invoices as $invoice){
        if($current_city != $invoice->city_id){
          $current_city = $invoice->city_id;
        }

        if(!isset($period['cities'][$current_city])){
          $period['cities'][$current_city] = array('usd'=>0, 'inr'=>0, 'aed'=>0);
        }

        // add current month contribution to the user
        $period['cities'][$current_city]['usd'] += $invoice->currentMonthContributionUSD($current_month, $current_year);
        $period['cities'][$current_city]['inr'] += $invoice->currentMonthContributionINR($current_month, $current_year);
        $period['cities'][$current_city]['aed'] += $invoice->currentMonthContributionAED($current_month, $current_year);

        $period['total_usd'] += $invoice->currentMonthContributionUSD($current_month, $current_year);
        $period['total_inr'] += $invoice->currentMonthContributionINR($current_month, $current_year);
        $period['total_aed'] += $invoice->currentMonthContributionAED($current_month, $current_year);

      }

      foreach($credit_notes as $credit_note){
        $period['credit_total_usd'] += $credit_note->currentMonthContributionUSD($current_month, $current_year);
        $period['credit_total_inr'] += $credit_note->currentMonthContributionINR($current_month, $current_year);
        $period['credit_total_aed'] += $credit_note->currentMonthContributionAED($current_month, $current_year);
      }

      foreach($current_month_expenses as $expense){
        $period['expenses_usd'] += $expense->amount_usd;
        $period['expenses_inr'] += $expense->amount_inr;
        $period['expenses_aed'] += $expense->amount_aed;
      }

      $result[] = $period;
      $dte = \Carbon\Carbon::createFromDate($current_year, $current_month, 15);
      $dte = $dte->addMonth();
      $current_month = date('n', strtotime($dte));
      $current_year = date('Y', strtotime($dte));

    endwhile;



    
    return View::make('administration.finance.billings-city', compact('result', 'aed', 'inr', 'usd', 'start_month','start_year','end_month','end_year','all_cities'));
  }


  public function billings(){
    //$this->layout = View::make('layouts.admin');

    $input = Input::all();

    $this->billingsByPOC();

  }

  public function expenses(){
    $expenses = $this->finance->query();

    $expenses = $expenses->orderby('id', 'desc')->paginate(20);//->get();

    //$this->layout = View::make('layouts.admin');
    return  View::make('administration.finance.expenses.index', compact('expenses'));
  }

  public function getCreateExpense(){
    return View::make('administration.finance.expenses.create');
  }
  
  public function postCreateExpense(){

    $input = Input::all();
    $data = [];
    foreach(Finance::$fields as $f){
      $data[$f] = isset($input[$f])?$input[$f]:'';
    }
    $data['user_id'] = Auth::user()->id;
    $data['type'] = Finance::CREDIT;

    Finance::create($data);

    return Redirect::to(route('finance.expenses'));
    
  }



}


