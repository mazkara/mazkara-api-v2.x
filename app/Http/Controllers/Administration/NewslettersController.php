<?php

namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input;

use App\Models\Post;
use App\Models\User;
use App\Models\Category;

use App\Http\Controllers\Administration\AdminBaseController;

class NewslettersController extends AdminBaseController {

	/**
	 * Comment Repository
	 *
	 * @var Comment
	 */


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->layout = View::make('layouts.admin-crm');
    return View::make('administration.newsletters.index');
	}


	public function weeklyPosts()
	{
    $user = User::where('email', '=', 'ali@mazkara.com')->first();
    $posts = Post::with('cover')->onlyPosts()->isViewable()->orderBy('published_on', 'DESC')->take(5)->get();

		$user->name = "{{NAME}}";

    return View::make('emails.weekly.posts', compact('user', 'posts'));
	}


}
