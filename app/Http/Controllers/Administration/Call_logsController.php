<?php
namespace App\Http\Controllers\Administration;


use View, Config, Validator, Response, Redirect, Input, MazkaraHelper;

use App\Models\Call_log;
use App\Models\Virtual_number;
use App\Models\Comment;
use App\Models\Virtual_number_allocation;
use App\Models\Business;

use App\Http\Controllers\Administration\AdminBaseController;


class Call_logsController extends AdminBaseController {

	/**
	 * Call_log Repository
	 *
	 * @var Call_log
	 */
	protected $call_log, $business;

	public function __construct(Call_log $call_log, Business $business)
	{
		$this->call_log = $call_log;
		$this->business = $business;
    $this->layout = 'layouts.admin-crm';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$call_logs = $this->call_log->all();

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.call_logs.index', compact('call_logs'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.call_logs.create');
	}


	public function getIndex($id)
	{
		$call_logs = $this->call_log->query();
		$business = $this->business->find($id);
		$call_logs = $call_logs->byBusiness($business->id)->onlyConnected()->orderBy('call_logs.id', 'DESC');
		$call_logs = $call_logs->paginate(20);

    return View::make('administration.call_logs.index', compact('call_logs', 'business'));
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Call_log::$rules);

		if ($validation->passes())
		{
			$this->call_log->create($input);

			return Redirect::route('admin.call_logs.index');
		}

		return Redirect::route('admin.call_logs.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$business = $this->business->findOrFail($id);
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.call_logs.show', compact('business'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$call_log = $this->call_log->find($id);

		if (is_null($call_log))
		{
			return Redirect::route('admin.call_logs.index');
		}
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.call_logs.edit', compact('call_log'));
	}

	public function getCallLogsForInvoiceForm(){
		$data = Input::all();
		$outlets = count($data['outlets'])>0 ? $data['outlets'] : [];
		$start_date = $data['start_date'] ? $data['start_date'] : date('Y-m-d');
		$end_date = $data['end_date'] ? $data['end_date'] : date('Y-m-d');
		$unit_price = $data['unit_price'] ? $data['unit_price'] : 5;

		$call_logs = Call_log::query()
														->billable()
														->byBusinesses($outlets)
														->onlyConnected()
														->betweenDates($start_date, $end_date)
														->get();
		$result = array();
		$view = View::make('administration.invoices.ppl.call-logs-list', compact('data', 'call_logs'));
		$result['html'] = $view->render();
		return Response::Json($result);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Call_log::$rules);

		if ($validation->passes())
		{
			$call_log = $this->call_log->find($id);
			$call_log->update($input);

			return Redirect::route('admin.call_logs.show', $id);
		}

		return Redirect::route('admin.call_logs.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->call_log->find($id)->delete();

		return Redirect::route('admin.call_logs.index');
	}

}
