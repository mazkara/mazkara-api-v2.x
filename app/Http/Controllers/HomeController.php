<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Mail, Validator, Request, Lang;
use MazkaraHelper;

use App\Models\Ad;
use App\Models\Business;
use App\Models\Post;
use App\Models\Service;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Review;
use App\Models\Activity;
use App\Models\Group;
use App\Models\Zone;
use App\Models\Job;
use App\Models\User;


class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $meta;

  public function __construct() {

  }

  public function incrementCTR(){
    $inp = Input::all();
    $ad = Ad::find($inp['ad_id']);
    $ad->incrementCTRCount();
  }

  private function getButtons(){
    $buttons = [];


    $buttons[] = [
                    'title'=>'Luxury Salon & Spas',
                    'sub-title'=>'150+ listings',
                    'image'=>'luxury.jpg',
                    'params'=>['highlights'=>[12]]
                  ];

//    $buttons[] = [
//                    'title'=>'Kid Services',
//                    'sub-title'=>'70+ listings',
//                    'image'=>'kid-services.jpg',
//                    'params'=>['highlights'=>[14]]
//                  ];
//    $buttons[] = [
//                    'title'=>'Medi-Spas',
//                    'sub-title'=>'40+ listings',
//                    'image'=>'medispa.jpg',
//                    'params'=>['highlights'=>[17]]
//                  ];
//    $buttons[] = [
//                    'title'=>'Wedding',
//                    'sub-title'=>'300+ listings',
//                    'image'=>'wedding.jpg',
//                    'params'=>['service'=>[30]]
//                  ];
    $buttons[] = [
                    'title'=>'Hair Consulting',
                    'sub-title'=>'150+ listings',
                    'image'=>'hair-consult.jpg',
                    'params'=>['service'=>[24]]
                  ];

    $buttons[] = [
                    'title'=>'Home Services',
                    'sub-title'=>'40+ listings',
                    'image'=>'home.jpg',
                    'params'=>['highlights'=>[13]]
                  ];

    $buttons[] = [
                    'title'=>'Gel Nails',
                    'sub-title'=>'600+ listings',
                    'image'=>'gel.jpg',
                    'params'=>['service'=>[46]]
                  ];

    $buttons[] = [
                    'title'=>'Weight Loss',
                    'sub-title'=>'80+ listings',
                    'image'=>'weight-loss.jpg',
                    'params'=>['service'=>[12]]
                  ];


    $buttons[] = [
                    'title'=>'Manicure',
                    'sub-title'=>'1000+ listings',
                    'image'=>'manicure.jpg',
                    'params'=>['service'=>[47]]
                  ];

    $buttons[] = [
                    'title'=>'Moroccan Bath',
                    'sub-title'=>'250+ listings',
                    'image'=>'moroccon.jpg',
                    'params'=>['service'=>[8]]
                  ];

    $buttons[] = [
                    'title'=>'Couple Massage',
                    'sub-title'=>'40+ listings',
                    'image'=>'couple.jpg',
                    'params'=>['service'=>[75]]
                  ];
    return $buttons;

  }

  public function email(){
    $user = User::where('email', '=', 'ali@mazkara.com')->first();
    $posts = Post::with('cover')->onlyPosts()->isViewable()->take(5)->get();


    return View::make('emails.weekly.posts', compact('user', 'posts'));
  }


	public function index(){
		//if (App::environment('production')){
		//	return View::make('hello');
		//}

    $cc = Location::get(Request::getClientIp())->countryCode;

    $segment = Request::segment(1);
    $segment = !is_null($segment) ? $segment : (($cc == "IN") ? 'pune' : $segment);

    if($cc=='IN'){
      MazkaraHelper::setLocale($segment);
    }

    $description = !is_null($segment) ? sprintf(Lang::get('seo.city-description'), ucwords($segment)) : Lang::get('seo.description');

    $this->meta->set(
    		array('title' => Lang::get('seo.title').' - Discover salons and spas in your neighborhood', 
    					'description' => $description,
    					'og'=>[
    						'title' => Lang::get('seo.title'),
								'description' => $description
    					]
			)
  	);



    $business_count = 4500;//mzk_round_to_hundred(Business::select()->onlyActive()->byLocale()->count());

    $posts = Post::with('cover', 'likes', 'comments')->onlyPosts()->isViewable()->orderBy('id', 'DESC')->take(5)->get();

    $featured_venues = Business::select()->byLocale()->hasPhotos()->onlyActive()->orderBy('rating_average', 'DESC')->take(10)->get();

    $buttons = $this->getButtons();
    
    return View::make('index', compact('buttons', 'services', 'featured_venues', 'posts'))
                      ->with('meta', $this->meta)
                      ->with('business_count', $business_count);;

    return View::make('layouts.home')
                      ->with('meta', $this->meta)
                      ->with('business_count', $business_count)
                      ->with('content');

    return view($this->layout, ['content' => $content]);
	}



  public function feed(){
    //if (App::environment('production')){
    //  return View::make('hello');
    //}
    //remember(60)
    $zones = Zone::select()->showActive()->byLocale()->defaultOrder()->get()->linkNodes();
    $this->meta->set(
        array('title' => Lang::get('seo.title').' - Discover salons and spas in your neighborhood', 
              'description' => Lang::get('seo.description'),
              'og'=>[
                'title' => Lang::get('seo.title'),
                'description' => Lang::get('seo.description')
              ]
      )
    );

    $buttons = $this->getButtons();

    $this->layout = View::make('layouts.page')->with('meta', $this->meta);
    if(Auth::check()){
      $user = User::find(Auth::user()->id);
      $followed_users = array_keys($user->followsUsers()->lists('favorable_id','favorable_id'));
      if(Auth::user()->hasRole('admin')){
      $activities = Activity::select()->orderby('id', 'desc')->paginate(10);

      }else{
        $activities = Activity::select()->byUserIds($followed_users)->orderby('id', 'desc')->paginate(10);
      }
      $suggested_salons = Business::select()->hasPhotos()->onlyActive()->where('zone_id', '>', 0)->takeRandom(10)->get();

      $suggested_users_to_follow = User::select()->excludeUser($user->id)->orderBy('id', 'DESC')->take(10);//notFollowedBy($user->id);
      $this->layout->content = View::make('site.users.index', compact('zones', 'buttons', 'suggested_salons', 'suggested_users_to_follow', 'user', 'services', 'activities' ));
    }
  }

  public function theFabReviewContest(){

    $this->meta->set(
        array('title' => 'The Fab Review Contest - Mazkara', 
              'description' => Lang::get('seo.description'),
              'og'=>[
                'title' => 'The Fab Review Contest - Mazkara',
                'description' => Lang::get('seo.description')
              ]
      )
    );

    $dubai_user = User::find(2630);
    $pune_user = User::find(1912);

    $dubai_review = Review::find(10917);
    $pune_review = Review::find(10977);


    return View::make('review-contest', compact('dubai_user', 'dubai_review', 'pune_review', 'pune_user'))->with('meta', $this->meta);
  }


	public function jobs(){
		$cities = Zone::getCitiesArray();

    $this->meta->set(
    		array('title' => Lang::get('seo.jobs_title').', Jobs in '.join(', ',$cities), 
    					'description' => Lang::get('seo.jobs_description').', Jobs in '.join(', ',$cities),
    					'og'=>[
    						'title' => Lang::get('seo.jobs_title').', Jobs in '.join(',',$cities),
								'description' => Lang::get('seo.jobs_description').', Jobs in '.join(', ',$cities)
    					]
			)
  	);

		$jobs = Job::where('state','=', 'active')->get();
		return View::make('jobs', compact('jobs', 'cities'))->with('meta', $this->meta);
	}

	public function showJob($id){
		$cities = Zone::getCitiesArray();
		$job = Job::find($id);
    $this->meta->set(
    		array('title' => $job->title.','.Lang::get('seo.jobs_title').', Jobs in '.join(', ',$cities), 
    					'description' => Lang::get('seo.jobs_description').', Jobs in '.join(', ',$cities),
    					'og'=>[
    						'title' => $job->title.','.Lang::get('seo.jobs_title').', Jobs in '.join(', ',$cities),
								'description' => Lang::get('seo.jobs_description').', Jobs in '.join(', ',$cities)
    					]
			)
  	);

		return View::make('job', compact('job', 'cities'))->with('meta', $this->meta);
	}

  public function changeLocale($from, $to){
    $redirect_to = URL::previous();
    MazkaraHelper::setLocale($to);

    if(strstr($redirect_to.'/', '/'.$from.'/') || (rtrim($redirect_to, '/') == Request::root())){
      $redirect_to = rtrim(Request::root(), '/').'/'.$to;
    }

    return Redirect::to($redirect_to);
  }


  public function applyJob(){

    $input = Input::all();


    $rules = array(
        'name' => 'Required|Min:7',
        'email'     => 'Required|Email',
        'phone' => 'Required',
        'message' => 'Required',
        'cv' => 'Required'
    );


    $validation = Validator::make(Input::all(), $rules);

    if ($validation->fails()){
        // Validation has failed.
        return Redirect::back()->withInput()->withErrors($validation);
    }

    $input['job'] = Job::find($input['job_id']);
    $data = $input;
    Mail::send('emails.application',['data'=>$input, 'server'=>$_SERVER], function($message) use ($input)
    {
        $message->to('jobs@mazkara.com');
        $message->subject($input['name'].' applied for post of '.$input['job']->title.' on Mazkara ['.time().']');
        $message->from($input['email']);
        $message->attach($input['cv']->getRealPath(), array(
            'as' => 'resume.' . $input['cv']->getClientOriginalExtension(), 
            'mime' => $input['cv']->getMimeType())
        );
    });

    return Redirect::back()->with('notice', 'Thanks for applying we will get back to you soon');
;

  }

  public function redirectToClientDashboard(){
    $input = Input::all();
    mzk_client_set_default_business($input['id']);
    return Redirect::to('/partner/'.$input['id']);
  }


	public function about(){
		$this->meta->set(['title'=>'About Mazkara']);
		return View::make('about')->with('meta', $this->meta);
	}

	public function contact(){
		$this->meta->set(['title'=>'Contact us at Mazkara']);
		return View::make('contact')->with('meta', $this->meta);
	}

	public function tos(){
		$this->meta->set(['title'=>'Mazkara - Terms of Service']);
		return View::make('tos')->with('meta', $this->meta);
	}

	public function privacy(){
		$this->meta->set(['title'=>'Mazkara - Privacy Policy']);
		return View::make('privacy')->with('meta', $this->meta);
	}

	public function cookie(){
		$this->meta->set(['title'=>'Mazkara - Cookie Policy']);
		return View::make('cookie')->with('meta', $this->meta);
	}
  
  public function faq(){
    $this->meta->set(['title'=>'Mazkara - FAQ']);
    return View::make('faq')->with('meta', $this->meta);
  }

	public function postContact(){
		$rules = array('name'=>'required','phone'=>'required','email'=>'required','message'=>'required');
		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()){

			Mail::send('emails.contact', ['data'=>$input, 'server'=>$_SERVER], function($message) {
				$message->to('hello@mazkara.com', 'Mazkara')
                ->cc('business@mazkara.com', 'Mazkara')
                ->subject('Mazkara: Message from contact form ['.time().']');
			});			

			return Redirect::back()->with('notice', 'Thanks for getting in touch.');
		}
		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

		return Redirect::back();
	}



  public function unsubscribe(){
    $rules = array('email'=>'required');
    $input = Input::all();
    $validation = Validator::make($input, $rules);

    if ($validation->passes()){

      Mail::send('emails.unsubscribe', ['data'=>$input['email'], 'server'=>$_SERVER], function($message) use ($input) {
        $message->to('marketing@mazkara.com', 'Mazkara')
                ->subject('Mazkara: Unsubscribe '.$input['email'].'  ['.time().']');
      });     
      $this->layout = View::make('layouts.page');
      $this->layout->content = View::make('unsubscribe', ['email'=>$input['email']]);
    }else{
      return Redirect::to('/error');
    }
  }

  public function apiRedirectHack(){
    $url = 'http://api.mazkara.com/white?'.$_SERVER['QUERY_STRING'];
    return Redirect::to($url);
  }




	public function showWelcome()
	{
		return View::make('hello');
	}

	public function zones(){
	}

	public function getReport(){
		$input = Input::all();
		$url = $input['url'];
		return View::make('report', compact('url'));
	}

	public function postReport(){
		$rules = array('name'=>'required','phone'=>'required','email'=>'required','message'=>'required');
		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()){

			Mail::send('emails.report', ['data'=>$input, 'server'=>$_SERVER], function($message) {
				$message->to('moderation@mazkara.com', 'Mazkara')
                ->subject('Mazkara['.MazkaraHelper::getLocaleLabel().']: Has a Report ['.time().']');
			});			

			return Redirect::back()->with('notice', 'Thanks - a customer support executive would contact you soon.');
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

		return Redirect::back();
	}

	public function getAddBusiness(){
		$this->meta->set(['title'=>'Add your business on Mazkara']);

		return View::make('add-business')->with('meta', $this->meta);
	}

	public function postAddBusiness(){
		$rules = array('name'=>'required','phone'=>'required');
		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()){

			Mail::send('emails.add-business', ['data'=>$input, 'server'=>$_SERVER], function($message)  use ($input) {
				$message->to('business@mazkara.com', 'Mazkara')
                ->subject('Mazkara['.MazkaraHelper::getLocaleLabel().']: '.$input['name'].' wants to add a new business ['.time(). ']');
			});			
			return Redirect::back()->with('notice', 'We have received your request and will get in touch with you shortly');
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

		return Redirect::back();
	}


  public function share(){
    $data = Input::all();
    $status_update = ['message' => $data['message']];
  	$status_update['link'] = $data['url'];

  	if($data['sharable']=='Business'):
	  	$business = Business::find($data['sharable_id']);
			$status_update['name'] = $business->getSeoTitle();
			$status_update['description'] = $business->getSeoDesc();
			$status_update['picture'] = $business->getSeoThumbnailUrl();
		elseif($data['sharable']=='Review'):
	  	$review = Review::find($data['sharable_id']);
	  	$business = $review->business;

	  	if(Auth::user()->id == $review->user_id):
				$status_update['name'] = 'I just reviewed '.$business->getSeoTitle();
			else:
				$status_update['name'] = 'Check out this review for '.$business->getSeoTitle();
			endif;
			$status_update['description'] = $review->body;
			$status_update['picture'] = $business->getSeoThumbnailUrl();
		endif;

    $token =  MazkaraHelper::fbToken();
    Facebook::setAccessToken($token);
    $share_data = array('type'=>Share::FACEBOOK, 
                        'user_id'=>Auth::user()->id,
                        'sharable_type'=>$data['sharable_type'],
                        'sharable_id'=>$data['sharable_id']);
    Share::create($share_data);

    $response = Facebook::object('me/feed')->with($status_update)->post();
  }

  public function searchZones(){
    //$cities = Zone::query()->cities()->showActive()->get()->lists('id', 'id');
    $result = [];
    //->whereIn('city_id', $cities+[null,0])
    $url = URL::previous();

    if(Input::has('term')):
      $search = Input::get('term');
      $zones = Zone::query()->search($search)->showActive()->get()->take(10);
      foreach($zones as $zone){
        $result[] = [ 'id' => $zone->id, 'value' => $zone->id, 'name' => $zone->name, 
                      'label' => $zone->isCity() ? join(' ', [$zone->name, ' ('.$zone->countryFromCountryCode().')']) : join(' ', [$zone->name, ' ('.$zone->parent->name.')']), 
                      'url'=> $zone->isCity()? mzk_url_city_change($url, $zone->slug):false,
                      'type' => $zone->isCity() ? 'city' : 'zone'];
      }
    endif;

    return Response::Json($result);
  }

	public function search(){
		$q = Input::get('term');
		$service_lists = Service::query()->showOnly()->byLocaleActive()->showOnly()->search($q)->take(5)->get();
    $num_outlets = count($service_lists) < 5 ?(5 - count($service_lists))+2:2;
    $outlets = Business::query()->select('name', 'zone_cache', 'id', 'slug')->byLocale(mzk_get_localeID())->search($q)->onlyActive()->take($num_outlets)->get();
    $categories = Category::query()->byLocaleActive()->search($q)->take(3)->get()->toArray();


		$chains = Group::search($q)->byLocale()->take(3)->get();
		$result = [];
		$zone = Input::get('zone');
		if(empty($zone)){
			$zone = false;
		}else{
			$zone = Zone::find($zone);
		}
    $services = [];
    foreach($service_lists as $v){
      
      $p = [
            'service' =>[
                          $v->id
                        ]
            ];
      $service_name = '';
      if($v->parent_id > 0){
        $service_name = '('.$v->parent->name.') ';
      }

      $service_name.= $v->name;

      $result[] = [ 'name'=>$service_name, 'id'=>$v->id, 'type'=>'SERVICE', 
                    'url' => ($zone ? MazkaraHelper::slugCityZone($zone, $p):MazkaraHelper::slugCity(null, $p)) ];

    }

		foreach($outlets as $v){
			$result[] = ['name'=>$v->name, 
										'zone'=>$v->zone_cache,
									'type'=>'OUTLET', 
									'url'=>MazkaraHelper::slugSingle($v)];
		}

	  foreach($categories as $v){
			$p = ['category'=>[$v['id']]];
			$result[] = ['id'=>$v['id'],'name'=>$v['name'], 
									'type'=>'CATEGORY', 
									//'url' => ($zone?MazkaraHelper::slugCityZone($zone, $p):MazkaraHelper::slugCity(null, $p)) 
                  ];
		}


		foreach($chains as $v){
			$result[] = [	'name'=>$v->name, 'type'=>'CHAIN', 'label'=>$v->businesses->count().' OUTLET(S)', 
									'url' => (MazkaraHelper::slugCityChain(null, $v)) ];
  	}

		return Response::json($result);

	}

}
