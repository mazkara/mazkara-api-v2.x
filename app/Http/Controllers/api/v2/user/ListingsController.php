<?php
namespace App\Http\Controllers\api\v2\user;

use Illuminate\Routing\Controller;
use Validator;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use MazkaraHelper, Paginator;

use App\Models\Activity;
use App\Models\Business;
use App\Models\Category;
use App\Models\Check_in;
use App\Models\Timing;
use App\Models\Highlight;
use App\Models\Photo;
use App\Models\Post;
use App\Models\Favorite;
use App\Models\Group;
use App\Models\Service;
use App\Models\User;
use App\Models\Zone;

use Auth, DB, Input, Mail, URL, View, Redirect, Response;
use Location, Request, Lang;

class ListingsController extends Controller {


  protected $business;
  protected $feed_manager;
  protected $title;
  protected $breadcrumbs;
  protected $businesses;
  protected $params;
  protected $ads;
  protected $filters;
  protected $meta;
  protected $where;
  protected $what;
  protected $deliverables;
  protected $favorites;


  public function __construct(Business $business, Activity $activity){
    $this->business = $business;
    $this->feed_manager = $activity;


    $this->businesses = [];
    $this->nearby_businesses = [];
    $this->params = [];
    $this->filters = [];

    $this->favorites = [];
    
    $this->what = '';
    $this->where = '';

    $this->deliverables = [];
  }

  protected function setParams($key, $val){
    $this->params[$key] = $val;
  }

  protected function getParams($key){
    return isset($this->params[$key]) ? $this->params[$key] : '';
  }

  protected function setFilters($key, $val){
    $this->filters[$key] = $val;
  }

  protected function getFilters($key){
    return isset($this->filters[$key])?$this->filters[$key]:false;
  }

  protected function initListings($tables = null){
    $tables = is_null($tables) ? ['businesses.*'] : $tables;
    if(!(Input::has('zone') && (Input::get('zone')==-1))){
      
      $tables = 'businesses.*';
    }else{
      $tables = '*';
    }
    $this->businesses = $this->business->select($tables)
                              /*->with( 'photos', 'favourites', 'offers',
                                      'active_offers', 'virtual_number_allocations',
                                       'cover', 'highlights', 'zone', 'services')*/
                              ->isDisplayable()->byLocale();
    return $this->businesses;
  }

  protected function initListingsNoLocale(){
    //onlyActive()
    if(!(Input::has('zone') && (Input::get('zone')==-1))){
      
      $tables = 'businesses.*';
    }else{
      $tables = '*';
    }

    $this->businesses = $this->business->select($tables)->with('photos');
    return $this->businesses;
  }






  protected function initListingsNoLocaleMinimal(){
    $this->businesses = $this->business->select('businesses.id', 'businesses.name', 'businesses.geolocated', 
                                                'businesses.zone_id', 'businesses.active', 'businesses.zone_cache', 
                                                'businesses.geolocation_address', 'businesses.geolocation_city', 
                                                'businesses.total_ratings_count', 'businesses.phone', 'businesses.rating_average')
                                        ->with('photos')->isDisplayable();//->onlyActive();
    return $this->businesses;
  }


  protected function setupBusinessSingle($business, $user, $lat = false, $lon = false, $offers = false){
      $business->resetMetaForApi();

      $business->user_has_checked_in = $user->hasCheckedIn($business->id);
      $business->user_has_favourited = $user->hasFavourited('Business', $business->id);

      $distance_from = false;

      if(!is_null($lat) && !is_null($lon)){
        $distance_from = mzk_distance_between($business->geolocation_latitude, 
                                              $business->geolocation_longitude, 
                                              $lat, $lon);
      }

      $data = [
        'id'=>$business->id,
        'name'=>$business->name,
        'description'=>$business->description,
        'website'=>$business->website,
        'zone_id'=>$business->zone_id,
        'geolocated'=>$business->geolocated,
        'geolocation_city'=>$business->geolocation_city,
        'geolocation_state'=>$business->geolocation_state,
        'geolocation_country'=>$business->geolocation_country,
        'geolocation_address'=>$business->geolocation_address,
        'created_at'=>(string)$business->created_at,
        'updated_at'=>(string)$business->updated_at,
        'active'=>$business->active,
        'chain_id'=>$business->chain_id,
        'landmark'=>$business->landmark,
        'geolocation_longitude'=>$business->geolocation_longitude,
        'geolocation_latitude'=>$business->geolocation_latitude,
        'rate_card_count'=>$business->rate_card_count,
        'image_count'=>$business->image_count,
        'reviews_count'=>$business->reviews_count,
        'favorites_count'=>$business->favorites_count,
        'checkins_count'=>$business->checkins_count,
        'services_count'=>$business->services_count,
        'categories_count'=>$business->categories_count,
        'highlights_count'=>$business->highlights_count,
        'zone_cache'=>$business->zone_cache,
        'rating_average'=>$business->rating_average,
        'user_has_checked_in' => $user->hasCheckedIn($business->id),
        'user_has_favourited' => $user->hasFavourited('Business', $business->id),
        'phone'=>$business->displayablePhone(),
        'is_open' => $business->isOpenNow(),
        'is_open_until' => $business->openUntilNow(),
        'will_open' => $business->willOpen(),        
        'has_offers' => $business->active_offers_count > 0 ? true : false,

      ];

      if($offers!=false){
        $data['offers'] = [];
        $offers = $business->offers()->onlyActive()->get();

        $photos  = array();
        foreach($business->photos as $photo){
          $photos[$photo->id] = $photo->image->url('xlarge');
        }

        foreach($offers as $ii=>$offr){
          $offer = $offr->toArray();
          $data['offers'][$ii] = $offer;
          $data['offers'][$ii]['price_meta'] = $offr->price_meta;
          mzk_shuffle_assoc($photos);
          $data['offers'][$ii]['currency'] = mzk_currency_symbol($business->city_id);
          $p = Photo::where('imageable_type', '=','Zone')->where('imageable_id', '=', $offer['id'])->get();
          $data['offers'][$ii]['photos'] = $photos;
        }
      }

    if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage()){
      $data['photo'] = mzk_cloudfront_image($business->getCoverUrl('largeCropped'));
    }else{
      $data['photo'] = '';
    }

      if(isset($business->distance)){
        $data['distance_raw'] = $business->distance;
        if($data['distance_raw']>=1){
          $data['distance'] = round($data['distance_raw'], 1).'km';
        }else{
          $data['distance'] = (round($data['distance_raw'], 3)*1000).'m';
        }
      }

      if(isset($this->params['service']) && (count($this->params['service'])>0)){
        $service = $this->params['service'][0];
        $service = $business->services()->where('services.id', '=', $service)->first();
        if($service /*&& ($business->has_sample_menu ==0)*/){
          $data['service_name'] = $service->name;//['']
          $data['service_starting_price'] = $service->pivot->starting_price;
          $data['service_currency'] = 'AED';
          $data['service_display_starting_price'] = $service->getDisplayablePrice(MazkaraHelper::getCitySlugFromID($business->city_id));
        }/*else{
          $data['service_name'] = '';
          $data['service_starting_price'] = '';
          $data['service_currency'] = '';
          $data['service_display_starting_price'] = '';
        }*/
      }

    return $data;
  }

  public function incrementBusinessCalls(){
    $id = Input::get('id');
    $business = $this->business->findOrFail($id);

    $business->incrementCallsCount();
    $result = mzk_api_response([], 200, true, 'Success');
    return Response::Json($result);
  }

	public function getIndex(){
    $lat = Input::has('latitude') ? Input::get('latitude') : false;
    $lon = Input::has('longitude') ? Input::get('longitude') : false;
    $offers = Input::has('offers') ? Input::get('offers') : false;

    $params = [];
    $key = Input::get('key');
    $apiKey = ApiKey::where('key', '=', $key)->get()->first();

    $this->initListingsNoLocale();

    if(($lat!=false) && ($lon!=false)){
      $params['latitude'] = $lat;
      $params['longitude'] = $lon;
    }else{
      if($apiKey){
        if($apiKey->current_zone>0){
          $z = Zone::find($apiKey->current_zone);
          $this->businesses = $this->businesses->byLocale($z->isCity()?$z->id:$z->city_id);
        }
      }
    }

    $this->filterAllListings(['nearby'=>true]);
    if(($lat!=false) && ($lon!=false)){
      $this->SortListings('distance');
    }else{

      $this->SortListings();

    }

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);


    $businesses = $this->businesses->isDisplayable()->paginate(20);//take(20)->get();
    $entries = [];
    
    foreach($businesses as $i=>$business){
      $businesses[$i] = $this->setupBusinessSingle($business, $user, $lat, $lon, $offers);
    }

    $businesses = $businesses->toArray();

    //set the current users location if he selected a zone
    if((Input::has('zone')) && (Input::get('zone')!=-1)){
      if($apiKey){
        $z = Input::get('zone');
        $apiKey->current_zone = is_array($z) ? array_pop($z) : $z;
        $apiKey->save();
      }
    }

    if((Input::has('zone')) && (Input::get('zone')==-1)){ // user enabled auto detect location
      // dirty hack - pick the zone of the first business entry
      if($businesses['current_page'] == 1){
        if(count($businesses['data'])>0){
          $apiKey->current_zone = ($businesses['data'][0]['zone_id']);
          $apiKey->save();
        }
      }
    }

    $params = Input::all();

    $result = mzk_api_response(compact('businesses', 'params'), 200, true, 'Success');

    return Response::Json($result);
  }


  protected function SortListings($force_sortable = false){
    if(!(Input::has('sort') && (Input::get('sort')!=''))){
      $sort = 'popularity';
    }else{
      $sort = Input::get('sort');
    }

    $sort = $force_sortable != false?$force_sortable:$sort;

    $this->setParams('sort', $sort);
    if(!(Input::has('zone') && (Input::get('zone')==-1))){
      $this->businesses->groupBy('businesses.id');
    }

    switch($sort){
      case 'nameAsc':
      case 'name':
        $this->businesses->orderBy('name', 'ASC');
      break; 
      case 'popularity':
        $this->businesses->orderBy('popularity', 'DESC');
      break; 
      case 'distance':
        $this->businesses->orderBy('distance', 'ASC');
      break; 
      case 'likes':
        $this->businesses->orderBy('favorites_count', 'DESC');
      break; 
      case 'ratings':
      case 'rating':
        $this->businesses->orderBy('rating_average', 'DESC');
      break; 
      case 'nameDesc':
        $this->businesses->orderBy('name', 'DESC');
      break; 
      case 'lastUpdateAsc':
        $this->businesses->orderBy('updated_at', 'ASC');
      break; 
      case 'lastUpdateDesc':
        $this->businesses->orderBy('updated_at', 'DESC');
      break; 
    }


    return $this->businesses;
  }


  protected function bySearchListings($search){
    $this->businesses->searchNameServices($search);



    $this->setParams('search', $search);
    $this->setFilters('search', $search);

    return $this->businesses;
  }

  protected function mergeNearbyWithSearch(){
    $this->businesses = new Collection($this->businesses);
    $this->businesses = $this->businesses($this->nearby_businesses); 
  }

  protected function byLeveledZoneListings($zones, $force = false){
    //$this->nearby_businesses = $this->businesses;
    $znes = [];
    $szones = [];

    foreach($zones as $z):
      // get all the siblings of each zone now
      $znes[] = $z;// get all desendents of this zone
      //$mrgers = MazkaraHelper::getZonesChildrenListAttributes($z);
      //$mrgers = array_keys(Zone::remember(60, 'zones.descendents.of.'.$z)->descendantsOf($z)->lists('name', 'id')->all());
      $mrgers = array_keys(Zone::descendantsOf($z)->lists('name', 'id')->all());
      $znes = array_merge($znes, $mrgers);

      //$znes = array_merge($znes, array_keys(Zone::descendantsOf($z)->lists('name', 'id')));
      // if this is a first level subzone - then don't get the siblings
      // but if it is a second level subzone
      if(Zone::find($z)->isSubZone()):
        // get all the siblings here
        $siblings = array_keys(Zone::find($z)->siblings()->get()->lists('name', 'id')->all());
        $szones = array_merge($szones, $siblings);
        foreach($siblings as $zn){
          // get decendents of each zone here
          $znes[] = $zn;
          
          //$des = MazkaraHelper::getZonesChildrenListAttributes($zn);//Zone::descendantsOf($zn)->lists('name', 'id');
          $des = Zone::descendantsOf($zn)->lists('name', 'id')->all();

          $znes = array_merge($znes, array_keys($des));
          $szones = array_merge($szones, array_keys($des));

        }
      endif;

    endforeach;

    $this->nearby_businesses = clone($this->businesses);
    $this->businesses->ofZones($znes);
    if($force == true){
      $this->nearby_businesses->ofZones($szones);
    }

    //$this->nearby_businesses = $this->businesses;
  }

  protected function byOnlyZoneListings($zones){
    $znes = $zones;
    $zz = $zones;

    foreach($zz as $z):
      //$mrgers = MazkaraHelper::getZonesChildrenListAttributes($z->id);
      //$mrgers = array_keys(Zone::remember(60, 'zones.descendents.of.'.$z)->descendantsOf($z)->lists('name', 'id')->all());
      $mrgers = array_keys(Zone::descendantsOf($z)->lists('name', 'id')->all());
      $znes = array_merge($znes, $mrgers);
    endforeach;


    $this->businesses->ofZones($znes);
  }

  protected function byZoneListings($zones){
    $this->byOnlyZoneListings($zones);
    $this->setParams('zone', $zones);
    $this->setFilters('zone',  Zone::whereIn('id', $zones)->lists('name', 'id')->all());
  }

  protected function byCoordinatesNearby($latitude, $longitude){
    $this->businesses->NearLatLng($latitude, $longitude);
    return $this->businesses;
  }

  protected function byDealListings($deals){
    $this->businesses->hasActivePackages();
    $this->setParams('deals', $deals);
    $this->setFilters('deals', 'Deals/Packages');

    return $this->businesses;
  }

  protected function byActiveOffers(){
    $this->businesses->hasActiveOffers();
    $this->setParams('specials', 'true');
    $this->setFilters('specials', 'Specials');

    return $this->businesses;
  }

  protected function byPackageListings($deals){
    $this->businesses->hasActivePackages();
    $this->setParams('packages', $deals);
    $this->setFilters('packages', 'Packages Available');

    return $this->businesses;
  }

  protected function byOpenNow(){
    $this->businesses->openNow();
    $this->setParams('open', 'now');
    $this->setFilters('open', 'Open Now');

    return $this->businesses;
  }

  protected function byChains($chains){
    $this->businesses->ofChains($chains);

    return $this->businesses;
  }

  protected function byCostEstimate($estimate){
    $this->businesses->byCostEstimate($estimate);
    $this->setParams('cost', $estimate);
    $this->setFilters('cost', $estimate);

    return $this->businesses;
  }

  protected function byServiceListings($services){

    $this->businesses->withServices($services);
    $this->setParams('service', $services);
    $this->setFilters('service', Service::whereIn('id', $services)->lists('name', 'id')->all());

    return $this->businesses;
  }

  protected function byCategoryListings($categories){
    $this->businesses->ofCategories($categories);

    $this->setParams('category', $categories);
    $this->setFilters('category', Category::whereIn('id', $categories)->lists('name', 'id')->all());
    return $this->businesses;
  }

  protected function byHighlightListings($highlights){
    $this->businesses->withHighlights($highlights);

    $this->setParams('highlights', $highlights);
    $this->setFilters('highlights', Highlight::whereIn('id', $highlights)->lists('name', 'id')->all());
    return $this->businesses;
  }

  protected function byActiveStateListings($state){
    $this->businesses->where('active', $state);
    $this->setParams('active', $state);
    $this->setFilters('active', ucwords(str_replace('.', ' ', Input::get('active'))));
    return $this->businesses;
  }


  protected function filterAllListings($params = false){
    if(Input::has('search') && (Input::get('search')!='')){
      $this->bySearchListings(Input::get('search'));

    }else{
      if(Input::has('zone') && is_array(Input::get('zone')) &&(count(array_filter(Input::get('zone')))>0)){
      
          if(is_array($params) && isset($params['nearby']) ){
            $subzone = Input::get('zone');
            $this->byLeveledZoneListings($subzone, true);//->ofZones($znes);
            $this->businesses->orderBy(DB::RAW('zone_id="'.$subzone[0].'"'), 'DESC');
          }else{ 
            $this->byZoneListings(Input::get('zone'));//->ofZones($znes);
          //$where = join(',', $this->filters['zone']);
        }

      }

    }

    if(Input::has('zone') && (Input::get('zone')==-1)){
      $this->byCoordinatesNearby(Input::get('latitude'), Input::get('longitude'));
    }

    if(Input::has('deals') && (Input::get('deals')!='')){
      $this->byDealListings(Input::get('deals'));
    }


    if(Input::has('specials') && (Input::get('specials')!='')){
      $this->byActiveOffers();
    }

    if(Input::has('offers') && (Input::get('offers')!='')){
      $this->byActiveOffers();
    }


    if(Input::has('packages') && (Input::get('packages')!='')){
      $this->byPackageListings(Input::get('packages'));
    }

    if(Input::has('open') && (Input::get('open')!='')){
      $this->byOpenNow();
    }

    if(Input::has('cost') && (Input::get('cost')!='')){
      $this->byCostEstimate(Input::get('cost'));
    }


    if(Input::has('service') && (Input::get('service')!='')){
      $this->byServiceListings(Input::get('service'));
    }

    if(Input::has('category')  && (count(Input::get('category'))>0)){
      $this->byCategoryListings(Input::get('category'));
      //$what = join(',', $this->filters['category']);
    }


    if(Input::has('highlights') && (count(Input::get('highlights'))>0)){
      $this->byHighlightListings(Input::get('highlights'));
    }

    if(Input::has('active') && (Input::get('active')!='')){
      $this->byActiveStateListings(Input::get('active'));
    }




  }


  public function getCurrentCity($apiKey){
    $result = false;
    if($apiKey->current_zone>0){
      $zn = Zone::where('id', '=', $apiKey->current_zone)->get()->first();
      $current_zone = $zn;
      if($zn->isCity()){
        $current_city = $zn->toArray();
        $result = $current_city['id'];
      }else{
        $current_city = Zone::where('id', '=', $zn->city_id)->get()->first()->toArray();
        $result = $current_city['id'];
      }

    }
    return $result;
  }

  public function getConciseIndex(){
    $this->initListingsNoLocale();
    $key = Input::get('key');
    $apiKey = ApiKey::where('key', '=', $key)->get()->first();

    if($apiKey){
      if($apiKey->current_zone>0){
        $z = Zone::find($apiKey->current_zone);
        $this->businesses = $this->businesses->byLocale($z->isCity()?$z->id:$z->city_id);
      }
    }

    $this->filterAllListings();

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    $this->SortListings();

    $businesses = $this->businesses->isDisplayable()->paginate(20);//take(20)->get();

    $entries = [];
    foreach($businesses as $i=>$business){
      $business->user_has_checked_in = $user->hasCheckedIn($business->id);
      $business->user_has_favourited = $user->hasFavourited('Business', $business->id);

      $businesses[$i] = $business;

      $businesses[$i] = [
        'id'=>$business->id,
        'name'=>$business->name,
        'geolocation_address'=>$business->geolocation_address,
        'zone_cache'=>$business->zone_cache,
        'rating_average'=>$business->rating_average
      ];
    }

    $businesses = $businesses->toArray();
    
    $result = mzk_api_response(compact('businesses'), 200, true, 'Success');

    return Response::Json($result);
  }



  public function getShow(){
    $id = Input::get('id');
    $business = $this->business->findOrFail($id);
    $result = [];
    $key = Input::get('key');

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    $apiKey = ApiKey::where('key', '=', $key)->first();

    $current_locale = $this->getCurrentCity($apiKey);

    $business->resetMetaForApi();
    $business->phone = $business->displayablePhone();
    $result['business'] = $business->toArray();
    $result['zone'] = count($business->zone) ? $business->zone->toArray():(object)[];
    $result['categories'] = count($business->categories) ? $business->categories->toArray():[];

    if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage()){
      $result['photo'] = mzk_cloudfront_image($business->getCoverUrl('largeCropped'));
    }else{
      $result['photo'] = '';
    }

    $result['timings'] = count($business->timings) ? $business->timings->toArray():[];
    $result['services'] = count($business->services) ? $business->services->toArray():[];
    $result['highlights'] = [];//count($business->highlights) ? $business->highlights:[];
    $reviews = count($business->completedReviews()) ? $business->completedReviews()->get():[];
    $result['offers'] = [];
    $offers = count($business->offers()->onlyActive()->get()) ? $business->offers()->onlyActive()->get():[];

    $result['business']['is_open'] = $business->isOpenNow();
    $result['business']['is_open_until'] = $business->openUntilNow();

    foreach($business->highlights as $ii=>$vv){
      if($vv->isActive($current_locale)){
        $vv = $vv->toArray();
        $vv['icon'] = 'https://s3.amazonaws.com/mazkaracdn/mobile/icons/'.$vv['slug'].'.png';
        $result['highlights'][] = $vv;
      }
    }

    $s = Service::query()->showParents()->get();
    $items = [];

    if(($business->has_sample_menu==0)):
      foreach($s as $ii=>$v){
        $children = [];

        if(count($items)>2){
          continue;
        }

        $cs = $business->services()->where('services.parent_id', '=', $v->id)->get();

        foreach($cs as $ix=>$child){
          if($ix > 2){
            continue;
          }
          $children[] = ['id'=>$child->id, 
                                'name'=>$child->name, 
                                'price'=>$child->pivot->starting_price,
                                'currency'=>'AED',
                                'service_display_starting_price' => $child->getDisplayablePrice(MazkaraHelper::getCitySlugFromID($business->city_id))
                                ];
        }

        if(count($children)>0){
          $items[] = ['parent'=>$v->name, 'children'=>$children];
        }
      }
    endif;

    $result['items'] = $items;

    foreach($reviews as $ii=>$review){
      if($review->user_id > 0){
        $usr = $review->user;
        $business = $review->business;

        $f = ["description","phone","email","website","geolocated","zone_id","geolocation_city","geolocation_state","geolocation_country","geolocation_address","created_at","updated_at",
"slug","active","facebook","twitter","instagram","google","chain_id","landmark","created_by_id","updated_by_id","deleted_by_id","geolocation_longitude",
"geolocation_latitude","rate_card_count","image_count","reviews_count","favorites_count","checkins_count","services_count","categories_count","highlights_count","timings_count","zone_cache","rating_average","active_deals_count",
"total_deals_count","total_packages_count","active_packages_count","total_ratings_count","cost_estimate","active_offers_count","city_id","ref","lot_id","has_sample_menu","has_stock_cover_image","popularity"];

        foreach($f as $vv){
          unset($review->business->$vv);
        }

        unset($review->business->meta);

        $review->business->photo = $review->business->hasThumbnail() ? $business->thumbnail('largeCropped') : '';
        $review->user->photo = '';//['user']['photo'] =  '';
        if(count($usr->avatar)>0){
          $review->user->photo = $usr->avatar->image->url('small');
        }

        $fs = ['username','email','password','confirmation_code','remember_token','confirmed',
                'created_at','avatar', 'updated_at','twitter','instagram','slug','check_ins_count'];
        foreach($fs as $vv){
          unset($review->user->$vv);
        }

      }else{
        //$review = $review->toArray();
      }

      //$result[] = $review;
    }

    $reviews = $reviews->toArray();

    $result['reviews'] = $reviews;

    //$result['photos'] = $business->photos() ? $business->photos->toArray():[];

    $result['user_has_checked_in'] = $user->hasCheckedIn($id);
    $result['user_has_favourited'] = $user->hasFavourited('Business', $id);
    $user_review = $business->completedReviews()->where('reviews.user_id', '=', $user->id)->get()->count()>0?true:false;
    
    //dump(DB::getQueryLog());
    
    $result['user_has_reviewed'] = $user_review == false ? false : true;
    $result['user_review'] = $user_review == false ? (object)array() : $business->completedReviews()->where('user_id', '=', $user->id)->get()->first();
    $pics = [];
    $result['photos'] = array();
    foreach($business->photos as $photo){
      $result['photos'][] = ['id'=>$photo->id, 'thumb'=>$photo->image->url('small'), 'medium'=>$photo->image->url('medium'), 'full'=>$photo->image->url()];
      $pics[$photo->id] = $photo->image->url('xlarge');
    }
    $result['rateCards'] = array();//$business->rateCards() ? $business->rateCards->toArray():[];
 
    foreach($business->rateCards as $photo){
      $result['rateCards'][] = ['id'=>$photo->id, 'thumb'=>$photo->image->url('small'), 'medium'=>$photo->image->url('medium'), 'full'=>$photo->image->url()];
    }

    $result['packages'] = array();

    foreach($business->packages as $photo){
      if($photo->hasImage()):
        $result['packages'][] = ['id'=>$photo->id, 
                                'thumb'=>$photo->photo->image->url('small'), 
                                'full'=>$photo->photo->image->url()];
      endif;

    }

    foreach($offers as $ii=>$offr){

      $offer = $offr->toArray();
      $result['offers'][$ii] = $offer;
      $result['offers'][$ii]['price_meta'] = $offr->price_meta;

      mzk_shuffle_assoc($pics);
      $result['offers'][$ii]['currency'] = mzk_currency_symbol($result['business']['city_id']);
      $p = Photo::where('imageable_type', '=','Zone')->where('imageable_id', '=', $offer['id'])->get();
      $result['offers'][$ii]['photos'] = $pics;//$p->count() > 0 ?$p->first()->image->url():'';
    }
    
    $result = mzk_api_response($result, 200, true, 'Success');
    return Response::Json($result);

  }

  public function getServicesShow(){
    $id = Input::get('id');
    $business = $this->business->findOrFail($id);
    $result = [];
    $key = Input::get('key');

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    $s = Service::query()->showParents()->get();
    $items = [];
    if($business->has_sample_menu==0):
      foreach($s as $ii=>$v){
        $children = [];

        $cs = $business->services()->where('services.parent_id', '=', $v->id)->get();

        foreach($cs as $ix=>$child){
          $children[] = ['id'=>$child->id, 
                                'name'=>$child->name, 
                                'price'=>$child->pivot->starting_price,
                                'currency'=>'AED',
                                'service_display_starting_price' => $child->getDisplayablePrice(MazkaraHelper::getCitySlugFromID($business->city_id))
                                ];
        }

        if(count($children)>0){
          $items[] = ['parent'=>$v->name, 'children'=>$children];
        }
      }
    endif;


    $result['items'] = $items;

    $result = mzk_api_response($result, 200, true, 'Success');
    return Response::Json($result);

  }

  public function actionToggleCheckInOut(){
    $id = Input::get('id');
    $key = Input::get('key');
    $checkin = Input::get('checkin');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    if($checkin=='true'){

      if(!$user->hasCheckedIn($id)){
        Check_in::create(['user_id'=>$user->id, 'business_id'=>$id]);
        $result = mzk_api_response([], 200, true, 'Success - User checked in');
      }else{
        $result = mzk_api_response([], 200, false, 'User was already checked in');
      }
    }else{

      if($user->hasCheckedIn($id)){
        $user->check_ins()->where('business_id', '=', $id)->delete();
        $result = mzk_api_response([], 200, true, 'Success - User checked out');
      }else{
        $result = mzk_api_response([], 200, false, 'User was already checked out');
      }
    }

    return Response::Json($result);

  }

  public function actionCheckin(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    if(!$user->hasCheckedIn($id)){
      Check_in::create(['user_id'=>$user->id, 'business_id'=>$id]);
      $result = mzk_api_response([], 200, true, 'Success - User checked in');
    }else{
      $result = mzk_api_response([], 200, false, 'User was already checked in');
    }
    return Response::Json($result);

  }

  public function actionCheckout(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    if($user->hasCheckedIn($id)){
      $user->check_ins()->where('business_id', '=', $id)->delete();
      $result = mzk_api_response([], 200, true, 'Success - User checked out');
    }else{
      $result = mzk_api_response([], 200, false, 'User was already checked out');
    }
    return Response::Json($result);
  }

  public function actionFavourite(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    if(!$user->hasFavourited('Business', $id)){
      Favorite::create(['user_id'=>$user->id, 'favorable_type'=>'Business', 'favorable_id'=>$id]);
      $data = array('user_id' =>  $user->id, 
                    'verb'  =>  'likes', 
                    'itemable_type' =>  'Business', 
                    'itemable_id' =>  $id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $user->id; 
      $feed->verb = 'likes'; 
      $feed->itemable_type = 'Business';
      $feed->itemable_id = $id;
      $feed->save();

      $result = mzk_api_response([], 200, true, 'Success - User has favorited');
    }else{
      $result = mzk_api_response([], 200, false, 'User had already favorited');
    }
    return Response::Json($result);
  }

  public function actionUnfavourite(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    if($user->hasFavourited('Business', $id)){
      $user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', 'Business')->delete();
      $result = mzk_api_response([], 200, true, 'Success - User has unfavorited');
    }else{
      $result = mzk_api_response([], 200, false, 'User had already unfavorited');
    }
    return Response::Json($result);
  }





  public function actionToggleFavourite(){
    $id = Input::get('id');
    $key = Input::get('key');

    $fav = Input::get('favourite');

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    if($fav=='true'):
      if(!$user->hasFavourited('Business', $id)){
        Favorite::create(['user_id'=>$user->id, 'favorable_type'=>'Business', 'favorable_id'=>$id]);
        $data = array('user_id' =>  $user->id, 
                      'verb'  =>  'likes', 
                      'itemable_type' =>  'Business', 
                      'itemable_id' =>  $id);
        $feed = $this->feed_manager->create($data);
        $feed->user_id = $user->id; 
        $feed->verb = 'likes'; 
        $feed->itemable_type = 'Business';
        $feed->itemable_id = $id;
        $feed->save();

        $result = mzk_api_response([], 200, true, 'Success - User has favorited');
      }else{
        $result = mzk_api_response([], 200, false, 'User had already favorited');
      }
    else:
      if($user->hasFavourited('Business', $id)){
        $user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', 'Business')->delete();
        $result = mzk_api_response([], 200, true, 'Success - User has unfavorited');
      }else{
        $result = mzk_api_response([], 200, false, 'User had already unfavorited');
      }
    endif;

    return Response::Json($result);
  }












  protected function getUserIdFromApi($key){
    $apiKey = ApiKey::where('key', '=', $key)->first();
    if(!$apiKey){
      return false;
    }else{
      return $apiKey->user_id;
    }
  }




}
