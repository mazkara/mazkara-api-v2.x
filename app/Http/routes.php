<?php
use App\Models\Zone;
use App\Models\Category;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'WelcomeController@index');
// 
// Route::get('home', 'HomeController@index');
// 
// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);



Route::any('api/white', 'HomeController@apiRedirectHack');



  Route::group(array('domain' => Config::get('app.api_url') ), function () { //'protected' => true, 
    $action = Input::get('action');
    $pink = 'v1';
    $white = 'a'; //user
    $v2 = 'v2'; //user
    $black = 'black';

    $auth_admin = 'auth.api.admin';
    $auth_user = 'auth.api.user';
    $auth_black = 'auth.api.black';

    Route::any('call_logs/create', array('uses'=>'api\v1\pink\Call_logsController@create'));

    switch($action){
      case 'authenticate':

        Route::any($v2, array( 'uses'=>'api\v2\user\UsersController@authenticate'));
      break;
  /*
      case 'guest':
        Route::any($white, array( 'uses'=>'api\v1\user\UsersController@guest'));
      break;
  */
      case 'home':
        Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\PageController@home'));
      break;

      case 'search/zones':
        Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\PageController@searchZones'));
      break;
      case 'search/venues':
        Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\PageController@searchVenues'));
      break;


      case 'get/current/zone':
        Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\PageController@setLocation'));
      break;

      case 'users/set/zone':
        Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@setCurrentUsersZone'));
      break;

      case 'register':
        Route::any($v2, array( 'uses'=>'api\v2\user\UsersController@register'));
      break;

      case 'forgot/password':
        Route::any($v2, array( 'uses'=>'api\v2\user\UsersController@forgotPassword'));
      break;

      case 'account/update':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@updateAccount'));
      break;

      case 'deauthenticate':

        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@deauthenticate'));
      break;

      case 'account/user':

        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getUserDetails'));
      break;

      case 'users/login/facebook':
        Route::any($v2, array('uses'=>'api\v2\user\UsersController@authWithFacebook'));
      break;

      case 'users/login/google':
        Route::any($v2, array('uses'=>'api\v2\user\UsersController@authWithGoogle'));
      break;

      case 'follows':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@follows'));
      break;

      case 'users/follow':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@follow'));
      break;

      case 'users/unfollow':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@unfollow'));
      break;

      case 'users/favorites':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getFavorites'));
      break;

      case 'users/my/favorites':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getMyFavorites'));
      break;

      case 'users/my/favorites/count':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getFavoritesCount'));
      break;

      case 'users/show':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@show'));
      break;

      case 'users':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getIndex'));
      break;

      case 'users/followers':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getFollowers'));
      break;

      case 'users/follows':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getFollows'));
      break;

      case 'businesses/reload':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@reload'));
      break;

      case 'businesses':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@getIndex'));
      break;

      case 'businesses/calls/increment':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@incrementBusinessCalls'));
      break;

      case 'businesses/concise':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@getConciseIndex'));
      break;

      case 'reviews':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@getReviewsIndex'));
      break;

      case 'ratings':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@getRatingsIndex'));
      break;

      case 'reviews/show':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@getShow'));
      break;

      case 'ratings/show':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@getShow'));
      break;

      case 'reviews/add':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@addReview'));
      break;

      case 'ratings/add':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@addRating'));
      break;

      case 'reviews/update':
      case 'ratings/update':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@updateReview'));
      break;
      case 'reviews/delete':
      case 'ratings/delete':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@destroyReview'));
      break;

      case 'offers/screen':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@getOffersScreen'));
      break;

      case 'offers':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\OffersController@getIndex'));
      break;

      case 'offers/show':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\OffersController@getShow'));
      break;


      case 'businesses/show':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@getShow'));
      break;
      case 'search':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@search'));
      break;

      case 'search/all':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@megaSearch'));
      break;

      case 'feed':
      case 'notifications':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@feed'));
      break;


      case 'search/services':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@searchServices'));
      break;
      case 'businesses/services':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@getServicesShow'));
      break;

      case 'businesses/check_in':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionCheckin'));
      break;
      case 'businesses/check_out':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionCheckout'));
      break;
      case 'businesses/toggle/checkin':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionToggleCheckInOut'));
      break;

      case 'businesses/favourite':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionFavourite'));
      break;
      case 'businesses/unfavourite':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionUnfavourite'));
      break;
      case 'businesses/toggle/favourite':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionToggleFavourite'));
      break;



      // V2 Exclusive and above

      case 'posts/show':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@show'));
      break;

      case 'posts':
      case 'timeline':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@index'));
      break;

      case 'posts/create':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@store'));
      break;

      case 'posts/delete':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@destroy'));
      break;


      case 'posts/like':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@follow'));
      break;

      case 'posts/unlike':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@unfollow'));
      break;

      case 'posts/bookmark':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@bookmark'));
      break;

      case 'posts/unbookmark':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@unbookmark'));
      break;

      case 'comments':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\CommentsController@index'));
      break;

      case 'comments/create':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\CommentsController@store'));
      break;

      case 'comments/update':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\CommentsController@update'));
      break;

      case 'comments/delete':
        Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\CommentsController@destroy'));
      break;





      default:
        Route::any($white, array('uses'=>'api\v2\user\UsersController@error404'));
        Route::any($v2, array('uses'=>'api\v2\user\UsersController@error404'));
        Route::any($pink, array('uses'=>'api\v2\user\UsersController@error404'));
      break;
    }

    //Route::get('categories', 'api\v1\CategoriesController@index');
    //Route::post('authenticate', 'api\v1\UsersController@authenticate');
    //Route::get('account/user', 'api\v1\UsersController@getUserDetails');
  });


//if(mzkIsApiDomain()){
//  return;
//  // no need to load more of the routes
//}




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['before' => ''], function(){

  Route::group(array('prefix'=>'api2' ), function () { //'protected' => true, 
    $action = Input::get('action');
    $pink = 'v1';
    $white = 'white'; //user
    $v2 = 'v2'; //user
    $black = 'black';

    $auth_admin = 'auth.api.admin';
    $auth_user = 'auth.api.user';
    $auth_black = 'auth.api.black';

    Route::any('call_logs/create', array('uses'=>'api\v1\pink\Call_logsController@create'));

    switch($action){
      case 'authenticate':
        Route::any($pink, array( 'uses'=>'api\v1\pink\UsersController@authenticate'));
        Route::any($white, array( 'uses'=>'api\v1\user\UsersController@authenticate'));
        Route::any($black, array( 'uses'=>'api\v1\black\UsersController@authenticate'));

        //Route::any($v2, array( 'uses'=>'api\v2\user\UsersController@authenticate'));
      break;
  /*
      case 'guest':
        Route::any($white, array( 'uses'=>'api\v1\user\UsersController@guest'));
      break;
  */
      case 'home':
        Route::any($white, array( 'before'=>$auth_user, 'uses'=>'api\v1\user\PageController@home'));
        //Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\PageController@home'));
      break;

      case 'search/zones':
        Route::any($white, array( 'before'=>$auth_user, 'uses'=>'api\v1\user\PageController@searchZones'));
        //Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\PageController@searchZones'));
      break;
      case 'search/venues':
        //Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\PageController@searchVenues'));
      break;


      case 'get/current/zone':
        Route::any($white, array( 'before'=>$auth_user, 'uses'=>'api\v1\user\PageController@setLocation'));
        //Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\PageController@setLocation'));
      break;

      case 'users/set/zone':
        Route::any($white, array( 'before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@setCurrentUsersZone'));
        //Route::any($v2, array( 'before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@setCurrentUsersZone'));
      break;

      case 'register':
        Route::any($pink, array( 'uses'=>'api\v1\pink\UsersController@register'));
        Route::any($white, array( 'uses'=>'api\v1\user\UsersController@register'));
        //Route::any($v2, array( 'uses'=>'api\v2\user\UsersController@register'));
      break;

      case 'forgot/password':
        Route::any($white, array( 'uses'=>'api\v1\user\UsersController@forgotPassword'));
        //Route::any($v2, array( 'uses'=>'api\v2\user\UsersController@forgotPassword'));
      break;

      case 'account/update':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@updateAccount'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@updateAccount'));
      break;

      case 'deauthenticate':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\UsersController@deauthenticate'));
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@deauthenticate'));
        Route::any($black, array('before'=>$auth_black, 'uses'=>'api\v1\black\UsersController@deauthenticate'));

        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@deauthenticate'));
      break;

      case 'account/user':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\UsersController@getUserDetails'));
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@getUserDetails'));
        Route::any($black, array('before'=>$auth_black, 'uses'=>'api\v1\black\UsersController@getUserDetails'));

        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getUserDetails'));
      break;

      case 'users/login/facebook':
        Route::any($white, array('uses'=>'api\v1\user\UsersController@authWithFacebook'));
        //Route::any($v2, array('uses'=>'api\v2\user\UsersController@authWithFacebook'));
      break;

      case 'users/login/google':
        Route::any($white, array('uses'=>'api\v1\user\UsersController@authWithGoogle'));
        //Route::any($v2, array('uses'=>'api\v2\user\UsersController@authWithGoogle'));
      break;

      case 'follows':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@follows'));
      break;




      case 'users/follow':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@follow'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@follow'));
      break;

      case 'users/unfollow':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@unfollow'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@unfollow'));
      break;

      case 'users/favorites':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@getFavorites'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getFavorites'));
      break;

      case 'users/my/favorites':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@getMyFavorites'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getMyFavorites'));
      break;

      case 'users/my/favorites/count':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@getFavoritesCount'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getFavoritesCount'));
      break;

      case 'users/show':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@show'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@show'));
      break;

      case 'users':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@getIndex'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getIndex'));
      break;

      case 'users/followers':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@getFollowers'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getFollowers'));
      break;

      case 'users/follows':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\UsersController@getFollows'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\UsersController@getFollows'));
      break;

      case 'businesses/reload':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@reload'));
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@reload'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@reload'));
      break;

      case 'businesses':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@index'));
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@getIndex'));
        Route::any($black, array('before'=>$auth_black, 'uses'=>'api\v1\black\UsersController@getAllocatedBusinesses'));

        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@getIndex'));
      break;

      case 'businesses/calls/increment':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@incrementBusinessCalls'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@incrementBusinessCalls'));
      break;

      case 'businesses/concise':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@getConciseIndex'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@getConciseIndex'));
      break;

      case 'reviews':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ReviewsController@getReviewsIndex'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@getReviewsIndex'));
      break;

      case 'ratings':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ReviewsController@getRatingsIndex'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@getRatingsIndex'));
      break;

      case 'reviews/show':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ReviewsController@getShow'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@getShow'));
      break;

      case 'ratings/show':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ReviewsController@getShow'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@getShow'));
      break;

      case 'reviews/add':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ReviewsController@addReview'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@addReview'));
      break;

      case 'ratings/add':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ReviewsController@addRating'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@addRating'));
      break;
      case 'reviews/update':
      case 'ratings/update':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ReviewsController@updateReview'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@updateReview'));
      break;
      case 'reviews/delete':
      case 'ratings/delete':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ReviewsController@destroyReview'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ReviewsController@destroyReview'));
      break;

      case 'offers/screen':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\PageController@getOffersScreen'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@getOffersScreen'));
      break;

      case 'offers':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\OffersController@getIndex'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\OffersController@getIndex'));
      break;

      case 'offers/show':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\OffersController@getShow'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\OffersController@getShow'));
      break;

      case 'businesses/form':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@form'));
      break;
      case 'businesses/create':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@store'));
      break;

      case 'businesses/show':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@show'));
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@getShow'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@getShow'));
      break;
      case 'search':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\PageController@search'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@search'));
      break;

      case 'search/all':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@megaSearch'));
      break;

      case 'feed':
      case 'notifications':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@feed'));
      break;


      case 'search/services':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PageController@searchServices'));
      break;
      case 'businesses/services':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@getServicesShow'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@getServicesShow'));
      break;

      case 'businesses/check_in':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@actionCheckin'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionCheckin'));
      break;
      case 'businesses/check_out':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@actionCheckout'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionCheckout'));
      break;
      case 'businesses/toggle/checkin':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@actionToggleCheckInOut'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionToggleCheckInOut'));
      break;

      case 'businesses/favourite':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@actionFavourite'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionFavourite'));
      break;
      case 'businesses/unfavourite':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@actionUnfavourite'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionUnfavourite'));
      break;
      case 'businesses/toggle/favourite':
        Route::any($white, array('before'=>$auth_user, 'uses'=>'api\v1\user\ListingsController@actionToggleFavourite'));
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\ListingsController@actionToggleFavourite'));
      break;


      case 'businesses/update':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@update'));
      break;

      case 'businesses/location/update':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@updateLocation'));
      break;

      case 'businesses/highlights/update':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@updateHighlights'));
      break;
      case 'businesses/timings/update':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@updateTimings'));
      break;
      case 'businesses/services/update':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@updateServices'));
      break;
      case 'businesses/photos/update':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@updatePhotos'));
      break;

      case 'businesses/rate_cards/update':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@updateRateCards'));
      break;

      case 'businesses/images/delete':
        Route::any($pink, array('before'=>$auth_admin, 'uses'=>'api\v1\pink\ListingsController@deleteImages'));
      break;

      // V2 Exclusive and above

      case 'posts/show':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@show'));
      break;

      case 'posts':
      case 'timeline':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@index'));
      break;

      case 'posts/create':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@store'));
      break;

      case 'posts/delete':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@destroy'));
      break;


      case 'posts/like':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@follow'));
      break;

      case 'posts/unlike':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@unfollow'));
      break;

      case 'posts/bookmark':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@bookmark'));
      break;

      case 'posts/unbookmark':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\PostsController@unbookmark'));
      break;

      case 'comments':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\CommentsController@index'));
      break;

      case 'comments/create':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\CommentsController@store'));
      break;

      case 'comments/update':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\CommentsController@update'));
      break;

      case 'comments/delete':
        //Route::any($v2, array('before'=>$auth_user, 'uses'=>'api\v2\user\CommentsController@destroy'));
      break;





      default:
        Route::any($white, array('uses'=>'api\v1\pink\UsersController@error404'));
        //Route::any($v2, array('uses'=>'api\v1\pink\UsersController@error404'));
        Route::any($pink, array('uses'=>'api\v1\pink\UsersController@error404'));
      break;
    }

    //Route::get('categories', 'api\v1\CategoriesController@index');
    //Route::post('authenticate', 'api\v1\UsersController@authenticate');
    //Route::get('account/user', 'api\v1\UsersController@getUserDetails');
  });


});


